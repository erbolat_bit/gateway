<?php

return [

  'version' => 'v1',
  'spa' => [
    'domain' => env('SPA_DOMAIN', 'https://uzexpress.ru'),
    'verify-url' => env('SPA_VERIFY_URL', 'email-verify'),
    'password-reset' => env('SPA_PASS_RESET_URL', 'password-reset'),
  ],
  'type_mailing' => [ // параметры для создании купона
    'service_users' => 1, // создать для всех пользователей сервиса
    'store_users' => 2 // создать для всех пользователей магазина
  ],
];