<?php


return [

  'shop' => [
    'base_url' => env('SHOP_SERVICE_BASE_URL', ''),
    'token' => env('SHOP_SERVICE_TOKEN', ''),
  ],
  'review' => [
    'base_url' => env('REVIEW_SERVICE_BASE_URL', ''),
    'token' => env('REVIEW_SERVICE_TOKEN', ''),
  ],
  'lot' => [
    'base_url' => env('LOT_SERVICE_BASE_URL', ''),
    'token' => env('LOT_SERVICE_TOKEN', ''),
  ],
  'ads' => [
    'base_url' => env('ADS_SERVICE_BASE_URL', ''),
    'token' => env('ADS_SERVICE_TOKEN', ''),
  ],
  'coupon' => [
    'base_url' => env('COUPON_SERVICE_BASE_URL', ''),
    'token' => env('COUPON_SERVICE_TOKEN', ''),
  ],
  'add-on' => [
    'base_url' => env('ADD_ON_SERVICE_BASE_URL', ''),
    'test_base_url' => env('ADD_ON_SERVICE_TEST_BASE_URL', ''),
    'token' => env('ADD_ON_SERVICE_TOKEN', ''),
  ],
  'cart' => [
    'base_url' => env('CART_SERVICE_BASE_URL', ''),
    'token' => env('CART_SERVICE_TOKEN', ''),
  ],
  'user' => [
    'base_url' => env('USER_SERVICE_BASE_URL', ''),
    'token' => env('USER_SERVICE_TOKEN', ''),
  ],
  'blog' => [
    'base_url' => env('BLOG_SERVICE_BASE_URL', ''),
    'token' => env('BLOG_SERVICE_TOKEN', ''),
  ],
  'poll' => [
    'base_url' => env('POLL_SERVICE_BASE_URL', ''),
    'token' => env('POLL_SERVICE_TOKEN', ''),
  ]

];
