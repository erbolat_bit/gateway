<?php

return [
  //Don't touch this
  'categories' => [
    'user',
    'category',
    'manufacturer',
    'country',
    'knowledge-db-category',
    'knowledge-db-post',
    'product-param',
    'general-setting',
    'api-setting',
    'delivery-type',
    'poll',
    'cart-order',
    'coupon',
    'lot',
    'lot-offer',
    'shop',
    'shop-product',
    'review-comment',
    'review',
    'review-question',
    'ads',
    'department',
    'ticket',
    'ticket-category'
  ],
  'operations' => [
    'update',
    'delete',
    'view',
    'create'
  ],
  'translations' => [
    'user' => 'Пользователи',
    'permission' => 'Изменять полномочия',
    'category' => 'Категории',
    'manufacturer' => 'Производители',
    'country' => 'Страны',
    'knowledge-db-category' => 'База знаний -> Категории',
    'knowledge-db-post' => 'База знаний -> посты',
    'product-param' => 'Параметры продука',
    'general-setting' => 'Общие настройки',
    'api-setting' => 'API настройки',
    'delivery-type' => 'Вид доставки',
    'poll' => 'Опросники',
    'cart-order' => 'Корзина -> заказы',
    'coupon' => 'Купоны',
    'lot' => 'Аукционы',
    'lot-offer' => 'Аукционы -> предложения',
    'shop' => 'Магазины',
    'shop-product' => 'Магазины -> продукты',
    'review-comment' => 'Отзывы -> комменты',
    'review' => 'Отзывы',
    'review-question' => 'Отзывы -> вопросники',
    'ads' => 'Объявления',
    'update' => 'Изменить',
    'delete' => 'Удалить',
    'view' => 'Посмотреть',
    'create' => 'Создать',
    'department' => 'Отделения'
  ]
];
