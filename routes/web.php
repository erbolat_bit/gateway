<?php

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Cart\PaymentController;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdsController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CountryController;
use App\Http\Controllers\Admin\CouponController;
use App\Http\Controllers\Admin\DepartmentController;
use App\Http\Controllers\Admin\LogController;
use App\Http\Controllers\Admin\LotController;
use App\Http\Controllers\Admin\ManufacturerController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ParameterController;
use App\Http\Controllers\Admin\PollController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\ShopController;
use App\Http\Controllers\Admin\SiteController;
use App\Http\Controllers\Admin\SystemMessageController;
use App\Http\Controllers\Admin\TicketController;
use App\Http\Controllers\Admin\UploadController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('login', function () {
  return view('auth.login');
});

Route::get('/', function () {
  return redirect('/login');
});
Route::get('/test', [LogController::class, 'index']);

Route::group(['namespace' => 'Admin', 'middleware' => 'auth:admin'], function () {

  Route::get('dashboard', [SiteController::class, 'index'])->name('dashboard');
  Route::post('logout', [AuthController::class, 'logout'])->name('logout');


  Route::group(['prefix' => 'banners'], function () {
    Route::get('index', [BannerController::class, 'index'])->name('banners.index');
    Route::get('create', [BannerController::class, 'create'])->name('banners.create');
    Route::post('store', [BannerController::class, 'store'])->name('banners.store');
    Route::get('edit/{id}', [BannerController::class, 'edit'])->name('banners.edit');
    Route::post('update/{id}', [BannerController::class, 'update'])->name('banners.update');
    Route::delete('delete/{id}', [BannerController::class, 'destroy'])->name('banners.destroy');
  });

  Route::group(['prefix' => 'ticket', 'middleware' => 'add_user_id'], function () {
    Route::get('/', [TicketController::class, 'index'])->name('ticket.index');
    Route::post('close/{ticket_id}', [TicketController::class, 'update'])->name('ticket.update');
    Route::post('add-audits/{ticket_id}', [TicketController::class, 'addAudits'])->name('ticket.add.audits');
    Route::get('message/list/{ticket_id}', [TicketController::class, 'show'])->name('ticket.show');
    Route::post('message/send/{ticket_id}', [TicketController::class, 'store'])->name('ticket.message.store');
    Route::post('message/read', [TicketController::class, 'store'])->name('ticket.message.read');

    Route::group(['prefix' => 'categories'], function () {
      Route::get('/', [TicketController::class, 'categories'])->name('ticket.categories');
      Route::get('/update/{id}', [TicketController::class, 'categoryUpdateView'])->where(['id' => '[0-9]+'])->name('ticket.categories.update.view');
      Route::post('/update/{id}', [TicketController::class, 'categoryUpdate'])->where(['id' => '[0-9]+'])->name('ticket.categories.update');
      Route::get('/create', [TicketController::class, 'categoryCreateView'])->name('ticket.categories.create.view');
      Route::post('/create', [TicketController::class, 'categoryCreate'])->name('ticket.categories.create');
      Route::delete('/delete/{id}', [TicketController::class, 'categoryDelete'])->where(['id' => '[0-9]+'])->name('ticket.categories.delete');
    });
  });

  Route::group(['prefix' => 'users/admin'], function () {
    Route::get('all', [AdminController::class, 'admins'])->name('users.admins');
    Route::get('roles', [AdminController::class, 'roles'])->name('users.admin.roles');
    Route::get('manage/{admin}', [AdminController::class, 'manage'])->name('users.admin.manage');
    Route::post('manage/{admin}', [AdminController::class, 'change'])->name('users.admin.change');
    Route::post('create', [AdminController::class, 'create'])->name('users.admin.create');
    Route::delete('delete/', [AdminController::class, 'delete'])->name('users.admin.delete');
  });
  Route::group(['prefix' => 'users'], function () {
    Route::get('all', [UserController::class, 'all'])->name('users.all');
    Route::get('edit/{id}', [UserController::class, 'edit'])->name('users.user.edit');
    Route::put('update/{id}', [UserController::class, 'update'])->name('users.user.update');
    Route::delete('/{id}', [UserController::class, 'destroy'])->name('users.user.destroy');
  });
  Route::group(['prefix' => 'department'], function () {
    Route::get('/all', [DepartmentController::class, 'index'])->name('department.all');
    Route::get('/update/{id}', [DepartmentController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('department.update.view');
    Route::post('/update/{id}', [DepartmentController::class, 'update'])->where(['id' => '[0-9]+'])->name('department.update');
    Route::delete('/delete', [DepartmentController::class, 'delete'])->name('department.delete');
    Route::get('/create', [DepartmentController::class, 'createView'])->name('department.create.view');
    Route::post('/create', [DepartmentController::class, 'create'])->name('department.create');
  });

  Route::group(['prefix' => 'categories'], function () {
    Route::get('/', [CategoryController::class, 'index'])->name('categories');
    Route::get('/create', [CategoryController::class, 'createView'])->name('categories.create.view');
    Route::post('/create', [CategoryController::class, 'createCategory'])->name('categories.create');
    Route::get('/update/{id}', [CategoryController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('categories.update.view');
    Route::post('/update/{id}', [CategoryController::class, 'updateCategory'])->where(['id' => '[0-9]+'])->name('categories.update');
    Route::get('/show/{id}', [CategoryController::class, 'show'])->where(['id' => '[0-9]+'])->name('categories.show');
    Route::delete('/delete', [CategoryController::class, 'deleteCategory'])->name('categories.delete');
  });

  Route::group(['prefix' => 'manufacturers'], function () {
    Route::get('/', [ManufacturerController::class, 'index'])->name('manufacturers.index');
    Route::get('/{id}', [ManufacturerController::class, 'show'])->where(['id' => '[0-9]+'])->name('manufacturers.show');
    Route::get('/create', [ManufacturerController::class, 'createView'])->name('manufacturers.create.view');
    Route::post('/create', [ManufacturerController::class, 'createManufacturer'])->name('manufacturers.create');
    Route::delete('/delete', [ManufacturerController::class, 'destroy'])->name('manufacturers.destroy');
    Route::get('/update/{id}', [ManufacturerController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('manufacturers.update.view');
    Route::post('/update/{id}', [ManufacturerController::class, 'updateManufacturer'])->where(['id' => '[0-9]+'])->name('manufacturers.update');
  });

  Route::group(['prefix' => 'countries'], function () {
    Route::get('/', [CountryController::class, 'index'])->name('countries.index');
    Route::get('/{id}', [CountryController::class, 'show'])->where(['id' => '[0-9]+'])->name('countries.show');
    Route::get('/create', [CountryController::class, 'createView'])->name('countries.create.view');
    Route::post('/create', [CountryController::class, 'createCountry'])->name("coutries.create");
    Route::get('/udpate/{id}', [CountryController::class, 'updateView'])->name('countries.update.view');
    Route::post('/update/{id}', [CountryController::class, 'updateCountry'])->name('countries.update');
    Route::delete('/delete', [CountryController::class, 'destroy'])->name('countries.destroy');
  });

  Route::group(['prefix' => 'parameters'], function () {
    Route::get('/', [ParameterController::class, 'index'])->name('parameters.index');
    Route::get('/update/{id}', [ParameterController::class, 'updateView'])->name('parameters.update.view');
    Route::post('/update/{id}', [ParameterController::class, 'update'])->name('parameters.update');
    Route::get('/create', [ParameterController::class, 'createView'])->name('parameters.create.view');
    Route::post('/create', [ParameterController::class, 'create'])->name('parameters.create');
    Route::delete('/delete/{parameter}', [ParameterController::class, 'destroy'])->name('parameters.destroy');
    Route::delete('parameter-option/{paramOption}', [ParameterController::class, 'destroyOption'])->name('parameter-option.delete');
  });

  Route::group(['prefix' => 'role'], function () {
    Route::post('create', [PermissionController::class, 'createRole'])->name('role.create');
    Route::delete('delete', [PermissionController::class, 'deleteRole'])->name('role.delete');
  });

  Route::group(['prefix' => 'blog'], function () {

    Route::get('/', [BlogController::class, 'index'])->name('blog.posts');
    Route::get('/update/{id}', [BlogController::class, 'UpdateView'])->name('blog.posts.update.view');
    Route::post('/update/{id}', [BlogController::class, 'Update'])->name('blog.posts.update');
    Route::delete('delete', [BlogController::class, 'destroy'])->name('blog.posts.destroy');
    Route::get('/create', [BlogController::class, 'createView'])->name('blog.posts.create.view');
    Route::post('/create', [BlogController::class, 'create'])->name('blog.posts.create');

    Route::group(['prefix' => 'categories'], function () {
      Route::get('/', [BlogController::class, 'categories'])->name('blog.categories');
      Route::get('/create', [BlogController::class, 'categoriesCreateView'])->name('blog.categories.create.view');
      Route::post('/create', [BlogController::class, 'categoriesCreate'])->name('blog.categories.create');
      Route::get('/update/{id}', [BlogController::class, 'categoriesUpdateView'])->name('blog.categories.update.view');
      Route::post('/update/{id}', [BlogController::class, 'categoriesUpdate'])->name('blog.categories.update');
      Route::delete('/delete', [BlogController::class, 'categoriesDestroy'])->name('blog.categories.destroy');
    });
  });

  Route::group(['prefix' => 'setting'], function () {
    Route::get('/', [SettingController::class, 'index'])->name('setting');
    Route::post('/update/{id}', [SettingController::class, 'update'])->where(['id' => '[0-9]+'])->name('setting.update');
    Route::get('/update/{id}', [SettingController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('setting.update.view');
    Route::get('socials', [SettingController::class, 'apiSettings'])->name('setting.socials');
    Route::get('delivery', [SettingController::class, 'delivery'])->name('setting.delivery');
    Route::get('delivery/update/{id}', [SettingController::class, 'deliveryUpdateView'])->where(['id' => '[0-9]+'])->name('setting.delivery.update.view');
    Route::post('delivery/update/{id}', [SettingController::class, 'deliveryUpdate'])->where(['id' => '[0-9]+'])->name('setting.delivery.update');
    Route::get('/create', [SettingController::class, 'createDeliveryView'])->name('setting.delivery.create.view');
    Route::post('/create', [SettingController::class, 'createDelivery'])->name('setting.delivery.create');
    Route::delete('/delete/{id}', [SettingController::class, 'deleteDelivery'])->name('setting.delivery.delete');
  });

  Route::get('system-messages/create', [SystemMessageController::class, 'create'])->name('system-messages.create');
  Route::post('system-messages/store', [SystemMessageController::class, 'store'])->name('system-messages.store');
  Route::get('system-messages', [SystemMessageController::class, 'index'])->name('system-messages.index');
  Route::get('system-messages/{id}', [SystemMessageController::class, 'show'])->name('system-messages.show');

  Route::group(['prefix' => 'permission'], function () {
    Route::post('update/{role}', [PermissionController::class, 'assignPermissionToRole'])->name('permission.update');
    Route::get('/{role}', [PermissionController::class, 'permissionsOfRole'])->name('role.permissions');
  });

  Route::group(['prefix' => 'poll'], function () {
    Route::get('/', [PollController::class, 'index'])->name('poll');
    Route::get('/show/{id}', [PollController::class, 'show'])->where(['id' => '[0-9]+'])->name('poll.show');
    Route::get('/update/{id}', [PollController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('poll.update.view');
    Route::post('/update/{id}', [PollController::class, 'update'])->where(['id' => '[0-9]+'])->name('poll.update');
    Route::get('/create', [PollController::class, 'createView'])->name('poll.create.view');
    Route::post('/create', [PollController::class, 'create'])->name('poll.create');
    Route::delete('/delete/{id}', [PollController::class, 'delete'])->where(['id' => '[0-9]+'])->name('poll.delete');
  });

  Route::group(['prefix' => 'cart'], function () {
    Route::get('/orders', [OrderController::class, 'getOrders'])->name('cart.orders');
    Route::get('/orders/{id}', [OrderController::class, 'showOrder'])->where(['id' => '[0-9]+'])->name('cart.orders.show');
    Route::post('/orders/{id}', [OrderController::class, 'update'])->where(['id' => '[0-9]+'])->name('cart.orders.update');
  });

  Route::group(['prefix' => 'coupon'], function () {
    Route::get('/', [CouponController::class, 'getCoupons'])->name('coupon');
    Route::get('/{id}', [CouponController::class, 'showCoupon'])->where(['id' => '[0-9]+'])->name('coupon.show');
    Route::post('/update/{id}', [CouponController::class, 'update'])->where(['id' => '[0-9]+'])->name('coupon.update');
    Route::get('/update/{id}', [CouponController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('coupon.update.view');
    Route::get('/show/{id}', [CouponController::class, 'showCoupon'])->where(['id' => '[0-9]+'])->name('coupon.item.show');
    Route::get('/create', [CouponController::class, 'create'])->name('coupon.create');
    Route::post('/store', [CouponController::class, 'store'])->name('admin.coupon.store');
  });

  Route::group(['prefix' => 'lot'], function () {
    Route::get('/', [LotController::class, 'getLots'])->name('lot');
    Route::get('/{id}', [LotController::class, 'showLot'])->where(['id' => '[0-9]+'])->name('lot.show.1');
    Route::get('/offers', [LotController::class, 'getOffers'])->name('lot.offers');
    Route::get('/update/{id}', [LotController::class, 'updateLotView'])->where(['id' => '[0-9]+'])->name('lot.update.view');
    Route::post('/update/{id}', [LotController::class, 'updateLot'])->where(['id' => '[0-9]+'])->name('lot.update.1');
    Route::delete('/delete/{id}', [LotController::class, 'deleteLot'])->where(['id' => '[0-9]+'])->name('lot.delete.1');
    Route::delete('/offer/delete/{id}', [LotController::class, 'deleteOffer'])->where(['id' => '[0-9]+'])->name('lot.offers.delete');
  });

  Route::group(['prefix' => 'shop'], function () {
    Route::get('/', [ShopController::class, 'getShopList'])->name('shop');
    Route::get('/show/{id}', [ShopController::class, 'showShop'])->where(['id' => '[0-9]+'])->name('shop.show');
    Route::post('/update/{id}', [ShopController::class, 'updateShop'])->where(['id' => '[0-9]+'])->name('shop.update.1');
    Route::get('/update/{id}', [ShopController::class, 'updateView'])->where(['id' => '[0-9]+'])->name('shop.update.view');
    Route::delete('/delete/{id}', [ShopController::class, 'deleteShop'])->where(['id' => '[0-9]+'])->name('shop.delete.1');
    Route::get('/products', [ShopController::class, 'getProductList'])->name('shop.products');
    Route::get('/products/show/{id}', [ShopController::class, 'showProduct'])->where(['id' => '[0-9]+'])->name('shop.products.show');
    Route::get('/products/update/{id}', [ShopController::class, 'updateProductView'])->where(['id' => '[0-9]+'])->name('shop.products.update.view');
    Route::post('/products/update/{id}', [ShopController::class, 'updateProduct'])->where(['id' => '[0-9]+'])->name('shop.products.update');
    Route::delete('/products/delete/{id}', [ShopController::class, 'deleteProduct'])->where(['id' => '[0-9]+'])->name('shop.products.delete');
  });

  Route::group(['prefix' => 'reviews'], function () {
    Route::get('/', [ReviewController::class, 'getReviews'])->name('review');
    Route::get('/comments', [ReviewController::class, 'getComments'])->name('review.comments');
    Route::delete('/comments/delete/{id}', [ReviewController::class, 'deleteComment'])->name('review.comments.delete');
    Route::put('/comments/upadte/{id}', [ReviewController::class, 'updateComment'])->name('review.comments.update');
    Route::get('/questions', [ReviewController::class, 'getQuestions'])->name('review.questions');
    
    Route::delete('/questions/delete/{id}', [ReviewController::class, 'deleteQuestion'])->name('review.questions.delete');
    Route::put('/questions/upadte/{id}', [ReviewController::class, 'updateQuestion'])->name('review.questions.update');

    Route::delete('/delete/{id}', [ReviewController::class, 'deleteReview'])->name('review.delete');
    Route::put('/upadte/{id}', [ReviewController::class, 'updateReview'])->name('review.update');
  });
  Route::group(['prefix' => 'ads'], function () {
    Route::get('/', [AdsController::class, 'getAdList'])->name('ads');
  });
  
  Route::get('/logs', [LogController::class, 'index'])->name('logs.all');
  Route::post('/logs', [LogController::class, 'index'])->name('logs.all.search');
  Route::post('image-upload', [UploadController::class, 'upload'])->name('upload.image');
});
