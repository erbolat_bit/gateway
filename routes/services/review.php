<?php

use App\Http\Controllers\Review\CommentController;
use App\Http\Controllers\Review\QuestionController;
use App\Http\Controllers\Review\ReviewController;
use Illuminate\Support\Facades\Route;


Route::group([
  'prefix' => 'reviews'
], function () {

  

  Route::group(['middleware' => 'client'], function(){
    
    Route::get('collection/{product_id}', [ReviewController::class, 'getReviewsQuestionsCount'])->name('review.questions.count')->where(['product_id' => '[0-9]+']);
    Route::get('/{product_id}', [ReviewController::class, 'getReviewsByProduct'])->name('review.by.product')->where(['product_id' => '[0-9]+']);
    Route::get('img/{id}', [ReviewController::class, 'getReviewImages'])->name('review.images')->where(['id' => '[0-9]+']);
    Route::get('rates/{product_id}', [ReviewController::class, 'getReviewRateCount'])->name('review.rate.count')->where(['product_id' => '[0-9]+']);
    Route::get('shop/{shop_id}', [ReviewController::class, 'getShopReviews'])->name('review.by.shop')->where(['shop_id' => '[0-9]+']);
    Route::get('rates/collection/{shop_id}', [ReviewController::class, 'getShopRates'])->name('review.shop.rates')->where(['shop_id' => '[0-9]+']);
    Route::get('rates/collection/by-products', [ReviewController::class, 'getRatesByProducts'])->name('review.products.rates');
    
  });
  
  Route::group(['middleware' => 'auth:api'], function(){
    
    Route::get('/auth/{product_id}', [ReviewController::class, 'getReviewsByProduct'])->name('review.auth.by.product')->where(['product_id' => '[0-9]+']);
    Route::post('/{product_id}', [ReviewController::class, 'store'])->name('review.create')->where(['shop_id' => '[0-9]+']);
    Route::post('/{model}/like/{id}', [ReviewController::class, 'like'])->name('review.like')->where(['id' => '[0-9]+']);
  
  });

});

Route::group([
  'prefix' => 'comment'
], function () {

  Route::group(['middleware' => 'client'], function(){
    Route::get('list/{post_id}', [CommentController::class, 'getCommentsByPost'])->name('comment.list.by.post')->where(['post_id' => '[0-9]+']);
    Route::get('count/{post_id}', [CommentController::class, 'getCommentsCountByPost'])->name('comment.count.by.post')->where(['post_id' => '[0-9]+']);
  });
  
  Route::group(['middleware' => 'auth:api'], function(){
    Route::get('auth/list/{post_id}', [CommentController::class, 'getCommentsByPost'])->name('comment.auth.list.by.post')->where(['post_id' => '[0-9]+']);
    Route::post('add', [CommentController::class, 'store'])->name('comment.create');
  });

});

Route::group([
  'prefix' => 'questions'
], function () {

  Route::group(['middleware' => 'client'], function(){
    
    Route::get('/{product_id}', [QuestionController::class, 'getQuestionsByProduct'])->name('question.by.product');
    
  });
  
  Route::group(['middleware' => 'auth:api'], function(){
    Route::get('auth/{product_id}', [QuestionController::class, 'getQuestionsByProduct'])->name('question.auth.by.product');
    Route::post('/{product_id}', [QuestionController::class, 'store'])->name('question.create')->where(['post_id' => '[0-9]+']);
    Route::delete('/{review_id}', [ReviewController::class, 'destroy'])->name('question.delete')->where(['post_id' => '[0-9]+']);
  });

});
