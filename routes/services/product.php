<?php

use App\Http\Controllers\Shop\ProductController;
use Illuminate\Support\Facades\Route;


Route::group([
  'prefix' => 'product'
], function () {

  Route::group(['middleware' => 'client'], function(){
    Route::get('by-shop/{store}', [ProductController::class, 'getShopProducts'])->name('product.list.by.shop');
    Route::get('show/{product}', [ProductController::class, 'getProductById'])->name('product.item');
    Route::get('all', [ProductController::class, 'getProductList'])->name('product.all.list');
    Route::get('all-by-shop/{store}', [ProductController::class, 'getProductListByShop'])->name('product.all.list.by.shop');
    Route::post('views/{product}', [ProductController::class, 'setProductViewCounter'])->name('product.views.counter');
    
  });
  
  Route::group(['middleware' => 'auth:api'], function(){
    
    Route::post('store/{shop}', [ProductController::class, 'store'])->name('product.create');
    Route::post('update/{product}', [ProductController::class, 'update'])->name('product.update');
    Route::get('basket', [ProductController::class, 'basket'])->name('product.basket');
    Route::get('wishlist', [ProductController::class, 'wishlist'])->name('product.wishlist');
    Route::post('auth/views/{product}', [ProductController::class, 'setProductViewCounter'])->name('auth.product.views.counter');

    Route::delete('/{product}',[ProductController::class, 'destroy'])->name('product.delete');
    Route::delete('photo/{photo}',[ProductController::class, 'deleteProductPhoto'])->name('product.delete.photo');

    Route::post('document/{product}',[ProductController::class, 'productCreateDoc'])->name('product.create.doc');
    Route::delete('document/{document}',[ProductController::class, 'productDeleteDoc'])->name('product.delete.doc');
  });

});
