<?php

use App\Http\Controllers\Coupon\CouponController;
use Illuminate\Support\Facades\Route;


Route::group([
	'prefix' => 'coupon'
], function () {

	Route::group(['middleware' => 'auth:api'], function () {

		Route::get('by-user', [CouponController::class, 'getUserCoupons'])->name('coupon.by.user');
		Route::get('by-shop/{store}', [CouponController::class, 'getShopCoupons'])->name('coupon.by.shop');
		Route::get('by-shop/archive/{store}', [CouponController::class, 'getShopArchiveCoupons'])->name('coupon.archive.by.shop');
		Route::get('by-product/{product}', [CouponController::class, 'getProductCoupons'])->name('coupon.by.product');
		Route::post('store', [CouponController::class, 'store'])->name('coupon.store');
		Route::get('show/{coupon}', [CouponController::class, 'show'])->name('coupon.show.item');
		Route::delete('delete/{coupon}', [CouponController::class, 'destroy'])->name('coupon.delete');
		Route::delete('delete-item/{item}', [CouponController::class, 'deleteCouponItem'])->name('coupon.delete.item');
		Route::post('give/{item}', [CouponController::class, 'giveCoupon'])->name('coupon.give');
	});
});
