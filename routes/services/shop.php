<?php

use App\Http\Controllers\Shop\ShopController;
use Illuminate\Support\Facades\Route;


Route::group([
  'prefix' => 'shop'
], function () {

  Route::group(['middleware' => 'client'], function(){
    Route::get('/', [ShopController::class, 'getShopList'])->name('shop.list');
    Route::get('/{store}', [ShopController::class, 'getShopById'])->name('shop.id.item')->where(['store' => '[0-9]+']);
    Route::get('store-by-domain/{domain}', [ShopController::class, 'getShopByDomain'])->name('shop.domain.item');
    Route::post('views/{store}', [ShopController::class, 'setViewToShop'])->name('shop.counter');
    Route::get('main-data', [ShopController::class, 'mainData'])->name('shop.main.data');
    Route::get('user/stores/{user_id}', [ShopController::class, 'userStores'])->name('shop.user.stores.id');
  });
  
  Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user/stores', [ShopController::class, 'userStores'])->name('shop.user.stores');
    Route::post('auth/views/{store}', [ShopController::class, 'setViewToShop'])->name('auth.shop.counter');
    Route::post('store', [ShopController::class, 'store'])->name('shop.create');
    Route::post('/{store}', [ShopController::class, 'update'])->name('shop.update')->where(['store' => '[0-9]+']);
    Route::delete('/{store}', [ShopController::class, 'destroy'])->name('shop.delete')->where(['store' => '[0-9]+']);
    Route::delete('bg-image/{store}', [ShopController::class, 'deleteBgImage'])->name('shop.delete.bg_image');
  });

});
