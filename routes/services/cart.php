<?php

use App\Http\Controllers\Cart\BalanceController;
use App\Http\Controllers\Cart\BasketController;
use App\Http\Controllers\Cart\OrderController;
use App\Http\Controllers\Cart\Payment\BalancePaymentController;
use App\Http\Controllers\Cart\Payment\BankPaymentController;
use Illuminate\Support\Facades\Route;


Route::group([
	'prefix' => 'cart'
], function () {

	Route::group(['middleware' => 'auth:api'], function () {
    Route::get('order', [OrderController::class, 'getUserOrders'])->name('cart.order.by.user');
    Route::get('order/check/{order_id}', [OrderController::class, 'checkStatus'])->name('cart.order.check.status');
    
    Route::post('order/store', [OrderController::class, 'store'])->name('cart.order.create');
    Route::delete('order/{order}', [OrderController::class, 'destroy'])->name('cart.order.delete');
    Route::get('order/{order}', [OrderController::class, 'show'])->name('cart.order.show');
    Route::post('order-item/confirm/{item}', [OrderController::class, 'confirmOrderItem'])->name('cart.order.confirm.item');
    Route::post('order-item/review/{item}', [OrderController::class, 'reviewOrderItem'])->name('cart.order.review.item');
    Route::get('order-item/{item}', [OrderController::class, 'getOrderItem'])->name('cart.order.item.show');
    Route::delete('order-item/{item}', [OrderController::class, 'deleteOrderItem'])->name('cart.order.item.delete');
    Route::get('order/store-buyers/{store_id}', [OrderController::class, 'getStoreUserIds'])->name('cart.order.store.users');

    Route::post('balance/store', [BalanceController::class, 'store'])->name('cart.balance.create');
    Route::get('balance', [OrderController::class, 'index'])->name('cart.balance.index');

    Route::get('basket', [BasketController::class, 'getUserBasket'])->name('cart.basket.by.user');
    Route::post('basket/store', [BasketController::class, 'store'])->name('cart.basket.store');
    Route::delete('basket/{basket}', [BasketController::class, 'destroy'])->name('cart.basket.delete');

    Route::get('transactions', [OrderController::class, 'getUserTransactions'])->name('cart.user.transactions');

  });

  Route::group(['middleware' => 'client'], function(){
    // Эти роуты используется внутри gateway при необходимости можно вкл
    Route::post('order/update/{order}', [OrderController::class, 'update'])->name('cart.order.update');
  });



});



Route::group(['prefix' => 'payment'], function(){
  Route::post('balance/success', [BalancePaymentController::class, 'success']);
  Route::post('success', [BankPaymentController::class, 'success']);
  Route::post('fail', [BankPaymentController::class, 'fail']);
  Route::get('redirect', [BankPaymentController::class, 'redirect']);
  Route::get('pay', [BankPaymentController::class, 'pay']);
  Route::post('send', [BankPaymentController::class, 'send'])->name('payment.send');
});
