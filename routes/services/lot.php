<?php

use App\Http\Controllers\Lot\LotController;
use App\Http\Controllers\Lot\OfferController;
use Illuminate\Support\Facades\Route;


Route::group([
  'prefix' => 'lot'
], function () {

  Route::group(['middleware' => 'client'], function(){
    
    Route::get('all', [LotController::class, 'getAllLot'])->name('lot.all');
    Route::get('show/{lot}', [LotController::class, 'getLotById'])->name('lot.show');
    Route::get('by-user/{user_id}', [LotController::class, 'getUserLots'])->name('lot.by.user.id');

  });
  
  Route::group(['middleware' => 'auth:api'], function(){

    Route::get('by-user', [LotController::class, 'getUserLots'])->name('lot.by.user');
    Route::post('store', [LotController::class, 'store'])->name('lot.create');
    Route::post('update/{lot}', [LotController::class, 'update'])->name('lot.update');
    Route::delete('/{lot}', [LotController::class, 'destroy'])->name('lot.delete');
    Route::delete('photo/{photo}', [LotController::class, 'deleteLotPhoto'])->name('lot.delete.photo');
    Route::post('document/{lot}', [LotController::class, 'uploadLotDoc'])->name('lot.upload.doc');
    Route::delete('document/{document}', [LotController::class, 'deleteLotDoc'])->name('lot.delete.doc');

  });

});

Route::group([
  'prefix' => 'offer'
], function () {

  Route::group(['middleware' => 'client'], function(){
  
    Route::get('by-lot/{lot}', [OfferController::class, 'getLotOffers'])->name('offer.by.lot');

  });
  
  Route::group(['middleware' => 'auth:api'], function(){
    Route::get('by-user', [OfferController::class, 'getUserOffers'])->name('offer.by.user');
    Route::post('store', [OfferController::class, 'store'])->name('offer.create');
  });

});
