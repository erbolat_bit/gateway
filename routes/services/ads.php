<?php

use App\Http\Controllers\Ads\AdsController;
use Illuminate\Support\Facades\Route;


Route::group([
	'prefix' => 'ads'
], function () {

	Route::group(['middleware' => 'auth:api'], function () {

		Route::get('by-user', [AdsController::class, 'getUserAds'])->name('ads.by.user');
		Route::post('store', [AdsController::class, 'store'])->name('ads.create');
		Route::post('update/{ad}', [AdsController::class, 'update'])->name('ads.update');
		Route::delete('/{ad}', [AdsController::class, 'destroy'])->name('ads.delete');
		Route::delete('photo/{photo}', [AdsController::class, 'deleteAdsPhoto'])->name('ads.photo.delete');
		Route::post('document/{ad}', [AdsController::class, 'uploadAdsDoc'])->name('ads.upload.doc');
		Route::delete('document/{document}', [AdsController::class, 'deleteAdsDoc'])->name('ads.delete.doc');
	});

	Route::group(['middleware' => 'client'], function () {
		Route::get('by-user/{user_id}', [AdsController::class, 'getUserAds'])->name('ads.by.user.id');
		Route::get('all', [AdsController::class, 'getAll'])->name('ads.all');
		Route::get('show/{ad}', [AdsController::class, 'getAdsById'])->name('ads.show');
	});
});
