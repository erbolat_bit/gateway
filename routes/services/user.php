<?php

use App\Http\Controllers\User\BonusController;
use App\Http\Controllers\User\ChatController;
use App\Http\Controllers\User\HistoryController;
use App\Http\Controllers\User\ReferralController;
use App\Http\Controllers\User\TicketController;
use App\Http\Controllers\User\UserController;
use App\Http\Controllers\User\WishlistController;
use Illuminate\Support\Facades\Route;

Route::group([
  'prefix' => 'user'
], function () {

  Route::group(['middleware' => 'auth:api'], function(){
    
    Route::get('/', [UserController::class, 'index'])->name('user.index');
    Route::post('profile/update', [UserController::class, 'updateProfile'])->name('user.update');
    Route::post('profile/document/add', [UserController::class, 'addDocument'])->name('user.add.document');
    Route::post('profile/document/delete/{id}', [UserController::class, 'deleteDocument'])->name('user.delete.document');
    Route::post('profile/address/add', [UserController::class, 'addAddress'])->name('user.add.address');
  	Route::post('profile/address/remove/{id}', [UserController::class, 'deleteAddress'])->name('user.delete.address');

    Route::get('rating/{user_id}', [UserController::class, 'getUserRating'])->name('user.rating');
    Route::post('rating/create', [UserController::class, 'vote'])->name('user.rating.create');

    Route::get('history', [HistoryController::class, 'index'])->name('user.history');
    Route::put('history', [HistoryController::class, 'store'])->name('user.history.create');
    Route::delete('history/{product_id}', [HistoryController::class, 'destroy'])->name('user.history.delete');

    Route::get('wishlist', [WishlistController::class, 'index'])->name('user.wishlist');
    Route::put('wishlist', [WishlistController::class, 'store'])->name('user.wishlist.store');
    Route::delete('wishlist', [WishlistController::class, 'destroy'])->name('user.wishlist.delete');

    Route::group([
      'prefix' => 'bonuses'
    ], function () {
        
      Route::get('/', [BonusController::class, 'index'])->name('user.bonuses');
      // Эти роуты используется внутри gateway при необходимости можно вкл
      // Route::post('/create', [BonusController::class, 'create'])->name('user.bonuses.create');
    
    });

  });

  Route::group([
    'middleware' => 'client'
  ], function(){
    Route::get('profile/{nickname}', [UserController::class, 'getUserDataAndProducts'])->name('user.profile');
  });
  
});

Route::group([
  'prefix' => 'referral'
], function () {

  Route::group(['middleware' => 'auth:api'], function(){
    
    Route::get('by-user', [ReferralController::class, 'index'])->name('user.referral.by.user');
    Route::post('link', [ReferralController::class, 'store'])->name('user.referral.store');
    
  });
  // Эти роуты используется внутри gateway при необходимости можно вкл
  // Route::group(['middleware' => 'client'], function(){
    // Route::get('/{token}', [ReferralController::class, 'getReferralByToken'])->name('user.referral.token');
    // Route::post('create', [ReferralController::class, 'createReferral'])->name('user.referral.create');
  // });

});



Route::group([
  'prefix' => 'chats'
], function () {

  Route::group(['middleware' => 'auth:api'], function(){
    
    Route::get('user', [ChatController::class, 'index'])->name('user.chats');
    Route::post('send/message', [ChatController::class, 'store'])->name('user.chats.create');
    Route::get('user/search/{chat_id}', [ChatController::class, 'searchMessage'])->name('user.chats.search');
    Route::get('view/{chat_id}', [ChatController::class, 'show'])->name('user.chats.show');
    Route::get('view/messages/{chat_id}', [ChatController::class, 'getChatMessages'])->name('user.chats.messages');
    Route::post('messages/is_read', [ChatController::class, 'messSetRead'])->name('user.chats.messages.read');

  });
  
});

Route::group([
  'prefix' => 'ticket'
], function () {

  Route::get('category/list', [TicketController::class, 'categoryList'])->name('user.ticket.category.list');

  Route::group(['middleware' => 'auth:api'], function(){
    
    Route::get('list', [TicketController::class, 'index'])->name('user.tickets');
    
    Route::group(['prefix' => 'message'], function(){
      Route::get('list/{ticket_id}', [TicketController::class, 'getTicketMessages'])->name('user.ticket.messages');
      Route::post('create', [TicketController::class, 'createMessage'])->name('user.ticket.message');
      Route::post('read', [TicketController::class, 'messageUpdate'])->name('user.ticket.message.update');
      Route::get('search/{ticket_id}', [TicketController::class, 'searchMessages'])->name('user.ticket.messages.search');
    });

  });
  
});