<?php

use App\Http\Controllers\Poll\PollController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
  'prefix' => 'poll'
], function () {

  Route::group([
    'middleware' => 'auth:api'
  ], function(){
    Route::post('store', [PollController::class, 'store']);
    Route::post('update/{id}', [PollController::class, 'updatePoll']);
    Route::post('vote', [PollController::class, 'vote']);
    
    Route::post('delete-photo/{id}', [PollController::class, 'deletePhotoPoll']);
    Route::post('delete-question-photo/{id}', [PollController::class, 'deleteQuestionPhoto']);
    Route::post('delete-answer-photo/{id}', [PollController::class, 'deleteAnswerPhoto']);

    Route::delete('delete/{id}', [PollController::class, 'deletePoll']);
    Route::delete('delete-question/{id}', [PollController::class, 'deleteQuestion']);
    Route::delete('delete-answer/{id}', [PollController::class, 'deleteAnswer']);
    Route::get('vote/auth/{id}', [PollController::class, 'getPollVotes']);
    Route::get('/auth/{store}', [PollController::class, 'getShopPolls'])->where(['store' => '[0-9]+']);
    Route::get('/by-user', [PollController::class, 'getUserPolls']);

  });

  Route::group([
    'middleware' => 'client'
  ], function(){
    Route::get('/{store}', [PollController::class, 'getShopPolls'])->where(['store' => '[0-9]+']);
    Route::get('show/{id}', [PollController::class, 'showPoll']);
    Route::get('vote/{id}', [PollController::class, 'getPollVotes']);

  });
});
