<?php

use App\Http\Controllers\Blog\CategoriesController;
use App\Http\Controllers\Blog\PostController;
use Illuminate\Support\Facades\Route;

Route::group([
  'middleware' => 'client',
  'prefix' => 'blog'
], function () {

  Route::group(['prefix' => 'posts'], function () {
    Route::get('list', [PostController::class, 'postList']);
    //Now search does not give any comment_counts
    Route::get('search', [PostController::class, 'postSearch']);
    Route::get('view/{alias}', [PostController::class, 'postView']);

  });
  Route::get('categories/list', [CategoriesController::class, 'categoryList']);

});
