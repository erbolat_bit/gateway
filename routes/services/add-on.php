<?php

use App\Http\Controllers\AddOn\CategoryController;
use App\Http\Controllers\AddOn\CountryController;
use App\Http\Controllers\AddOn\ManufacturerController;
use App\Http\Controllers\AddOn\ParameterController;
use App\Http\Controllers\AddOn\SettingController;
use Illuminate\Support\Facades\Route;


Route::group([
	'prefix' => 'add-on'
], function () {

	Route::group(['middleware' => 'client'], function () {
		Route::get('parameters', [ParameterController::class, 'index'])->name('add-on.parameters');
		Route::get('manufacturers', [ManufacturerController::class, 'index'])->name('add-on.manufacturers');
		Route::get('categories', [CategoryController::class, 'index'])->name('add-on.categories');
		Route::get('filter-categories', [CategoryController::class, 'filterCategories'])->name('add-on.filter.categories');
		Route::get('countries', [CountryController::class, 'index'])->name('add-on.countries');
		Route::get('store-deliveries', [SettingController::class, 'index'])->name('add-on.store-deliveries');
	});
});
