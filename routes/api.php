  <?php

  use App\Http\Controllers\Auth\EmailVerifyController;
  use App\Http\Controllers\Auth\LoginController;
  use App\Http\Controllers\Auth\LogoutController;
  use App\Http\Controllers\Auth\RegisterController;
  use App\Http\Controllers\Auth\ResetPasswordController;
  use App\Http\Controllers\Auth\SocialController;
use App\Http\Controllers\Banner\BannerController;
use App\Http\Controllers\Main\SiteController;
  use App\Http\Controllers\Test\TestController;
  use Illuminate\Support\Facades\DB;
  use Illuminate\Support\Facades\Hash;
  use Illuminate\Support\Facades\Route;


  require __DIR__ . '/services/shop.php';
  require __DIR__ . '/services/product.php';
  require __DIR__ . '/services/review.php';
  require __DIR__ . '/services/lot.php';
  require __DIR__ . '/services/ads.php';
  require __DIR__ . '/services/coupon.php';
  require __DIR__ . '/services/add-on.php';
  require __DIR__ . '/services/cart.php';
  require __DIR__ . '/services/user.php';
  require __DIR__ . '/services/blog.php';
  require __DIR__ . '/services/poll.php';

  // foreach (glob(__DIR__ . 'services/*.php') as $route) {
  //   require_once $route;
  // }

  // Auth
  Route::group([
    'prefix' => 'auth'
  ], function () {

    Route::group(['middleware' => 'client'], function () {
      Route::post('register', [RegisterController::class, 'index'])->name('auth.register');
      Route::get('email-verify/{id}/{hash}', [EmailVerifyController::class, 'verify'])->name('auth.email.verify');
      Route::post('email-verify/resend', [EmailVerifyController::class, 'resend'])->name('auth.email.resend');
      Route::post('login', LoginController::class)->name('auth.login');
      Route::post('forgot-password', [ResetPasswordController::class, 'forgot'])->name('auth.password.forgot');
      Route::post('reset-password', [ResetPasswordController::class, 'reset'])->name('auth.password.reset');
      Route::post('social-login/{provider}', SocialController::class)->name('auth.social.login');
    });

    Route::group(['middleware' => 'auth:api'], function () {
      Route::post('logout', LogoutController::class)->name('auth.logout');
    });
  });
  // Auth end

  Route::group(['prefix' => 'main', 'middleware' => 'client'], function () {
    Route::get('search', [SiteController::class, 'search'])->name('main.search');
    Route::get('helper-data', [SiteController::class, 'getCategotyAndManufacture'])->name('main.category.manufacturer');
    Route::get('banners', [BannerController::class, 'index'])->name('main.banners');
  });

  Route::group(['prefix' => 'test', 'middleware' => 'client'], function () {

    Route::get('categories', [TestController::class, 'categories'])->name('test.categories');
  });
