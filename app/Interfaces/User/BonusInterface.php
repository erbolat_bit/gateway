<?php

namespace App\Interfaces\User;

interface BonusInterface{

  /**
   * @param string $value
   */
  public function setName(string $value);

  /**
   * @param int $value
   */
  public function setBonus(int $value);

  /**
   * @param string $value referral | poll
   */
  public function setType(string $value);
  
  /**
   * @param int $value referral ID | shop ID
   */
  public function setTypeId(int $value);

  /**
   * @return array
   */
  public function getData(): array;

}