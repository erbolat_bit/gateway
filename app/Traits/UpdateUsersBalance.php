<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\DB;

trait UpdateUsersBalance{

  /**
   * 
   * Updated users balanse
   */
  public function updateUserBalanse($user_ids, $balance_data)
  {
    $balance_data = collect($balance_data);
    foreach ($user_ids as $key => $value) {
      User::where('id', $value['user_id'])
          ->update(['balance' => DB::raw('balance + ' . $balance_data->where('store_id', $value['store_id'])->first()['total_sum'])]);
    }
  }
}