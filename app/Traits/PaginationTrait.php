<?php

namespace App\Traits;

trait PaginationTrait
{
  public function prepare(array $array1, array $array2): array
  {
    $combine = array_merge($array2, $array1);
    $combine['prev_page_url'] = $combine['prev'];
    $combine['next_page_url'] = $combine['next'];
    $combine['first_page_url'] = $combine['first'];
    $combine['last_page_url'] = $combine['last'];
    return $combine;
  }
}
