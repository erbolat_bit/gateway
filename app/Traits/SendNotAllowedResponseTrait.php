<?php

namespace App\Traits;

use App\Http\Controllers\Admin\UserController;
use CKSource\CKFinder\Filesystem\Path;

trait SendNotAllowedResponseTrait
{
  public function notAllowed()
  {
    return redirect()->back()->with('error', "You don't have required permission to do this action");
  }

  public function flashError($message = "Something Went Wrong")
  {
    return redirect()->back()->with('error', $message);
  }
}
