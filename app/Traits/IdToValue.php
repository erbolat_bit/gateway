<?php

namespace App\Traits;

use App\AdminServices\AddOn\CategoryService;
use App\AdminServices\AddOn\CountryService;
use App\AdminServices\AddOn\ManufacturerService;
use App\AdminServices\Blog\BlogService;
use App\AdminServices\Cart\OrderService;
use App\AdminServices\Lot\LotService;
use App\AdminServices\Shop\ShopService;
use App\AdminServices\User\UserService;
use App\Models\User;

trait IdToValue
{
  public $userKey = 'user_id';
  public $productKey = 'product_id';
  public $storeKey = 'store_id';
  public $countryKey = 'country_id';
  public $categoryKey = 'category_id';
  public $manufacturerKey = 'manufacturer_id';
  public $postKey = 'post_id';
  public $addressKey = 'address_id';

  public $userIds;
  public $productIds;
  public $storeIds;
  public $countryIds;
  public $categoryIds;
  public $manufacturerIds;
  public $postIds;
  public $addressIds;


  /**
   * Redirect back the admin if some error occures
   */
  public function flashError($message)
  {
    return redirect()->back()->with('error', $message);
  }

  /**
   * Extract User Ids
   * @return Array
   */
  public function extractUserIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->userIds[] = $val[$this->userKey];
    }
  }

  /**
   * Extract Address Ids
   * @return Array
   */
  public function extractAddressIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->addressIds[] = $val[$this->addressKey];
    }
  }

  /**
   * Extract Procut Ids
   * @return Array
   */
  public function extractProductIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->productIds[] = $val[$this->productKey];
    }
  }

  /**
   * Extract Store Ids
   * @return Array
   */
  public function extractStoreIds(array $data)
  {
    foreach ($data as $key => $val) {
      if ((bool)$val[$this->storeKey]) {
        $this->storeIds[] = $val[$this->storeKey];
      }
    }
  }

  /**
   * Extract Country Ids
   * @return Array
   */
  public function extractCountryIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->countryIds[] = $val[$this->countryKey];
    }
  }

  /**
   * Extract Category Ids
   * @return Array
   */
  public function extractCtategoryIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->categoryIds[] = $val[$this->categoryKey];
    }
  }

  /**
   * Extract Manufacturer Ids
   * @return Array
   */
  public function extractManufacturerIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->manufacturerIds[] = $val[$this->manufacturerKey];
    }
  }

  /**
   * Extract Post Ids
   * @return Array
   */
  public function extractPostIds(array $data)
  {
    foreach ($data as $key => $val) {
      $this->postIds[] = $val[$this->postKey];
    }
  }

  /**
   * Transform User Ids to Equivalent Email values
   * @param $data -> where the data should be appended
   * @param $user_ids -> not required if extractUserIds function is used
   * @return Array
   */
  public function userIdToEmail(array $data, array $user_ids = []): array
  {
    $userDetails = User::find($this->userIds ?? $user_ids)->pluck('id', 'email')->toArray();
    foreach ($data as $key => $val) {
      $data[$key]['user_email'] = array_search($val[$this->userKey], (array)$userDetails);
    }
    return $data;
  }

  /**
   * Transform Product Ids to Equivalent Product titles
   * @param $data -> where the data should be appended
   * @param $product_ids -> not required if extractProductIds function is used
   * @return Array
   */
  public function productIdToName($request, array $data, array $product_ids = []): array
  {
    $shopService = new ShopService();
    $productDetails = $shopService->getProductNames($request, $this->productIds ?? $product_ids);
    if ($productDetails['error'])
      return $this->flashError($productDetails['message']);
    foreach ($data as $key => $val) {
      $data[$key]['product_title'] = array_search($val[$this->productKey], (array)$productDetails['data']);
    }
    return $data;
  }

  /**
   * Transform Store Ids to Equivalent Store titles
   * @param $data -> where the data should be appended
   * @param $store_ids -> not required if extractProductIds function is used
   * @return Array
   */
  public function storeIdToTitle($request, array $data, array $store_ids = []): array
  {
    $shopService = new ShopService();
    if ($this->storeIds) {
      $productDetails = $shopService->getStoreTitles($request, $this->storeIds ?? $store_ids);
      if ($productDetails['error'])
        return $this->flashError($productDetails['message']);
  
      foreach ($data as $key => $val) {
        $data[$key]['store_title'] = array_search($val[$this->storeKey], (array)$productDetails['data']);
      }
      return $data;
    }
    return $data;
  }

  /**
   * Transform Country Ids to Equivalent country names
   * @param $data -> where the data should be appended
   * @param $country_ids -> not required if extractCountryIds function is used
   * @return Array
   */
  public function countryIdToName($request, array $data, array $country_ids = []): array
  {
    $countryService = new CountryService();
    $countryDetails = $countryService->getCountryList($request, $this->countryIds ?? $country_ids);
    if ($countryDetails['error'])
      return $this->flashError($countryDetails['message']);
    foreach ($data as $key => $val) {
      $data[$key]['country_name'] = array_search($val[$this->countryKey], (array)$countryDetails['data']);
    }
    return $data;
  }

  /**
   * Transform Category Ids to Equivalent category titles
   * @param $data -> where the data should be appended
   * @param $category_ids -> not required if extractCategoryIds function is used
   * @return Array
   */
  public function categoryIdToTitle($request, array $data, array $category_ids = []): array
  {
    $categoryService = new CategoryService();
    $categoryDetails = $categoryService->getCategoryList($request, $this->categoryIds ?? $category_ids);
    if ($categoryDetails['error'])
      return $this->falshError($categoryDetails['message']);
    foreach ($data as $key => $val) {
      $data[$key]['category_name'] = array_search($val[$this->categoryKey], (array)$categoryDetails['data']);
    }
    return $data;
  }

  /**
   * Transform Manufacturer Ids to Equivalent manufacturer titles
   * @param $data -> where the data should be appended
   * @param $manufacturer_ids -> not required if extractCategoryIds function is used
   * @return Array
   */
  public function manufacturerIdToTitle($request, array $data, array $manufacturer_ids = []): array
  {
    $categoryService = new ManufacturerService();
    $manufacturerDetails = $categoryService->getManufacturerList($request, $this->manufacturerIds ?? $manufacturer_ids);
    if ($manufacturerDetails['error'])
      return $this->flashError($manufacturerDetails['message']);
    foreach ($data as $key => $val) {
      $data[$key]['manufacturer_name'] = array_search($val[$this->manufacturerKey], (array)$manufacturerDetails['data']);
    }
    return $data;
  }

  /**
   * Transform Post Ids to Equivalent post titles
   * @param $data -> where the data should be appended
   * @param $post_ids -> not required if extractPostIds function is used
   * @return Array
   */
  public function postIdToTitle($request, array $data, array $post_ids = []): array
  {
    $blogService = new BlogService();
    $blogDetails = $blogService->getPostList($request, $this->postIds ?? $post_ids);
    if ($blogDetails['error'])
      return $this->flashError($blogDetails['message']);
    foreach ($data as $key => $val) {
      $data[$key]['post_title'] = $blogDetails['data'][$val[$this->postKey]]['title_ru'];
      $data[$key]['post_alias'] = $blogDetails['data'][$val[$this->postKey]]['alias'];
      // $data[$key]['post_title'] = array_search($val[$this->postKey], (array)$blogDetails['data']);
    }
    return $data;
  }

  /**
   * Transform address ids to address titles
   * @param $data -> where the data should be appended
   * @param $address_ids -> not required if extractAddressIds function is used
   * @return Array
   */
  public function addressIdToTitle($request, array $data, array $address_ids = []): array
  {
    $userService = new UserService();
    $addressDetails = $userService->getAddresses($request, $this->addressIds ?? $address_ids);
    if ($addressDetails['error'])
      return $this->flashError($addressDetails['message']);
    foreach ($data as $key => $val) {
      $data[$key]['address'] = array_search($val[$this->addressKey], (array)$addressDetails['data']);
    }
    return $data;
  }

  //* Send Not allowed response if the user doesn't have required permissions
  public function notAllowed($message = 'You don\' have required permission to do this action')
  {
    return redirect()->back()->with('error', $message);
  }
}
