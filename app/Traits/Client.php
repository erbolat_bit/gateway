<?php

namespace App\Traits;

use App\Models\Log;
use GuzzleHttp\Client as GuzzleHttpClient;

/**
 * Client
 */
trait Client
{

  /**
   * performRequest
   *
   * @param  mixed $request
   * @param  string $method
   * @param  string $url
   * @param  array $params Request params
   * @param  int $type_params 1 form_params, 2 multipart, 3 query
   * @param  array $headers
   * @return void
   */
  public function performRequest($request, string $method, string $url, $params = [], int $type_params = 1, array $headers = [])
  {
    $headers = $this->headers($request, $headers);

    $client = new GuzzleHttpClient([
      'base_uri' => $this->base_url,
      'headers' => $headers,
      'verify' => false
    ]);
    switch ($type_params) {
      case 1:
        $params = ['form_params' => $params];
        break;
      case 2:
        $params = ['multipart' => $params];
        break;
      case 3:
        $params = ['query' => $params];
        break;
    }

    if ($method == 'delete') {
      $url .= '?user_id=' . $request->user_id;
    }
    $response = $client->request($method, $url, $params);
    $current_user = $request->user();
    if ($current_user && $current_user->is_admin && $method != 'get')
      $this->log($request, $method, $url, $params, $current_user, $response);

    return json_decode((string) $response->getBody(), true);
  }


  /**
   * attachFiles
   *
   * @param  mixed $files
   * @return void
   */
  private function attachFiles(array $files)
  {
    $input = [];
    foreach ($files as $key => $file) {
      $input[] = [
        'name' => $file['name'],
        'contents' => $file['contents']
      ];
    }

    return $input;
  }

  /**
   * multipartData
   *
   * @param  mixed $data
   * @return void
   */
  public function multipartData(array $data, $attach)
  {
    $input = [];
    foreach ($data as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $child_key => $child) {
          if (is_array($child)) {
            foreach ($child as $c_key => $c_value) {
              if (is_array($c_value)) {
                foreach ($c_value as $cl_key => $cl_value) {
                  foreach ($cl_value as $cll_key => $cll_value) {
                    $input[] = [
                      'name' => $key . '[' . $child_key . '][' . $c_key . '][' . $cl_key . '][' . $cll_key . ']',
                      'contents' => $cll_value
                    ];
                  }
                }
              } else {
                $input[] = [
                  'name' => $key . '[' . $child_key . '][' . $c_key . ']',
                  'contents' => $c_value
                ];
              }
            }
          } else {
            $input[] = [
              'name' => $key . '[' . $child_key . ']',
              'contents' => $child
            ];
          }
        }
      } else {
        $input[] = [
          'name' => $key,
          'contents' => $value
        ];
      }
    }
    $params = array_merge($input, $this->attachFiles($attach));
    return (array) $params;
  }

  public function headers($request, $headers)
  {
    if ($request->header('Authorization')) {
      $headers['Authorization'] = $request->header('Authorization');
    }

    if ($request->header('Lang')) {
      $headers['Lang'] = $request->header('Lang');
    }

    if ($request->header('Currency')) {
      $headers['Currency'] = $request->header('Currency');
    }

    $headers['Accept'] = 'application/json';
    $headers['Content-Type'] = 'application/json';
    $headers['X-Client-Token'] = $this->service_token;

    return $headers;
  }

  private function log($request, $method, $url, $params, $user, $response)
  {
    $action = null;
    if ((strpos($url, 'delete') !== false) || (strpos($url, 'destroy') !== false) || ($method == 'delete')) {
      $action = 'delete';
    } else if ((strpos($url, 'update') !== false) || (strpos($url, 'alter') !== false)) {
      $action = 'update';
    } else if ((strpos($url, 'create') !== false) || (strpos($url, 'store') !== false)) {
      $action = 'create';
    } else {
      $action = 'undef';
    }
    $response = json_decode((string)$response->getBody());
    Log::create([
      'user_id' => $user->id,
      'action' => $action,
      'status_code' => isset($response->status_code) ?? $response->status_code,
      'route' => $this->base_url . '/' . $url,
      'properties' => json_encode($request->except(['_token']))
    ]);
  }
}
