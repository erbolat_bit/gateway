<?php

namespace App\Traits;

/**
 * Success response
 */
trait ResponseTrait
{

  public function successResponse($data, $message = null, $code = 200)
  {
    if (isset($data['error']) && $data['error'] == true)
      return $this->errorResponse($data['message'], [], $data['status_code']);

    return response()->json([
      'data' => $data,
      'error' => (bool) false,
      'success' => (bool) true,
      'code' => $code,
      'message' => $message,
    ]);
  }

  public function errorResponse($message, $data = [], $code = 500)
  {
    return response()->json([
      'message' => $message,
      'data' => $data,
      'error' => (bool) true,
      'success' => (bool) false,
      'code' => $code,
    ]);
  }

  /**
   * Redirect Admin back if some error occurs
   *    !!!!ATTENTION!!!!
   * Only for ADMIN Routes
   *    !!!!ATTENTION!!!!
   */
  public function flashEror($message)
  {
    return redirect()->back()->with('error', $message);
  }
}
