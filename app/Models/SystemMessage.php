<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemMessage extends Model
{
    
    protected $fillable = [
        'filter_params', 'message', 'title'
    ];

    protected $casts = [
        'filter_params' => 'array',
    ];

    public function userIds(){
        return $this->hasMany(SystemMessagesId::class);
    }
}
