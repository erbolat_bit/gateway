<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemMessagesId extends Model
{
    protected $table = 'system_messages_ids';

    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'system_message_id'
    ];
    
}
