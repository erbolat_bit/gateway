<?php

namespace App\Models;

use App\Scopes\UserScope;
use DB;
use Spatie\Permission\Traits\HasRoles;

class Admin extends BaseUser
{
  
  use HasRoles;

  protected $table = 'users';
  protected $guard = 'admin';
  protected $guard_name = 'web';

  protected static function booted()
  {
    static::addGlobalScope(new UserScope(true));
  }
  

  public function ratings(){
    return $this->hasMany(Rating::class, 'user_id', 'id');
  }

}
