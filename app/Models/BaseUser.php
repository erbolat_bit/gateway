<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

abstract class BaseUser extends User
{
  use HasFactory, Notifiable;

  const LETTER_1 = '1_letter'; // общей (отправляется всем пользователям);
  const LETTER_2 = '2_letter'; // func scopeWithStore() пользователям имеющим магазины;
  const LETTER_3 = '3_letter'; // func scopeWithSoleProductsForSale() пользователям имеющие единоличные товары на продажу;
  const LETTER_4 = '4_letter'; // func scopeWhoHntStoresSingleProductSale() пользователям не имеющих ни магазинов ни единоличных товаров на продажу;

  const LETTER_CAT_1 = 1; // по категории (по возрасту)
  const LETTER_CAT_2 = 2; // по категории (пользователям которые не до конца заполнили свою анкету)

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'email',
    'password',
    'email_verified_at',
    'path',
    'surname',
    'birthdate',
    'phone',
    'sex',
    'nickname',
    'parent_id',
    'bonuses',
    'is_admin',
    'department_id',
    'has_shop',
    'has_ads',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
    'is_admin' => 'boolean',
    'has_shop' => 'boolean',
    'has_ads' => 'boolean'
  ];

  public static function letterList()
  {
    $letters = self::letters();
    $roles = Role::pluck('name', 'id')->toArray();
    $roles = array_combine(
      array_map(function ($key) {
        return $key . '_role';
      }, array_keys($roles)),
      $roles
    );

    $result = array_merge($letters, $roles);
    return $result;
  }

  public static function letters()
  {
    return [
      self::LETTER_1 => 'общей (отправляется всем пользователям)',
      self::LETTER_2 => 'пользователям имеющим магазины',
      self::LETTER_3 => 'пользователям имеющие единоличные товары на продажу',
      self::LETTER_4 => 'пользователям не имеющих ни магазинов ни единоличных товаров на продажу',
    ];
  }

  public static function letterCatList()
  {
    return [
      self::LETTER_CAT_1 => 'по возрасту',
      self::LETTER_CAT_2 => 'пользователям которые не до конца заполнили свою анкету',
    ];
  }

}
