<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

  public $timestamps = false;

  protected $fillable = [
    'user_id',
    'value'
  ];

  public function user(){
    return $this->belongsTo('user');
  }
}