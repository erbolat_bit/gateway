<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use App\Scopes\UserScope;

class User extends BaseUser
{
  use HasApiTokens;

  protected $guard = 'web';

  protected static function booted()
  {
    static::addGlobalScope(new UserScope(false));
  }

  public function sexList()
  {
    return [
      'male' => 'Мужчина',
      'famale' => 'Женщина'
    ];
  }
}
