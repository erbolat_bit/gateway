<?php

namespace App\Exceptions;

use App\Mail\ExceptionMail;
use App\Traits\ResponseTrait;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;

class Handler extends ExceptionHandler
{

	use ResponseTrait;
	/**
	 * A list of the exception types that are not reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		//
	];

	/**
	 * A list of the inputs that are never flashed for validation exceptions.
	 *
	 * @var array
	 */
	protected $dontFlash = [
		'password',
		'password_confirmation',
	];

	public function report(Throwable $e)
	{
		parent::report($e);
		
		// $error = [
		// 	'message' => $e->getMessage(),
		// 	'content' => array_merge(
		// 			$this->exceptionContext($e),
		// 			$this->context(),
		// 			['exception' => $e]
		// 	)
		// ];

		// logger()->error(
		// 	$error['message'],
		// 	$error['content']
		// );
		// if (config('mail.send_errors')) {
		// 	Mail::to(config('mail.admin_email'))->queue(new ExceptionMail($error));
		// }
	}

	/**
	 * Register the exception handling callbacks for the application.
	 *
	 * @return void
	 */
	public function register()
	{
		
		$this->renderable(function(AuthenticationException $e, $request){
			return $this->errorResponse('Unauthenticated', [], 401);
		});

		$this->renderable(function(BadRequestHttpException $e, $request){
			return $this->errorResponse('Bad request (something wrong with URL or parameters)', [], 400);
		});

		$this->renderable(function(UnauthorizedHttpException $e, $request){
			return $this->errorResponse('Not authorized (not logged in)', [], 401);
		});

		$this->renderable(function(AccessDeniedException $e, $request){
			return $this->errorResponse('Logged in but access to requested area is forbidden', [], 403);
		});

		$this->renderable(function(NotFoundHttpException $e, $request){
			return $this->errorResponse('Not Found (page or other resource doesn’t exist)', [], 404);
		});

		$this->renderable(function(MethodNotAllowedHttpException $e, $request){
			return $this->errorResponse('The specified method for the request is invalid', [], 405);
		});

		$this->renderable(function(QueryException $e, $request){
			return $this->errorResponse('Server error (DB error)');
		});

		$this->renderable(function(ThrottleRequestsException $e, $request){
			return $this->errorResponse('Too Many Attempts');
		});

	}
}
