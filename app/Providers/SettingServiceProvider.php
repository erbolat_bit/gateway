<?php

namespace App\Providers;

use App\AdminServices\AddOn\SettingService;
use Cache;
use Config;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{



    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(SettingService $settingService)
    {

        if (!Cache::get('settings')) {
            $settings = $settingService->getApiSettings(request());
        }else{
            $settings = Cache::get('settings');
        }

        if (!$settings['error']) {
            Cache::put('settings', $settings);
            foreach ($settings['data']['settings']['data'] as $key => $setting) {
                Config::set('services.' . $setting['key'] . '.client_id', $setting['client_id']);
                Config::set('services.' . $setting['key'] . '.client_secret', $setting['client_secret']);
                Config::set('services.' . $setting['key'] . '.redirect', $setting['redirect']);
            }
        }
    }
}
