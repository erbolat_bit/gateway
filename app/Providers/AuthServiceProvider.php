<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    // 'App\Models\Model' => 'App\Policies\ModelPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot()
  {
    $this->registerPolicies();

    Passport::tokensCan([
      'user' => 'User',
      'admin' => 'Administrator',
      'moder' => 'Moderator',
      'app_client' => 'App client'
    ]);
    Passport::hashClientSecrets();
    Passport::routes(null, ['prefix' => config('setting.version') . '/api/oauth']);
    if (config('app.env') == 'production') {
      Passport::loadKeysFrom(posix_getpwuid(posix_getuid())['dir'] . '/secret-keys/oauth');
    }

    Gate::before(function ($user, $ability) {
      return $user->hasRole('Super Admin') ? true : null;
    });
  }
}
