<?php

namespace App\Jobs;

use App\Interfaces\User\BonusInterface;
use App\Models\User;
use App\Services\User\BonusService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UserBonuses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    private $data;
    private $bonusService;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, BonusInterface $data)
    {
        $this->bonusService = new BonusService;
        $this->user = $user;
        $this->data = $data->getData();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->data['user_id'] = $this->user->id;
        $this->bonusService->createBonus(request(), (array) $this->data);
    }
}
