<?php

namespace App\Jobs;

use App\Mail\UserEmailVerify as AppUserEmailVerify;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class UserEmailVerify implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  private $user;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct(User $user)
  {
    $this->user = $user;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    Mail::to($this->user->email)->send(new AppUserEmailVerify($this->user));
  }

  public function failed(\Exception $e){

    logger('User email verify', [
      'user' => $this->user,
      'error' => $e->getMessage()
    ]);
  }
}
