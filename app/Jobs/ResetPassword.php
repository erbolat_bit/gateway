<?php

namespace App\Jobs;

use App\Mail\ResetPassword as AppResetPassword;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ResetPassword implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  private $user;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct(User $user)
  {
    $this->user = $user;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    Mail::to($this->user)->send(new AppResetPassword($this->user));
  }

  public function failed(\Exception $e){

    logger('User reset password', [
      'user' => $this->user,
      'error' => $e->getMessage()
    ]);
  }
}
