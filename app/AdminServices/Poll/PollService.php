<?php

namespace App\AdminServices\Poll;

use App\Services\Poll\PollService as PollPollService;

class PollService extends PollPollService
{
  /**
   * Get all Polls
   */
  public function getPolls($request)
  {
    return $this->performRequest($request, 'get', 'admin/index', $request->input(), 3);
  }

  //* Retreive poll by id
  public function getPollById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/show/$id", $request->input(), 3);
  }

  //* Update poll
  public function updatePollAsAdmin($request, $id)
  {
    $input = $request->except(['photo', '_token']);
    $res = array();
    foreach ($input as $key => $val) {
      $res[$key] = [
        'name' => $key,
        'contents' => $val
      ];
    }
    if ($request->has('photo')) {
      $res['photo'] = [
        'name' => 'photo',
        'contents' => fopen($request->file('photo'), 'r')
      ];
    }
    return $this->performRequest($request, 'post', "admin/update/$id", $res, 2);
  }

  //* Delete Poll
  public function deletePollAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/delete/$id", $request->input(), 1);
  }
}
