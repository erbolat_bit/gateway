<?php

namespace App\AdminServices\User;

use App\Services\User\UserDataService;

class TicketService extends UserDataService
{

  public function getUserTicktes($request)
  {
    return $this->performRequest($request, 'get', 'admin/ticket/list', $request->input(), 3);
  }

  public function getMessagesByTicketId($request, $ticket_id)
  {
    return $this->performRequest($request, 'get', 'admin/ticket/message/list/' . $ticket_id, $request->input(), 3);
  }

  public function sendMessage($request, $ticket_id)
  {
    return $this->performRequest($request, 'post', 'admin/ticket/message/create', $request->input(), 1);
  }

  public function updateTicketStatus($request, $ticket_id)
  {
    return $this->performRequest($request, 'post', 'admin/ticket/update/' . $ticket_id, $request->input(), 1);
  }

  public function addAudit($request, $ticket_id)
  {
    return $this->performRequest($request, 'post', 'admin/ticket/add-audit/' . $ticket_id, $request->input(), 1);
  }

  public function updateMessageAsRead($request)
  {
    return $this->performRequest($request, 'post', 'admin/ticket/message/read', $request->input(), 1);
  }

  //* Get all ticket Categories
  public function getTicketCategories($request)
  {
    return $this->performRequest($request, 'get', 'admin/ticket/categories', $request->input(), 3);
  }

  //* Create new Ticket Category
  public function createTicketCategory($request)
  {
    return $this->performRequest($request, 'post', 'admin/ticket/categories/create', $request->input(), 1);
  }

  //* Get all departments for ticket Category
  public function getDepartments($request)
  {
    return $this->performRequest($request, 'get', 'admin/department/all', $request->input(), 3);
  }

  //* get Ticket Category credentials
  public function getTicketCategoryAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/ticket/categories/show/$id", $request->input(), 3);
  }

  //* Update Ticket category
  public function updateTicketCategoryAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/ticket/categories/update/$id", $request->input(), 1);
  }

  //* Delete Ticket Category
  public function deletTicketCategory($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/ticket/categories/delete/$id", $request->input(), 1);
  }
}
