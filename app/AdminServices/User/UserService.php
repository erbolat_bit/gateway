<?php

namespace App\AdminServices\User;

use App\Services\User\UserDataService;

class UserService extends UserDataService
{
  /**
   * Get address names from Address Ids
   */
  public function getAddresses($request, $address_ids)
  {
    $request->merge(['addresses' => $address_ids]);
    return $this->performRequest($request, 'get', 'admin/addresses', $request->input(), 3);
  }

  //* Get all departments
  public function getDepartments($request, $withPagination = false)
  {
    if ($withPagination) {
      $request->merge(['paginate' => true]);
    }
    return $this->performRequest($request, 'get', 'admin/department/all', $request->input(), 3);
  }

  //* Retreive one department credentials by its id
  public function getDepartmentById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/department/show/$id", $request->input(), 3);
  }

  //* Update departmetn as Admin
  public function updateDepartment($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/department/update/$id", $request->input(), 1);
  }

  //* Delete Department
  public function deleteDepartmentAsAdmin($request)
  {
    return $this->performRequest($request, 'delete', 'admin/department/delete', $request->input(), 1);
  }

  //* Create department as Admin
  public function createDepartment($request)
  {
    return $this->performRequest($request, 'post', 'admin/department/create', $request->input(), 1);
  }

  public function wishlistStatistic($request)
  {
    return $this->performRequest($request, 'get', 'admin/wishlist/statistic', $request->input(), 3);
  } 
}
