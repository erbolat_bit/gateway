<?php

namespace App\AdminServices\User;

use App\AdminServices\Ads\AdsService;
use App\AdminServices\Shop\ShopService;
use App\Models\Admin;
use App\Traits\Client;

class UserFilterService
{
  use Client;

  private $shopService;
  private $adsService;

  public function __construct(ShopService $shopService, AdsService $adsService)
  {
    $this->shopService = $shopService;
    $this->adsService = $adsService;
  }

  public function getFilterData(array $params)
  {
    $users = Admin::withoutGlobalScopes();
    $users = $this->filterUsersLetterAndRole($users, $params);
    $users = $this->filterUsersAgeAndProfile($users, $params);

    $users = $users->select(['id', 'email', 'name'])->get()->toArray();
    return $users;
  }

  private function filterUsersAgeAndProfile($users, array $params)
  {

    if (isset($params['age']) && (bool)$params['age']) {
      if (isset($params['age_from'])) {
        $users->whereYear('birthdate', '<=', (int)date('Y', time()) - (int)$params['age_from']);
      }

      if (isset($params['age_to'])) {
        $users->whereYear('birthdate', '>=', (int)date('Y', time()) - (int)$params['age_to']);
      }
    }

    if (isset($params['profile']) && (bool)$params['profile']) {
      $users->orWhereNull('birthdate')->orWhereNull('phone')->orWhereNull('sex');
    }

    return $users;
  }

  public function filterUsersLetterAndRole($users, array $params)
  {
    if (isset($params['letter'])) {
      $value = explode('_', $params['letter']);
      if ($value[1] == 'letter') {
        switch ($params['letter']) {
          case Admin::LETTER_1:
            break;
          case Admin::LETTER_2:
            $users = $users->where('has_shop', true);
            break;
          case Admin::LETTER_3:
            $users = $users->where('has_ads', true);
            break;
          case Admin::LETTER_4:
            $users = $users->where('has_shop', false)->where('has_ads', false);
            break;
          default:
            break;
        }
      }
      if ($value[1] == 'role') {
        $users = $users->whereHas('roles', function ($query) use ($value) {
          $query->where('id', $value[0]);
        });
      }
    }

    return $users;
  }
}
