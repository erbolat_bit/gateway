<?php

namespace App\AdminServices\Lot;

use App\Services\LotService as ServicesLotService;

class LotService extends ServicesLotService
{
  /**
   * Get all Lots
   */
  public function getLots($request)
  {
    return $this->performRequest($request, 'get', 'admin/lot/list', $request->input(), 3);
  }

  /**
   * Get All Offers
   */
  public function getOffers($request)
  {
    return $this->performRequest($request, 'get', 'admin/offer/list', $request->input(), 3);
  }

  //* Show Lot By Its id
  public function getLotByIdAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/lot/show/$id", $request->input(), 3);
  }

  //* Update Lot As Admin
  public function updateLotAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/lot/update/$id", $request->input(), 1);
  }

  //* Delete Lot As Admin
  public function deletLotAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/lot/delete/$id", $request->input(), 1);
  }

  //* Delete Offer
  public function deleteOfferAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/offer/delete/$id", $request->input(), 1);
  }
}
