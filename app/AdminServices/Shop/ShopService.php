<?php

namespace App\AdminServices\Shop;

use App\Services\ShopService as ServicesShopService;

class ShopService extends ServicesShopService
{
  /**
   * Get all Products
   */
  public function getProductList($request)
  {
    return $this->performRequest($request, 'get', 'admin/product/list', $request->input(), 3);
  }

  /**
   * Get all shops
   */
  public function getShopList($request)
  {
    return $this->performRequest($request, 'get', 'admin/shop/list', $request->input(), 3);
  }

  /**
   * Get product names from ids
   */
  public function getProductNames($request, array $product_ids)
  {
    $request->merge(['product_ids' => $product_ids]);
    return $this->performRequest($request, 'get', 'admin/product/names', $request->input(), 3);
  }

  /**
   * Get product titles from ids
   */
  public function getStoreTitles($request, array $store_ids)
  {
    $request->merge(['store_ids' => $store_ids]);
    return $this->performRequest($request, 'get', 'admin/shop/names', $request->input(), 3);
  }

  //* Show shop details by its id
  public function getShopAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/shop/show/$id", $request->input(), 3);
  }

  //* Shop details for update
  public function getShopByIdForUpdate($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/shop/details/$id", $request->input(), 3);
  }

  //* Update shop details as Admin
  public function udpateShopAsAdmin($request, $id)
  {
    $data = $request->except(['logotype', 'bg_image']);
    $res = [];
    foreach ($data as $key => $value) {
      $res[$key] = [
        'name' => $key,
        'contents' => $value
      ];
    }
    if ($request->has('logotype'))
      $res['logotype'] = ['name' => 'logotype', 'contents' => fopen($request->file('logotype'), 'r')];
    if ($request->has('bg_image'))
      $res['bg_image'] = ['name' => 'bg_image', 'contents' => fopen($request->file('bg_image'), 'r')];
    return $this->performRequest($request, 'post', "admin/shop/update/$id", $res, 2);
  }

  //* Delete Shop As Admin
  public function deleteShopAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/shop/delete/$id", $request->input(), 1);
  }

  //* Show product detials by its id
  public function getProductByIdAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/product/show/$id", $request->input(), 3);
  }

  //* Update product details as Admin
  public function updateProductAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/product/update/$id", $request->input(), 1);
  }

  //* Delete Product as Admin
  public function deleteProductAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/product/delete/$id", $request->input(), 1);
  }

  public function getShopsViews($request, $id=null){
    
    return $this->performRequest($request, 'get', "admin/shop/all-views", $request->input(), 3);
  }

  public function getShopViews($request, $id=''){
    return $this->performRequest($request, 'get', "admin/shop/views/" . $id, $request->input(), 3);
  }
}
