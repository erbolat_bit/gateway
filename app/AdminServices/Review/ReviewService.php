<?php

namespace App\AdminServices\Review;

use App\Services\ReviewService as ServicesReviewService;

class ReviewService extends ServicesReviewService
{
  /**
   * Get all Comments
   */
  public function getCommentList($request)
  {
    return $this->performRequest($request, 'get', 'admin/comments/list', $request->input(), 3);
  }

  /**
   * Get all questions
   */
  public function getQuestionList($request)
  {
    return $this->performRequest($request, 'get', 'admin/questions/list', $request->input(), 3);
  }

  /**
   * Get all Comments
   */
  public function getReviewList($request)
  {
    return $this->performRequest($request, 'get', 'admin/reviews/list', $request->input(), 3);
  }

  public function deleteComment($request, $id)
  {
    return $this->performRequest($request, 'delete', 'admin/comments/delete/' . $id, $request->input());
  }
  public function updateComment($request, $id)
  {
    return $this->performRequest($request, 'put', 'admin/comments/update/' . $id, $request->input());
  }

  public function deleteReview($request, $id)
  {
    return $this->performRequest($request, 'delete', 'admin/reviews/delete/' . $id, $request->input());
  }
  public function updateReview($request, $id)
  {
    return $this->performRequest($request, 'put', 'admin/reviews/update/' . $id, $request->input());
  }

  public function deleteQuestion($request, $id)
  {
    return $this->performRequest($request, 'delete', 'admin/questions/delete/' . $id, $request->input());
  }
  public function updateQuestion($request, $id)
  {
    return $this->performRequest($request, 'put', 'admin/questions/update/' . $id, $request->input());
  }

}
