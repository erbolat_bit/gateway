<?php

namespace App\AdminServices\Cart;

use App\Services\Cart\OrderService as CartOrderService;

class OrderService extends CartOrderService
{
  /**
   * Get all Orders
   */
  public function getOrders($request)
  {
    return $this->performRequest($request, 'get', 'admin/order/list', $request->input(), 3);
  }
  /**
   * Get all baskets
   */
  public function getBaskets($request)
  {
    return $this->performRequest($request, 'get', 'admin/basket/list', $request->input(), 3);
  }

  //* Get Order by Its id
  public function getOrderByIdAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/order/show/$id", $request->input(), 3);
  }

  //* Get Basket by id
  public function getBasketById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/basket/show/$id", $request->input(), 3);
  }

  public function getOrderStatistic($request)
  {
    return $this->performRequest($request, 'get', "admin/order/statistic", $request->input(), 3);
  }
}
