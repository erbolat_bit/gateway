<?php

namespace App\AdminServices\Ads;

use App\Services\AdsService as ServicesAdsService;

class AdsService extends ServicesAdsService
{
  /**
   * Get All Ads
   */
  public function getAds($request)
  {
    return $this->performRequest($request, 'get', 'admin/list', $request->input(), 3);
  }
}
