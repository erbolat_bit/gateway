<?php

namespace App\AdminServices\Coupon;

use App\AdminServices\User\UserFilterService;
use App\Services\CouponService as ServicesCouponService;

class CouponService extends ServicesCouponService
{

  private $userFilterService;

  public function __construct(UserFilterService $userFilterService)
  {
    $this->userFilterService = $userFilterService;

    parent::__construct();
  }

  /**
   * Get all Coupons
   */
  public function getCoupons($request)
  {
    return $this->performRequest($request, 'get', 'admin/coupons/list', $request->input(), 3);
  }

  /**
   * Get all Items
   */
  public function getItems($request)
  {
    return $this->performRequest($request, 'get', 'admin/items/list', $request->input(), 3);
  }

  //* Get Coupon by Id as Admin
  public function getCouponByIdAsAdmin($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/coupons/show/$id", $request->input(), 3);
  }

  //* Update coupon Details
  public function updateCouponAsAdmin($request, $id)
  {
    $data = $request->except('image');

    foreach ($data as $key => $val) {
      $res[$key] = [
        'name' => $key,
        'contents' => $val
      ];
    }
    if ($request->has('image')) {
      $res['image'] = [
        'name' => 'image',
        'contents' => fopen($request->file('image'), 'r'),
      ];
    }
    return $this->performRequest($request, 'post', "admin/coupons/update/$id", $res, 2);
  }

  public function createAdminCoupon($request)
  {
    $params = $request->input();
    $params['by_admin'] = 1;
    $params['type_mailing'] = 1;
    $users = $this->userFilterService->getFilterData($params);

		$params['users'] = $users;
		$type = 1;

		if ($request->file('image')) {
			$files = [
				[
					'name' => 'image',
					'contents' => fopen($request->file('image')->path(), 'r')
				]
			];
			$params = $this->multipartData($params, $files);
			$type = 2;
		}
    $result = $this->performRequest($request, 'post', 'admin/coupons/create', $params, $type);
		return $result;
  }
}
