<?php

namespace App\AdminServices\Banner;

use App\Repositories\BannerRepository;
use App\Traits\UploadFileTrait;

class BannerService
{
    use UploadFileTrait;

    private $repo;

    public function __construct(BannerRepository $bannerRepository)
    {
        $this->repo = $bannerRepository;
    }


    public function getBannerList()
    {
        return $this->repo->getBannerList();
    }

    public function createBanner($request){

        $data = $request->input();
        $data['image'] = $this->upload_file($request, 'image', 'banners');

        $model = $this->repo->create($data);

        return $model;
    }

    public function updateBanner($request, $id)
    {
        $data = $request->input();
        if ($request->hasFile('image')) {
            $data['image'] = $this->upload_file($request, 'image', 'banners');
        }
        
        $model = $this->repo->update($id, $data);
        return $model;
    }

    public function deleteBanner($id){

        return $this->repo->delete($id);
    }

    public function getBunner(int $id){
        return $this->repo->getOne($id);
    }
} 