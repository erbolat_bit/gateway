<?php

namespace App\AdminServices\Blog;

use App\Services\Blog\BlogService as BlogBlogService;

class BlogService extends BlogBlogService
{
  /**
   * Get All Posts
   */
  public function getPosts($request)
  {
    return $this->performRequest($request, 'get', 'admin/posts/list', $request->input(), 3);
  }

  /**
   * Get all Categories
   */
  public function getCategories($request)
  {
    return $this->performRequest($request, 'get', 'admin/categories/list', $request->input(), 3, ['lang' => 'ru']);
  }

  //* Categories for updating post
  public function getCategoriesWithoutPagination($request)
  {
    return $this->performRequest($request, 'get', "admin/categories/list/without/pagination", $request->input(), 3, ['lang' => 'ru']);
  }

  //* Get post titles
  public function getPostList($request, $post_ids)
  {
    $request->merge(['post_ids' => $post_ids]);
    return $this->performRequest($request, 'get', 'admin/posts/names', $request->input(), 3);
  }
  //* Get category by id
  public function getCategoryById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/categories/show/$id", $request->input(), 3);
  }

  //* Store BlogCategories
  public function storeCategory($request)
  {
    return $this->performRequest($request, 'post', 'admin/categories/store', $request->input(), 1);
  }

  //* Update category
  public function updateCategory($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/categories/update/$id", $request->input(), 1);
  }

  //* Delete Category
  public function deleteCategory($request)
  {
    return $this->performRequest($request, 'delete', "admin/categories/delete", $request->input(), 1);
  }

  //* Get Post by Id
  public function getPostById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/posts/show/$id", $request->input(), 3);
  }

  //* Update Blog post
  public function updatePost($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/posts/update/" . $id, $request->input(), 1);
  }

  //* Create new Blog Post
  public function createNewPost($request)
  {
    return $this->performRequest($request, 'post', 'admin/posts/store', $request->input(), 1);
  }

  //* Delete Post
  public function deletePost($request)
  {
    return $this->performRequest($request, 'delete', 'admin/posts/destroy', $request->input(), 1);
  }

  //* Search for categories
  public function searchCategories($request)
  {
    return $this->performRequest($request, "get", "admin/categories/search", $request->input(), 3);
  }
}
