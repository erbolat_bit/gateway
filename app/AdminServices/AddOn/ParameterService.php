<?php

namespace App\AdminServices\AddOn;

use App\Services\AddOn\ParameterService as AddOnParameterService;

class ParameterService extends AddOnParameterService
{
  /**
   * Get all parameters from Add-on Service
   */
  public function getParameters($request)
  {
    return $this->performRequest($request, 'get', 'admin/parameter', $request->input(), 3);
  }

  //* Get parameter types
  public function getTypesOfParameters($request)
  {
    return $this->performRequest($request, 'get', "admin/parameter/types", $request->input(), 3);
  }

  //* Create Parameter
  public function createParameter($request)
  {
    $params = $request->input();
    if (isset($params['multiselect']) && $params['multiselect'] == 'on') {
      $params['multiselect'] = 1;
    } elseif (isset($params['multiselect']) && $params['multiselect'] == 'off') {
      $params['multiselect'] = 0;
    }

    return $this->performRequest($request, 'post', 'admin/parameter/store', $params, 1);
  }

  //* Create Parameter
  public function updateParameter($request, $id)
  {
    $params = $request->input();
    if (isset($params['multiselect']) && $params['multiselect'] == 'on') {
      $params['multiselect'] = 1;
    } elseif (isset($params['multiselect']) && $params['multiselect'] == 'off') {
      $params['multiselect'] = 0;
    }
    return $this->performRequest($request, 'post', 'admin/parameter/update/' . $id, $params, 1);
  }

  //* Get all Categories
  public function getCategories($request)
  {
    $request->merge(['all' => true]);
    return $this->performRequest($request, 'get', 'admin/category', $request->input(), 3);
  }

  //* Get all Categories
  public function getCategoriesWithChild($request, $parameter = false)
  {
    return $this->performRequest($request, 'get', $parameter ? 'admin/category/all/' . $parameter : 'admin/category/all', $request->input(), 3);
  }

  public function destroyOption($request, $optionId)
  {
    return $this->performRequest($request, 'delete', 'admin/parameter/parameter-option/' . $optionId, $request->input());
  }

  public function destroy($request, $parameter)
  {
    return $this->performRequest($request, 'delete', 'admin/parameter/delete/' . $parameter, $request->input());
  }
}
