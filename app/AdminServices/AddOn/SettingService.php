<?php

namespace App\AdminServices\AddOn;

use App\Services\AddOn\SettingService as AddOnSettingService;

class SettingService extends AddOnSettingService
{
  /**
   * Get all settings
   */
  public function getSettings($request)
  {
    return $this->performRequest($request, 'get', 'admin/setting/', $request->input(), 3);
  }

  /**
   * Get Social Settings
   */
  public function getApiSettings($request)
  {
    return $this->performRequest($request, 'get', 'admin/setting/social', $request->input(), 3);
  }

  /**
   * Get all Devlivery Settings
   */
  public function getDeliverySettings($request)
  {
    return $this->performRequest($request, 'get', 'admin/delivery-setting/', $request->input(), 3);
  }

  //* Get one setting details for update view
  public function getOneSettingDetails($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/setting/get-one/$id", $request->input(), 3);
  }

  //* Update Setting
  public function updateSetting($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/setting/update/$id", $request->input(), 1);
  }

  //* Update delivery-setting
  public function updateDeliverySetting($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/delivery-setting/update/$id", $request->input(), 1);
  }

  //* Get Delivery setting by its id
  public function getDeliverySettingById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/delivery-setting/show/$id", $request->input(), 3);
  }

  //* Get the list of currencies
  public function getCurrencies($request)
  {
    return $this->performRequest($request, 'get', "admin/delivery-setting/currencies", $request->input(), 3);
  }

  //* Create new Delivery-setting
  public function createDeliverySetting($request)
  {
    return $this->performRequest($request, 'post', "admin/delivery-setting/store", $request->input(), 1);
  }

  //* Delete delivery-setting
  public function deleteDelivery($request, $id)
  {
    return $this->performRequest($request, 'delete', "admin/delivery-setting/delete/$id", $request->input(), 1);
  }
}
