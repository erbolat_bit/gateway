<?php

namespace App\AdminServices\AddOn;

use App\Services\AddOn\CategoryService as AddOnCategoryService;

class CategoryService extends AddOnCategoryService
{
  public function createCategory($request)
  {
    return $this->performRequest($request, 'post', 'admin/category/store', $request->input(), 1);
  }

  public function getCategories($request)
  {
    return $this->performRequest($request, 'get', 'admin/category', $request->input(), 3);
  }

  public function deleteCategory($request, $id)
  {
    return $this->performRequest($request, 'delete', 'admin/category/delete/' . $id, $request->input(), 1);
  }

  public function showCategory($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/category/show/$id", $request->input(), 3);
  }

  public function updateCategory($request, $id)
  {
    $data = $request->except(['image', '_token']);
    $res = [];
    foreach ($data as $key => $value) {
      $res[$key] = [
        'name' => $key,
        'contents' => $value
      ];
    }
    if ($request->hasFile('image')) {
      $res['image'] = [
        'name' => 'image',
        'contents' => fopen($request->file('image'), 'r')
      ];
    }
    return $this->performRequest($request, 'post', 'admin/category/update/' . $id, $res, 2);
  }

  /**
   * Get all category Titles from ids
   */
  public function getCategoryList($request, $category_ids)
  {
    $request->merge(['category_ids' => $category_ids]);
    return $this->performRequest($request, 'get', 'admin/category/names', $request->input(), 3);
  }
}
