<?php

namespace App\AdminServices\AddOn;

use App\Services\AddOn\CountryService as AddOnCountryService;

class CountryService extends AddOnCountryService
{
  public function getCountries($request)
  {
    return $this->performRequest($request, 'get', 'admin/country', $request->input(), 3);
  }

  /**
   * Get all country names form ids
   */
  public function getCountryList($request, $country_ids)
  {
    $request->merge(['country_ids' => $country_ids]);
    return $this->performRequest($request, 'get', 'admin/country/names', $request->input(), 3);
  }

  //* Retrieve Country by its id
  public function getCountryById($request, $id)
  {
    return $this->performRequest($request, 'get', "admin/country/show/$id", $request->input(), 3);
  }

  //* Update Country Credentials
  public function updateCountry($request, $id)
  {
    return $this->performRequest($request, 'post', "admin/country/update/$id", $request->input(), 1);
  }

  //* Delete Country
  public function deleteCountry($request)
  {
    return $this->performRequest($request, 'delete', "admin/country/delete/$request->id", $request->input(), 1);
  }

  //* Create New Country
  public function createCountry($request)
  {
    return $this->performRequest($request, 'post', "admin/country/store", $request->input(), 1);
  }
}
