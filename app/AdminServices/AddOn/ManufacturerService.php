<?php

namespace App\AdminServices\AddOn;

use App\Services\AddOn\ManufacturerService as AddOnManufacturerService;

class ManufacturerService extends AddOnManufacturerService
{
  public function getManufacturers($request)
  {
    return $this->performRequest($request, 'get', 'admin/manufacturer', $request->input(), 3);
  }

  /**
   * Get manufacturer titles
   */
  public function getManufacturerList($request, $manufacturer_ids)
  {
    $request->merge(['manufacturer_ids' => $manufacturer_ids]);
    return $this->performRequest($request, 'get', 'admin/manufacturer/names', $request->input(), 3);
  }

  //* Get Manufacturer By Id
  public function getManufacturerById($request, $id)
  {
    $request->merge(['id' => $id]);
    return $this->performRequest($request, 'get', 'admin/manufacturer/byId', $request->input(), 3);
  }

  //* Update manufacturer
  public function updateManufacturer($request, $id)
  {
    $type = 1;
    $params = $request->input();

    if ($request->file('logotype')) {
      $files[] = [
        'name' => 'logotype',
        'contents' => fopen($request->file('logotype')->path(), 'r')
      ];

      $params = $this->multipartData($request->input(), $files);
      $type = 2;
    }
    return $this->performRequest($request, 'post', "admin/manufacturer/update/$id", $params, $type);
  }

  //* Delete Manufacturer
  public function deleteManufacturer($request)
  {
    return $this->performRequest($request, 'delete', "admin/manufacturer/delete", $request->input(), 1);
  }

  //* Create Manufacturer
  public function createManufacturer($request)
  {
    $type = 1;
    $params = $request->input();

    if ($request->file('logotype')) {
      $files[] = [
        'name' => 'logotype',
        'contents' => fopen($request->file('logotype')->path(), 'r')
      ];

      $params = $this->multipartData($request->input(), $files);
      $type = 2;
    }
    return $this->performRequest($request, 'post', 'admin/manufacturer/store', $params, $type);
  }
}
