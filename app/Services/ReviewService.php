<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class ReviewService
{

  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.review.base_url');
    $this->service_token = config('api_services.review.token');
  }

  public function getReviewsQuestionsCount($request, $product_id)
  {
    $result = $this->performRequest($request, 'get', 'collection/' . $product_id, $request->input(), 3);
    return $result;
  }


  public function getReviewsByProduct($request, $product_id)
  {
    $result = $this->performRequest($request, 'get', 'reviews/' . $product_id, $request->input(), 3);
    $reviews = collect($result['data']);

    if ($reviews->isNotEmpty()) {
      $users = UserResource::collection(User::whereIn('id', $reviews->unique('user_id')->pluck('user_id')->toArray())->get());
      if (!$users->isEmpty()) {
        $reviews->transform(function ($review) use ($users) {
          $user = $users->where('id', $review['user_id'])->first();
          $review['user'] = $user ? $user : null;
          return $review;
        });
      }
    }
    $result['data'] = $reviews->toArray();
    return $result;
  }

  public function getReviewImages($request, $id)
  {
    $result = $this->performRequest($request, 'get', 'reviews/img/' . $id, $request->input(), 3);
    return $result;
  }

  public function getReviewRateCount($request, $product_id)
  {
    $result = $this->performRequest($request, 'get', 'reviews/rates/' . $product_id, $request->input(), 3);
    return $result;
  }

  public function getShopReviews($request, $shop_id, $product_ids = [])
  {
    $input = $request->input();
    $input['product_ids'] = $product_ids;
    $result = $this->performRequest($request, 'get', 'reviews/show/' . $shop_id, $input, 3);
    $reviews = collect($result['data']);

    if ($reviews->isNotEmpty()) {
      $users = UserResource::collection(User::whereIn('id', $reviews->unique('user_id')->pluck('user_id')->toArray())->get());
      if (!$users->isEmpty()) {
        $reviews->transform(function ($review) use ($users) {
          $user = $users->where('id', $review['user_id'])->first();
          $review['user'] = $user ? $user : null;
          return $review;
        });
      }
    }

    $result['data'] = $reviews->toArray();
    return $result;
  }

  public function getShopRates($request, $shop_id, $product_ids = [])
  {
    $input = $request->input();
    $input['product_ids'] = $product_ids;
    $result = $this->performRequest($request, 'get', 'reviews/rates/collection', $input, 3);
    return $result;
  }

  public function getRatesByProducts($request, $product_ids = [])
  {
    $input = $request->input();
    $input['product_ids'] = $product_ids;
    $result = $this->performRequest($request, 'get', 'reviews/rates/by-products', $input, 3);
    return $result;
  }

  public function store($request, $product_id)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('images')) {
      foreach ($request->file('images') as $key => $value) {
        $files[] = [
          'name' => 'images[' . $key . ']',
          'contents' => fopen($request->file('images.' . $key)->path(), 'r')
        ];
      }

      $params = $this->multipartData($params, $files);
      $type = 2;
    }

    $result = $this->performRequest($request, 'post', 'reviews/' . $product_id, $params, $type);
    return $result;
  }

  public function like($request, $model, $id)
  {
    $params = $request->input();

    $result = $this->performRequest($request, 'put', $model . '/like/' . $id, $params);
    return $result;
  }
}
