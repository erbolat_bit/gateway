<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;
use Illuminate\Http\Request;

class CommentService
{

  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.review.base_url');
    $this->service_token = config('api_services.review.token');
  }

  public function getCommentsByPost($request, $post_id)
  {
    $params = $request->input();
    $result = $this->performRequest($request, 'get', 'post/comments/list/' . $post_id, $params, 3);
    $comments = collect($result['data']);

    if ($comments->isNotEmpty()) {
      $users = UserResource::collection(User::whereIn('id', $comments->unique('user_id')->pluck('user_id')->toArray())->get());
      if (!$users->isEmpty()) {
        $comments->transform(function ($review) use ($users) {
          $user = $users->where('id', $review['user_id'])->first();
          $review['user'] = $user ? $user : null;
          return $review;
        });
      }
    }
    $result['data'] = $comments->toArray();

    return $result;
  }

  public function getCommentsCountByPost($request, $post_id)
  {
    $params = $request->input();
    $result = $this->performRequest($request, 'get', 'post/comments/count/' . $post_id, $params, 3);
    return $result;
  }

  public function createComment($request)
  {
    $params = $request->input();
    $result = $this->performRequest($request, 'put', 'post/comments/add', $params);
    $result['data']['user'] = new UserResource(User::find($result['data']['user_id']));
    return $result['data'];
  }

  public function getPostsCommentsCount(Request $request, array $ids)
  {
    $request->merge(['post_ids' => $ids]);
    return $this->performRequest($request, 'get', 'post/comments/count/all', $request->input(), 3);
  }
}
