<?php

namespace App\Services\Poll;

use App\Traits\Client;

class PollService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.poll.base_url');
    $this->service_token = config('api_services.poll.token');
  }

  public function store($request)
  {

    $params = $request->input();
    $type = 1;
    
    if ($request->file('photo')) {
      $fails[] = [
        'name' => 'photo',
        'contents' => fopen($request->file('photo')->path(), 'r')
      ];
    }
    
    if ($request->file('questions')) {
      foreach ($request->file('questions') as $key => $value) {
        $fails[] = [
          'name' => 'questions[' . $key . '][photo]',
          'contents' => fopen($request->file('questions.' . $key . '.photo')->path(), 'r')
        ];
        if (isset($value['answers'])) {
          foreach ($value['answers'] as $a_key => $a_value) {
            $fails[] = [
              'name' => 'questions[' . $key . '][answers][' . $a_key . '][photo]',
              'contents' => fopen($request->file('questions.' . $key . '.answers.' . $a_key . '.photo')->path(), 'r')
            ];
          }
        }
      }
    }

    if ($request->file('photo') || $request->file('questions')) {
      $params = $this->multipartData($request->input(), $fails);
      $type = 2;
    }

    return $this->performRequest($request, 'post', 'store', $params, $type);
  }

  public function showPoll($request, $id)
  {
    return $this->performRequest($request, 'get', "show/$id", $request->input(), 3);
  }

  public function getShopPolls($request, $id)
  {
    return $this->performRequest($request, 'get', $id, $request->input(), 3);
  }

  public function getUserPolls($request)
  {
    return $this->performRequest($request, 'get', 'by-user', $request->input(), 3);
  }

  public function getPollVotes($request, $id)
  {
    return $this->performRequest($request, 'get', "vote/$id", $request->input(), 3);
  }

  public function deletePoll($request, $id)
  {
    return $this->performRequest($request, 'delete', "delete/$id", $request->input(), 3);
  }

  public function deleteQuestion($request, $id)
  {
    return $this->performRequest($request, 'delete', "delete-question/$id", $request->input(), 3);
  }

  public function deleteAnswer($request, $id)
  {
    return $this->performRequest($request, 'delete', "delete-answer/$id", $request->input(), 3);
  }

  public function deletePhotoPoll($request, $id)
  {
    return $this->performRequest($request, 'post', "delete-photo/$id", $request->input(), 3);
  }

  public function deleteQuestionPhoto($request, $id)
  {
    return $this->performRequest($request, 'post', "delete-question-photo/$id", $request->input(), 3);
  }

  public function deleteAnswerPhoto($request, $id)
  {
    return $this->performRequest($request, 'post', "delete-answer-photo/$id", $request->input(), 3);
  }

  public function vote($request)
  {
    return $this->performRequest($request, 'post', 'vote', $request->input(), 1);
  }

  public function updatePoll($request, $id)
  {

    $params = $request->input();
    $type = 1;
    
    if ($request->file('photo')) {
      $fails[] = [
        'name' => 'photo',
        'contents' => fopen($request->file('photo')->path(), 'r')
      ];
    }
    
    if ($request->file('questions')) {
      foreach ($request->file('questions') as $key => $value) {
        $fails[] = [
          'name' => 'questions[' . $key . '][photo]',
          'contents' => fopen($request->file('questions.' . $key . '.photo')->path(), 'r')
        ];
        if (isset($value['answers'])) {
          foreach ($value['answers'] as $a_key => $a_value) {
            $fails[] = [
              'name' => 'questions[' . $key . '][answers][' . $a_key . '][photo]',
              'contents' => fopen($request->file('questions.' . $key . '.answers.' . $a_key . '.photo')->path(), 'r')
            ];
          }
        }
      }
    }

    if ($request->file('photo') || $request->file('questions')) {
      $params = $this->multipartData($request->input(), $fails);
      $type = 2;
    }

    return $this->performRequest($request, 'post', "update/$id", $params, $type);
  }
}
