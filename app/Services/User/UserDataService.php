<?php

namespace App\Services\User;

use App\Traits\Client;

class UserDataService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function getDocsAndAddresses($request)
  {
    return $this->performRequest($request, 'get', 'user/data', $request->input(), 3);
  }
}
