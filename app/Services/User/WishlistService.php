<?php

namespace App\Services\User;

use App\Traits\Client;

class WishlistService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function getUserWishlist($request)
  {
    return $this->performRequest($request, 'get', 'user/wishlist', $request->input(), 3);
  }

  public function createWishlist($request)
  {
    return $this->performRequest($request, 'put', 'user/wishlist', $request->input());
  }

  public function deleteWishlist($request, $product_id)
  {
    return $this->performRequest($request, 'delete', 'user/wishlist/' . $product_id, $request->input());
  }
}
