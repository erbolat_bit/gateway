<?php

namespace App\Services\User;

use App\Traits\Client;

class BonusService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function getBonuses($request)
  {
    return $this->performRequest($request, 'get', 'bonuses', $request->input(), 3);
  }

  public function createBonus($request, array $data=[])
  {
    if (empty($data)) {
      $data = $request->input();
    }

    return $this->performRequest($request, 'post', 'bonuses/store', $data, 1);
  }
}
