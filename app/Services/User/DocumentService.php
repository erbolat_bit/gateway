<?php

namespace App\Services\User;

use App\Traits\Client;

class DocumentService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function addDocument($request)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('document')) {
      $files = [];
      foreach ($request->file('document') as $key => $value) {
        $files[$key]['name'] = 'document[' . $key . '][file]';
        $files[$key]['contents'] = fopen($value['file']->path(), 'r');
      }
      $params = $this->multipartData($params, $files);
      $type = 2;
    }

    return $this->performRequest($request, 'post', 'profile/document/add', $params, $type);
  }

  public function deleteDocument($request, $id)
  {
    return $this->performRequest($request, 'post', 'profile/document/remove/' . $id, $request->input());
  }
}
