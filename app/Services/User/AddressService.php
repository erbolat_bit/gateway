<?php

namespace App\Services\User;

use App\Traits\Client;

class AddressService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function addAddress($request)
  {
    return $this->performRequest($request, 'post', 'profile/address/add', $request->input());
  }

  public function deleteAddress($request, $id)
  {
    return $this->performRequest($request, 'post', 'profile/address/remove/' . $id, $request->input());
  }
}
