<?php

namespace App\Services\User;

use App\Models\User;
use App\Traits\Client;

class ChatService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function getUserChats($request)
  {
    $input = $request->input();
    if ($request->has('keyword')) {
      $user_ids = User::select(['id', 'name'])->where('name', 'like', '%' . $request->keyword . '%')->get();
      if ($user_ids->isEmpty()) {
        unset($input['keyword']);
      }else{
        $input['user_ids'] = $user_ids->pluck('id')->toArray();
      }
    }
    return $this->performRequest($request, 'get', 'chats/user', $input, 3);
  }

  public function sendMessage($request)
  {
    $params = $request->input();
    $user = $request->user();
    $params['user'] = ['id' => $user->id, 'name' => $user->name, 'path' => $user->path ? $user->path : false];
    $type = 1;

    if ($request->file('file')) {
      $files = [
        [
          'name' => 'file',
          'contents' => fopen($request->file('file')->path(), 'r')
        ]
      ];
      $params = $this->multipartData($params, $files);
      $type = 2;
    }
    return $this->performRequest($request, 'post', 'chats/send/message', $params, $type);
  }

  public function sendMessageForOrder($request, $data)
  {
    $params = $data;
    $html = '';
    $html .= 'Пользователь ' . $params['user']['name'] . ', оплатил заказ № ' . $data['order_id'] . '.<br>';
    $html .= 'Магазин: ' . $data['store_title'] . '<br>';
    $html .= '<ul>';
    foreach ($data['products'] as $key => $product) {
      $html .= '<li> Товар: ' . $product['product_title'] . ' x ' . $product['quantity'] . ' - ' . $product['amount'] . ' ' . $product['currency'];
      $html .= '</li>';
    }
    $html .= '</ul>';

    $params['text'] = html_entity_decode($html);
    return $this->performRequest($request, 'post', 'chats/send/message', $params, 1);
  }

  public function searchMessFromChat($request, $chat_id)
  {
    return $this->performRequest($request, 'get', 'chats/user/search/' . $chat_id, $request->input(), 3)['data'];
  }

  public function getChat($request, $chat_id)
  {
    return $this->performRequest($request, 'get', 'chats/view/' . $chat_id, $request->input(), 3)['data'];
  }

  public function getChatMessages($request, $chat_id)
  {
    return $this->performRequest($request, 'get', 'chats/view/messages/' . $chat_id, $request->input(), 3);
  }

  public function messSetRead($request)
  {
    return $this->performRequest($request, 'post', 'chats/messages/is_read', $request->input());
  }
}
