<?php

namespace App\Services\User;

use App\Traits\Client;

class HistoryService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function getUserHistory($request)
  {
    return $this->performRequest($request, 'get', 'user/history', $request->input(), 3);
  }

  public function createHistory($request)
  {
    return $this->performRequest($request, 'put', 'user/history', $request->input());
  }

  public function deleteHistory($request, $product_id)
  {
    return $this->performRequest($request, 'delete', 'user/history/' . $product_id, $request->input());
  }
}
