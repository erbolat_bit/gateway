<?php

namespace App\Services\User;

use App\Traits\Client;

class ReferralService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  public function getUserReferrals($request, $user_ids)
  {
    $params = $request->input();
    $params['user_ids'] = $user_ids;

    return $this->performRequest($request, 'get', 'referrals', $params, 3);
  }

  public function createReferralLink($request)
  {
    return $this->performRequest($request, 'post', 'referral-link', $request->input())['data'];
  }

  public function getReferralByToken($request, $token)
  {
    return $this->performRequest($request, 'get', 'referral/' . $token, [], 3);
  }

  public function createReferral($request, $data)
  {
    $params = $request->input();
    $params['user_id'] = $data['user_id'];
    $params['bonuses'] = $data['bonuses'];
    
    return $this->performRequest($request, 'post', 'referral/create', $params);
  }
}
