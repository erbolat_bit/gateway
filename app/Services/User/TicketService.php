<?php

namespace App\Services\User;

use App\Models\Admin;
use App\Models\User;
use App\Traits\Client;

class TicketService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.user.base_url');
    $this->service_token = config('api_services.user.token');
  }

  /**
   * @param mixed $request
   * 
   * @return array
   */
  public function getUserTickets($request)
  {
    $tickets = $this->performRequest($request, 'get', 'ticket/list', $request->input(), 3);
    $tickets_collection = collect($tickets['data']);
    $user_ids = $tickets_collection->unique('message.user_id')->pluck('message.user_id')->toArray();
    $users = $this->getUsers($user_ids);
    $tickets_collection->transform(function($ticket) use($users){
      $ticket['user'] = $users[$ticket['message']['user_id']];
      return $ticket;
    });

    return $tickets_collection->toArray();
  }

  /**
   * @param mixed $request
   * @param mixed $ticket_id
   * 
   * @return array
   */
  public function getTicketMessages($request, $ticket_id)
  {
    $messages = $this->performRequest($request, 'get', 'ticket/message/list/' . $ticket_id, $request->input(), 3);
    if (!isset($messages['error'])) {
      $user_ids = collect($messages['data'])->unique('user_id')->pluck('user_id')->toArray();
      $users = $this->getUsers($user_ids);
      
      foreach ($messages['data'] as $key => $message) {
        $messages['data'][$key]['user'] = isset($users[$message['user_id']]) ? $users[$message['user_id']] : null;
      }
    }
    return $messages;
  }


  /**
   * @param mixed $request
   * 
   * @return array
   */
  public function createMessage($request)
  {
    $params = $request->input();
    $type = 1;
    
    if (isset($params['department_id']) && $params['department_id']) {
      $user_ids = Admin::select('id')->where('department_id', $params['department_id'])->get()->pluck('id')->toArray();
      $params['user_ids'] = $user_ids;
    }

    if ($request->file('file')) {
			$files = [
				[
					'name' => 'file',
					'contents' => fopen($request->file('file')->path(), 'r')
				]
			];
			$params = $this->multipartData($params, $files);
			$type = 2;
		}

    $message = $this->performRequest($request, 'post', 'ticket/message/create', $params, $type);

    if(!isset($message['error'])){
      $message['user'] = User::select(['path', 'name', 'id'])->where('id', $message['user_id'])->get()->toArray();
    }
    return $message;
  }

  /**
   * @param mixed $request
   * 
   * @return array
   */
  public function messageUpdate($request)
  {
    return $this->performRequest($request, 'put', 'ticket/message/read', $request->input());
  }

  /**
   * @param mixed $request
   * @param mixed $ticket_id
   * 
   * @return array
   */
  public function searchMessages($request, $ticket_id)
  {
    $messages = $this->performRequest($request, 'get', 'ticket/message/search/' . $ticket_id, $request->input(), 3);
    
    if (!isset($messages['error'])) {
      $user_ids = collect($messages)->unique('user_id')->pluck('user_id')->toArray();
      $users = User::select(['path', 'name', 'id'])->whereIn('id', $user_ids)->get()->keyBy('id')->toArray();
      
      foreach ($messages as $key => $message) {
        $messages[$key]['user'] = isset($users[$message['user_id']]) ? $users[$message['user_id']] : null;
      }
    }

    return $messages;
  }

  public function categoryList($request)
  {
    $categories = $this->performRequest($request, 'get', 'ticket/category/list', $request->input(), 3);
    
    return $categories;
  }

  private function getUsers(array $user_ids)
  {
    $users = User::withoutGlobalScopes()->select(['path', 'name', 'id'])->whereIn('id', $user_ids)->get()->keyBy('id')->toArray();
    return $users;
  }
}
