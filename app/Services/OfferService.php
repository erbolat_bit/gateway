<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class OfferService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.lot.base_url');
    $this->service_token = config('api_services.lot.token');
  }

  /**
   * getLotOffers
   *
   * @param  mixed $request
   * @param  mixed $lot
   * @return void
   */
  public function getLotOffers($request, $lot)
  {
    $result = $this->performRequest($request, 'get', 'offer/by-lot/' . $lot, $request->input(), 3);
    $offers = collect($result);
    $users = collect(UserResource::collection(User::whereIn('id', $offers->unique('user_id')->pluck('user_id')->toArray())->get())->toArray($request));

    $offers->transform(function ($item) use ($users) {
      $item['user'] = $users->where('id', $item['user_id'])->first();
      return $item;
    });

    return $offers->toArray();
  }

  /**
   * getUserOffers
   *
   * @param  mixed $request
   * @return void
   */
  public function getUserOffers($request)
  {
    return $this->performRequest($request, 'get', 'offer/by-user', $request->input(), 3);
  }

  /**
   * store
   *
   * @param  mixed $request
   * @return void
   */
  public function store($request)
  {
    $user = $request->user();
    $params = $request->input();
    $params['user'] = ['id' => $user->id, 'path' => $user->path, 'name' => $user->name, 'surname' => $user->surname];
    return $this->performRequest($request, 'post', 'offer/store', $params);
  }
}
