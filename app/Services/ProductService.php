<?php

namespace App\Services;

use App\Traits\Client;

class ProductService
{

  use Client;

  public $base_url;
  public $service_token;
  private $shopService;
  private $adsService;
  private $lotService;

  public function __construct(ShopService $shopService, AdsService $adsService, LotService $lotService)
  {
    $this->base_url = config('api_services.shop.base_url');
    $this->service_token = config('api_services.shop.token');

    $this->shopService = $shopService;
    $this->adsService = $adsService;
    $this->lotService = $lotService;
  }

  /**
   * createProduct
   *
   * @param  mixed $request
   * @param  mixed $shop
   * @return void
   */
  public function createProduct($request, $shop)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('photos')) {
      foreach ($request->file('photos') as $key => $value) {
        $files[] = [
          'name' => 'photos[' . $key . '][photo]',
          'contents' => fopen($request->file('photos.' . $key . '.photo')->path(), 'r')
        ];
      }

      $params = $this->multipartData($request->input(), $files);
      $type = 2;
    }

    $result = $this->performRequest($request, 'post', 'product/store/' . $shop, $params, $type);
    return $result;
  }

  /**
   * updateProduct
   *
   * @param  mixed $request
   * @param  mixed $product
   * @return void
   */
  public function updateProduct($request, $product)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('photos')) {
      foreach ($request->file('photos') as $key => $value) {
        $files[] = [
          'name' => 'photos[' . $key . '][photo]',
          'contents' => fopen($request->file('photos.' . $key . '.photo')->path(), 'r')
        ];
      }

      $params = $this->multipartData($request->input(), $files);
      $type = 2;
    }

    $result = $this->performRequest($request, 'post', 'product/update/' . $product, $params, $type);
    return $result;
  }

  /**
   * basket
   *
   * @param  mixed $request
   * @return void
   */
  public function userBasketProducts($request, $product_ids = [])
  {
    $params = $request->input();
    $params['products'] = $product_ids;
    $result = $this->performRequest($request, 'get', 'product/basket', $params, 3);
    return $result;
  }

  /**
   * wishlist
   *
   * @param  mixed $request
   * @return void
   */
  public function wishlist($request)
  {
    $result = $this->performRequest($request, 'get', 'product/wishlist', $request->input(), 3);
    return $result;
  }

  /**
   * deleteProduct
   *
   * @param  mixed $request
   * @param  mixed $product
   * @return void
   */
  public function deleteProduct($request, $product)
  {
    $result = $this->performRequest($request, 'delete', 'product/' . $product);
    return $result;
  }

  /**
   * deleteProductPhoto
   *
   * @param  mixed $request
   * @param  mixed $photo
   * @return void
   */
  public function deleteProductPhoto($request, $photo)
  {
    $result = $this->performRequest($request, 'delete', 'product/photo/' . $photo);
    return $result;
  }

  /**
   * productCreateDoc
   *
   * @param  mixed $request
   * @param  mixed $product
   * @return void
   */
  public function productCreateDoc($request, $product)
  {
    if ($request->file('document')) {
      $files = [
        [
          'name' => 'document',
          'contents' => fopen($request->file('document')->path(), 'r')
        ]
      ];
    }

    $params = $this->multipartData($request->input(), $files);
    $result = $this->performRequest($request, 'post', 'product/document/' . $product, $params, 2);
    return $result;
  }

  /**
   * productDeleteDoc
   *
   * @param  mixed $request
   * @param  mixed $document
   * @return void
   */
  public function productDeleteDoc($request, $document)
  {
    $result = $this->performRequest($request, 'delete', 'product/document/' . $document);
    return $result;
  }

  /**
   * getProductsByhop
   *
   * @param  mixed $request
   * @param  mixed $store
   * @return void
   */
  public function getProductsByhop($request, $store)
  {
    $result = $this->performRequest($request, 'get', 'product/by-shop/' . $store, $request->input(), 3);
    return $result;
  }

  /**
   * getProductById
   *
   * @param  mixed $request
   * @param  mixed $product
   * @return void
   */
  public function getProductById($request, $product)
  {
    $result = $this->performRequest($request, 'get', 'product/show/' . $product, $request->input(), 3);
    return $result;
  }

  /**
   * getProductList
   *
   * @param  mixed $request
   * @return void
   */
  public function getProductList($request)
  {
    $result = $this->performRequest($request, 'get', 'product/all', $request->input(), 3);
    return $result;
  }

  /**
   * getProductListByShop
   *
   * @param  mixed $request
   * @param  mixed $store
   * @return void
   */
  public function getAllProductsByShop($request, $store)
  {
    $result = $this->performRequest($request, 'get', 'product/all-by-shop/' . $store, $request->input(), 3);
    return $result;
  }

  /**
   * setProductViewCounter
   *
   * @param  mixed $request
   * @param  mixed $product
   * @return void
   */
  public function setProductViewCounter($request, $product)
  {
    if ($request->has('user_id')) {
      $shop_has = (bool)$this->shopService->userHasShop($request, $request->user_id) ? 1 : 0;
      $ad_has = (bool)$this->adsService->userHasAd($request, $request->user_id) ? 1 : 0;
      $lot_has = (bool)$this->lotService->userHasLot($request, $request->user_id) ? 1 : 0;
      $request->merge([
        'has_shop' => (bool)$shop_has,  
        'has_ad' => (bool)$ad_has,  
        'has_lot' => (bool)$lot_has,    
      ]);
    }
    $result = $this->performRequest($request, 'post', 'product/views/' . $product, $request->input());
    return $result;
  }
}
