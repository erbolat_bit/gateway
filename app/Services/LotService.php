<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class LotService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.lot.base_url');
    $this->service_token = config('api_services.lot.token');
  }

  /**
   * getAllLot
   *
   * @param  mixed $request
   * @return void
   */
  public function getAllLot($request)
  {
    return $this->performRequest($request, 'get', 'lot/all', $request->input(), 3);
  }

  /**
   * getLotById
   *
   * @param  mixed $request
   * @param  mixed $lot
   * @return void
   */
  public function getLotById($request, $lot)
  {
    $result = $this->performRequest($request, 'get', 'lot/show/' . $lot, $request->input(), 3);
    $result['user'] = new UserResource(User::find($result['user_id']));

    return $result;
  }

  /**
   * getUserLots
   *
   * @param  mixed $request
   * @return void
   */
  public function getUserLots($request, $user_id = false)
  {
    $input = $request->input();
    if ($user_id) {
      $input['user_id'] = $user_id;
      $input['per_page'] = 3;
    }
    return $this->performRequest($request, 'get', 'lot/by-user', $input, 3);
  }

  /**
   * createLot
   *
   * @param  mixed $request
   * @return void
   */
  public function createLot($request)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('photos')) {
      $files = [];
      foreach ($request->file('photos') as $key => $value) {
        $files[] = [
          'name' => 'photos[' . $key . '][photo]',
          'contents' => fopen($request->file('photos.' . $key . '.photo')->path(), 'r')
        ];
      }
      $params = $this->multipartData($params, $files);
      $type = 2;
    }
    $result = $this->performRequest($request, 'post', 'lot/store', $params, $type);
    $result['user'] = new UserResource($request->user());
    return $result;
  }

  /**
   * updateLot
   *
   * @param  mixed $request
   * @param  mixed $lot
   * @return void
   */
  public function updateLot($request, $lot)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('photos')) {
      $files = [];
      foreach ($request->file('photos') as $key => $value) {
        $files[] = [
          'name' => 'photos[' . $key . '][photo]',
          'contents' => fopen($request->file('photos.' . $key . '.photo')->path(), 'r')
        ];
      }
      $params = $this->multipartData($params, $files);
      $type = 2;
    }
    $result = $this->performRequest($request, 'post', 'lot/update/' . $lot, $params, $type);
    $result['user'] = new UserResource($request->user());
    return $result;
  }

  /**
   * deleteLot
   *
   * @param  mixed $request
   * @param  mixed $lot
   * @return void
   */
  public function deleteLot($request, $lot)
  {
    return $this->performRequest($request, 'delete', 'lot/' . $lot);
  }

  /**
   * deleteLotPhoto
   *
   * @param  mixed $request
   * @param  mixed $photo
   * @return void
   */
  public function deleteLotPhoto($request, $photo)
  {
    return $this->performRequest($request, 'delete', 'lot/photo/' . $photo);
  }

  /**
   * uploadLotDoc
   *
   * @param  mixed $request
   * @param  mixed $lot
   * @return void
   */
  public function uploadLotDoc($request, $lot)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('document')) {
      $files = [
        [
          'name' => 'document',
          'contents' => fopen($request->file('document')->path(), 'r')
        ]
      ];
      $params = $this->multipartData($params, $files);
      $type = 2;
    }
    return $this->performRequest($request, 'post', 'lot/document/' . $lot, $params, $type);
  }

  /**
   * deleteLotDoc
   *
   * @param  mixed $request
   * @param  mixed $document
   * @return void
   */
  public function deleteLotDoc($request, $document)
  {
    return $this->performRequest($request, 'delete', 'lot/document/' . $document);
  }

  public function userHasLot($request, $user_id)
  {
    return $this->performRequest($request, 'get', 'lot/has/' . $user_id, $request->input(), 3);
  }
}
