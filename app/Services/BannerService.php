<?php


namespace App\Services;

use App\Repositories\BannerRepository;

class BannerService
{

    private $repo;

    public function __construct(BannerRepository $bannerRepository)
    {
        $this->repo = $bannerRepository;
    }
    
    public function getBanners()
    {
        $data = [];
        $data['slide'] = $this->repo->getBannersForFront(1, 10)->toArray();
        $banner = $this->repo->getBannersForFront(2, 1)->toArray();
        $data['banner'] = array_shift($banner);
        return $data;
    }
}