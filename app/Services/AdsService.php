<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class AdsService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.ads.base_url');
    $this->service_token = config('api_services.ads.token');
  }

  public function getUserAds($request, $user_id = false)
  {
    $input = $request->input();
    if ($user_id) {
      $input['user_id'] = $user_id;
      $input['per_page'] = 3;
      return $this->performRequest($request, 'get', 'ads/by-user', $input, 3)['ads'];
    } else {
      return $this->performRequest($request, 'get', 'ads/by-user', $input, 3);
    }
  }

  public function createAds($request)
  {

    $params = $request->input();
    $type = 1;

    if ($request->file('photos')) {
      $files = [];
      foreach ($request->file('photos') as $key => $value) {
        $files[] = [
          'name' => 'photos[' . $key . '][photo]',
          'contents' => fopen($request->file('photos.' . $key . '.photo')->path(), 'r')
        ];
      }
      $params = $this->multipartData($params, $files);
      $type = 2;
    }
    return $this->performRequest($request, 'post', 'ads/store', $params, $type);
  }

  public function updateAds($request, $ad)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('photos')) {
      $files = [];
      foreach ($request->file('photos') as $key => $value) {
        $files[] = [
          'name' => 'photos[' . $key . '][photo]',
          'contents' => fopen($request->file('photos.' . $key . '.photo')->path(), 'r')
        ];
      }

      $params = $this->multipartData($params, $files);
      $type = 2;
    }

    return $this->performRequest($request, 'post', 'ads/update/' . $ad, $params, $type);
  }

  public function deleteAds($request, $ad)
  {
    return $this->performRequest($request, 'delete', 'ads/' . $ad);
  }

  public function deleteAdsPhoto($request, $photo)
  {
    return $this->performRequest($request, 'delete', 'ads/photo/' . $photo);
  }

  public function uploadAdsDoc($request, $ad)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('document')) {
      $files = [
        [
          'name' => 'document',
          'contents' => fopen($request->file('document')->path(), 'r')
        ]
      ];
      $params = $this->multipartData($params, $files);
      $type = 2;
    }
    return $this->performRequest($request, 'post', 'ads/document/' . $request->ad, $params, $type);
  }

  public function deleteAdsDoc($request, $document)
  {
    return $this->performRequest($request, 'delete', 'ads/document/' . $document);
  }

  public function getAllAds($request)
  {
    return $this->performRequest($request, 'get', 'ads/all', $request->input(), 3);
  }

  public function getAdsById($request, $ad)
  {
    $result = $this->performRequest($request, 'get', 'ads/show/' . $ad, $request->input(), 3);
    $result['user'] = new UserResource(User::find($result['user_id']));
    return $result;
  }

  public function userHasAd($request, $user_id)
  {
    return $this->performRequest($request, 'get', 'ads/has/' . $user_id, $request->input(), 3);
  }
}
