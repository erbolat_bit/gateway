<?php

namespace App\Services\Blog;

use App\Traits\Client;

class PostService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.blog.base_url');
    $this->service_token = config('api_services.blog.token');
  }

  public function postList($request)
  {
    return $this->performRequest($request, 'get', 'posts/list', $request->input(), 3);
  }

  public function postSearch($request)
  {
    return $this->performRequest($request, 'get', 'posts/search', $request->input(), 3);
  }

  public function getPostByAlias($request, $alias)
  {
    return $this->performRequest($request, 'get', 'posts/view/' . $alias, $request->input(), 3);
  }
}
