<?php

namespace App\Services\Blog;

use App\Traits\Client;

class CategoryService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.blog.base_url');
    $this->service_token = config('api_services.blog.token');
  }

  public function categoryList($request)
  {
    $categories = $this->performRequest($request, 'get', 'categories/list', $request->input(), 3);
    if (isset($categories['error']) && $categories['error']) {
      return $categories;
    }else{
      return $categories;
    }
  }
}
