<?php

namespace App\Services\Blog;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class BlogService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.blog.base_url');
    $this->service_token = config('api_services.blog.token');
  }

  public function getPosts($request)
  {
    return $this->performRequest($request, 'get', 'posts/list', $request->input(), 3);
  }
}
