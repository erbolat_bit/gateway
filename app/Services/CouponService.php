<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class CouponService
{
	use Client;

	public $base_url;
	public $service_token;

	public function __construct()
	{
		$this->base_url = config('api_services.coupon.base_url');
		$this->service_token = config('api_services.coupon.token');
	}

	public function getUserCoupons($request)
	{
		return $this->performRequest($request, 'get', 'by-user', $request->input(), 3);
	}

	public function getShopCoupons($request, $store)
	{
		return $this->performRequest($request, 'get', 'by-shop/' . $store, $request->input(), 3);
	}

	public function getShopArchiveCoupons($request, $store)
	{
		return $this->performRequest($request, 'get', 'by-shop/archive/' . $store, $request->input(), 3);
	}

	public function getProductCoupons($request, $product)
	{
		return $this->performRequest($request, 'get', 'by-product/' . $product, $request->input(), 3);
	}

	public function createCoupon($request, $users)
	{
		$params = $request->input();
		$params['users'] = $users;
		$type = 1;

		if ($request->file('image')) {
			$files = [
				[
					'name' => 'image',
					'contents' => fopen($request->file('image')->path(), 'r')
				]
			];
			$params = $this->multipartData($params, $files);
			$type = 2;
		}
		return $this->performRequest($request, 'post', 'store', $params, $type);
	}

	public function getCouponById($request, $coupon)
	{
		return $this->performRequest($request, 'get', 'show/' . $coupon, $request->input(), 3);
	}

	public function deleteCoupon($request, $coupon)
	{
		return $this->performRequest($request, 'delete', 'delete/' . $coupon);
	}

	public function deleteCouponItem($request, $item)
	{
		return $this->performRequest($request, 'delete', 'delete-item/' . $item);
	}

	public function giveCoupon($request, $item)
	{
		$params = $request->input();
		$user = User::where('email', $params['user_param'])->orWhere('nickname', $params['user_param'])->first(); // после добаеления nickname поле в базе иползовать
		
		if (!$user) {
			return false;
		}
		// $user = new UserResource(User::where('email', $params['user_param'])->first());
		$params['user'] = $user->toArray($request);

		return $this->performRequest($request, 'post', 'give/' . $item, $params);
	}
}
