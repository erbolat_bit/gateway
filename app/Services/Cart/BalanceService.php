<?php

namespace App\Services\Cart;

use App\Traits\Client;

class BalanceService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.cart.base_url');
    $this->service_token = config('api_services.cart.token');
  }

  public function balanceReplenishment($request)
  {
    return $this->performRequest($request, 'post', 'balance/store', $request->input());
  }

  public function getBalance($request)
  {
    return $this->performRequest($request, 'post', 'balance/store', $request->input());
  }

}
