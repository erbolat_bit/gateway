<?php

namespace App\Services\Cart;

use App\Traits\Client;

class BasketService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.cart.base_url');
    $this->service_token = config('api_services.cart.token');
  }

  public function getUserBasket($request)
  {
    return $this->performRequest($request, 'get', 'basket', $request->input(), 3);
  }

  public function createBasket($request)
  {
    return $this->performRequest($request, 'post', 'basket/store', $request->input());
  }

  public function deleteBasket($request, $basket)
  {
    return $this->performRequest($request, 'delete', 'basket/' . $basket);
  }
}
