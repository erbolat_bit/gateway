<?php

namespace App\Services\Cart;

use App\Models\User;
use App\Services\ShopService;
use App\Services\User\ChatService;
use App\Traits\Client;
use App\Traits\UpdateUsersBalance;
use DB;
use Exception;

class OrderService
{
  use Client, UpdateUsersBalance;

  public $base_url;
  public $service_token;
  public $shopService;
  public $chatService;

  public function __construct(ShopService $shopService, ChatService $chatService)
  {
    $this->base_url = config('api_services.cart.base_url');
    $this->service_token = config('api_services.cart.token');
    $this->shopService = $shopService;
    $this->chatService = $chatService;
  }

  public function getUserOrders($request)
  {
    return $this->performRequest($request, 'get', 'order', $request->input(), 3)['data'];
  }

  public function getUserTransactions($request)
  {
    return $this->performRequest($request, 'get', 'transactions', $request->input(), 3);
  }

  public function createOrder($request)
  {
    $user = $request->user();
    if ($request->has('total_sum') && $request->payment == 3) {
      if ($user->balance < $request->total_sum) {
        return [
          'error' => true,
          'message' => 'У вас недостаточно средств на балансе',
          'status_code' => 500
        ];
      }
    }

    $data = $this->performRequest($request, 'post', 'order/store', $request->input());
    if (isset($data['data']['payment_response']['from_balance']) && $data['data']['payment_response']['from_balance']) {
      if ($user->balance < $data['data']['payment_response']['total_sum']) {
        return [
          'error' => true,
          'message' => 'У вас недостаточно средств на балансе',
          'status_code' => 500
        ];
      }
      $amount = $data['data']['payment_response']['total_sum'];
      $user->balance = $user->balance - $amount;
      if ($user->save()) {
        $request->merge(['globalID' => null, 'transactionID' => $data['data']['id']]);
        $this->paymentProcessing($request); 
      }else{
        throw new Exception('error payment from balance');
      }
    }

    return $data;
  }


  public function paymentProcessing($request)
  {

    try {
      $input = $request->input();
      $update_data['status'] = 1;
      $update_data['error_code'] = 0;
      $update_data['transaction_id'] = $input['globalID'];
      $data = $this->updateOrder($request, $input['transactionID'], $update_data);
      $user_ids = $this->shopService->getUserIds($request, collect($data['data']['data'])->pluck('store_id')->toArray());
      $this->updateUserBalanse($user_ids, $data['data']['data']);
  
      $user_ids = collect($user_ids);
      $order = collect($data['data']['order']);
      $user = User::select(['id', 'name', 'path'])->where('id', $order['user_id'])->first()->toArray();
      $user_ids = $user_ids->transform(function($item) use($order, $user){
        $item['recevier_id'] = $item['user_id'];
        $item['user'] = ['id' => $user['id'], 'name' => $user['name'], 'path' => $user['path'] ? $user['path'] : false];
        $item['user_id'] = $order['user_id'];
        $item['order_id'] = $order['id'];
        $item['store_title'] = isset($order['items'][$item['store_id']][0]) ? $order['items'][$item['store_id']][0]['store_title'] : null;
        $item['products'] = $order['items'][$item['store_id']];
        return $item;
      });
  
      foreach ($user_ids as $key => $message) {
        $this->chatService->sendMessageForOrder($request, $message);
      }
      
      return true;
    } catch (\Exception $e) {
      throw $e;
    }

  }


  public function deleteOrder($request, $order)
  {
    return $this->performRequest($request, 'delete', 'order/' . $order);
  }

  public function getOrderById($request, $order)
  {
    return $this->performRequest($request, 'get', 'order/' . $order, $request->input(), 3)['data'];
  }

  public function confirmOrderItem($request, $item)
  {
    return $this->performRequest($request, 'post', 'order-item/confirm/' . $item, $request->input())['data'];
  }

  public function reviewOrderItem($request, $item)
  {
    return $this->performRequest($request, 'post', 'order-item/review/' . $item, $request->input())['data'];
  }

  public function getOrderItem($request, $item)
  {
    return $this->performRequest($request, 'get', 'order-item/' . $item, $request->input(), 3)['data'];
  }

  public function deleteOrderItem($request, $item)
  {
    return $this->performRequest($request, 'delete', 'order-item/' . $request->item);
  }

  public function getStoreUserIds($request, $store_id)
  {
    return $this->performRequest($request, 'get', 'order/store-buyers/' . $store_id, $request->input(), 3);
  }

  public function updateOrder($request, $order_id, $data = [])
  {
    if (empty($data)) {
      $data = $request->input();
    }

    return $this->performRequest($request, 'post', 'order/update/' . $order_id, $data);
  }

  public function checkStatus($request, $order_id)
  {
    return $this->performRequest($request, 'get', 'order/check/' . $order_id, $request->input(), 3);
  }
}
