<?php

namespace App\Services\AddOn;

use App\Traits\Client;

class CategoryService
{
  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.add-on.base_url');
    $this->service_token = config('api_services.add-on.token');
  }

  public function getCategories($request)
  {
    return $this->performRequest($request, 'get', 'categories', $request->input(), 3);
  }

  public function filterCategories($request)
  {
    return $this->performRequest($request, 'get', 'filter-categories', $request->input(), 3);
  }

  public function getChildCategories($request)
  {
    return $this->performRequest($request, 'get', 'child-categories', ['parent_category_id' => $request->input()['parent_category_id']], 3);
  }
}
