<?php

namespace App\Services\AddOn;

use App\Traits\Client;

class ManufacturerService
{
	use Client;

	public $base_url;
	public $service_token;

	public function __construct()
	{
		$this->base_url = config('api_services.add-on.base_url');
		$this->service_token = config('api_services.add-on.token');
	}

	public function getManufacturers($request)
	{
		return $this->performRequest($request, 'get', 'manufacturers', $request->input(), 3);
	}
}
