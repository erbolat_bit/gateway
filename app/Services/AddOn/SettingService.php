<?php

namespace App\Services\AddOn;

use App\Traits\Client;

class SettingService
{
	use Client;

	public $base_url;
	public $service_token;

	public function __construct()
	{
		$this->base_url = config('api_services.add-on.base_url');
		$this->service_token = config('api_services.add-on.token');
	}

	public function getStoreDeliveries($request)
	{
		return $this->performRequest($request, 'get', 'store-deliveries', $request->input(), 3);
	}
}
