<?php

namespace App\Services;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\Client;

class QuestionService
{

  use Client;

  public $base_url;
  public $service_token;

  public function __construct()
  {
    $this->base_url = config('api_services.review.base_url');
    $this->service_token = config('api_services.review.token');
  }


  public function getQuestionsByProduct($request, $product_id)
  {
    $params = $request->input();

    $result = $this->performRequest($request, 'get', 'questions/' . $product_id, $params, 3);
    $questions = collect($result['data']);

    if ($questions->isNotEmpty()) {
      $users = UserResource::collection(User::whereIn('id', $questions->unique('user_id')->pluck('user_id')->toArray())->get());
      if (!$users->isEmpty()) {
        $questions->transform(function ($review) use ($users) {
          $user = $users->where('id', $review['user_id'])->first();
          $review['user'] = $user ? $user : null;
          return $review;
        });
      }
    }
    $result['data'] = $questions->toArray();
    return $result;
  }

  public function createQuestion($request, $product_id)
  {
    $params = $request->input();
    $result = $this->performRequest($request, 'put', 'questions/' . $product_id, $params);
    if (isset($result['error']) && $result['error']) {
      return $result;
    }
    $result['data']['user'] = new UserResource($request->user());
    return $result['data'];
  }

  public function deleteQuestion($request, $product_id)
  {
    # code...
  }
}
