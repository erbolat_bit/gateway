<?php

namespace App\Services;

use App\Traits\Client;

class ShopService
{

  use Client;

  public $base_url;
  public $service_token;
  private $adsService;
  private $lotService;

  public function __construct()
  {
    $this->base_url = config('api_services.shop.base_url');
    $this->service_token = config('api_services.shop.token');

    $this->adsService = app(AdsService::class);
    $this->lotService = app(LotService::class);
  }

  /**
   * getShops
   *
   * @param  mixed $request
   * @return void
   */
  public function getShops($request, $all = false, $user_id = false)
  {
    if ($all) {
      $shops = $this->performRequest($request, 'get', 'shop', $request->input(), 3);
    } else {
      $input = $request->input();
      if ($user_id) {
        $input['user_id'] = $user_id;
        $input['per_page'] = 3;
      }
      $shops = $this->performRequest($request, 'get', 'shop/user/stores', $input, 3);
    }
    return $shops;
  }

  /**
   * createShop
   *
   * @param  mixed $request
   * @return void
   */
  public function createShop($request)
  {
    $params = $request->input();
    $type = 1;

    if ($request->file('logotype')) {
      $files = [
        [
          'name' => 'logotype',
          'contents' => fopen($request->file('logotype')->path(), 'r')
        ]
      ];
      $params = $this->multipartData($params, $files);
      $type = 2;
    }

    $shop = $this->performRequest($request, 'post', 'shop/store', $params, $type);

    return $shop;
  }

  /**
   * updateShop
   *
   * @param  mixed $request
   * @param  mixed $shop
   * @return void
   */
  public function updateShop($request, $shop)
  {
    $params = $request->input();
    $type = 1;

    $files = [];
    if ($request->file('logotype')) {
      $files = [
        [
          'name' => 'logotype',
          'contents' => fopen($request->file('logotype')->path(), 'r')
        ]
      ];
    }

    if ($request->file('bg_image')) {
      $files[] = [
        'name' => 'bg_image',
        'contents' => fopen($request->file('bg_image')->path(), 'r')
      ];
    }

    if ($request->file('logotype') || $request->file('bg_image')) {
      $params = $this->multipartData($params, $files);
      $type = 2;
    }

    $shop = $this->performRequest($request, 'post', 'shop/' . $shop, $params, $type);

    return $shop;
  }

  /**
   * deleteShop
   *
   * @param  mixed $request
   * @param  mixed $shop
   * @return void
   */
  public function deleteShop($request, $shop)
  {
    $shop = $this->performRequest($request, 'delete', 'shop/' . $shop);
    return $shop;
  }

  /**
   * deleteShopBgImage
   *
   * @param  mixed $request
   * @param  mixed $shop
   * @return void
   */
  public function deleteShopBgImage($request, $shop)
  {
    $shop = $this->performRequest($request, 'delete', 'shop/bg-image/' . $request->store);
    return $shop;
  }


  /**
   * getShopById
   *
   * @param  mixed $request
   * @param  mixed $shop
   * @return void
   */
  public function getShopById($request, $shop)
  {
    $shop = $this->performRequest($request, 'get', 'shop/' . $shop, $request->input(), 3);
    return $shop;
  }

  /**
   * getShopByDomain
   *
   * @param  mixed $request
   * @param  mixed $domain
   * @return void
   */
  public function getShopByDomain($request, $domain)
  {
    $shop = $this->performRequest($request, 'get', 'shop/store-by-domain/' . $domain, $request->input(), 3);
    return $shop;
  }

  /**
   * counterShopView
   *
   * @param  mixed $request
   * @param  mixed $domain
   * @return void
   */
  public function counterShopView($request, $store)
  {
    if ($request->has('user_id')) {
      $shop_has = $this->userHasShop($request, $request->user_id) ? 1 : 0;
      $ad_has = $this->adsService->userHasAd($request, $request->user_id) ? 1 : 0;
      $lot_has = $this->lotService->userHasLot($request, $request->user_id) ? 1 : 0;
      $request->merge([
        'has_shop' => (bool)$shop_has,  
        'has_ad' => (bool)$ad_has,  
        'has_lot' => (bool)$lot_has,  
      ]);
    }
    $shop = $this->performRequest($request, 'post', 'shop/views/' . $store, $request->input());
    return $shop;
  }

  public function getMainData($request)
  {
    $shop = $this->performRequest($request, 'get', 'main-data', $request->input(), 3)['data'];
    return $shop;
  }

  public function getUserIds($request, $store_ids)
  {
    return $this->performRequest($request, 'get', 'shop/user/ids', ['store_ids' => $store_ids], 3);
  }

  public function userHasShop($request, $user_id)
  {
    return $this->performRequest($request, 'get', 'shop/has/' . $user_id, $request->input(), 3);
  }
}
