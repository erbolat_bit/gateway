<?php

namespace App\Repositories;

use App\Models\Rating;

class RatingRepository extends BaseRepository
{

  protected function getModel(): string
  {
    return Rating::class;
  }

  public function create(array $data)
  {
    $rate = $this->init()->create($data);

    return $rate->toArray();
  }

  public function getUserRating(int $user_id)
  {

    $rating = $this->init()->where('user_id', $user_id)->avg('value');

    return round($rating, 1);
  }
}
