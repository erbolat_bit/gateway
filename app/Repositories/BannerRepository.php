<?php

namespace App\Repositories;

use App\Models\Banner;

class BannerRepository extends BaseRepository
{

    protected function getModel(): string
    {
        return Banner::class;
    }

    public function getBannersForFront(int $type=1, int $take=10)
    {
        $list = $this->init()
                    ->inRandomOrder()
                    ->where('type', $type)
                    ->take($take)
                    ->get();
        return $list;
    }

    public function getBannerList()
    {
        $list = $this->init()->paginate();
        return $list;
    }

    public function create(array $data)
    {
        $model = $this->init()->create($data);
        return $model;
    }

    public function update(int $id, array $data)
    {
        $model = $this->init()->where('id', $id)->first();
        $model = $model->update($data);
        return $model;
    }

    public function delete(int $id)
    {
        $model = $this->init()->where('id', $id)->delete();
        return $model;
    }

    public function getOne(int $id)
    {
        return $this->init()
                    ->where('id', $id)
                    ->first();
    }

    public function getRandomOne(int $id)
    {
        return $this->init()
                    ->where('id', $id)
                    ->first();
    }
}
