<?php

namespace App\Repositories;

use App\AdminServices\Message\SystemMessageService;
use App\Models\SystemMessage;
use App\Models\SystemMessagesId;
use App\Models\User;

class SystemMessageRepository extends BaseRepository
{

  private $messageService;

  public function __construct(SystemMessageService $systemMessageService)
  {
    $this->messageService = $systemMessageService;

    parent::__construct();
  }

  protected function getModel(): string
  {
    return SystemMessage::class;
  }

  public function getMessageById(int $id)
  {
    $result = $this->init()->with('userIds')->whereId($id)->first();
    return $result;
  }

  public function getMessageList()
  {
    $result = $this->init()->paginate();
    return $result;
  }

  public function deleteMessage(int $id)
  {
    $result = $this->init()->delete($id);
    return $result;
  }

  public function createMessage(array $data)
  {
    $message = $this->init()->create($data);
    $userIds = $this->getUserIds($data['filter_params']);
    $message_user_ids = [];
    foreach ($userIds as $key => $value) {
      $message_user_ids[] = new SystemMessagesId(['user_id' => $value['id']]);
    }
    $message->userIds()->saveMany($message_user_ids);

    return $message;
  }

  private function getUserIds(array $data): array
  {
    $result = $this->messageService->getFilterData($data);
    return $result;
  }

  public function getFilterParamsForCreate(): array
  {
    $result = [];
    $result['letters'] = User::letterList();

    return $result;
  }
}
