<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{

  protected $model;

  public function __construct()
  {
    $this->model = app($this->getModel());
  }

  abstract protected function getModel(): string; 

  protected function init(): Model
  {
    return clone $this->model;
  }
}
