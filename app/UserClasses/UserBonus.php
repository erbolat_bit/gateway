<?php

namespace App\UserClasses;

use App\Interfaces\User\BonusInterface;

class UserBonus implements BonusInterface
{
  
  private array $data;

  public function setName(string $value)
  {
    $this->data['name'] = $value;
  }

  public function setBonus(int $value)
  {
    $this->data['bonus'] = $value;
  }

  public function setType(string $value){
    $this->data['type'] = $value;
  }
  
  public function setTypeId(int $value){
    $this->data['type_id'] = $value;
  }

  public function getData(): array{
    return $this->data;
  }

}
