<?php

namespace App\Observers;

use App\Jobs\SystemMessageJob;
use App\Models\Admin;
use App\Models\SystemMessage;
use App\Models\SystemMessagesId;
use App\Models\User;

class SystemMessageObserver
{
    /**
     * Handle the system message "created" event.
     *
     * @param  \App\Models\SystemMessage  $systemMessage
     * @return void
     */
    public function created(SystemMessage $systemMessage)
    {
        $userIds = SystemMessagesId::where('system_message_id', 1)->get()->pluck('user_id')->toArray();
        $users = Admin::withoutGlobalScopes()->whereIn('id', $userIds)->get();
        foreach ($users as $key => $user) {
            SystemMessageJob::dispatch($user, $systemMessage);
        }
    }

    /**
     * Handle the system message "updated" event.
     *
     * @param  \App\Models\SystemMessage  $systemMessage
     * @return void
     */
    public function updated(SystemMessage $systemMessage)
    {
        //
    }

    /**
     * Handle the system message "deleted" event.
     *
     * @param  \App\Models\SystemMessage  $systemMessage
     * @return void
     */
    public function deleted(SystemMessage $systemMessage)
    {
        //
    }

    /**
     * Handle the system message "restored" event.
     *
     * @param  \App\Models\SystemMessage  $systemMessage
     * @return void
     */
    public function restored(SystemMessage $systemMessage)
    {
        //
    }

    /**
     * Handle the system message "force deleted" event.
     *
     * @param  \App\Models\SystemMessage  $systemMessage
     * @return void
     */
    public function forceDeleted(SystemMessage $systemMessage)
    {
        //
    }
}
