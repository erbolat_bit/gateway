<?php

namespace App\Observers;

use App\Models\SystemMessagesId;

class SystemMessagesIdObserver
{
    /**
     * Handle the system messages id "created" event.
     *
     * @param  \App\Models\SystemMessagesId  $systemMessagesId
     * @return void
     */
    public function created(SystemMessagesId $systemMessagesId)
    {
        info($systemMessagesId);
    }

    /**
     * Handle the system messages id "updated" event.
     *
     * @param  \App\Models\SystemMessagesId  $systemMessagesId
     * @return void
     */
    public function updated(SystemMessagesId $systemMessagesId)
    {
        //
    }

    /**
     * Handle the system messages id "deleted" event.
     *
     * @param  \App\Models\SystemMessagesId  $systemMessagesId
     * @return void
     */
    public function deleted(SystemMessagesId $systemMessagesId)
    {
        //
    }

    /**
     * Handle the system messages id "restored" event.
     *
     * @param  \App\Models\SystemMessagesId  $systemMessagesId
     * @return void
     */
    public function restored(SystemMessagesId $systemMessagesId)
    {
        //
    }

    /**
     * Handle the system messages id "force deleted" event.
     *
     * @param  \App\Models\SystemMessagesId  $systemMessagesId
     * @return void
     */
    public function forceDeleted(SystemMessagesId $systemMessagesId)
    {
        //
    }
}
