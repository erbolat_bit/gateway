<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{

	private $with_token;
	private $data;

	public function __construct($resource, bool $with_token = false, $data=false)
	{
		$this->with_token = $with_token;
		$this->data = $data;
		parent::__construct($resource);
	}

	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */
	public function toArray($request)
	{
		$data = [
			'id' => $this->id,
			'name' => $this->name,
			'surname' => $this->surname,
			'email' => $this->email,
			'path' => $this->path,
			'birthdate' => $this->birthdate,
			'phone' => $this->phone,
			'sex' => $this->sex,
			'nickname' => $this->nickname,
			'parent_id' => $this->parent_id,
			'bonuses' => $this->bonuses,
			'balance' => $this->balance,
			'created_at' => $this->created_at->format('d-m-Y'),
		];

		if ($this->with_token) {
			$data['access_token'] = $this->createToken('UzexpressApp', ['user'])->accessToken;
		}
		if ($this->data) {
			$data['documents'] = isset($this->data['data']['documents']) ? $this->data['data']['documents'] : [];
			$data['addresses'] = isset($this->data['data']['addresses']) ? $this->data['data']['addresses'] : [];
		}

		return $data;
	}
}
