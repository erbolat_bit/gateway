<?php

namespace App\Http\Controllers\AddOn;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\ManufacturerService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ManufacturerController extends BaseController
{
	use ResponseTrait;

	public $manufacturerService;

	public function __construct(ManufacturerService $manufacturerService)
	{
		$this->manufacturerService = $manufacturerService;
	}
	/**
	 * @group Add-On
	 * 
	 * 
	 * @response [
   *     {
   *         "id": 1,
   *         "title": "Sumsung",
   *         "logotype": null
   *     },
   *     {
   *         "id": 2,
   *         "title": "LG",
   *         "logotype": null
   *     },
   *     {
   *         "id": 3,
   *         "title": "Lenova",
   *         "logotype": null
   *     }
   * ] 
	 */
	public function index(Request $request)
	{
    $manufacturers = Cache::rememberForever(config('cache.storage_names.manufactures'), function() use($request) {
      return $this->manufacturerService->getManufacturers($request);
    });
		return $this->successResponse($manufacturers);
	}
}
