<?php

namespace App\Http\Controllers\AddOn;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\CategoryService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoryController extends BaseController
{
	use ResponseTrait;

	public $categoryService;

	public function __construct(CategoryService $categoryService)
	{
		$this->categoryService = $categoryService;
	}

	/**
	 * @group Add-On
	 * 
	 * 
	 * @response [
   *     {
   *         "id": 1,
   *         "title": null,
   *         "slug": "bytovaya-texnika",
   *         "parent_id": 0,
   *         "image": null,
   *         "children": [
   *             {
   *                 "id": 4,
   *                 "title": null,
   *                 "slug": "mikrovolnovki",
   *                 "parent_id": 1,
   *                 "image": null,
   *                 "children": []
   *             },
   *             {
   *                 "id": 5,
   *                 "title": null,
   *                 "slug": "feny",
   *                 "parent_id": 1,
   *                 "image": null,
   *                 "children": []
   *             }
   *         ]
   *     }
   * ]
	 */
	public function index(Request $request)
	{
    $categories = Cache::rememberForever(config('cache.storage_names.categories') . $request->header('Lang'), function() use($request){
      return $this->categoryService->getCategories($request);
    });
		return $this->successResponse($categories);
	}

	/**
	 * @group Add-On
	 * 
	 * 
	 * @response [
   *     {
   *         "id": 4,
   *         "title": null,
   *         "slug": "mikrovolnovki",
   *         "parent_id": 1,
   *         "image": null,
   *         "children": []
   *     },
   *     {
   *         "id": 5,
   *         "title": null,
   *         "slug": "feny",
   *         "parent_id": 1,
   *         "image": null,
   *         "children": []
   *     }
   * ]
	 */
	public function filterCategories(Request $request)
	{
		return $this->successResponse($this->categoryService->filterCategories($request));
	}
}
