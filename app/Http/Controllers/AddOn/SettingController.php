<?php

namespace App\Http\Controllers\AddOn;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\SettingService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class SettingController extends BaseController
{
    use ResponseTrait;

	public $settingService;

	public function __construct(SettingService $settingService)
	{
		$this->settingService = $settingService;
	}

	/**
	 * @group Add-On
	 * 
	 * 
	 * @response [
   *     {
   *         "id": 1,
   *         "title": "Доставка сервисом en",
   *         "amount": "10000.00",
   *         "currency": "UZS",
   *         "selected": false
   *     },
   *     {
   *         "id": 2,
   *         "title": "Самовывоз en",
   *         "amount": "0.00",
   *         "currency": "UZS",
   *         "selected": false
   *     }
   * ]
	 */
	public function index(Request $request)
	{
		return $this->successResponse($this->settingService->getStoreDeliveries($request));
	}
}
