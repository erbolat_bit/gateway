<?php

namespace App\Http\Controllers\AddOn;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\CountryService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CountryController extends BaseController
{
	use ResponseTrait;

	public $countryService;

	public function __construct(CountryService $countryService)
	{
		$this->countryService = $countryService;
	}

	/**
	 * @group Add-On
	 * 
	 * 
	 * @response [
   *     {
   *         "id": 1,
   *         "title": "Abkhazia",
   *         "flag": "ab"
   *     },
   *     {
   *         "id": 2,
   *         "title": "Australia",
   *         "flag": "au"
   *     },
   *     {
   *         "id": 3,
   *         "title": "Austria",
   *         "flag": "at"
   *     }
   * ]
	 */
	public function index(Request $request)
	{
    $countries = Cache::rememberForever(config('cache.storage_names.countries') . $request->header('Lang'), function() use($request){
      return $this->countryService->getCountries($request);
    });
		return $this->successResponse($countries);
	}
}
