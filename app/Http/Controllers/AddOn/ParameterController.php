<?php

namespace App\Http\Controllers\AddOn;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\ParameterService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ParameterController extends BaseController
{

	use ResponseTrait;

	public $parameterService;

	public function __construct(ParameterService $parameterService)
	{
		$this->parameterService = $parameterService;
	}

	/**
	 * @group Add-On
	 * 
	 * 
	 * @response [
   *     {
   *         "id": 1,
   *         "title": "Материал",
   *         "type": "text",
   *         "multiselect": 0,
   *         "options": [
   *             {
   *                 "id": 1,
   *                 "parameter_id": 1,
   *                 "title": "Цвет 1",
   *                 "value": null,
   *                 "type": "text"
   *             }
   *         ]
   *     }
   * ] 
	 */
	public function index(Request $request)
	{
		return $this->successResponse($this->parameterService->getParams($request));
	}
}
