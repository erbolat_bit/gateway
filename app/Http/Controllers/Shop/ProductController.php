<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\BaseController;
use App\Services\ProductService;
use App\Services\ReviewService;
use Illuminate\Http\Request;

class ProductController extends BaseController
{
  
  public $productService;
  public $reviewService;

  public function __construct(ProductService $productService, ReviewService $reviewService)
  {
    $this->productService = $productService;
    $this->reviewService = $reviewService;
  }

  /**
   * @group Product
   * 
   * @urlParam shop integer required Shop ID
   * @bodyParam category_id integer required Category ID
   * @bodyParam title string required
   * @bodyParam price number required
   * @bodyParam discount_price number required
   * @bodyParam currency_id integer required
   * @bodyParam quantity integer nullable
   * @bodyParam quantity_size integer nullable
   * @bodyParam delivery_description string nullable
   * @bodyParam description string nullable
   * @bodyParam country_id integer required
   * @bodyParam manufacturer_id integer required
   * @bodyParam detailed_description string nullable
   * @bodyParam tags.*.title string nullable
   * @bodyParam param_options.*.option_id integer nullable
   * @bodyParam param_options.*.value string nullable
   * @bodyParam param_options.*.parameter_id integer nullable
   * @bodyParam param_options.*.type string nullable
   * @bodyParam brand_id integer nullable
   * @bodyParam brand_id integer nullable
   * @bodyParam photos.*.photo string nullable
   * 
   * @response {
   *  "id": 1,
   *  "category_id": 1,
   *  "title": "фывфыв",
   *  "price": "200000",
   *  "discount_price": "300000",
   *  "price_to_basket": "200000",
   *  "convert_price": "200 000,00 UZS",
   *  "convert_discount_price": "300 000,00 UZS",
   *  "currency": {
   *    "id": "1",
   *    "title": "UZS"
   *  },
   *  "quantity": "2",
   *  "quantity_size": "1",
   *  "delivery_description": "фыв фыв фыв",
   *  "description": "asd asd asd",
   *  "country_id": 1,
   *  "manufacturer_id": 2,
   *  "detailed_description": "asd zxc zxcvxcv",
   *  "views": null,
   *  "param_options": [
   *    {
   *      "id": 1,
   *      "parameter_id": 2,
   *      "value": "null",
   *      "type": "select"
   *    },
   *    {
   *      "id": 2,
   *      "parameter_id": 5,
   *      "value": "asd",
   *      "type": "checkbox"
   *    },
   *  ],
   *  "photos": [
   *    {
   *      "id": 1,
   *      "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *    },
   *    {
   *      "id": 2,
   *      "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *    }
   *  ],
   *  "tags": [
   *    {
   *      "id": 1,
   *      "title": "asdas"
   *    },
   *    {
   *      "id": 2,
   *      "title": "asdas"
   *    }
   *  ],
   *  "documents": []
   * }
   */
  public function store(Request $request, $shop)
  {
    return $this->successResponse($this->productService->createProduct($request, $shop));
  }

  /**
   * @group Product
   * 
   * @urlParam product integer required Product ID
   * @bodyParam category_id integer required Category ID
   * @bodyParam title string required
   * @bodyParam price number required
   * @bodyParam discount_price number required
   * @bodyParam currency_id integer required
   * @bodyParam quantity integer nullable
   * @bodyParam quantity_size integer nullable
   * @bodyParam delivery_description string nullable
   * @bodyParam description string nullable
   * @bodyParam country_id integer required
   * @bodyParam manufacturer_id integer required
   * @bodyParam detailed_description string nullable
   * @bodyParam tags.*.title string nullable
   * @bodyParam param_options.*.option_id integer nullable
   * @bodyParam param_options.*.value string nullable
   * @bodyParam param_options.*.parameter_id integer nullable
   * @bodyParam param_options.*.type string nullable
   * @bodyParam brand_id integer nullable
   * @bodyParam brand_id integer nullable
   * @bodyParam photos.*.photo string nullable
   * 
   * @response {
   *  "id": 1,
   *  "category_id": 1,
   *  "title": "фывфыв",
   *  "price": "200000",
   *  "discount_price": "300000",
   *  "price_to_basket": "200000",
   *  "convert_price": "200 000,00 UZS",
   *  "convert_discount_price": "300 000,00 UZS",
   *  "currency": {
   *    "id": "1",
   *    "title": "UZS"
   *  },
   *  "quantity": "2",
   *  "quantity_size": "1",
   *  "delivery_description": "фыв фыв фыв",
   *  "description": "asd asd asd",
   *  "country_id": 1,
   *  "manufacturer_id": 2,
   *  "detailed_description": "asd zxc zxcvxcv",
   *  "views": null,
   *  "param_options": [
   *    {
   *      "id": 1,
   *      "parameter_id": 2,
   *      "value": "null",
   *      "type": "select"
   *    },
   *    {
   *      "id": 2,
   *      "parameter_id": 5,
   *      "value": "asd",
   *      "type": "checkbox"
   *    },
   *  ],
   *  "photos": [
   *    {
   *      "id": 1,
   *      "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *    },
   *    {
   *      "id": 2,
   *      "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *    }
   *  ],
   *  "tags": [
   *    {
   *      "id": 1,
   *      "title": "asdas"
   *    },
   *    {
   *      "id": 2,
   *      "title": "asdas"
   *    }
   *  ],
   *  "documents": []
   * }
   */
  public function update(Request $request, $product)
  {
    return $this->successResponse($this->productService->updateProduct($request, $product));
  }

  /**
   * @group Product
   * 
   * @queryParam products array required Product ids
   * 
   * @response [
   *  {
   *    "id": 1,
   *    "category_id": 1,
   *    "title": "title",
   *    "price": 200000,
   *    "discount_price": 300000,
   *    "price_to_basket": 200000,
   *    "convert_price": "200 000,00 UZS",
   *    "convert_discount_price": "300 000,00 UZS",
   *    "currency": {
   *     "id": 1,
   *     "title": "UZS"
   *    },
   *    "quantity": 2,
   *    "quantity_size": 1,
   *    "delivery_description": "фыв фыв фыв",
   *    "description": "asd asd asd",
   *    "country_id": 1,
   *    "manufacturer_id": 2,
   *    "detailed_description": "asd zxc zxcvxcv",
   *    "views": 0,
   *    "param_options": [
   *      {
   *       "id": 1,
   *       "parameter_id": 2,
   *       "value": "null",
   *       "type": "select"
   *      },
   *      {
   *       "id": 2,
   *       "parameter_id": 5,
   *       "value": "asd",
   *       "type": "checkbox"
   *      }
   *    ],
   *    "photos": [
   *      {
   *       "id": 1,
   *       "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *      },
   *      {
   *       "id": 2,
   *       "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *      }
   *    ],
   *    "tags": [
   *      {
   *       "id": 4,
   *       "title": "asdas"
   *      },
   *      {
   *       "id": 5,
   *       "title": "asdas"
   *      }
   *    ],
   *    "documents": [],
   *    "store": {
   *      "id": 3,
   *      "user_id": 1,
   *      "title": "titleasdas",
   *      "description": "descripti",
   *      "logotype": "/uploads/store/logotype/0iiSEGfJuGPaH8WyMpG6sHpuV4UMbDypuBxvWQoG.png",
   *      "type": 2,
   *      "subdomain": "zxc",
   *      "delivery": [
   *        {
   *         "id": "1",
   *         "title": "Доставка сервисом"
   *        },
   *        {
   *         "id": "2",
   *         "title": "Самовывоз"
   *        }
   *      ],
   *      "bg_color": [
   *        {
   *         "key": "asdasdasd",
   *         "color": "asd"
   *        }
   *      ],
   *      "bg_image": null,
   *      "product_count": 1,
   *      "views": 2,
   *      "data": [
   *        {
   *         "title": "asd",
   *         "category": "address",
   *         "main": 1
   *        }
   *      ]
   *    }
   *  }
   * ]
   */
  public function basket(Request $request)
  {
    return $this->successResponse($this->productService->userBasketProducts($request));
  }

  /**
   * @group Product
   * 
   * @queryParam products array required Product ids
   * 
   * @response [
   *  {
   *    "id": 1,
   *    "category_id": 1,
   *    "title": "title",
   *    "price": 200000,
   *    "discount_price": 300000,
   *    "price_to_basket": 200000,
   *    "convert_price": "200 000,00 UZS",
   *    "convert_discount_price": "300 000,00 UZS",
   *    "currency": {
   *     "id": 1,
   *     "title": "UZS"
   *    },
   *    "quantity": 2,
   *    "quantity_size": 1,
   *    "delivery_description": "фыв фыв фыв",
   *    "description": "asd asd asd",
   *    "country_id": 1,
   *    "manufacturer_id": 2,
   *    "detailed_description": "asd zxc zxcvxcv",
   *    "views": 0,
   *    "param_options": [
   *      {
   *       "id": 1,
   *       "parameter_id": 2,
   *       "value": "null",
   *       "type": "select"
   *      },
   *      {
   *       "id": 2,
   *       "parameter_id": 5,
   *       "value": "asd",
   *       "type": "checkbox"
   *      }
   *    ],
   *    "photos": [
   *      {
   *       "id": 1,
   *       "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *      },
   *      {
   *       "id": 2,
   *       "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *      }
   *    ],
   *    "tags": [
   *      {
   *       "id": 4,
   *       "title": "asdas"
   *      },
   *      {
   *       "id": 5,
   *       "title": "asdas"
   *      }
   *    ],
   *    "documents": [],
   *    "store": {
   *      "id": 3,
   *      "user_id": 1,
   *      "title": "titleasdas",
   *      "description": "descripti",
   *      "logotype": "/uploads/store/logotype/0iiSEGfJuGPaH8WyMpG6sHpuV4UMbDypuBxvWQoG.png",
   *      "type": 2,
   *      "subdomain": "zxc",
   *      "delivery": [
   *        {
   *         "id": "1",
   *         "title": "Доставка сервисом"
   *        },
   *        {
   *         "id": "2",
   *         "title": "Самовывоз"
   *        }
   *      ],
   *      "bg_color": [
   *        {
   *         "key": "asdasdasd",
   *         "color": "asd"
   *        }
   *      ],
   *      "bg_image": null,
   *      "product_count": 1,
   *      "views": 2,
   *      "data": [
   *        {
   *         "title": "asd",
   *         "category": "address",
   *         "main": 1
   *        }
   *      ]
   *    }
   *  }
   * ]
   */
  public function wishlist(Request $request)
  {
    return $this->successResponse($this->productService->wishlist($request));
  }

  /**
   * @group Product
   * 
   * @urlParam product integer required Product ID
   * 
   * @response {
   *  "id": 1,
   *  "category_id": 1,
   *  "title": "фывфыв",
   *  "price": "200000",
   *  "discount_price": "300000",
   *  "price_to_basket": "200000",
   *  "convert_price": "200 000,00 UZS",
   *  "convert_discount_price": "300 000,00 UZS",
   *  "currency": {
   *    "id": "1",
   *    "title": "UZS"
   *  },
   *  "quantity": "2",
   *  "quantity_size": "1",
   *  "delivery_description": "фыв фыв фыв",
   *  "description": "asd asd asd",
   *  "country_id": 1,
   *  "manufacturer_id": 2,
   *  "detailed_description": "asd zxc zxcvxcv",
   *  "views": null,
   *  "param_options": [
   *    {
   *      "id": 1,
   *      "parameter_id": 2,
   *      "value": "null",
   *      "type": "select"
   *    },
   *    {
   *      "id": 2,
   *      "parameter_id": 5,
   *      "value": "asd",
   *      "type": "checkbox"
   *    },
   *  ],
   *  "photos": [
   *    {
   *      "id": 1,
   *      "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *    },
   *    {
   *      "id": 2,
   *      "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *    }
   *  ],
   *  "tags": [
   *    {
   *      "id": 1,
   *      "title": "asdas"
   *    },
   *    {
   *      "id": 2,
   *      "title": "asdas"
   *    }
   *  ],
   *  "documents": []
   * }
   */
  public function destroy(Request $request, $product)
  {
    return $this->successResponse($this->productService->deleteProduct($request, $product));
  }

  /**
   * @group Product
   * 
   * @urlParam photo integer required Product photo ID
   * 
   * @response {
   *  "id": 6,
   *  "product_id": 3,
   *  "photo": "/uploads/product/photos/R6zWp6wuYWhOgA5PEVaNypuczfZ9Bs62dSU1SW8G.png"
   * }
   */
  public function deleteProductPhoto(Request $request, $photo)
  {
    return $this->successResponse($this->productService->deleteProductPhoto($request, $photo));
  }

  /**
   * @group Product
   * 
   * @urlParam product integer required Product ID
   * 
   * @bodyParam title string required
   * @bodyParam document string required
   * @bodyParam size integer required
   * 
   * @response {
   *     "id": 1,
   *     "title": "asdasd",
   *     "document": "/uploads/lot/document/fbuAlgLow4tessMraD8lCQscnA49razIDKzSuUQz.pdf",
   *     "size": "1024"
   * }
   */
  public function productCreateDoc(Request $request, $product)
  {
    return $this->successResponse($this->productService->productCreateDoc($request, $product));
  }

  /**
   * @group Product
   * 
   * @urlParam document integer required Product document ID
   * 
   * @response {
   *     "id": 1,
   *     "title": "asdasd",
   *     "document": "/uploads/lot/document/fbuAlgLow4tessMraD8lCQscnA49razIDKzSuUQz.pdf",
   *     "size": "1024"
   * }
   */
  public function productDeleteDoc(Request $request, $document)
  {
    return $this->successResponse($this->productService->productDeleteDoc($request, $document));
  }

  /**
   * @group Product
   * 
   * @urlParam store integer required Shop ID
   * 
   * @response {
   *   "data": [
   *     {
   *       "id": 2,
   *       "category_id": 1,
   *       "title": "фывфыв",
   *       "price": 200000,
   *       "discount_price": 300000,
   *       "price_to_basket": 200000,
   *       "convert_price": "200 000,00 UZS",
   *       "convert_discount_price": "300 000,00 UZS",
   *       "currency": {
   *         "id": 1,
   *         "title": "UZS"
   *       },
   *       "quantity": 2,
   *       "quantity_size": 1,
   *       "delivery_description": "фыв фыв фыв",
   *       "description": "asd asd asd",
   *       "country_id": 1,
   *       "manufacturer_id": 2,
   *       "detailed_description": "asd zxc zxcvxcv",
   *       "views": 0,
   *       "reviews_total_count": 0,
   *       "reviews_total_rate": 0,
   *       "param_options": {
   *         "data": [
   *           {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "null",
   *             "type": "select"
   *           }
   *         ]
   *       },
   *       "photos": {
   *         "data": [
   *           {
   *             "id": 4,
   *             "photo": "/uploads/product/photos/7juNc3hr83qWEQ4YOqcvRifbpP4DNFM1mhJgbtdA.png"
   *           }
   *         ]
   *       },
   *       "tags": {
   *         "data": [
   *           {
   *               "id": 7,
   *               "title": "asdas"
   *           }
   *         ]
   *       },
   *       "documents": {
   *         "data": []
   *       }
   *     }
   *   ],
   *   "meta": {
   *     "pagination": {
   *       "total": 2,
   *       "count": 2,
   *       "per_page": 15,
   *       "current_page": 1,
   *       "total_pages": 1,
   *       "links": []
   *     }
   *   }
   * }
   */
  public function getShopProducts(Request $request, $store)
  {
    $result = $this->productService->getProductsByhop($request, $store);
    $products = collect($result['data']);
    $product_ids = $products->pluck('id')->toArray();

    $product_rates = $this->reviewService->getRatesByProducts($request, $product_ids);
    $products->transform(function($item) use($product_rates){
      $item['reviews_total_count'] = $product_rates[$item['id']]['count'];
      $item['reviews_total_rate'] = $product_rates[$item['id']]['rate'];
      return $item;
    });
    $result['data'] = $products->toArray();
    
    return $this->successResponse($result);
  }

  /**
   * @group Product
   * 
   * @urlParam product integer required Product ID
   * 
   * @response {
   *  "id": 1,
   *  "category_id": 1,
   *  "title": "фывфыв",
   *  "price": "200000",
   *  "discount_price": "300000",
   *  "price_to_basket": "200000",
   *  "convert_price": "200 000,00 UZS",
   *  "convert_discount_price": "300 000,00 UZS",
   *  "currency": {
   *    "id": "1",
   *    "title": "UZS"
   *  },
   *  "quantity": "2",
   *  "quantity_size": "1",
   *  "delivery_description": "фыв фыв фыв",
   *  "description": "asd asd asd",
   *  "country_id": 1,
   *  "manufacturer_id": 2,
   *  "detailed_description": "asd zxc zxcvxcv",
   *  "views": null,
   *  "param_options": [
   *    {
   *      "id": 1,
   *      "parameter_id": 2,
   *      "value": "null",
   *      "type": "select"
   *    },
   *    {
   *      "id": 2,
   *      "parameter_id": 5,
   *      "value": "asd",
   *      "type": "checkbox"
   *    },
   *  ],
   *  "photos": [
   *    {
   *      "id": 1,
   *      "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *    },
   *    {
   *      "id": 2,
   *      "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *    }
   *  ],
   *  "tags": [
   *    {
   *      "id": 1,
   *      "title": "asdas"
   *    },
   *    {
   *      "id": 2,
   *      "title": "asdas"
   *    }
   *  ],
   *  "documents": []
   * }
   */
  public function getProductById(Request $request, $product)
  {
    $result = $this->productService->getProductById($request, $product);
    $product_reviews_questions_count = $this->reviewService->getReviewsQuestionsCount($request, $product);
    $result['reviews_total_count'] = $product_reviews_questions_count['reviews'];
    $result['question_total_count'] = $product_reviews_questions_count['questions'];
    return $this->successResponse($result);
  }
  
  /**
   * @group Product
   * 
   * @response {
   *   "data": [
   *     {
   *       "id": 2,
   *       "category_id": 1,
   *       "title": "фывфыв",
   *       "price": 200000,
   *       "discount_price": 300000,
   *       "price_to_basket": 200000,
   *       "convert_price": "200 000,00 UZS",
   *       "convert_discount_price": "300 000,00 UZS",
   *       "currency": {
   *         "id": 1,
   *         "title": "UZS"
   *       },
   *       "quantity": 2,
   *       "quantity_size": 1,
   *       "delivery_description": "фыв фыв фыв",
   *       "description": "asd asd asd",
   *       "country_id": 1,
   *       "manufacturer_id": 2,
   *       "detailed_description": "asd zxc zxcvxcv",
   *       "views": 0,
   *       "reviews_total_count": 0,
   *       "reviews_total_rate": 0,
   *       "param_options": {
   *         "data": [
   *           {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "null",
   *             "type": "select"
   *           }
   *         ]
   *       },
   *       "photos": {
   *         "data": [
   *           {
   *             "id": 4,
   *             "photo": "/uploads/product/photos/7juNc3hr83qWEQ4YOqcvRifbpP4DNFM1mhJgbtdA.png"
   *           }
   *         ]
   *       },
   *       "tags": {
   *         "data": [
   *           {
   *               "id": 7,
   *               "title": "asdas"
   *           }
   *         ]
   *       },
   *       "documents": {
   *         "data": []
   *       }
   *     }
   *   ],
   *   "meta": {
   *     "pagination": {
   *       "total": 2,
   *       "count": 2,
   *       "per_page": 15,
   *       "current_page": 1,
   *       "total_pages": 1,
   *       "links": []
   *     }
   *   }
   * }
   */
  public function getProductList(Request $request)
  {
    return $this->successResponse($this->productService->getProductList($request));
  }

  /**
   * @group Product
   * 
   * @urlParam store integer required Shop ID
   * 
   * @response {
   *   "data": [
   *     {
   *       "id": 2,
   *       "category_id": 1,
   *       "title": "фывфыв",
   *       "price": 200000,
   *       "discount_price": 300000,
   *       "price_to_basket": 200000,
   *       "convert_price": "200 000,00 UZS",
   *       "convert_discount_price": "300 000,00 UZS",
   *       "currency": {
   *         "id": 1,
   *         "title": "UZS"
   *       },
   *       "quantity": 2,
   *       "quantity_size": 1,
   *       "delivery_description": "фыв фыв фыв",
   *       "description": "asd asd asd",
   *       "country_id": 1,
   *       "manufacturer_id": 2,
   *       "detailed_description": "asd zxc zxcvxcv",
   *       "views": 0,
   *       "reviews_total_count": 0,
   *       "reviews_total_rate": 0,
   *       "param_options": {
   *         "data": [
   *           {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "null",
   *             "type": "select"
   *           }
   *         ]
   *       },
   *       "photos": {
   *         "data": [
   *           {
   *             "id": 4,
   *             "photo": "/uploads/product/photos/7juNc3hr83qWEQ4YOqcvRifbpP4DNFM1mhJgbtdA.png"
   *           }
   *         ]
   *       },
   *       "tags": {
   *         "data": [
   *           {
   *               "id": 7,
   *               "title": "asdas"
   *           }
   *         ]
   *       },
   *       "documents": {
   *         "data": []
   *       }
   *     }
   *   ],
   *   "meta": {
   *     "pagination": {
   *       "total": 2,
   *       "count": 2,
   *       "per_page": 15,
   *       "current_page": 1,
   *       "total_pages": 1,
   *       "links": []
   *     }
   *   }
   * }
   */
  public function getProductListByShop(Request $request, $store)
  {
    return $this->successResponse($this->productService->getAllProductsByShop($request, $store));
  }

  /**
   * @group Product
   * 
   * @urlParam product integer required Product ID
   * 
   * @response {
   *  "id": 1,
   *  "category_id": 1,
   *  "title": "фывфыв",
   *  "price": "200000",
   *  "discount_price": "300000",
   *  "price_to_basket": "200000",
   *  "convert_price": "200 000,00 UZS",
   *  "convert_discount_price": "300 000,00 UZS",
   *  "currency": {
   *    "id": "1",
   *    "title": "UZS"
   *  },
   *  "quantity": "2",
   *  "quantity_size": "1",
   *  "delivery_description": "фыв фыв фыв",
   *  "description": "asd asd asd",
   *  "country_id": 1,
   *  "manufacturer_id": 2,
   *  "detailed_description": "asd zxc zxcvxcv",
   *  "views": null,
   *  "param_options": [
   *    {
   *      "id": 1,
   *      "parameter_id": 2,
   *      "value": "null",
   *      "type": "select"
   *    },
   *    {
   *      "id": 2,
   *      "parameter_id": 5,
   *      "value": "asd",
   *      "type": "checkbox"
   *    },
   *  ],
   *  "photos": [
   *    {
   *      "id": 1,
   *      "photo": "/uploads/product/photos/b9bPi7sT3X4qzThdsGX24enOUk4F4eh29UlkmC37.png"
   *    },
   *    {
   *      "id": 2,
   *      "photo": "/uploads/product/photos/fNSHBQXoqpXDLgdItX4VsY34UUXZSMKT0vaLdu7k.png"
   *    }
   *  ],
   *  "tags": [
   *    {
   *      "id": 1,
   *      "title": "asdas"
   *    },
   *    {
   *      "id": 2,
   *      "title": "asdas"
   *    }
   *  ],
   *  "documents": []
   * }
   */
  public function setProductViewCounter(Request $request, $product)
  {
    return $this->successResponse($this->productService->setProductViewCounter($request, $product));
  }
}
