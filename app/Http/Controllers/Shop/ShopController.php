<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\BaseController;
use App\Services\ProductService;
use App\Services\ReviewService;
use App\Services\ShopService;
use Illuminate\Http\Request;

class ShopController extends BaseController
{
  
  public $shopService;
  public $reviewService;
  public $productService;

  public function __construct(ShopService $shopService, ProductService $productService, ReviewService $reviewService)
  {
    $this->shopService = $shopService;
    $this->reviewService = $reviewService;
    $this->productService = $productService;
  }
  
  /**
   * @group Shop
   * 
   * @response {
   *  "data": [
   *    {
   *      "id": 2,
   *      "user_id": 1,
   *      "title": "title",
   *      "description": "description",
   *      "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *      "type": 1,
   *      "subdomain": "my-store",
   *      "delivery": [
   *         {
   *           "id": "1",
   *           "title": "Доставка сервисом en"
   *         },
   *         {
   *           "id": "2",
   *           "title": "Самовывоз en"
   *         }
   *      ],
   *      "bg_color": null,
   *      "bg_image": null,
   *      "product_count": 0,
   *      "views": 0,
   *      "data": {
   *        "data": [
   *          {
   *            "title": "asd",
   *            "category": "address",
   *            "main": 1
   *          },
   *          {
   *            "title": "zxczxc",
   *            "category": "2",
   *            "main": 0
   *          }
   *        ]
   *      }
   *    },
   *  ],
   *  "meta": {
   *    "pagination": {
   *      "total": 2,
   *      "count": 2,
   *      "per_page": 15,
   *      "current_page": 1,
   *      "total_pages": 1,
   *      "links": []
   *    }
   *  }
   * }
   */
  public function mainData(Request $request)
  {
    return $this->successResponse($this->shopService->getMainData($request));
  }
  

  /**
   * @group Shop
   * 
   * @response {
   *  "data": [
   *    {
   *      "id": 2,
   *      "user_id": 1,
   *      "title": "title",
   *      "description": "description",
   *      "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *      "type": 1,
   *      "subdomain": "my-store",
   *      "delivery": [
   *         {
   *           "id": "1",
   *           "title": "Доставка сервисом en"
   *         },
   *         {
   *           "id": "2",
   *           "title": "Самовывоз en"
   *         }
   *      ],
   *      "bg_color": null,
   *      "bg_image": null,
   *      "product_count": 0,
   *      "views": 0,
   *      "data": {
   *        "data": [
   *          {
   *            "title": "asd",
   *            "category": "address",
   *            "main": 1
   *          },
   *          {
   *            "title": "zxczxc",
   *            "category": "2",
   *            "main": 0
   *          }
   *        ]
   *      }
   *    },
   *  ],
   *  "meta": {
   *    "pagination": {
   *      "total": 2,
   *      "count": 2,
   *      "per_page": 15,
   *      "current_page": 1,
   *      "total_pages": 1,
   *      "links": []
   *    }
   *  }
   * }
   */
  public function userStores(Request $request, $user_id=false)
  {
    return $this->successResponse($this->shopService->getShops($request, false, $user_id));
  }
  
  /**
   * @group Shop
   *
   * @bodyParam title string required
   * @bodyParam description string nullable
   * @bodyParam type integer required
   * @bodyParam subdomain string nullable
   * @bodyParam delivery.*.title string nullable
   * @bodyParam delivery.*.id integer nullable
   * @bodyParam data.*.title string nullable
   * @bodyParam data.*.category integer nullable
   * @bodyParam data.*.main integer nullable
   * @bodyParam logotype string nullable
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function store(Request $request)
  {
    return $this->successResponse($this->shopService->createShop($request));
  }

  /**
   * @group Shop
   *
   * @urlParam store integer required Store ID
   * @bodyParam title string required
   * @bodyParam description string nullable
   * @bodyParam type integer required
   * @bodyParam subdomain string nullable
   * @bodyParam delivery.*.title string nullable
   * @bodyParam delivery.*.id integer nullable
   * @bodyParam data.*.title string nullable
   * @bodyParam data.*.category integer nullable
   * @bodyParam data.*.main integer nullable
   * @bodyParam logotype string nullable
   * @bodyParam bg_image string nullable
   * @bodyParam bg_color.*.key string nullable
   * @bodyParam bg_color.*.color string nullable
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function update(Request $request, $store)
  {
    return $this->successResponse($this->shopService->updateShop($request, $store));
  }
  
  /**
   * @group Shop
   *
   * @urlParam store integer required Store ID
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function destroy(Request $request, $store)
  {
    return $this->successResponse($this->shopService->deleteShop($request, $store));
  }

  /**
   * @group Shop
   *
   * @urlParam store integer required Store ID
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function deleteBgImage(Request $request, $store)
  {
    return $this->successResponse($this->shopService->deleteShopBgImage($request, $store));
  }

  /**
   * @group Shop
   * 
   * @response {
   *  "data": [
   *    {
   *      "id": 2,
   *      "user_id": 1,
   *      "title": "title",
   *      "description": "description",
   *      "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *      "type": 1,
   *      "subdomain": "my-store",
   *      "delivery": [
   *         {
   *           "id": "1",
   *           "title": "Доставка сервисом en"
   *         },
   *         {
   *           "id": "2",
   *           "title": "Самовывоз en"
   *         }
   *      ],
   *      "bg_color": null,
   *      "bg_image": null,
   *      "product_count": 0,
   *      "views": 0,
   *      "data": {
   *        "data": [
   *          {
   *            "title": "asd",
   *            "category": "address",
   *            "main": 1
   *          },
   *          {
   *            "title": "zxczxc",
   *            "category": "2",
   *            "main": 0
   *          }
   *        ]
   *      }
   *    },
   *  ],
   *  "meta": {
   *    "pagination": {
   *      "total": 2,
   *      "count": 2,
   *      "per_page": 15,
   *      "current_page": 1,
   *      "total_pages": 1,
   *      "links": []
   *    }
   *  }
   * }
   */
  public function getShopList(Request $request)
  {
    return $this->successResponse($this->shopService->getShops($request, true));
  }

  /**
   * @group Shop
   *
   * @urlParam store integer required Shop ID
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function getShopById(Request $request, $store)
  {
    $shop = $this->shopService->getShopById($request, $store);
    $product_ids = collect($this->productService->getAllProductsByShop($request, $store))->pluck('id')->toArray();
    $raiting = $this->reviewService->getShopRates($request, $shop['id'], $product_ids);
    if ($raiting) {
      if (isset($raiting['total_rating'])) {
        $shop['total_rating'] = $raiting['total_rating'];
        $shop['rates'] = $raiting['rates'];
      }else{
        $shop['total_rating'] = 0;
        $shop['rates'] = $raiting['rates'];
      }
    }
    return $this->successResponse($shop);
  }

  /**
   * @group Shop
   *
   * @urlParam domain string required Shop domain
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function getShopByDomain(Request $request, $domain)
  {
    $shop = $this->shopService->getShopByDomain($request, $domain);
    if (isset($shop['error']) && $shop['error']) {
      return $this->successResponse($shop);
    }

    $product_ids = collect($this->productService->getAllProductsByShop($request, $shop['id']))->pluck('id')->toArray();
    $raiting = $this->reviewService->getShopRates($request, $shop['id'], $product_ids);
    if ($raiting) {
      if (isset($raiting['total_rating'])) {
        $shop['total_rating'] = $raiting['total_rating'];
        $shop['rates'] = $raiting['rates'];
      }else{
        $shop['total_rating'] = 0;
        $shop['rates'] = $raiting['rates'];
      }
    }
    return $this->successResponse($shop);
  }

  /**
   * @group Shop
   *
   * @urlParam store integer required Shop ID
   * 
   * @response {
   *   "id": 2,
   *   "user_id": 1,
   *   "title": "title",
   *   "description": "description",
   *   "logotype": "/uploads/store/logotype/aUD84TaorKm7Z7p5WmQFfTBbOnZqn8NcWiWgNue2.png",
   *   "type": "1",
   *   "subdomain": "my-store",
   *   "delivery": [
   *       {
   *           "title": "Доставка сервисом en",
   *           "id": "1"
   *       },
   *       {
   *           "title": "Самовывоз en",
   *           "id": "2"
   *       }
   *   ],
   *   "bg_color": null,
   *   "bg_image": null,
   *   "product_count": 0,
   *   "views": null,
   *   "data": [
   *       {
   *           "title": "asd",
   *           "category": "address",
   *           "main": 1
   *       },
   *       {
   *           "title": "zxczxc",
   *           "category": "2",
   *           "main": 0
   *       }
   *   ]
   * }
   *
   */
  public function setViewToShop(Request $request, $store)
  {
    return $this->successResponse($this->shopService->counterShopView($request, $store));
  }
}
