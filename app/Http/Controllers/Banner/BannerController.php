<?php

namespace App\Http\Controllers\Banner;

use App\Http\Controllers\Controller;
use App\Services\BannerService;
use App\Traits\ResponseTrait;

class BannerController extends Controller
{
    use ResponseTrait;

    private $service;

    public function __construct(BannerService $bannerService)
    {
        $this->service = $bannerService;
    }

    public function index()
    {
        $data = $this->service->getBanners();
        return $this->successResponse($data);
    }
}