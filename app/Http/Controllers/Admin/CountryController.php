<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\AddOn\CountryService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\SendNotAllowedResponseTrait;

class CountryController extends Controller
{
  use SendNotAllowedResponseTrait;
  public $countryService;

  public function __construct(CountryService $countryService)
  {
    $this->countryService = $countryService;
  }

  public function index(Request $request)
  {
    if (!$request->user()->can('country.view'))
      return $this->notAllowed();
    $response = $this->countryService->getCountries($request);
    $data = [
      'countries' => $response['data']
    ];
    return view('categories.countries.index', ['data' => $data]);
  }

  //* Create new Country
  public function createView(Request $request)
  {
    if (!$request->user()->can('country.create'))
      return $this->notAllowed();
    return view('categories.countries.create');
  }

  //* Describe your method
  public function createCountry(Request $request)
  {
    if (!$request->user()->can('country.create'))
      return $this->notAllowed();
    $response = $this->countryService->createCountry($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([CountryController::class, 'index'])->with('message', "Created Successfully");
  }

  //* Get Country By Id
  //! And Return Update view
  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('country.update'))
      return $this->notAllowed();
    $response = $this->countryService->getCountryById($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'country' => $response['data'],
    ];
    return view('categories.countries.edit', ['data' => $data]);
  }

  //* Update country credentials
  public function updateCountry(Request $request, $id)
  {
    if (!$request->user()->can('country.update'))
      return $this->notAllowed();
    $response = $this->countryService->updateCountry($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([CountryController::class, 'index'])->with('message', "Updated Successfully");
  }

  //* Delete Country
  public function destroy(Request $request)
  {
    if (!$request->user()->can('country.delete'))
      return $this->notAllowed();
    $response = $this->countryService->deleteCountry($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([CountryController::class, 'index'])->with('message', "Deleted Successfully");
  }
}
