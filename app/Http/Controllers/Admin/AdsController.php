<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Ads\AdsService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;

class AdsController extends Controller
{
  use IdToValue;
  public $adsService;

  public function __construct(AdsService $adsService)
  {
    $this->adsService = $adsService;
  }

  /**
   * Describe your method
   * @return \Illuminate\Http\Response
   */
  public function getAdList(Request $request)
  {
    if (!$request->user()->can('ads.view'))
      return $this->notAllowed();
    $response = $this->adsService->getAds($request);
    if ($response['success']) {
      $this->extractUserIds($response['data']['data']);
      $response['data']['data'] = $this->userIdToEmail($response['data']['data']);
      $this->extractCtategoryIds($response['data']['data']);
      $response['data']['data'] = $this->categoryIdToTitle($request, $response['data']['data']);
      $this->extractCountryIds($response['data']['data']);
      $response['data']['data'] = $this->countryIdToName($request, $response['data']['data']);
      $this->extractManufacturerIds($response['data']['data']);
      $response['data']['data'] = $this->manufacturerIdToTitle($request, $response['data']['data']);
      $data = [
        'ads' => $response['data'],
      ];
      // dd($data);
      return view('categories.ads.index', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['data']);
    }
  }
}
