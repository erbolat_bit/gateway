<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Blog\BlogService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\SendNotAllowedResponseTrait;

class BlogController extends Controller
{
  use SendNotAllowedResponseTrait;
  public $blogService;

  public function __construct(BlogService $blogService)
  {
    $this->blogService = $blogService;
  }

  /**
   * Call getPosts Method from Service
   */
  public function index(Request $request)
  {
    if (!$request->user()->can('knowledge-db-post.view'))
      return $this->notAllowed();
    $response = $this->blogService->getPosts($request);
    if ($response['success']) {
      if ($request->wantsJson()) {
        return $response['data'];
      }
      $data = [
        'posts' => $response['data'],
      ];
      return view('categories.knowledgeDb.posts', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  /**
   * Get all Categories
   */
  public function categories(Request $request)
  {
    if (!$request->user()->can('knowledge-db-category.view'))
      return $this->notAllowed();
    $response = $this->blogService->getCategories($request);
    if ($response['success']) {
      if ($request->wantsJson()) {
        return $response['data'];
      } else {
        $data = [
          'categories' => $response['data'],
        ];
        return view('categories.knowledgeDb.categories', ['data' => $data]);
      }
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //* Create View for Posts
  public function createView(Request $request)
  {
    if (!$request->user()->can('knowledge-db-post.create'))
      return $this->notAllowed();
    $categories = $this->blogService->getCategoriesWithoutPagination($request);
    if ($categories['error'])
      return $this->flashError($categories['message']);
    $data = [
      'categories' => $categories['data']
    ];
    return view('categories.knowledgeDb.postsUpdate', ['data' => $data]);
  }

  //* Create new Blog Post
  public function create(Request $request)
  {
    if (!$request->user()->can('knowledge-db-post.create'))
      return $this->notAllowed();
    $response = $this->blogService->createNewPost($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([BlogController::class, 'index'])->with('message', 'Successfully created');
  }

  //* Delete Post as Admin
  public function destroy(Request $request)
  {
    if (!$request->user()->can('knowledge-db-post.delete'))
      return $this->notAllowed();
    $response = $this->blogService->deletePost($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([BlogController::class, 'index'])->with('message', 'Successfully deleted!');
  }

  //* Categories Create view
  public function categoriesCreateView(Request $request)
  {
    if (!$request->user()->can('knowledge-db-category.view'))
      return $this->notAllowed();
    return view('categories.knowledgeDb.categoriesCreate');
  }

  //* Store Category
  public function categoriesCreate(Request $request)
  {
    if (!$request->user()->can('knowledge-db-category.create'))
      return $this->notAllowed();
    $response = $this->blogService->storeCategory($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([BlogController::class, 'categories'])->with('message', 'Successfully Created a Category');
  }

  //* Updating vew for categories
  public function categoriesUpdateView(Request $request, $id)
  {
    if (!$request->user()->can('knowledge-db-category.update'))
      return $this->notAllowed();
    $response = $this->blogService->getCategoryById($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'category' => $response['data'],
    ];
    return view('categories.knowledgeDb.categoriesUpdate', ['data' => $data]);
  }

  //* Update category Credentials
  public function categoriesUpdate(Request $request, $id)
  {
    if (!$request->user()->can('knowledge-db-category.update'))
      return $this->notAllowed();
    $response = $this->blogService->updateCategory($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([BlogController::class, 'categories'])->with('message', 'Updated Successfully');
  }

  //* Delete Category
  public function categoriesDestroy(Request $request)
  {
    if (!$request->user()->can('knowledge-db-category.delete'))
      return $this->notAllowed();
    $response = $this->blogService->deleteCategory($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([BlogController::class, 'categories'])->with('message', 'Deleted Successfully');
  }

  //* Update view for posts
  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('knowledge-db-post.update'))
      return $this->notAllowed();
    $post = $this->blogService->getPostById($request, $id);
    if ($post['error'])
      return $this->falshError($post['message']);
    $categories = $this->blogService->getCategoriesWithoutPagination($request);
    if ($categories['error'])
      return $this->flashError($categories['message']);
    $data = [
      'post' => $post['data'],
      'categories' => $categories['data']
    ];
    return view('categories.knowledgeDb.postsUpdate', ['data' => $data]);
  }

  //* Update post
  public function Update(Request $request, $id)
  {
    if (!$request->user()->can('knowledge-db-post.update'))
      return $this->notAllowed();
    $response = $this->blogService->updatePost($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([BlogController::class, 'index'])->with('message', 'Updated Successfully');
  }
}
