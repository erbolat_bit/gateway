<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Lot\LotService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;
use App\Traits\PaginationTrait;

class LotController extends Controller
{
  use PaginationTrait;
  use IdToValue;
  public $lotService;

  public function __construct(LotService $lotService)
  {
    $this->lotService = $lotService;
  }

  /**
   * Get all lots
   */
  public function getLots(Request $request)
  {
    if (!$request->user()->can('lot.view'))
      return $this->notAllowed();
    $response = $this->lotService->getLots($request);
    if ($response['success']) {
      //Prepare pagination data for use in pagination component
      $pagination = $this->prepare($response['meta'], $response['links']);
      //Getting coutnry names from services
      $this->extractCountryIds($response['data']);
      $response['data'] = $this->countryIdToName($request, $response['data']);
      //Getting User emails from services
      $this->extractUserIds($response['data']);
      $response['data'] = $this->userIdToEmail($response['data']);
      //Getting CategoryIds from Services
      $this->extractCtategoryIds($response['data']);
      $response['data'] = $this->categoryIdToTitle($request, $response['data']);
      //Getting Manufacturers from Services
      $this->extractManufacturerIds($response['data']);
      $response['data'] = $this->manufacturerIdToTitle($request, $response['data']);
      $data = [
        'lots' => $response['data'],
        'pagination' => $pagination
      ];
      return view('categories.lot.index', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  /**
   * Get All Offers
   */
  public function getOffers(Request $request)
  {
    if (!$request->user()->can('lot-offer.view'))
      return $this->notAllowed();
    $response = $this->lotService->getOffers($request);
    if ($response['success']) {
      $pagination = $this->prepare($response['meta'], $response['links']);
      $this->extractUserIds($response['data']);
      $response['data'] = $this->userIdToEmail($response['data']);
      $data = [
        'offers' => $response['data'],
        'pagination' => $pagination
      ];
      return view('categories.lot.offer', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //* Show Lot Details
  public function showLot(Request $request, $id)
  {
    if (!$request->user()->can('lot.view'))
      return $this->notAllowed();
    $response = $this->lotService->getLotByIdAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $this->extractUserIds([$response['data']]);
    $response['data'] = $this->userIdToEmail([$response['data']]);
    $this->extractCountryIds($response['data']);
    $response['data'] = $this->countryIdToName($request, $response['data']);
    $this->extractManufacturerIds($response['data']);
    $response['data'] = $this->manufacturerIdToTitle($request, $response['data']);
    $this->extractCtategoryIds($response['data']);
    $response['data'] = $this->categoryIdToTitle($request, $response['data']);
    $data = [
      'lot' => $response['data'][0],
    ];
    return view('categories.lot.showLot', ['data' => $data]);
  }

  //* Update view for lot
  public function updateLotView(Request $request, $id)
  {
    if (!$request->user()->can('lot.update'))
      return $this->notAllowed();
    $response = $this->lotService->getLotByIdAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'lot' => $response['data'],
    ];
    return view('categories.lot.updateLot', ['data' => $data]);
  }

  //* Update lot as Admin
  public function updateLot(Request $request, $id)
  {
    if (!$request->user()->can('lot.update'))
      return $this->notAllowed();
    $response = $this->lotService->updateLotAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([LotController::class, 'getLots'])->with('message', 'Successfully Updated');
  }

  //* Delete Lot
  public function deleteLot(Request $request, $id)
  {
    if (!$request->user()->can('lot.delete'))
      return $this->notAllowed();
    $response = $this->lotService->deletLotAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([LotController::class, 'getLots'])->with('message', 'Deleted Successfully');
  }

  //* Delete offer As Admin
  public function deleteOffer(Request $request, $id)
  {
    if (!$request->user()->can('lot-offer.delete'))
      return $this->notAllowed();
    $response = $this->lotService->deleteOfferAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([LotController::class, 'getOffers'])->with('message', 'Successfully deleted');
  }
}
