<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\AddOn\ManufacturerService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\SendNotAllowedResponseTrait;
use Illuminate\Support\Facades\Cache;

class ManufacturerController extends Controller
{
  use SendNotAllowedResponseTrait;
  public $manufacturerService;

  public function __construct(ManufacturerService $manufacturerService)
  {
    $this->manufacturerService = $manufacturerService;
  }

  public function index(Request $request)
  {
    if (!$request->user()->can('manufacturer.view'))
      return $this->notAllowed();
    $response = $this->manufacturerService->getManufacturers($request);
    $data = [
      'manufacturers' => $response['data']
    ];
    return view('categories.manufacturers.index', ['data' => $data]);
  }

  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('manufacturer.update'))
      return $this->notAllowed();
    $response = $this->manufacturerService->getManufacturerById($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'manufacturer' => $response['data'],
    ];
    return view('categories.manufacturers.edit', ['data' => $data]);
  }

  //* Update manufacturer
  public function updateManufacturer(Request $request, $id)
  {
    if (!$request->user()->can('manufacturer.update'))
      return $this->notAllowed();
    $response = $this->manufacturerService->updateManufacturer($request, $id);
    Cache::forget(config('cache.storage_names.manufactures'));
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ManufacturerController::class, 'index'])->with('message', 'Updated Successfully');
  }

  //* Delete Manfucturer
  public function destroy(Request $request)
  {
    if (!$request->user()->can('manufacturer.delete'))
      return $this->notAllowed();
    $response = $this->manufacturerService->deleteManufacturer($request);
    Cache::forget(config('cache.storage_names.manufactures'));
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ManufacturerController::class, 'index'])->with('message', 'Deleted Successfully');
  }

  //* Show Manufacturer
  public function show(Request $request, $id)
  {
    if (!$request->user()->can('manufacturer.view'))
      return $this->notAllowed();
    $response = $this->manufacturerService->getManufacturerById($request, $id);
    if ($response['error'])
      return $this->flashError($request['message']);
    $data = [
      'manufacturer' => $response['data'],
    ];
    return view('categories.manufacturers.show', ['data' => $data]);
  }

  //* Create view for Manufacturers
  public function createView(Request $request)
  {
    if (!$request->user()->can('manufacturer.create'))
      return $this->notAllowed();
    return view('categories.manufacturers.create');
  }

  //* Create Manufacturer
  public function createManufacturer(Request $request)
  {
    if (!$request->user()->can('manufacturer.create'))
      return $this->notAllowed();
    $response = $this->manufacturerService->createManufacturer($request);
    Cache::forget(config('cache.storage_names.manufactures'));
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ManufacturerController::class, 'index'])->with('message', 'Successfully created');
  }
}
