<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\User\UserService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;

class DepartmentController extends Controller
{
    use IdToValue;

    public $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    //* Get all departments
    public function index(Request $request)
    {
        if (!$request->user()->can('department.view'))
            return $this->notAllowed();
        $departments = $this->userService->getDepartments($request, true);
        if ($departments['error'])
            return $this->flashError($departments['message']);
        $data = [
            'departments' => $departments['data'],
        ];
        return view('categories.department.index', ['data' => $data]);
    }

    //* Update view for Departments
    public function updateView(Request $request, $id)
    {
        if (!$request->user()->can('department.update'))
            return $this->notAllowed();
        $department = $this->userService->getDepartmentById($request, $id);
        if ($department['error'])
            return $this->flashError($department['message']);
        $data = [
            'department' => $department['data'],
        ];
        return view('categories.department.update', ['data' => $data]);
    }

    //* Update 
    public function update(Request $request, $id)
    {
        if (!$request->user()->can('department.update'))
            return $this->notAllowed();
        $response = $this->userService->updateDepartment($request, $id);
        if ($response['error'])
            return $this->flashError($response['message']);
        return redirect()->action([DepartmentController::class, 'index'])->with('message', 'Updated Successfully');
    }

    //* Delete department
    public function delete(Request $request)
    {
        if (!$request->user()->can('department.delete'))
            return $this->notAllowed();
        $response = $this->userService->deleteDepartmentAsAdmin($request);
        if ($response['error'])
            return $this->flashError($response['message']);
        return redirect()->action([DepartmentController::class, 'index'])->with('message', 'Deleted Successfully');
    }

    //* Create view for Departments
    public function createView(Request $request)
    {
        if (!$request->user()->can('department.create'))
            return $this->notAllowed();
        return view('categories.department.create');
    }

    //* Create Department As Admin
    public function create(Request $request)
    {
        if (!$request->user()->can('department.create'))
            return $this->notAllowed();
        $response = $this->userService->createDepartment($request);
        if ($response['error'])
            return $this->flashError($response['message']);
        return redirect()->action([DepartmentController::class, 'index'])->with('message', 'Successfully created');
    }
}
