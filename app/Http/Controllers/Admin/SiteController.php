<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Cart\OrderService;
use App\AdminServices\Shop\ShopService;
use App\AdminServices\User\UserService;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SiteController extends Controller
{

  private $shopService;
  private $orderService;
  private $userService;

  public function __construct(ShopService $shopService, OrderService $orderService, UserService $userService)
  {
    $this->orderService = $orderService;
    $this->shopService = $shopService;
    $this->userService = $userService;
  }

  public function index(Request $request)
  {
    $request->merge(['paginate' => 0]);
    if ($request->has('shop_id') && $request->shop_id == 0) {
      unset($request['shop_id']);
    }
    $shops = $this->shopService->getShopList($request, true);
    $shop_id = $request->has('shop_id') ? $request->shop_id : '';
    $data = $this->shopService->getShopViews($request, $shop_id);
    $users = $this->users($data['user_ids'], $shop_id);
    $orders = $this->orderService->getOrderStatistic($request);
    $wishlist = $this->userService->wishlistStatistic($request);
    return view('categories.dashboard', [
      'shops' => $shops['data'],
      'views' => $data,
      'orders' => $orders,
      'wishlist' => $wishlist['data'],
      'users' => $users
    ]);
  }

  private function users($data=[], $shop_id=null)
  {
    $ranges = [ 
        '0-25' => 0,
        '21-30' => 0,
        '31-40' => 0,
        '41-50' => 0,
        '51-60' => 0,
        '61+' => 0
    ];
    if ($shop_id) {
      $users = User::whereNotNull('birthdate')->whereIn('id', $data)->get();  
    }else{
      $users = User::whereNotNull('birthdate')->get();  
    }
    $users = $users->map(function ($user) {
                    $age = Carbon::parse(strtotime($user->birthdate))->age;
                    $ranges = [ 
                      '0-25' => 0,
                      '21-30' => 21,
                      '31-40' => 31,
                      '41-50' => 41,
                      '51-60' => 51,
                      '61+' => 61
                    ];
                    foreach($ranges as $key => $breakpoint)
                    {
                        if ($breakpoint >= $age)
                        {
                            $user->range = $key;
                            break;
                        }
                    }

                    return $user;
                })
                ->mapToGroups(function ($user, $key) {
                    return [$user->range => $user];
                })
                ->map(function ($group) {
                    return count($group);
                })
                ->sortKeys()
                ->toArray();
    return array_merge($ranges, $users);
  }
}