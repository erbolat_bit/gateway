<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
  public function login(Request $request)
  {
    $credentials = $request->only('email', 'password');
    // dd(Auth::attempt($credentials));
    // $user = Admin::where('email', $request->email)->first();

    // if (Hash::check($request->password, $user->password)) {

    //   dd(Auth::login($user));
    //   if (Auth()->user())
    //     return redirect()->intended('dashboard');
    // }
    if (Auth::guard('admin')->attempt($credentials)) {
      return redirect()->route('dashboard');
    }

    return redirect('login');

    // dd(Admin::where('id', 3)->update(['password' => Hash::make('123456789')]));
  }

  public function logout(Request $request)
  {
    Auth::logout();
    return redirect()->route('login');
  }
}
