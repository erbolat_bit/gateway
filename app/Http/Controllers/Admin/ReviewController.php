<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Review\ReviewService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;

class ReviewController extends Controller
{
  use IdToValue;
  public $reviewService;

  public function __construct(ReviewService $reviewService)
  {
    $this->reviewService = $reviewService;
  }

  /**
   * Get All Comments
   */
  public function getComments(Request $request)
  {
    if (!$request->user()->can('review-comment.view'))
      return $this->notAllowed();
    $response = $this->reviewService->getCommentList($request);
    if ($response['success'] ) {
      if (empty($response['data']['data'])) {
        return view('categories.reviews.comments', ['data' => []]);
      }else{
        $this->extractUserIds($response['data']['data']);
        
        $response['data']['data'] = $this->userIdToEmail($response['data']['data']);
        $this->extractPostIds($response['data']['data']);
        $response['data']['data'] = $this->postIdToTitle($request, (array)$response['data']['data']);
        $data = [
          'comments' => $response['data'],
        ];
        return view('categories.reviews.comments', ['data' => $data]);
      }
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }


  /**
   * Get All Questions
   */
  public function getQuestions(Request $request)
  {
    if (!$request->user()->can('review-question.view'))
      return $this->notAllowed();
    $response = $this->reviewService->getQuestionList($request);
    $this->extractProductIds($response['data']['data']);
    $response['data']['data'] = $this->productIdToName($request, $response['data']['data']);
    $this->extractUserIds($response['data']['data']);
    $response['data']['data'] = $this->userIdToEmail($response['data']['data']);
    if ($response['success']) {
      $data = [
        'questions' => $response['data'],
      ];
      return view('categories.reviews.questions', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }


  /**
   * Get All Reviews
   */
  public function getReviews(Request $request)
  {
    if (!$request->user()->can('review.view'))
      return $this->notAllowed();
    $response = $this->reviewService->getReviewList($request);
    if ($response['success']) {
      if (isset($response['data']['data']) && !empty($response['data']['data'])) {
        $this->extractUserIds($response['data']['data']);
        $response['data']['data'] = $this->userIdToEmail($response['data']['data']);
        $this->extractProductIds($response['data']['data']);
        $response['data']['data'] = $this->productIdToName($request, $response['data']['data']);
      }
      $data = [
        'reviews' => $response['data'],
      ];
      return view('categories.reviews.index', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  public function deleteReview(Request $request, $id)
  {
    $this->reviewService->deleteReview($request, $id);
    return redirect()->back();
  }

  public function updateReview(Request $request, $id)
  {
    $this->reviewService->updateReview($request, $id);
    return redirect()->back();
  }

  public function deleteComment(Request $request, $id)
  {
    $this->reviewService->deleteComment($request, $id);
    return redirect()->back();
  }

  public function updateComment(Request $request, $id)
  {
    $this->reviewService->updateComment($request, $id);
    return redirect()->back();
  }

  public function deleteQuestion(Request $request, $id)
  {
    $this->reviewService->deleteQuestion($request, $id);
    return redirect()->back();
  }

  public function updateQuestion(Request $request, $id)
  {
    $this->reviewService->updateQuestion($request, $id);
    return redirect()->back();
  }
}
