<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\AddOn\ParameterService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\SendNotAllowedResponseTrait;

class ParameterController extends Controller
{
  use SendNotAllowedResponseTrait;
  public $parametreService;

  public function __construct(ParameterService $parametreService)
  {
    $this->parametreService = $parametreService;
  }

  /**
   * Calls needed method to get parameters
   */
  public function index(Request $request)
  {
    if (!$request->user()->can('product-param.view'))
      return $this->notAllowed();
    $response = $this->parametreService->getParameters($request);
    if ($response['success']) {
      $data = [
        'parametres' => $response['data'],
      ];
      return view('categories.parametres.index', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //* Create form
  public function createView(Request $request)
  {
    if (!$request->user()->can('product-param.create'))
      return $this->notAllowed();
    $response = $this->parametreService->getTypesOfParameters($request);
    $response2 = $this->parametreService->getCategoriesWithChild($request);

    if ($response['error'])
      return $this->flashError($response['message']);
    if (isset($response2['error']) && $response2['error'])
      return $this->flashError($response2['message']);
    $data = [
      'types' => $response['data'],
      'categories' => $response2['html']
    ];
    return view('categories.parametres.create', ['data' => $data]);
  }

  //* create Product parameter
  public function create(Request $request)
  {
    if (!$request->user()->can('product-param.create'))
      return $this->notAllowed();
    $response = $this->parametreService->createParameter($request);

    if ($response['error'])
      return $this->flashError($response['message']);

    return redirect()->action([ParameterController::class, 'index'])->with('message', 'Created Successfully');
  }

  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('product-param.update'))
      return $this->notAllowed();
    $response = $this->parametreService->getTypesOfParameters($request);
    $response2 = $this->parametreService->getCategoriesWithChild($request, $id);

    if (isset($response['error']) && $response['error'])
      return $this->flashError($response['message']);
    if (isset($response2['error']) && $response2['error'])
      return $this->flashError($response2['message']);
    $data = [
      'types' => $response['data'],
      'categories' => $response2['html'],
      'parameter' => $response2['parameter']
    ];
    return view('categories.parametres.edit', ['data' => $data]);
  }

  public function update(Request $request, $id)
  {
    if (!$request->user()->can('product-param.update'))
      return $this->notAllowed();
    $response = $this->parametreService->updateParameter($request, $id);
    // dd($response);
    if ($response['error'])
      return $this->flashError($response['message']);

    return redirect()->action([ParameterController::class, 'index'])->with('message', 'Updated Successfully');
  }



  public function destroyOption(Request $request, $paramOption)
  {
    if (!$request->user()->can('product-param.delete'))
      return $this->notAllowed();
    $response = $this->parametreService->destroyOption($request, $paramOption);
    if ($response['status'] == 'success') {
      return response()->json(['status' => 'success']);
    } else {
      return response()->json(['status' => 'error']);
    }
  }

  public function destroy(Request $request, $parameter)
  {
    if (!$request->user()->can('product-param.delete'))
      return $this->notAllowed();
    $response = $this->parametreService->destroy($request, $parameter);

    if (isset($response['error']) && $response['error'])
      return $this->flashError($response['message']);

    return redirect()->action([ParameterController::class, 'index'])->with('message', 'Deleted Successfully');
  }

  // private function getChildren(array $first, array $data)
  // {
  //   $id = $first['id'];
  //   foreach ($data as $key => $val) {
  //     if ($val['parent_id'] == $id) {
  //       $first['children'][$val['id']] = $this->getChildren($val, $data);
  //     }
  //   }
  //   return $first;
  // }
}
