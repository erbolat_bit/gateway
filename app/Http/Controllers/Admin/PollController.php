<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Poll\PollService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;

class PollController extends Controller
{
  use IdToValue;
  public $pollService;

  public function __construct(PollService $pollService)
  {
    $this->pollService = $pollService;
  }

  /**
   * Get all Polls
   */
  public function index(Request $request)
  {
    if (!$request->user()->can('poll.view'))
      return $this->notAllowed();
    $response = $this->pollService->getPolls($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    $this->extractStoreIds($response['data']['data']);
    $response['data']['data'] = $this->storeIdToTitle($request, $response['data']['data']);
    $data = [
      'polls' => $response['data'],
    ];
    return view('categories.polls.index', ['data' => $data]);
  }

  //* Show Poll By Id
  public function show(Request $request, $id)
  {
    if (!$request->user()->can('poll.view'))
      return $this->notAllowed();
    $response = $this->pollService->getPollById($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $this->extractStoreIds($response['data']);
    $response['data'] = $this->storeIdToTitle($request, $response['data']);
    $data = [
      'poll' => $response['data'][0],
    ];
    return view('categories.polls.show', ['data' => $data]);
  }

  //* Update view for Poll
  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('poll.update'))
      return $this->notAllowed();
    $response = $this->pollService->getPollById($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'poll' => $response['data'][0],
    ];
    return view('categories.polls.update', ['data' => $data]);
  }

  //* Update Poll details
  public function update(Request $request, $id)
  {
    if (!$request->user()->can('poll.update'))
      return $this->notAllowed();
    $response = $this->pollService->updatePollAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([PollController::class, 'index'])->with('message', 'Updated Successfully');
  }

  //* Delete Poll
  public function delete(Request $request, $id)
  {
    if (!$request->user()->can('poll.delete'))
      return $this->notAllowed();
    $response = $this->pollService->deletePollAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([PollController::class, 'index'])->with('message', 'Deleted Successfully');
  }
}
