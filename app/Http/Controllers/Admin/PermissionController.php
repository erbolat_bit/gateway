<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use App\Traits\SendNotAllowedResponseTrait;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{

  use SendNotAllowedResponseTrait;

  public function createRole(Request $request)
  {
    // Permission::create(['name' => 'roles.*']);
    // dd($request->user());
    // dd($request->user()->getRoleNames());
    if ($request->user()->can('role.create')) {
      $request->validate([
        'role' => 'required'
      ]);
      Role::create(['name' => $request->role, 'guard_name' => 'web']);
      return redirect()->action([AdminController::class, 'roles'])->with('message', 'New role(' . $request->role . ') has been created successfully!');
    } else {
      return $this->notAllowed();
    }
  }

  public function deleteRole(Request $request)
  {
    if ($request->user()->can('role.delete')) {
      $request->validate(['id' => 'required|integer|exists:roles,id']);
      $role = Role::find($request->id);
      $roleName = $role->name;
      $role->delete();
      return redirect()->action([AdminController::class, 'roles'])
        ->with('message', 'Role ' . $roleName . ' has been deleted successfully!');
    } else {
      return $this->notAllowed();
    }
  }

  public function permissionsOfRole(Request $request, Role $role)
  {
    if (!$request->user()->can('permission.view'))
      return $this->notAllowed();
    // $permission = Permission::create(['name' => 'permission', 'guard_name' => 'web']);
    // dd($permission);
    $permissionsOfRole = $role->getAllPermissions()->pluck('name')->toArray();
    $categories = config('params.categories');
    $operations = config('params.operations');
    $permissions = [];
    foreach ($categories as $category) {
      $permissions[] = [
        'name' => $category,
        'operations' => $operations
      ];
    }
    $data = [
      'permissionsOfRole' => $permissionsOfRole,
      'allPermission' => $permissions,
      'role' => $role
    ];
    App::setLocale('ru');
    return view('categories.users.managePermissions', ['data' => $data]);
  }

  public function assignPermissionToRole(Request $request, Role $role)
  {
    if ($request->user()->can('permission.update')) {
      $data = $request->except('_token');
      $res = [];
      foreach ($data  as $key => $val) {
        $pieces = explode("_", $key);
        if (sizeof($pieces) == 2) {
          $res[] = $pieces[0] . '.' . $pieces[1];
        }
      }
      // $request->validate([
      //   'permissions' => 'required|array',
      //   'permissions.*' => 'required|string|exists:permissions,name'
      // ]);
  
      $role->syncPermissions($res);
      return redirect()->action([AdminController::class, 'roles'])
        ->with('message', 'Permissions for ' . $role->name . ' has been successfully chnaged!');
    } else {
      return $this->notAllowed();
    }
  }
}
