<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LogController extends Controller
{
    //* Get all logs
    public function index(Request $request)
    {
        $logs = Log::orderBy('created_at', 'desc')
            ->when(!is_null($request->from_date), function ($q) use ($request) {
                return $q->where('created_at', '>', $request->from_date);
            })
            ->when(!is_null($request->to_date), function ($q) use ($request) {
                return $q->where('created_at', '<', $request->to_date);
            })
            ->when(!is_null($request->action) && $request->action != '*', function ($q) use ($request) {
                return $q->where('action', $request->action);
            })
            ->paginate(10)->toArray();
        $admin_ids = [];
        foreach ($logs['data'] as $log) {
            $admin_ids[] = $log['user_id'];
        }
        $roles = DB::table('roles as t1')
            ->rightJoin('model_has_roles as t2', 't2.role_id', '=', 't1.id')
            ->whereIn('t2.model_id', $admin_ids)
            ->get();
        $admins = Admin::whereIn('id', $admin_ids)->get()->toArray();
        foreach ($admins as $a => $d) {
            foreach ($roles as $role) {
                if ($role->model_id == $d['id']) {
                    $admins[$a]['role'] = $role->name;
                }
            }
        }
        foreach ($logs['data'] as $key => $log) {
            foreach ($admins as $ad => $admin) {
                if ($admin['id'] == $log['user_id']) {
                    $logs['data'][$key]['user_name'] = $admin['name'];
                    $logs['data'][$key]['user_role'] = $admin['role'];
                    $logs['data'][$key]['properties'] = json_decode($logs['data'][$key]['properties']);
                }
            }
        }
        return view('categories.Logs.index', ['data' => [
            'logs' => $logs,
            'action' => $request->action ?? null,
            'from_date' => $request->from_date ?? null,
            'to_date' => $request->to_date ?? null
        ]]);
    }
}
