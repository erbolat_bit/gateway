<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\AdminServices\AddOn\CategoryService;
use Illuminate\Http\Request;
use App\Traits\SendNotAllowedResponseTrait;

class CategoryController extends Controller
{
  use SendNotAllowedResponseTrait;

  public $categoryService;

  public function __construct(CategoryService $categoryService)
  {
    $this->categoryService = $categoryService;
  }

  public function index(Request $request)
  {
    if (!$request->user()->can('category.view'))
      return $this->notAllowed();

    $categories = $this->categoryService->getCategories($request);

    $data = [
      'categories' => $categories['data']
    ];
    return view('categories.categories.all', ['data' => $data]);
  }

  public function createView(Request $request)
  {
    if (!$request->user()->can('category.create'))
      return $this->notAllowed();

    $request->merge(['dropdown' => true]);
    $categories = $this->categoryService->getCategories($request);
    $data = [
      'categories' => $categories['data']
    ];
    return view('categories.categories.create', ['data' => $data]);
  }

  public function createCategory(Request $request)
  {
    if (!$request->user()->can('category.create'))
      return $this->notAllowed();
    $response = $this->categoryService->createCategory($request);
    if ($response['success']) {
      return redirect()->route('categories')->with('message', 'Successfully added new category ' . $response['data']['title_ru'] . '!');
    } else {
      return redirect()->back()->withErrors($response['message']);
    }
  }

  public function deleteCategory(Request $request)
  {
    if (!$request->user()->can('category.delete'))
      return $this->notAllowed();
    $request->validate([
      'id' => 'required|integer'
    ]);
    $response = $this->categoryService->deleteCategory($request, $request->id);
    if ($response['success']) {
      return redirect()->route('categories')->with('message', 'Successfully deleted category!');
    } else {
      return redirect()->back()->withErrors($response['message']);
    }
  }

  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('category.update'))
      return $this->notAllowed();
    $response = $this->categoryService->showCategory($request, $id);
    $request->merge(['dropdown' => true]);
    $categories = $this->categoryService->getCategories($request);
    $data = [
      'categories' => $categories['data'],
      'category' => $response['data']['category']
    ];
    return view('categories.categories.update', ['data' => $data]);
  }

  public function updateCategory(Request $request, $id)
  {
    if (!$request->user()->can('category.update'))
      return $this->notAllowed();
    $response = $this->categoryService->updateCategory($request, $id);
    if ($response['success']) {
      return redirect()->action([CategoryController::class, 'index'])
        ->with('message', "Category Has been changed successfully!");
    } else {
      return redirect()->action([CategoryController::class, 'index'])->with('error', $response['message']);
    }
  }

  public function show(Request $request, $id)
  {
    if (!$request->user()->can('category.view'))
      return $this->notAllowed();
    $response = $this->categoryService->showCategory($request, $id);
    if ($response['error'])
      return redirect()->back()->with('error', 'Category doesn\'t exist');
    if ($response['data']['category']['parent_id'] !== 0) {
      $parent = $this->categoryService->showCategory($request, $response['data']['category']['parent_id']);
      $parent = $parent['data']['category'];
    } else {
      $parent = 'Основная категория';
    }
    $data = [
      'category' => $response['data']['category'],
      'parent' => $parent
    ];
    return view('categories.categories.show', ['data' => $data]);
  }
}
