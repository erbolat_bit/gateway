<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\IdToValue;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
  use IdToValue;
  public function all(Request $request)
  {
    if (!$request->user()->can('user.view'))
      return $this->notAllowed();
    $users = User::orderBy('created_at', 'desc')->paginate(10);
    $data = [
      'users' => $users
    ];
    return view('categories.users.allUsers', ['data' => $data]);
  }

  public function edit($id)
  {
    $user = User::findOrFail($id);
    return view('categories.users.edit', ['user' => $user]);
  }

  public function update(Request $request, $id)
  {
    $user = User::findOrFail($id);

    $request->validate([
			'name' => 'required|string',
			'surname' => 'nullable|string',
			'email' => 'required|email',
			'birthdate' => 'required|date',
			'sex' => 'nullable|string',
			'phone' => 'nullable|integer',
      'password' => 'nullable|string',
			Rule::unique('nickname')->ignore($user->id),
		]);

    $input = $request->input();
    if ($request->has('sex') && !in_array($request->sex, $user->sexList())) {
      $input['sex'] = null;
    }
    if ($request->has('password') && $request->password) {
      $input['password'] = Hash::make($request->password);
    }else{
      unset($input['password']);
    }

    $user->update($input);

    return redirect()->route('users.all')->with('success', 'User updated');
  }

  public function destroy($id)
  {
    $user = User::findOrFail($id);
    $user->delete();

    return redirect()->route('users.all')->with('success', 'User deleted');
  }
}
