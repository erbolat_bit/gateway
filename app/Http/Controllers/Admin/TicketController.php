<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\User\TicketService;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Traits\IdToValue;

class TicketController extends Controller
{
  use IdToValue;
  private $ticketService;

  public function __construct(TicketService $ticketService)
  {
    $this->ticketService = $ticketService;
  }

  public function index(Request $request)
  {
    if (!$request->user()->can('ticket.view'))
      return $this->notAllowed();
    $tickets = $this->ticketService->getUserTicktes($request);
    if (isset($tickets['error']) && $tickets['error']) {
      throw new \Exception($tickets['message']);
    }

    $tickets_coll = collect($tickets['data']);
    $tickets_user_ids = $tickets_coll->unique('user_id')->pluck('user_id')->toArray();
    $messages_user_ids = $tickets_coll->unique('message.user_id')->pluck('message.user_id')->toArray();
    $user_ids = array_unique(array_merge($tickets_user_ids, $messages_user_ids));
    $users = json_decode(json_encode(DB::table('users')->select(['id', 'name', 'path'])->whereIn('id', $user_ids)->get()->keyBy('id')->toArray()), true);
    $tickets_coll->transform(function ($item) use ($users) {
      $item['user'] = $users[$item['user_id']];
      $item['message']['user'] = $users[$item['message']['user_id']];
      return $item;
    });

    $tickets['data'] = $tickets_coll->toArray();
    $tickets['pagination'] = Arr::except($tickets, 'data');

    return view('categories.ticket.index', ['ticktes' => $tickets]);
  }

  public function show(Request $request, $ticket_id)
  {
    if (!$request->user()->can('ticket.view'))
      return $this->notAllowed();
    $messages = $this->ticketService->getMessagesByTicketId($request, $ticket_id);


    if (isset($messages['error']) && $messages['error']) {
      throw new \Exception($messages['message']);
    }

    $messages_coll = collect($messages['data']);
    $user_ids = $messages_coll->unique('user_id')->pluck('user_id')->toArray();
    $users = json_decode(json_encode(DB::table('users')->select(['id', 'name', 'path'])->whereIn('id', $user_ids)->get()->keyBy('id')->toArray()), true);
    $messages['data'] = $messages_coll->transform(function ($item) use ($users) {
      $item['user'] = $users[$item['user_id']];
      return $item;
    })->toArray();

    $message_ids = $messages_coll->where('is_read', false)->pluck('id')->toArray();

    if (!empty($message_ids)) {
      $request->merge(['message_ids' => $message_ids, 'ticket_id' => $ticket_id]);

      $this->ticketService->updateMessageAsRead($request);
    }

    $users = Admin::select(['id', 'name'])->get();

    return view('categories.ticket.show', [
      'messages' => $messages,
      'ticket_id' => $ticket_id,
      'user' => $request->user(),
      'users' => $users
    ]);
  }

  public function store(Request $request, $ticket_id)
  {
    if (!$request->user()->can('ticket.create'))
      return $this->notAllowed();
    $message = $this->ticketService->sendMessage($request, $ticket_id);

    return redirect()->route('ticket.show', ['ticket_id' => $message['ticket_id']]);
  }

  public function updateMessage(Request $request)
  {
    if (!$request->user()->can('ticket.update'))
      return $this->notAllowed();
    $result = $this->ticketService->updateMessageAsRead($request);

    return response()->json($result);
  }

  public function update(Request $request, $ticket_id)
  {
    if (!$request->user()->can('ticket.update'))
      return $this->notAllowed();
    $result = $this->ticketService->updateTicketStatus($request, $ticket_id);

    return redirect()->route('ticket.index');
  }

  public function addAudits(Request $request, $ticket_id)
  {
    if (!$request->user()->can('ticket.update'))
      return $this->notAllowed();
    $result = $this->ticketService->addAudit($request, $ticket_id);

    return redirect()->back();
  }

  //* Show All categories
  public function categories(Request $request)
  {
    if (!$request->user()->can('ticket-category.view'))
      return $this->notAllowed();
    $response = $this->ticketService->getTicketCategories($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'ticketCategories' => $response['data'],
    ];
    return view('categories.ticket.categories.index', ['data' => $data]);
  }

  //* Create new Category
  public function categoryCreate(Request $request)
  {
    if (!$request->user()->can('ticket-category.create'))
      return $this->notAllowed();
    $response = $this->ticketService->createTicketCategory($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([TicketController::class, 'categories'])->with('message', 'Successfully created new Category');
  }

  //* Create view for ticket Categories
  public function categoryCreateView(Request $request)
  {
    if (!$request->user()->can('ticket-category.create'))
      return $this->notAllowed();
    $response = $this->ticketService->getDepartments($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'departments' => $response['data'],
    ];
    return view('categories.ticket.categories.create', ['data' => $data]);
  }

  //* Update view ticket Category
  public function categoryUpdateView(Request $request, $id)
  {
    if (!$request->user()->can('ticket-category.update'))
      return $this->notAllowed();
    $departments = $this->ticketService->getDepartments($request);
    if ($departments['error'])
      return $this->flashError($departments['message']);
    $category = $this->ticketService->getTicketCategoryAsAdmin($request, $id);
    if ($category['error'])
      return $this->flashError($category['message']);
    $data = [
      'departments' => $departments['data'],
      'ticketCategory' => $category['data']
    ];
    return view('categories.ticket.categories.update', ['data' => $data]);
  }

  //* Update ticket Category
  public function categoryUpdate(Request $request, $id)
  {
    if (!$request->user()->can('ticket-category.update'))
      return $this->notAllowed();
    $response = $this->ticketService->updateTicketCategoryAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([TicketController::class, 'categories'])->with('message', 'Successfully updated');
  }

  //* Delete ticket Category
  public function categoryDelete(Request $request, $id)
  {
    if (!$request->user()->can('ticket-category.delete'))
      return $this->notAllowed();
    $response = $this->ticketService->deletTicketCategory($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([TicketController::class, 'categories'])->with('message', 'Successfully Deleted');
  }
}
