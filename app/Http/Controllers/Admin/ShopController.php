<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Shop\ShopService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;
use App\Traits\PaginationTrait;

class ShopController extends Controller
{
  use IdToValue;
  use PaginationTrait;
  public $shopService;

  public function __construct(ShopService $shopService)
  {
    $this->shopService = $shopService;
  }

  /**
   * Get all products
   */
  public function getProductList(Request $request)
  {
    if (!$request->user()->can('shop-product.view'))
      return $this->notAllowed();
    $response = $this->shopService->getProductList($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    $pagination = $this->prepare($response['links'], $response['meta']);
    if (!empty($response['data'])) {
      $this->extractCtategoryIds($response['data']);
      $response['data'] = $this->categoryIdToTitle($request, $response['data']);
    }
    $data = [
      'products' => $response['data'],
      'pagination' => $pagination
    ];
    return view('categories.shop.products', ['data' => $data]);
  }

  /**
   * Get all shops
   */
  public function getShopList(Request $request)
  {
    if (!$request->user()->can('shop.view'))
      return $this->notAllowed();
    $response = $this->shopService->getShopList($request);
    if ($response['success']) {
      $paginator = $this->prepare($response['meta'], $response['links']);
      $this->extractUserIds($response['data']);
      $response['data'] = $this->userIdToEmail($response['data']);
      $data = [
        'shops' => $response['data'],
        'pagination' => $paginator
      ];
      return view('categories.shop.index', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //* Show Shop details
  public function showShop(Request $request, $id)
  {
    if (!$request->user()->can('shop.view'))
      return $this->notAllowed();
    $response = $this->shopService->getShopAsAdmin($request, $id);
    if ($response['error'])
      $this->flashError($response['message']);
    $this->extractUserIds([$response['data']]);
    $response['data'] = $this->userIdToEmail([$response['data']]);
    $data = [
      'shop' => $response['data'][0],
    ];
    return view('categories.shop.show', ['data' => $data]);
  }

  //* Update view for shop
  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('shop.update'))
      return $this->notAllowed();
    $response = $this->shopService->getShopByIdForUpdate($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'shop' => $response['data'],
    ];
    return view('categories.shop.shopUpdate', ['data' => $data]);
  }

  //* Update Shop Details
  public function updateShop(Request $request, $id)
  {
    if (!$request->user()->can('shop.update'))
      return $this->notAllowed();
    $response = $this->shopService->udpateShopAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ShopController::class, 'getShopList'])->with('message', 'Successfully updated');
  }

  //* Delete Shop As Admin
  public function deleteShop(Request $request, $id)
  {
    if (!$request->user()->can('shop.delete'))
      return $this->notAllowed();
    $response = $this->shopService->deleteShopAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ShopController::class, 'getShopList'])->with('message', 'Successfully deleted');
  }

  //* Show Product information by Id
  public function showProduct(Request $request, $id)
  {
    if (!$request->user()->can('shop-product.view'))
      return $this->notAllowed();
    $response = $this->shopService->getProductByIdAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $this->extractCtategoryIds([$response['data']]);
    $response['data'] = $this->categoryIdToTitle($request, [$response['data']]);
    $this->extractCountryIds($response['data']);
    $response['data'] = $this->countryIdToName($request, $response['data']);
    $this->extractManufacturerIds($response['data']);
    $response['data'] = $this->manufacturerIdToTitle($request, $response['data']);
    $data = [
      'product' => $response['data'][0],
    ];
    return view('categories.shop.productShow', ['data' => $data]);
  }

  //* Update view for products
  public function updateProductView(Request $request, $id)
  {
    if (!$request->user()->can('shop-product.update'))
      return $this->notAllowed();
    $response = $this->shopService->getProductByIdAsAdmin($request, $id);
    $data = [
      'product' => $response['data']
    ];
    return view('categories.shop.productUpdate', ['data' => $data]);
  }

  //* Update Product as Admin
  public function updateProduct(Request $request, $id)
  {
    if (!$request->user()->can('shop-product.update'))
      return $this->notAllowed();
    $response = $this->shopService->updateProductAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ShopController::class, 'getProductList'])->with('message', 'Updated Successfully');
  }

  //* Delete Product as Admin
  public function deleteProduct(Request $request, $id)
  {
    if (!$request->user()->can('shop-product.delete'))
      return $this->notAllowed();
    $response = $this->shopService->deleteProductAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->action([ShopController::class, 'getProductList'])->with('message', 'Delted Successfully');
  }
}
