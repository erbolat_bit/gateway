<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SystemMessageCreateRequest;
use App\Models\User;
use App\Repositories\SystemMessageRepository;
use Illuminate\Http\Request;

class SystemMessageController extends Controller
{
    private $systemMessageRepository;

    public function __construct(SystemMessageRepository $systemMessageRepository)
    {
        $this->systemMessageRepository = $systemMessageRepository;    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $system_messages = $this->systemMessageRepository->getMessageList();
        return view('categories.system-messages.index', ['system_messages' => $system_messages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $filterParams = $this->systemMessageRepository->getFilterParamsForCreate();
        return view('categories.system-messages.create', ['data' => $filterParams]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SystemMessageCreateRequest $request)
    {
        try {
            $this->systemMessageRepository->createMessage($request->validated());
            return redirect()->route('system-messages.index')->with('success', 'Message send');
        } catch (\Throwable $e) {
            throw $e;
        }
        
        // SendSystemMessage::dispatch($request->except(['_token', 'message']), $message->id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SystemMessage  $systemMessage
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = $this->systemMessageRepository->getMessageById($id);
        $user_ids = $message->userIds->pluck('user_id')->toArray();
        $users = User::withoutGlobalScopes()->whereIn('id', $user_ids)->get();
        $filterParams = $this->systemMessageRepository->getFilterParamsForCreate();

        return view('categories.system-messages.show', ['data' => $filterParams, 'model' => $message, 'users' => $users]);
    }

}
