<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\AddOn\SettingService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\IdToValue;
use Cache;

class SettingController extends Controller
{
  use IdToValue;
  public $settingService;

  public function __construct(SettingService $settingService)
  {
    $this->settingService = $settingService;
  }

  /**
   * Get all settings with type social
   */
  public function index(Request $request)
  {
    if (!$request->user()->can('general-setting.view'))
      return $this->notAllowed();
    $response = $this->settingService->getSettings($request);
    if ($response['success']) {
      $data = [
        'settings' => $response['data']['settings'],
      ];
      return view('categories.settings.general', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  /**
   * Get social Settings
   */
  public function apiSettings(Request $request)
  {
    if (!$request->user()->can('api-setting.view'))
      return $this->notAllowed();
    $response = $this->settingService->getApiSettings($request);
    if ($response['success']) {
      $data = [
        'settings' => $response['data']['settings'],
      ];
      return view('categories.settings.socials', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  /**
   * Get All Delivery settings
   */
  public function delivery(Request $request)
  {
    if (!$request->user()->can('delivery-type.view'))
      return $this->notAllowed();
    $response = $this->settingService->getDeliverySettings($request);
    if ($response['success']) {
      $data = [
        'delivery' => $response['data'],
      ];
      return view('categories.settings.delivery', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //* updateView For Main Settings
  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('general-setting.update'))
      return $this->notAllowed();
    $response = $this->settingService->getOneSettingDetails($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'setting' => $response['data'],
    ];
    return view('categories.settings.update', ['data' => $data]);
  }

  //* Update setting
  public function update(Request $request, $id)
  {
    if (!$request->user()->can('general-setting.update'))
      return $this->notAllowed();
    $response = $this->settingService->updateSetting($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);

    Cache::forget('settings');
    return redirect()->back()->with('message', 'Updated Successfully');
  }

  //* Update delivery setting
  public function deliveryUpdateView(Request $request, $id)
  {
    if (!$request->user()->can('delivery-type.update'))
      return $this->notAllowed();
    $response = $this->settingService->getDeliverySettingById($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);

    $data = [
      'setting' => $response['data'],
    ];

    return view('categories.settings.updateDelivery', ['data' => $data]);
  }

  //* Update view for Delivery Setting
  public function deliveryUpdate(Request $request, $id)
  {
    if (!$request->user()->can('delivery-type.update'))
      return $this->notAllowed();
    $response = $this->settingService->updateDeliverySetting($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->back()->with('message', 'Updated Successfully');
  }

  //* Create delivery setting
  public function createDeliveryView(Request $request)
  {
    if (!$request->user()->can('delivery-type.create'))
      return $this->notAllowed();
    $response = $this->settingService->getCurrencies($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'currencies' => $response['data'],
    ];
    return view('categories.settings.deliveryCreate', ['data' => $data]);
  }

  //* Create new Delivery setting
  public function createDelivery(Request $request)
  {
    if (!$request->user()->can('delivery-type.create'))
      return $this->notAllowed();
    $response = $this->settingService->createDeliverySetting($request);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->back()->with('message', 'New Delivery Setting has been successfully created');
  }

  //* Delete Delivery
  public function deleteDelivery(Request $request, $id)
  {
    if (!$request->user()->can('delivery-type.delete'))
      return $this->notAllowed();
    $response = $this->settingService->deleteDelivery($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    return redirect()->back()->with('message', 'Deleted Successfully');
  }
}
