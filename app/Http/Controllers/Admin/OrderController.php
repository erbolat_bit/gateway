<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Cart\OrderService;
use App\AdminServices\User\UserService;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\PaginationTrait;
use App\Traits\ResponseTrait;
use App\Traits\IdToValue;

class OrderController extends Controller
{
  use ResponseTrait;
  use PaginationTrait;
  use IdToValue;
  public $orderService;
  public $userService;

  public function __construct(OrderService $orderService, UserService $userService)
  {
    $this->orderService = $orderService;
    $this->userService = $userService;
  }

  /**
   * Get all Orders
   */
  public function getOrders(Request $request)
  {
    if (!$request->user()->can('cart-order.view'))
      return $this->notAllowed();
    $response = $this->orderService->getOrders($request);
    if ($response['error'])
      $this->flashEror($response['message']);
    //Here pagination data is returned in different form
    //In order for pagination component to work properly on blade we have to reshape the pagination data
    //Here we are using pagination trait's function to do this
    $pagination = $this->prepare($response['links'], $response['meta']);
    $user_ids = array();
    //Extracting User ids and Address ids from response
    foreach ($response['data'] as $res) {
      $user_ids[] = $res['user_id'];
    }
    //Finding user Emails from User table according to user ids we have extracted
    //And do the same to Adress ids
    $userDetails = User::find($user_ids)->pluck('id', 'email')->toArray();
    //Appending user Emails and User address names to response
    foreach ($response['data'] as $key => $res) {
      $response['data'][$key]['user_email'] = array_search($res['user_id'], (array)$userDetails);
    }
    $data = [
      'orders' => $response,
      'pagination' => $pagination
    ];
    return view('categories.cart.orders', ['data' => $data]);
  }

  //* Show Order details
  public function showOrder(Request $request, $id)
  {
    if (!$request->user()->can('cart-order.view'))
      return $this->notAllowed();
    $response = $this->orderService->getOrderByIdAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashEror($response['message']);
    $this->extractUserIds([$response['data']]);
    $response['data'] = $this->userIdToEmail([$response['data']]);
    $data = [
      'order' => $response['data'][0],
    ];
    return view('categories.cart.ordersShow', ['data' => $data]);
  }

  public function update(Request $request, $id)
  {
    if (!$request->user()->can('cart-order.view'))
      return $this->notAllowed();

    $data = [
      'status' => 1,
      'error_code' => 0,
      'transaction_id' => null,
    ];
    $response = $this->orderService->updateOrder($request, $id, $data);
    if ($response['error'])
      return $this->flashEror($response['message']);

    return redirect()->back()->with('success', 'Заказ подтвержден');
  }
}
