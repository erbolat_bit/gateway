<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\User\TicketService;
use App\AdminServices\User\UserService;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\BaseUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use App\Traits\SendNotAllowedResponseTrait;

class AdminController extends Controller
{

  use SendNotAllowedResponseTrait;

  public $userService;

  public function __construct(UserService $userService)
  {
    $this->userService = $userService;
  }

  public function admins(Request $request)
  {
    if ($request->user()->can('admin.view')) {

      $admins = Admin::with('ratings')->where('email', '<>', 'superadmin@gmail.com')->paginate(10);

      $admins->transform(function($admin){
        $admin->rating = $admin->ratings->avg('value');
        return $admin;
      });
      $admins = $admins->toArray();

      $roles = Role::where('name', '<>', 'Super Admin')->get();
      $departments = $this->userService->getDepartments($request);
      foreach ($admins['data'] as $key => $admin) {
        $admins['data'][$key]['department'] = null;
        foreach ($departments['data'] as $department) {
          if ($admin['department_id'] == $department['id']) {
            $admins['data'][$key]['department'] = $department['title'];
          }
        }
      }
      $data = [
        'admins' => $admins,
        'roles' => $roles,
        'departments' => $departments['data']
      ];
      return view('categories.users.admins', ['data' => $data]);
    } else {
      return redirect()->route('dashboard')->with('error', 'Only Super Admin can Access this route');
    }
  }

  public function roles(Request $request)
  {
    if ($request->user()->can('role.view') || $request->user()->can('permission.view')) {
      $roles = Role::where('name', '<>', 'Super Admin')->paginate(10);
      $data = [
        'roles' => $roles
      ];
      return view('categories.users.adminRoles', ['data' => $data]);
    } else {
      return redirect()->route('dashboard')->with('error', 'Not Allowed');
    }
  }

  public function manage(Request $request, Admin $admin)
  {
    if (!$request->user()->can('admin.update'))
      return $this->notAllowed();
    $roles = Role::where('name', '<>', 'Super Admin')->get();
    $userRoles = $admin->getRoleNames()->toArray();
    $departments = $this->userService->getDepartments($request);
    $data = [
      'admin' => $admin,
      'roles' => $roles,
      'userRoles' => $userRoles,
      'departments' => $departments['data']
    ];
    return view('categories.users.manage', ['data' => $data]);
  }

  public function change(Request $request, Admin $admin)
  {
    if ($request->user()->can('admin.update')) {

      $rolesList = $this->getRolesListForValidation();
      $request->validate([
        'email' => 'required|email|unique:users,email,' . $admin->id,
        'name' => 'required|string',
        'password' => 'nullable',
        'roles' => 'required|array',
        'roles.*' => 'required|string|exists:roles,name',
        'department_id' => 'required'
      ]);
      $admin->syncRoles($request->roles);
      $admin->update([
        'email' => $request->email,
        'name' => $request->name,
        'password' => Hash::make($request->password),
        'department_id' => $request->department_id
      ]);
      return redirect()->action([AdminController::class, 'admins'])->with('message', 'Admin Credentials has been changed successfully!');
    } else {
      return $this->notAllowed();
    }
  }

  public function create(Request $request)
  {
    if ($request->user()->can('admin.create')) {
      DB::transaction(function () use ($request) {
        $request->validate([
          'name' => 'required|string',
          'email' => 'required|email|unique:users,email',
          'password' => 'required|min:5',
          'roles' => 'required|array',
          'roles.*' => 'required|string|exists:roles,name',
          'department_id' => 'required|integer'
        ]);

        $newAdmin = Admin::create([
          'name' => $request->name,
          'email' => $request->email,
          'password' => Hash::make($request->password),
          'is_admin' => true,
          'department_id' => $request->department_id
        ]);

        $newAdmin->assignRole($request->roles);
      });
      return redirect()->action([AdminController::class, 'admins'])
        ->with('message', 'New Admin ' . $request->name . ' has been created with the role ' . $request->role);
    } else {
      return $this->notAllowed();
    }
  }

  public function delete(Request $request)
  {
    if ($request->user()->can('admin.delete')) {
      $request->validate(['id' => 'required|integer|exists:users,id']);
      $admin = Admin::find($request->id);
      $adminName = $admin->name;
      $admin->syncRoles([]);
      $admin->delete();
      return redirect()->action([AdminController::class, 'admins'])->with('message', 'Admin ' . $adminName . ' has been successfully deleted!');
    } else {
      return $this->notAllowed();
    }
  }

  private function getRolesListForValidation()
  {
    $roles = Role::where('name', '<>', 'Super Admin')->pluck('name')->toArray();
    return $rolesForValidation = implode(",", $roles);
  }
}
