<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Coupon\CouponService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCouponRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\Traits\IdToValue;

class CouponController extends Controller
{
  use IdToValue;
  public $couponService;

  public function __construct(CouponService $couponService)
  {
    $this->couponService = $couponService;
  }

  /**
   * Get All Coupons
   */
  public function getCoupons(Request $request)
  {
    if (!$request->user()->can('coupon.view'))
      return $this->notAllowed();
    $response = $this->couponService->getCoupons($request);
    if ($response['success']) {
      $this->extractStoreIds($response['data']['data']);
      $response['data']['data'] = $this->storeIdToTitle($request, $response['data']['data']);
      $data = [
        'coupons' => $response['data'],
      ];
      return view('categories.coupon.index', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //    WARNING -- NEED TO CHECK THIS ROUTE

  /**
   * Get All Items
   */
  public function getItems(Request $request)
  {
    $response = $this->couponService->getItems($request);
    if ($response['success']) {
      $this->extractUserIds($response['data']['data']);
      $response['data']['data'] = $this->userIdToEmail($response['data']['data']);
      $data = [
        'items' => $response['data'],
      ];
      return view('categories.coupon.items', ['data' => $data]);
    } else {
      return redirect()->back()->with('error', $response['message']);
    }
  }

  //* Update view for coupon
  public function updateView(Request $request, $id)
  {
    if (!$request->user()->can('coupon.update'))
      return $this->notAllowed();
    $response = $this->couponService->getCouponByIdAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'coupon' => $response['data'],
    ];
    return view('categories.coupon.couponUpdate', ['data' => $data]);
  }

  //* Update coupon details
  public function update(Request $request, $id)
  {
    if (!$request->user()->can('coupon.update'))
      return $this->notAllowed();
    $response = $this->couponService->updateCouponAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);

    return redirect()->action([CouponController::class, 'getCoupons'])->with('message', 'Successfully Updated Cuopon Details');
  }

  //* Show Coupon Details
  public function showCoupon(Request $request, $id)
  {
    if (!$request->user()->can('coupon.view'))
      return $this->notAllowed();
    $response = $this->couponService->getCouponByIdAsAdmin($request, $id);
    if ($response['error'])
      return $this->flashError($response['message']);
    $data = [
      'coupon' => $response['data'],
    ];
    return view('categories.coupon.couponShow', ['data' => $data]);
  }


  public function create()
  {

    $data['letters'] = User::letters();
    return view('categories.coupon.create', ['data' => $data]);

  }

  public function store(CreateCouponRequest $request)
  {
    $this->couponService->createAdminCoupon($request);
    return redirect()->route('coupon');

  }
}
