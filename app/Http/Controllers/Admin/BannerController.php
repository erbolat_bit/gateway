<?php

namespace App\Http\Controllers\Admin;

use App\AdminServices\Banner\BannerService;
use App\Http\Controllers\Controller;
use App\Http\Requests\BannerRequest;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    private $service;

    public function __construct(BannerService $bannerService)
    {
        $this->service = $bannerService;
    }

    public function index()
    {
        $data = $this->service->getBannerList();

        return view('categories.banners.index', ['banners' => $data]);
    }

    public function create()
    {
        return view('categories.banners.create');
    }

    public function store(BannerRequest $request)
    {
        $this->service->createBanner($request);

        return redirect()->route('banners.index');        
    }

    public function edit($id)
    {
        $model = $this->service->getBunner($id);

        return view('categories.banners.edit', ['model' => $model]);
    }

    public function update(BannerRequest $request, $id){
        
        $this->service->updateBanner($request, $id);
        return redirect()->route('banners.index');
    }

    public function destroy($id)
    {
        $this->service->deleteBanner($id);
        return redirect()->route('banners.index');
    }
}
