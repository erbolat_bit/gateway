<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\BaseController;
use App\Services\Cart\BalanceService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BalanceController extends BaseController
{

  use ResponseTrait;

	public $balanceService;

	public function __construct(BalanceService $balanceService)
	{
		$this->balanceService = $balanceService;
	}

  /**
	 * @group Cart
	 * 
	 * @bodyParam products.*.product_id integer required
	 * @bodyParam products.*.product_title string required
	 * @bodyParam products.*.store_id integer required
	 * @bodyParam products.*.store_title string required
	 * @bodyParam products.*.amount integer required
	 * @bodyParam products.*.currency integer required
	 * @bodyParam products.*.discount string required
	 * @bodyParam products.*.quantity integer required
	 * @bodyParam products.*.delivery_method integer required
	 * @bodyParam products.*.delivery_price integer required
	 * @bodyParam address_id integer required
	 * @bodyParam payment integer required
	 *
	 * @response {
	 *		"id": 1,
	 *		"user_id": "1",
	 *		"address_id": "0",
	 *		"payment": "2",
	 *		"status": null,
	 *		"created_at": "16-10-2020 07:20",
	 *		"items": {
	 *				"data": [
	 *						{
	 *								"id": 1,
	 *								"order_id": 1,
	 *								"product_id": 1,
	 *								"product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *								"store_id": 12,
	 *								"store_title": "Всё для дома",
	 *								"amount": "11644132226.10",
	 *								"currency": "UZS",
	 *								"discount": "0.00",
	 *								"quantity": 2,
	 *								"delivery_method": 0,
	 *								"delivery_price": "0.00",
	 *								"status": 0,
	 *								"has_review": false,
	 *								"photo": null
	 *						},
	 *						{
	 *								"id": 2,
	 *								"order_id": 1,
	 *								"product_id": 2,
	 *								"product_title": "Phone",
	 *								"store_id": 11,
	 *								"store_title": "sdas",
	 *								"amount": "3000000.00",
	 *								"currency": "UZS",
	 *								"discount": "0.00",
	 *								"quantity": 3,
	 *								"delivery_method": 1,
	 *								"delivery_price": "10000.00",
	 *								"status": 0,
	 *								"has_review": false,
	 *								"photo": null
	 *						}
	 *				]
	 *		}
	 *	}
	 */
	public function store(Request $request)
	{
		$res = $this->balanceService->balanceReplenishment($request);
		return $this->successResponse(isset($res['error']) ? $res : $res['data']);
	}
}