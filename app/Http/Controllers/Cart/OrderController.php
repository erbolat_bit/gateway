<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\BaseController;
use App\Services\Cart\OrderService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
	use ResponseTrait;

	public $orderService;

	public function __construct(OrderService $orderService)
	{
		$this->orderService = $orderService;
	}

	/**
	 * @group Cart
	 * 
	 * @response {
	 *         "order": {
	 *             "data": [
	 *                 {
	 *                     "id": 1,
	 *                     "user_id": 1,
	 *                     "address_id": 0,
	 *                     "payment": 2,
	 *                     "status": 0,
	 *                     "created_at": "16-10-2020 07:20",
	 *                     "items": {
	 *                         "data": [
	 *                             {
	 *                                 "id": 1,
	 *                                 "order_id": 1,
	 *                                 "product_id": 1,
	 *                                 "product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *                                 "store_id": 12,
	 *                                 "store_title": "Всё для дома",
	 *                                 "amount": "11644132226.10",
	 *                                 "currency": "UZS",
	 *                                 "discount": "0.00",
	 *                                 "quantity": 2,
	 *                                 "delivery_method": 0,
	 *                                 "delivery_price": "0.00",
	 *                                 "status": 0,
	 *                                 "has_review": false,
	 *                                 "photo": null
	 *                             },
	 *                             {
	 *                                 "id": 2,
	 *                                 "order_id": 1,
	 *                                 "product_id": 2,
	 *                                 "product_title": "Phone",
	 *                                 "store_id": 11,
	 *                                 "store_title": "sdas",
	 *                                 "amount": "3000000.00",
	 *                                 "currency": "UZS",
	 *                                 "discount": "0.00",
	 *                                 "quantity": 3,
	 *                                 "delivery_method": 1,
	 *                                 "delivery_price": "10000.00",
	 *                                 "status": 0,
	 *                                 "has_review": false,
	 *                                 "photo": null
	 *                             }
	 *                         ]
	 *                     }
	 *                 }
	 *             ],
	 *             "meta": {
	 *                 "pagination": {
	 *                     "total": 1,
	 *                     "count": 1,
	 *                     "per_page": 15,
	 *                     "current_page": 1,
	 *                     "total_pages": 1,
	 *                     "links": []
	 *                 }
	 *             }
	 *         },
	 *         "count": {
	 *             "complated_count": 1,
	 *             "delivery_count": 0
	 *         }
	 *     }
	 */
	public function getUserOrders(Request $request)
	{
		return $this->successResponse($this->orderService->getUserOrders($request));
	}


	/**
	 * @group Cart
	 * 
	 * @response {
	 *         "order": {
	 *             "data": [
	 *                 {
	 *                     "id": 1,
	 *                     "user_id": 1,
	 *                     "address_id": 0,
	 *                     "payment": 2,
	 *                     "status": 0,
	 *                     "created_at": "16-10-2020 07:20",
	 *                     "items": {
	 *                         "data": [
	 *                             {
	 *                                 "id": 1,
	 *                                 "order_id": 1,
	 *                                 "product_id": 1,
	 *                                 "product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *                                 "store_id": 12,
	 *                                 "store_title": "Всё для дома",
	 *                                 "amount": "11644132226.10",
	 *                                 "currency": "UZS",
	 *                                 "discount": "0.00",
	 *                                 "quantity": 2,
	 *                                 "delivery_method": 0,
	 *                                 "delivery_price": "0.00",
	 *                                 "status": 0,
	 *                                 "has_review": false,
	 *                                 "photo": null
	 *                             },
	 *                             {
	 *                                 "id": 2,
	 *                                 "order_id": 1,
	 *                                 "product_id": 2,
	 *                                 "product_title": "Phone",
	 *                                 "store_id": 11,
	 *                                 "store_title": "sdas",
	 *                                 "amount": "3000000.00",
	 *                                 "currency": "UZS",
	 *                                 "discount": "0.00",
	 *                                 "quantity": 3,
	 *                                 "delivery_method": 1,
	 *                                 "delivery_price": "10000.00",
	 *                                 "status": 0,
	 *                                 "has_review": false,
	 *                                 "photo": null
	 *                             }
	 *                         ]
	 *                     }
	 *                 }
	 *             ],
	 *             "meta": {
	 *                 "pagination": {
	 *                     "total": 1,
	 *                     "count": 1,
	 *                     "per_page": 15,
	 *                     "current_page": 1,
	 *                     "total_pages": 1,
	 *                     "links": []
	 *                 }
	 *             }
	 *         },
	 *         "count": {
	 *             "complated_count": 1,
	 *             "delivery_count": 0
	 *         }
	 *     }
	 */
	public function getUserTransactions(Request $request)
	{
		return $this->successResponse($this->orderService->getUserTransactions($request));
	}

	/**
	 * @group Cart
	 * 
	 * @bodyParam products.*.product_id integer required
	 * @bodyParam products.*.product_title string required
	 * @bodyParam products.*.store_id integer required
	 * @bodyParam products.*.store_title string required
	 * @bodyParam products.*.amount integer required
	 * @bodyParam products.*.currency integer required
	 * @bodyParam products.*.discount string required
	 * @bodyParam products.*.quantity integer required
	 * @bodyParam products.*.delivery_method integer required
	 * @bodyParam products.*.delivery_price integer required
	 * @bodyParam address_id integer required
	 * @bodyParam payment integer required
	 *
	 * @response {
	 *		"id": 1,
	 *		"user_id": "1",
	 *		"address_id": "0",
	 *		"payment": "2",
	 *		"status": null,
	 *		"created_at": "16-10-2020 07:20",
	 *		"items": {
	 *				"data": [
	 *						{
	 *								"id": 1,
	 *								"order_id": 1,
	 *								"product_id": 1,
	 *								"product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *								"store_id": 12,
	 *								"store_title": "Всё для дома",
	 *								"amount": "11644132226.10",
	 *								"currency": "UZS",
	 *								"discount": "0.00",
	 *								"quantity": 2,
	 *								"delivery_method": 0,
	 *								"delivery_price": "0.00",
	 *								"status": 0,
	 *								"has_review": false,
	 *								"photo": null
	 *						},
	 *						{
	 *								"id": 2,
	 *								"order_id": 1,
	 *								"product_id": 2,
	 *								"product_title": "Phone",
	 *								"store_id": 11,
	 *								"store_title": "sdas",
	 *								"amount": "3000000.00",
	 *								"currency": "UZS",
	 *								"discount": "0.00",
	 *								"quantity": 3,
	 *								"delivery_method": 1,
	 *								"delivery_price": "10000.00",
	 *								"status": 0,
	 *								"has_review": false,
	 *								"photo": null
	 *						}
	 *				]
	 *		}
	 *	}
	 */
	public function store(Request $request)
	{
		$res = $this->orderService->createOrder($request);
		return $this->successResponse(isset($res['error']) ? $res : $res['data']);
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam order integer required Order ID
	 *
	 * @response {
	 * 	data: 1
	 * }
	 */
	public function destroy(Request $request, $order)
	{
		return $this->successResponse($this->orderService->deleteOrder($request, $order));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam order integer required Order ID
	 *
	 * @response {
	 *     "id": 1,
	 *     "user_id": 1,
	 *     "address_id": 0,
	 *     "payment": 2,
	 *     "status": 0,
	 *     "created_at": "16-10-2020 07:20",
	 *     "items": {
	 *         "data": [
	 *             {
	 *                 "id": 1,
	 *                 "order_id": 1,
	 *                 "product_id": 1,
	 *                 "product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *                 "store_id": 12,
	 *                 "store_title": "Всё для дома",
	 *                 "amount": "11644132226.10",
	 *                 "currency": "UZS",
	 *                 "discount": "0.00",
	 *                 "quantity": 2,
	 *                 "delivery_method": 0,
	 *                 "delivery_price": "0.00",
	 *                 "status": 0,
	 *                 "has_review": false,
	 *                 "photo": null
	 *             },
	 *             {
	 *                 "id": 2,
	 *                 "order_id": 1,
	 *                 "product_id": 2,
	 *                 "product_title": "Phone",
	 *                 "store_id": 11,
	 *                 "store_title": "sdas",
	 *                 "amount": "3000000.00",
	 *                 "currency": "UZS",
	 *                 "discount": "0.00",
	 *                 "quantity": 3,
	 *                 "delivery_method": 1,
	 *                 "delivery_price": "10000.00",
	 *                 "status": 0,
	 *                 "has_review": false,
	 *                 "photo": null
	 *             }
	 *         ]
	 *     }
	 * }
	 */
	public function show(Request $request, $order)
	{
		return $this->successResponse($this->orderService->getOrderById($request, $order));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam item integer required Order Item ID
	 *
	 * @response {
	 *     "id": 1,
	 *     "order_id": 1,
	 *     "product_id": 1,
	 *     "product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *     "store_id": 12,
	 *     "store_title": "Всё для дома",
	 *     "amount": "11644132226.10",
	 *     "currency": "UZS",
	 *     "discount": "0.00",
	 *     "quantity": 2,
	 *     "delivery_method": 0,
	 *     "delivery_price": "0.00",
	 *     "status": 1,
	 *     "has_review": false,
	 *     "photo": null
	 * }
	 */
	public function confirmOrderItem(Request $request, $item)
	{
		return $this->successResponse($this->orderService->confirmOrderItem($request, $item));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam item integer required Order Item ID
	 *
	 * @response {
	 *     "id": 1,
	 *     "order_id": 1,
	 *     "product_id": 1,
	 *     "product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *     "store_id": 12,
	 *     "store_title": "Всё для дома",
	 *     "amount": "11644132226.10",
	 *     "currency": "UZS",
	 *     "discount": "0.00",
	 *     "quantity": 2,
	 *     "delivery_method": 0,
	 *     "delivery_price": "0.00",
	 *     "status": 1,
	 *     "has_review": false,
	 *     "photo": null
	 * }
	 */
	public function reviewOrderItem(Request $request, $item)
	{
		return $this->successResponse($this->orderService->reviewOrderItem($request, $item));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam item integer required Order Item ID
	 *
	 * @response {
	 *     "id": 1,
	 *     "order_id": 1,
	 *     "product_id": 1,
	 *     "product_title": "MSI GS65 STEALTH 9SG-641RU",
	 *     "store_id": 12,
	 *     "store_title": "Всё для дома",
	 *     "amount": "11644132226.10",
	 *     "currency": "UZS",
	 *     "discount": "0.00",
	 *     "quantity": 2,
	 *     "delivery_method": 0,
	 *     "delivery_price": "0.00",
	 *     "status": 1,
	 *     "has_review": false,
	 *     "photo": null
	 * }
	 */
	public function getOrderItem(Request $request, $item)
	{
		return $this->successResponse($this->orderService->getOrderItem($request, $item));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam item integer required Order Item ID
	 *
	 * @response {
	 * 	data: 1
	 * }
	 */
	public function deleteOrderItem(Request $request, $item)
	{
		return $this->successResponse($this->orderService->deleteOrderItem($request, $item));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam item integer required Order Item ID
	 *
	 * @response [
	 * 		1,
	 * 		2
	 *  ]
	 */
	public function getStoreUserIds(Request $request, $store_id)
	{

		return $this->successResponse($this->orderService->getStoreUserIds($request, $store_id));
	}

	/**
	 * @group Cart
	 * 
	 * @urlParam order_id integer required Order
	 *
	 * @response 
	 */
	public function update(Request $request, $order_id)
	{

		return $this->successResponse($this->orderService->updateOrder($request, $order_id));
	}

	public function checkStatus(Request $request, $order_id)
	{
		return $this->successResponse($this->orderService->checkStatus($request, $order_id));
	}
}
