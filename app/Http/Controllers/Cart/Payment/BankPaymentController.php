<?php

namespace App\Http\Controllers\Cart\Payment;

use App\Http\Controllers\BaseController;
use App\Services\Cart\OrderService;
use App\Traits\UpdateUsersBalance;
use Exception;
use Illuminate\Http\Request;

class BankPaymentController extends BaseController implements PaymentInterface
{

  use UpdateUsersBalance;

  public $orderService;

  public function __construct(OrderService $orderService)
  {
    $this->orderService = $orderService;
  }

  /**
   * 
   * Payment success endpoint
   * 
   */
  public function success(Request $request)
  {
    // $input = $request->input();
    // info('success', $input);

    // $update_data['status'] = 1;
    // $update_data['error_code'] = 0;
    // $update_data['transaction_id'] = $input['globalID'];

    // $data = $this->orderService->updateOrder($request, $input['transactionID'], $update_data);
    // $user_ids = $this->shopService->getUserIds($request, collect($data['data']['data'])->pluck('store_id')->toArray());
    // $this->updateUserBalanse($user_ids, $data['data']['data']);

    // $user_ids = collect($user_ids);
    // $order = collect($data['data']['order']);
    // $user = User::select(['id', 'name', 'path'])->where('id', $order['user_id'])->first()->toArray();
    // $user_ids = $user_ids->transform(function($item) use($order, $user){
    //   $item['recevier_id'] = $item['user_id'];
    //   $item['user'] = ['id' => $user['id'], 'name' => $user['name'], 'path' => $user['path'] ? $user['path'] : false];
    //   $item['user_id'] = $order['user_id'];
    //   $item['order_id'] = $order['id'];
    //   $item['store_title'] = isset($order['items'][$item['store_id']][0]) ? $order['items'][$item['store_id']][0]['store_title'] : null;
    //   $item['products'] = $order['items'][$item['store_id']];
    //   return $item;
    // });

    // foreach ($user_ids as $key => $message) {
    //   $this->chatService->sendMessageForOrder($request, $message);
    // }

    if ($this->orderService->paymentProcessing($request)) {
      return response()->json(['code' => 0, 'message' => 'success']);
    }else{
      throw new Exception('Error payment processing', 500);
    }

  }

  /**
   * 
   * Payment error endpoint
   * 
   */
  public function fail(Request $request)
  {
    $data = $request->input();
    info('error', $data);

    $update_data['status'] = 3;
    $update_data['error_code'] = $data['code'];
    $update_data['transaction_id'] = $data['globalID'];

    $this->orderService->updateOrder($request, $data['transactionID'], $update_data);
    return response()->json(['code' => 100, 'message' => 'error']);
  }

}
