<?php

namespace App\Http\Controllers\Cart\Payment;

use App\Models\User;
use DB;
use Illuminate\Http\Request;

class BalancePaymentController extends BankPaymentController
{

  public function success(Request $request)
  {
    $input = $request->input();
    info('success', $input);

    $update_data['status'] = 1;
    $update_data['error_code'] = 0;
    $update_data['transaction_id'] = $input['globalID'];
    $update_data['balance'] = 1;

    $data = $this->orderService->updateOrder($request, $input['transactionID'], $update_data);
    $order = $data['data']['order'];
    $this->updateUserBalance($order['user_id'], $order['total_sum']);

    return response()->json(['code' => 0, 'message' => 'success']);
  }

  public function updateUserBalance($user_id, $balance_data)
  {
      User::where('id', $user_id)
          ->update(['balance' => DB::raw('balance + ' . $balance_data)]);
  }
}