<?php

namespace App\Http\Controllers\Cart\Payment;

use Illuminate\Http\Request;

interface PaymentInterface
{

	/**
	 * Success payment endpoint
	 */
	public function success(Request $request);

	/**
	 * Error payment endpoint
	 */
	public function fail(Request $request);

	/**
	 * Update users balance if payment return success
	 */
	public function updateUserBalanse($user_ids, $balance_data);
}
