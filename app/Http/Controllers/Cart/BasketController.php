<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Services\Cart\BasketService;
use App\Services\ProductService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BasketController extends Controller
{
	use ResponseTrait;

	public $basketService;
	public $productService;

	public function __construct(BasketService $basketService, ProductService $productService)
	{
		$this->basketService = $basketService;
		$this->productService = $productService;
	}

	/**
	 * @group Cart
	 * 
	 * @response [
	 *     {
	 *         "product": {
	 *             "id": 1,
	 *             "category_id": 1,
	 *             "title": "фывфыв",
	 *             "price": 200000,
	 *             "discount_price": 300000,
	 *             "price_to_basket": 200000,
	 *             "convert_price": "200 000,00 UZS",
	 *             "convert_discount_price": "300 000,00 UZS",
	 *             "currency": {
	 *                 "id": 1,
	 *                 "title": "UZS"
	 *             },
	 *             "quantity": 2,
	 *             "quantity_size": 1,
	 *             "delivery_description": "фыв фыв фыв",
	 *             "description": "asd asd asd",
	 *             "country_id": 1,
	 *             "manufacturer_id": 2,
	 *             "detailed_description": "asd zxc zxcvxcv",
	 *             "views": 0,
	 *             "param_options": [
	 *                 {
	 *                     "id": 1,
	 *                     "parameter_id": 2,
	 *                     "value": "null",
	 *                     "type": "select"
	 *                 },
	 *                 {
	 *                     "id": 2,
	 *                     "parameter_id": 5,
	 *                     "value": "asd",
	 *                     "type": "checkbox"
	 *                 },
	 *                 {
	 *                     "id": 3,
	 *                     "parameter_id": 3,
	 *                     "value": "null",
	 *                     "type": "text"
	 *                 }
	 *             ],
	 *             "photos": [],
	 *             "tags": [
	 *                 {
	 *                     "id": 1,
	 *                     "title": "asdas"
	 *                 },
	 *                 {
	 *                     "id": 2,
	 *                     "title": "asdas"
	 *                 },
	 *                 {
	 *                     "id": 3,
	 *                     "title": "asdsa"
	 *                 }
	 *             ],
	 *             "documents": [],
	 *             "store": {
	 *                 "id": 1,
	 *                 "user_id": 1,
	 *                 "title": "title",
	 *                 "description": "description",
	 *                 "logotype": null,
	 *                 "type": 1,
	 *                 "subdomain": "my-store",
	 *                 "delivery": [
	 *                     {
	 *                         "id": "1",
	 *                         "title": "Доставка сервисом en"
	 *                     },
	 *                     {
	 *                         "id": "2",
	 *                         "title": "Самовывоз en"
	 *                     }
	 *                 ],
	 *                 "bg_color": null,
	 *                 "bg_image": null,
	 *                 "product_count": 2,
	 *                 "views": 0,
	 *                 "data": [
	 *                     {
	 *                         "title": "asd",
	 *                         "category": "address",
	 *                         "main": 1
	 *                     },
	 *                     {
	 *                         "title": "zxczxc",
	 *                         "category": "2",
	 *                         "main": 0
	 *                     }
	 *                 ]
	 *             }
	 *         },
	 *         "basket": {
	 *             "id": 1,
	 *             "user_id": 1,
	 *             "product_id": 1,
	 *             "created_at": "2020-10-16T06:42:47.000000Z",
	 *             "updated_at": "2020-10-16T06:50:26.000000Z",
	 *             "quantity": 2
	 *         }
	 *     }
	 * ]
	 */
	public function getUserBasket(Request $request)
	{
		$baskets = collect($this->basketService->getUserBasket($request)['data']);
		if ($baskets->isEmpty()) {
			return $this->successResponse($baskets->toArray());
		}

		$product_ids = $baskets->pluck('product_id')->toArray();
		$products = collect($this->productService->userBasketProducts($request, $product_ids));

		$baskets->transform(function ($item) use ($products) {
			$product = $products->where('id', $item['product_id'])->first();

			return [
				'product' => $product,
				'basket' => $item
			];
		});
		return $this->successResponse($baskets->toArray());
		
		// $products->transform(function ($item) use ($baskets) {
		// 	$basket = $baskets->where('product_id', $item['id'])->first();
		// 	$item['basket'] = $basket;
		// 	return $item;
		// });

		// return $this->successResponse($products->groupBy('store_id')->toArray());
	}

	/**
	 * @group Cart
	 * 
	 * @bodyParam product_id integer required Product item ID 
	 *
	 * @response {
	 *         "id": 1,
	 *         "product_id": "1",
	 *         "quantity": null
	 *     }
	 */
	public function store(Request $request)
	{
		return $this->successResponse($this->basketService->createBasket($request));
	}

	/**
	 * @group Cart
	 * 
	 * @bodyParam basket integer required Basket ID 
	 *
	 * @response {
	 *   "data": 1,
	 * }
	 */
	public function destroy(Request $request, $basket)
	{
		return $this->successResponse($this->basketService->deleteBasket($request, $basket));
	}
}
