<?php

namespace App\Http\Controllers\Poll;

use App\Http\Controllers\Controller;
use App\Jobs\UserBonuses;
use App\Services\Poll\PollService;
use App\Services\ShopService;
use App\Traits\ResponseTrait;
use App\UserClasses\UserBonus;
use Illuminate\Http\Request;

class PollController extends Controller
{
  use ResponseTrait;

  public $pollService;

  public function __construct(PollService $pollS)
  {
    $this->pollService = $pollS;
  }

  public function store(Request $request)
  {
    return $this->successResponse($this->pollService->store($request));
  }

  public function showPoll(Request $request, $id)
  {
    return $this->successResponse($this->pollService->showPoll($request, $id));
  }

  public function getShopPolls(Request $request, $store)
  {
    return $this->successResponse($this->pollService->getShopPolls($request, $store));
  }

  public function getUserPolls(Request $request)
  {
    return $this->successResponse($this->pollService->getUserPolls($request));
  }

  public function getPollVotes(Request $request, $id)
  {
    return $this->successResponse($this->pollService->getPollVotes($request, $id));
  }

  public function deletePoll(Request $request, $id)
  {
    return $this->successResponse($this->pollService->deletePoll($request, $id));
  }

  public function deleteQuestion(Request $request, $id)
  {
    return $this->successResponse($this->pollService->deleteQuestion($request, $id));
  }

  public function deleteAnswer(Request $request, $id)
  {
    return $this->successResponse($this->pollService->deleteAnswer($request, $id));
  }

  public function deletePhotoPoll(Request $request, $id)
  {
    return $this->successResponse($this->pollService->deletePhotoPoll($request, $id));
  }

  public function deleteQuestionPhoto(Request $request, $id)
  {
    return $this->successResponse($this->pollService->deleteQuestionPhoto($request, $id));
  }

  public function deleteAnswerPhoto(Request $request, $id)
  {
    return $this->successResponse($this->pollService->deleteAnswerPhoto($request, $id));
  }

  public function vote(Request $request)
  {
    $poll = $this->pollService->vote($request);
    if (isset($poll['error']) && $poll['error']) {
      return $this->successResponse($poll);
    }

    $user = $request->user();

    if (!$this->createBonus($request, $user, $poll)) {
      return $this->errorResponse('Server error');
    }
    
    $user->bonuses += $poll['bonuses'];
    $user->save(); 
    
    return $this->successResponse('Poll passed');
  }

  public function updatePoll(Request $request, $id)
  {
    return $this->successResponse($this->pollService->updatePoll($request, $id));
  }

  private function createBonus($request, $user, $poll){

    $bonus = new UserBonus();
		$bonus->setName($poll['title']);
		$bonus->setBonus($poll['bonuses']);
		$bonus->setType('poll');
		$bonus->setTypeId($poll['store_id']);
    $this->dispatch(new UserBonuses($user, $bonus));
    
    return true;
	}
}
