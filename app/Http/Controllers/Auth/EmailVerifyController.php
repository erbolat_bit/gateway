<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Jobs\UserEmailVerify;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EmailVerifyController extends BaseController
{
  /**
   * @group Auth
   * @urlParam id integer required User.
   * @urlParam hash string required.
   * @response {
   * 	"id": 1
   * 	"name": "name",
   * 	"email": "email@email.com"
   *  "access_token": "token"
   * }
   */
  public function verify(Request $request, $id, $hash)
  {
    $user = User::find($id);

    if (!$request->hasValidSignature())
      return $this->errorResponse('Invalid/Expired url provided', [], 401);

    if ($user->email_verified_at)
      return $this->successResponse('You are already verified email', 208);

    $user->update(['email_verified_at' => now()]);
    Auth::login($user);

    $data = new UserResource($user, true);

    return $this->successResponse($data->toArray($request));
  }

  /**
   * @group Auth
   * @bodyParam email string required User email.
   * @response {
   *  "data": "Resend"
   * }
   */
  public function resend(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email' => 'required|email'
    ]);

    if ($validator->fails()) {
      return $this->errorResponse($validator->errors());
    }

    $user = User::where('email', $request->email)->first();

    if ($user->email_verified_at)
      return $this->successResponse('You are already verified email', 208);

    $this->dispatch(new UserEmailVerify($user));
    return $this->successResponse('Resend');
  }
}
