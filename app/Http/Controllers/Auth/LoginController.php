<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends BaseController
{
  /**
   * @group Auth
   * @bodyParam email string required.
   * @bodyParam password string required.
   * @bodyParam remember_me bool nullable
   * @response {
   * 	"id": 1
   * 	"name": "name",
   * 	"email": "email@email.com"
   *  "access_token": "token"
   * }
   */
  public function __invoke(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email' => 'required|email',
      'password' => 'required|string',
      'remember_me' => 'nullable|boolean'
    ]);

    if ($validator->fails()) {
      return $this->errorResponse($validator->errors());
    }

    if (!Auth::guard('web')->attempt($request->only(['email', 'password']), $request->remember_me))
      return $this->errorResponse('No login');
    
    if (!$request->user()->email_verified_at)
      return $this->errorResponse('Not verified');

    $request->user()->tokens()->delete();
    $data = new UserResource($request->user(), true);

    return $this->successResponse($data);
  }
}
