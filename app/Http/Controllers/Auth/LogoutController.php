<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

class LogoutController extends BaseController
{
    /**
     * @group Auth
     * @response {
     * 	"data": "User logout"
     * }
     */
    public function __invoke(Request $request)
    {
        $request->user()->tokens()->delete();
        return $this->successResponse('User logout');
    }
}
