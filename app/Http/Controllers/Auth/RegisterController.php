<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Jobs\UserBonuses;
use App\Jobs\UserEmailVerify;
use App\Models\User;
use App\Services\User\ReferralService;
use App\UserClasses\UserBonus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends BaseController
{

	public $referralService;

	public function __construct(ReferralService $referralService){
		$this->referralService = $referralService;
	}

	/**
	 * @group Auth
	 * @bodyParam  name string required.
	 * @bodyParam  email string required.
	 * @bodyParam  password string required.
	 * @bodyParam  password_confirmation required.
	 * @response {
	 * 	"id": 1
	 * 	"name": "name",
	 * 	"email": "email@email.com"
	 * }
	 */
	public function index(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'email' => 'required|email|unique:users',
			'password' => 'required|string|confirmed|min:6',
		]);

		if ($validator->fails()) {
			return $this->errorResponse($validator->errors());
		}
		$input = $request->toArray();
		$input['password'] = bcrypt($input['password']);

		if ($request->has('referral_token')) {
			
			$user = $this->createReferralUser($request, $input);

		}else{
			$user = new User($input);
			if ($user->save()) {
				$user->update(['nickname' => '~user' . $user->id]);
				$this->dispatch(new UserEmailVerify($user));
			}
		}
		

		return $this->successResponse(new UserResource($user));
	}

	private function createReferralUser($request, $input){

		$parent = User::find($this->referralService->getReferralByToken($request, $request->referral_token)['user_id']);

		$parent->bonuses += config('referral_bonuses.register');
		$parent->save();


		$input['parent_id'] = $parent->id;
		$user = new User($input);
		
		if ($user->save()) {
			$user->update(['nickname' => '~user' . $user->id]);
			$this->referralService->createReferral($request, [
				'user_id' => $user->id,
				'bonuses' => config('referral_bonuses.register')
			]);
			$this->dispatch(new UserEmailVerify($user));
		}
		$this->createBonus($request, $parent, $user);

		return $user;
	}

	private function createBonus($request, $user, $referral){

		$bonus = new UserBonus();
		$bonus->setName('Referral ' . $referral->name);
		$bonus->setBonus(config('referral_bonuses.register'));
		$bonus->setType('referral');
		$bonus->setTypeId($referral->id);
		
		$this->dispatch(new UserBonuses($user, $bonus));
	}
}
