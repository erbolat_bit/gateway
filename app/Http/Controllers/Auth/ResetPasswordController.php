<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Jobs\ResetPassword;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ResetPasswordController extends BaseController
{
    /**
     * @group Auth
     * @bodyParam email string required User email.
     * @response {
     *  "data": "Send email to email@email.com"
     * }
     */
    public function forgot(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors());
        }

        $user = User::where('email', $request->email)->first();
        
        if(!$user)
            return $this->errorResponse('User not found', [], 404);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60),
            'created_at' => now()
        ]);
        
        $this->dispatch(new ResetPassword($user));

        return $this->successResponse($user->email, "Send email to " . $user->email);
    }

    /**
     * @group Auth
     * @bodyParam email string required User email.
     * @bodyParam token string required Reset token.
     * @bodyParam password string required New password.
     * @bodyParam password_confirmation required New password.
     * @response {
     * 	"id": 1
     * 	"name": "name",
     * 	"email": "email@email.com"
     *  "access_token": "token"
     * }
     */
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors());
        }

        $tokenData = DB::table('password_resets')->where('token', $request->token)->first();

        if (!$tokenData)
            return $this->errorResponse('Token is invalid');

        $user = User::where('email', $tokenData->email)->first();

        if (!$user)
            return $this->errorResponse('User not found', [], 404);

        $user->update(['password' => bcrypt($request->password)]);

        Auth::login($user);
        DB::table('password_resets')->where('token', $request->token)->delete();
        $request->user()->tokens()->delete();
        
        $data = new UserResource($user, true);
        
        return $this->successResponse($data);
    }
}
