<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends BaseController
{
  
  /**
	 * @group Auth
	 * @urlParam  provider string required.
	 * @response {
	 * 	"id": 1
	 * 	"name": "name",
	 * 	"email": "email@email.com",
   *  "access_token": "token"
	 * }
	*/
  public function __invoke(Request $request, $provider)
  {
    
    $user_social_data = Socialite::driver($provider)->stateless()->user();
    $user = User::where('email', $user_social_data->getEmail())->first();

    if (!$user) {
      $user = new User([
          'email' => $user_social_data->getEmail(),
          'name' => $user_social_data->getName() ? $user_social_data->getName() : $user_social_data->getNickname(),
          'password' => bcrypt(Str::random(8)),
          'email_verified_at' => now(),
      ]);
      $user->save();
    }

    return $this->successResponse(new UserResource($user, true));
  }
}
