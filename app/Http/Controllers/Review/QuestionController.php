<?php

namespace App\Http\Controllers\Review;

use App\Http\Controllers\BaseController;
use App\Services\QuestionService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class QuestionController extends BaseController
{
  
  use ResponseTrait;

  public $questionService;

  public function __construct(QuestionService $questionService)
  {
    $this->questionService = $questionService;
  }
  
  /**
   * @group Review
   * 
   * @urlParam product_id integer required Product ID
   * 
   * @response {
   *  "data": [
   *    {
   *      "id": 2,
   *      "product_id": 2,
   *      "user_id": 1,
   *      "text": "asdas",
   *      "created": "Mon, Oct 12, 2020 1:56 PM",
   *      "cout_likes": {
   *          "likes": 0,
   *          "dislikes": 0
   *      },
   *      "total_replies": 0,
   *      "replies": null,
   *      "user": {
   *          "id": 1,
   *          "name": "Username",
   *          "email": "asd@asd.asd"
   *      }
   *    }
   *  ],
   *  "meta": {
   *    "pagination": {
   *      "total": 3,
   *      "count": 3,
   *      "per_page": 5,
   *      "current_page": 1,
   *      "total_pages": 1,
   *      "links": []
   *    }
   *  }
   * }
   */
  public function getQuestionsByProduct(Request $request, $product_id)
  {
    return $this->successResponse($this->questionService->getQuestionsByProduct($request, $product_id));
  }
  
  /**
   * store
   *
   * @param  mixed $request
   * @param  mixed $product_id
   * @return void
   */
  public function store(Request $request, $product_id)
  {
    return $this->successResponse($this->questionService->createQuestion($request, $product_id));
  }
  
  /**
   * destroy
   *
   * @param  mixed $request
   * @param  mixed $product_id
   * @return void
   */
  public function destroy(Request $request, $product_id)
  {
    return $this->successResponse($this->questionService->deleteQuestion($request, $product_id));
  }
}
