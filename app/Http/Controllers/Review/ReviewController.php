<?php

namespace App\Http\Controllers\Review;

use App\Http\Controllers\BaseController;
use App\Services\ProductService;
use App\Services\ReviewService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ReviewController extends BaseController
{
  
  use ResponseTrait;

  public $reviewService;
  public $productService;

  public function __construct(ReviewService $reviewService, ProductService $productService)
  {
    $this->reviewService = $reviewService;
    $this->productService = $productService;
  }
  
  /**
   * @group Review
   * 
   * @urlParam product_id integer required Product ID
   * 
   * @response {
   *   "reviews": 0,
   *   "questions": 0
   * }
   */
  public function getReviewsQuestionsCount(Request $request, $product_id)
  {
    return $this->successResponse($this->reviewService->getReviewsQuestionsCount($request, $product_id));
  }
  
  /**
   * @group Review
   * 
   * @urlParam product_id integer required Product ID
   * 
   * @response {
   *   "data": [
   *     {
   *       "id": 1,
   *       "product_id": 2,
   *       "stars": 3,
   *       "advantages": "asd",
   *       "disadvantages": "asd",
   *       "review": "asdasd",
   *       "cout_likes": {
   *         "likes": 0,
   *         "dislikes": 0
   *       },
   *       "user_like_active": [],
   *       "created": "Mon, Oct 12, 2020 12:02 PM",
   *       "user": {
   *         "id": 1,
   *         "name": "Username",
   *         "email": "asd@asd.asd"
   *       }
   *       "images": {
   *         "data": [
   *           {
   *             "id": 2,
   *             "links": {
   *               "original": "/uploads/reviews/1602505142_238.png",
   *               "thumb": "/uploads/reviews/thumb/1602505142.png"
   *             }
   *           }
   *         ]
   *       }
   *     }
   *   ],
   *   "meta": {
   *       "pagination": {
   *           "total": 14,
   *           "count": 5,
   *           "per_page": 5,
   *           "current_page": 1,
   *           "total_pages": 3,
   *           "links": {
   *               "next": "http://uzexpress-review.loc/reviews/2?page=2"
   *           }
   *       }
   *   }
   * }
   */
  public function getReviewsByProduct(Request $request, $product_id)
  {
    return $this->successResponse($this->reviewService->getReviewsByProduct($request, $product_id));
  }
  
  /**
   * @group Review
   * 
   * @urlParam id integer required Review ID
   * 
   * @response {
   *   "id": 1,
   *   "review_id": 19,
   *   "original_link": "/uploads/reviews/1602505054_832.png",
   *   "thumb_link": "/uploads/reviews/thumb/1602505055.png"
   * }
   */
  public function getReviewImages(Request $request, $id)
  {
    return $this->successResponse($this->reviewService->getReviewImages($request, $id));
  }
  
  /**
   * @group Review
   * 
   * @urlParam product_id integer required Product ID
   * 
   * @response {
   *   "product_id": 2,
   *   "total_rating": null,
   *   "rates": [
   *      {
   *          "1": 0
   *      },
   *      {
   *          "2": 0
   *      },
   *      {
   *          "3": 0
   *      },
   *      {
   *          "4": 0
   *      },
   *      {
   *          "5": 0
   *      }
   *   ]
   * }
   */
  public function getReviewRateCount(Request $request, $product_id)
  {
    return $this->successResponse($this->reviewService->getReviewRateCount($request, $product_id));
  }
  
  /**
   * @group Review
   * 
   * @urlParam shop_id integer required Shop ID
   * 
   * @response {
   *   "data": [
   *     {
   *       "id": 1,
   *       "product_id": 2,
   *       "stars": 3,
   *       "advantages": "asd",
   *       "disadvantages": "asd",
   *       "review": "asdasd",
   *       "cout_likes": {
   *         "likes": 0,
   *         "dislikes": 0
   *       },
   *       "user_like_active": [],
   *       "created": "Mon, Oct 12, 2020 12:02 PM",
   *       "user": {
   *         "id": 1,
   *         "name": "Username",
   *         "email": "asd@asd.asd"
   *       }
   *       "images": {
   *         "data": [
   *           {
   *             "id": 2,
   *             "links": {
   *               "original": "/uploads/reviews/1602505142_238.png",
   *               "thumb": "/uploads/reviews/thumb/1602505142.png"
   *             }
   *           }
   *         ]
   *       }
   *     }
   *   ],
   *   "meta": {
   *       "pagination": {
   *           "total": 14,
   *           "count": 5,
   *           "per_page": 5,
   *           "current_page": 1,
   *           "total_pages": 3,
   *           "links": {
   *               "next": "http://uzexpress-review.loc/reviews/2?page=2"
   *           }
   *       }
   *   }
   * }
   */
  public function getShopReviews(Request $request, $shop_id)
  {
    $product_ids = collect($this->productService->getAllProductsByShop($request, $shop_id))->pluck('id')->toArray();
    return $this->successResponse($this->reviewService->getShopReviews($request, $shop_id, $product_ids));
  }
  
  /**
   * @group Review
   * 
   * @urlParam shop_id integer required Shop ID
   * 
   * @response {
   *   "total_rating": 3,
   *   "rates": [
   *     {
   *         "1": 0
   *     },
   *     {
   *         "2": 0
   *     },
   *     {
   *         "3": 100
   *     },
   *     {
   *         "4": 0
   *     },
   *     {
   *         "5": 0
   *     }
   *   ]
   * }
   */
  public function getShopRates(Request $request, $shop_id)
  {
    $product_ids = collect($this->productService->getAllProductsByShop($request, $shop_id))->pluck('id')->toArray();
    return $this->successResponse($this->reviewService->getShopRates($request, $shop_id, $product_ids));
  }
  
  /**
   * @group Review
   * 
   * @queryParam product_ids.* integer required Product ID
   * 
   * @response {
   *     "1": {
   *         "rate": 0,
   *         "count": 0
   *     },
   *     "2": {
   *         "rate": 0,
   *         "count": 0
   *     }
   * }
   */
  public function getRatesByProducts(Request $request)
  {
    if (!$request->has('product_ids'))
      return $this->errorResponse('product_ids is required');

    return $this->successResponse($this->reviewService->getRatesByProducts($request, $request->product_ids));
  }
  
  /**
   * @group Review
   * 
   * @urlParam product_id integer required Product ID
   * 
   * @response {
   *   "id": 20,
   *   "product_id": "2",
   *   "stars": "3",
   *   "advantages": "asd",
   *   "disadvantages": "asd",
   *   "review": "asdasd",
   *   "cout_likes": {
   *     "likes": 0,
   *     "dislikes": 0
   *   },
   *   "user_like_active": [
   *     {
   *       "like": false,
   *       "dislike": false
   *     }
   *   ],
   *   "created": "Mon, Oct 12, 2020 12:19 PM",
   *   "images": {
   *     "data": [
   *       {
   *         "id": 2,
   *         "links": {
   *           "original": "/uploads/reviews/1602505142_238.png",
   *           "thumb": "/uploads/reviews/thumb/1602505142.png"
   *         }
   *       }
   *     ]
   *   }
   * }
   */
  public function store(Request $request, $product_id)
  {
    return $this->successResponse($this->reviewService->store($request, $product_id));
  }
  
  /**
   * @group Review
   * 
   * @urlParam model string required (reviews|questions|comment)
   * @urlParam id integer required (Reviews|Questions|Comment) ID
   * @bodyParam liked boolean required
   * 
   * @response {
   *  "liked": "1"
   * }
   */
  public function like(Request $request, $model, $id)
  {
    return $this->successResponse($this->reviewService->like($request, $model, $id));
  }
}
