<?php

namespace App\Http\Controllers\Review;

use App\Http\Controllers\BaseController;
use App\Services\CommentService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class CommentController extends BaseController
{
  
  use ResponseTrait;
  
  public $commentService;

  public function __construct(CommentService $commentService)
  {
    $this->commentService = $commentService;
  }
  
  /**
   * @group Review
   * 
   * @urlParam post_id integer required Post ID
   * 
   * @response {
   *  "data": [
   *    {
   *      "id": 1,
   *      "comment": "text",
   *      "user_id": 1,
   *      "cout_likes": {
   *        "likes": 0,
   *        "dislikes": 0
   *      },
   *      "replies": null,
   *      "replies_count": 0,
   *      "parent_id": 0,
   *      "created": "2020-10-12T13:32:44.000000Z",
   *      "user": {
   *        "id": 1,
   *        "name": "Username",
   *        "email": "asd@asd.asd"
   *      }
   *    }
   *  ],
   *  "links": {
   *    "first": "http://uzexpress-review.loc/post/comments/list/1?page=1",
   *    "last": "http://uzexpress-review.loc/post/comments/list/1?page=1",
   *    "prev": null,
   *    "next": null
   *  },
   *  "meta": {
   *    "current_page": 1,
   *    "from": 1,
   *    "last_page": 1,
   *    "path": "http://uzexpress-review.loc/post/comments/list/1",
   *    "per_page": 5,
   *    "to": 3,
   *    "total": 3
   *  }
   * }
   */
  public function getCommentsByPost(Request $request, $post_id)
  {
    return $this->successResponse($this->commentService->getCommentsByPost($request, $post_id));
  }
  
  /**
   * @group Review
   * 
   * @urlParam post_id integer required Post ID
   * 
   * @response {
   *  "data": 0
   * }
   */
  public function getCommentsCountByPost(Request $request, $post_id)
  {
    return $this->successResponse($this->commentService->getCommentsCountByPost($request, $post_id));
  }
  
  /**
   * @group Review
   * 
   * @bodyParam post_id integer required Post ID
   * @bodyParam parent_id integer nullable
   * @bodyParam text string nullable
   * 
   * @response  {
   *  "id": 3,
   *  "comment": "text",
   *  "user_id": "1",
   *  "cout_likes": {
   *   "likes": 0,
   *   "dislikes": 0
   *  },
   *  "replies": null,
   *  "replies_count": 0,
   *  "parent_id": "0",
   *  "created": "2020-10-12T13:37:30.000000Z",
   *  "user": {
   *    "id": 1,
   *    "name": "Username",
   *    "email": "asd@asd.asd"
   *  }
   * }
   */
  public function store(Request $request)
  {
      return $this->successResponse($this->commentService->createComment($request));
  }
}
