<?php

namespace App\Http\Controllers\Coupon;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\Cart\OrderService;
use App\Services\CouponService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class CouponController extends BaseController
{

  use ResponseTrait;

  public $couponService;
  public $orderService;

  public function __construct(CouponService $couponService, OrderService $orderService)
  {
    $this->couponService = $couponService;
    $this->orderService = $orderService;
  }
  
  /**
   * @group Coupon
   * 
   * 
   * @response [
   *     {
   *         "id": 2,
   *         "title": "asd",
   *         "description": "asdasd",
   *         "image": "/uploads/coupon/kXyJRW1jtD2eMHw9qH70sBedbLYVYsUuqiSSsMwO.png",
   *         "percent": 20,
   *         "start_date": "10.04.20",
   *         "end_date": "10.05.20",
   *         "type_mailing": 2,
   *         "created_at": "15.10.20",
   *         "item": {
   *             "id": 1,
   *             "coupon_id": 2,
   *             "user_id": 1,
   *             "product_ids": [
   *                 "1",
   *                 "2",
   *                 "3"
   *             ],
   *             "status": 0
   *         }
   *     }
   * ]
   */
  public function getUserCoupons(Request $request)
  {
    return $this->successResponse($this->couponService->getUserCoupons($request));
  }

  /**
   * @group Coupon
   * 
   * @urlParam store integer required Shop ID
   * @response [
   *     {
   *         "id": 2,
   *         "title": "asd",
   *         "description": "asdasd",
   *         "image": "/uploads/coupon/kXyJRW1jtD2eMHw9qH70sBedbLYVYsUuqiSSsMwO.png",
   *         "percent": 20,
   *         "start_date": "10.04.20",
   *         "end_date": "10.05.20",
   *         "type_mailing": 2,
   *         "created_at": "15.10.20",
   *         "count_data": {
   *             "total": 1,
   *             "used": 0
   *         }
   *     }
   * ]
   */
  public function getShopCoupons(Request $request, $store)
  {
    return $this->successResponse($this->couponService->getShopCoupons($request, $store));
  }

  /**
   * @group Coupon
   * 
   * @urlParam store integer required Shop ID
   * @response [
   *     {
   *         "id": 2,
   *         "title": "asd",
   *         "description": "asdasd",
   *         "image": "/uploads/coupon/kXyJRW1jtD2eMHw9qH70sBedbLYVYsUuqiSSsMwO.png",
   *         "percent": 20,
   *         "start_date": "10.04.20",
   *         "end_date": "10.05.20",
   *         "type_mailing": 2,
   *         "created_at": "15.10.20",
   *         "count_data": {
   *             "total": 1,
   *             "used": 0
   *         }
   *     }
   * ]
   */
  public function getShopArchiveCoupons(Request $request, $store)
  {
    return $this->successResponse($this->couponService->getShopArchiveCoupons($request, $store));
  }

  /**
   * @group Coupon
   * 
   * @urlParam product integer required Product ID
   * @response [
   *     {
   *         "id": 3,
   *         "title": "asd",
   *         "description": "asdasd",
   *         "image": "/uploads/coupon/kBeXkpcA9ThOXV0yPLOsBFdyqXW3rPwlZDXR93Ma.png",
   *         "percent": 20,
   *         "start_date": "10.04.20",
   *         "end_date": "10.05.20",
   *         "type_mailing": 2,
   *         "created_at": "15.10.20",
   *         "items": [
   *             {
   *                 "id": 2,
   *                 "coupon_id": 3,
   *                 "user_id": 1,
   *                 "product_ids": [
   *                     "5",
   *                     "2",
   *                     "3"
   *                 ],
   *                 "status": 0
   *             }
   *         ]
   *     }
   * ]
   */
  public function getProductCoupons(Request $request, $product)
  {
    return $this->successResponse($this->couponService->getProductCoupons($request, $product));
  }

  
  /**
   * @group Coupon
   * 
   * @bodyParam title string required
   * @bodyParam store_id integer required Shop ID
   * @bodyParam description string required
   * @bodyParam percent integer required
   * @bodyParam start_date date required
   * @bodyParam end_date date required
   * @bodyParam product_ids.* integer required
   * @bodyParam type_mailing integer required (1|2) создать купон 1 для всех пользователей сервиса, 2 для всех пользователей магазина
   * @bodyParam image string nullable
   * @response {
   *     "id": 8,
   *     "title": "asd",
   *     "description": "asdasd",
   *     "image": "/uploads/coupon/1QvGdEvEeavd4jjZ5FyaawPyl0bqI5Y3XbiDhxBM.png",
   *     "percent": "20",
   *     "start_date": "10.04.20",
   *     "end_date": "10.05.20",
   *     "type_mailing": "2",
   *     "created_at": "15.10.20"
   * }
   */
  public function store(Request $request)
  {
    if (!$request->type_mailing)
      return $this->errorResponse('type_mailing is required', [], 422);

    if ($request->type_mailing == config('setting.type_mailing.service_users')) {
      $users = User::select('id')->where('id', '!=', $request->user()->id)->get()->toArray();
    } elseif($request->type_mailing == config('setting.type_mailing.store_users')) {
      $user_ids = $this->orderService->getStoreUserIds($request, $request->store_id);
      if (!empty($user_ids['data'])) {
        $users = User::select('id')->whereIn('id', $user_ids['data'])->get()->toArray();
      }else{
        $users = [];
      }
    }

    return $this->successResponse($this->couponService->createCoupon($request, $users));
  }

  /**
   * @group Coupon
   * 
   * @urlParam coupon integer reauired Coupon ID 
   * @response {
   *     "id": 8,
   *     "title": "asd",
   *     "description": "asdasd",
   *     "image": "/uploads/coupon/1QvGdEvEeavd4jjZ5FyaawPyl0bqI5Y3XbiDhxBM.png",
   *     "percent": 20,
   *     "start_date": "10.04.20",
   *     "end_date": "10.05.20",
   *     "type_mailing": 2,
   *     "created_at": "15.10.20",
   *     "items": []
   * }
   */
  public function show(Request $request, $coupon)
  {
    return $this->successResponse($this->couponService->getCouponById($request, $coupon));
  }

  /**
   * @group Coupon
   * @urlParam coupon integer reauired Coupon ID 
   * @response {
   *     "id": 7,
   *     "title": "asd",
   *     "description": "asdasd",
   *     "image": "/uploads/coupon/iuy7SRlw7CyqkcFNmAusiTQSwAT1s4uN4xkRUZGr.png",
   *     "percent": 20,
   *     "start_date": "10.04.20",
   *     "end_date": "10.05.20",
   *     "type_mailing": 2,
   *     "created_at": "15.10.20"
   * }
   */
  public function destroy(Request $request, $coupon)
  {
    return $this->successResponse($this->couponService->deleteCoupon($request, $coupon));
  }

  /**
   * @group Coupon
   * @urlParam item integer reauired Coupon item ID 
   *
   * @response {
   *     "id": 2,
   *     "coupon_id": 3,
   *     "user_id": 1,
   *     "product_ids": [
   *         "5",
   *         "2",
   *         "3"
   *     ],
   *     "status": 0
   * }
   */
  public function deleteCouponItem(Request $request, $item)
  {
    return $this->successResponse($this->couponService->deleteCouponItem($request, $item));
  }

  /**
   * @group Coupon
   * 
   * @urlParam item integer reauired Coupon item ID 
   *
   * @response {
   *     "id": 3,
   *     "coupon_id": 4,
   *     "user_id": "1",
   *     "product_ids": [
   *         "1",
   *         "2",
   *         "3"
   *     ],
   *     "status": 0
   * }
   */
  public function giveCoupon(Request $request, $item)
  {
    if (!$request->has('user_param'))
      return $this->errorResponse('user_param is required');
    $resp = $this->couponService->giveCoupon($request, $item);
    if (!$resp) {
      return $this->errorResponse('User not found', [], 404);
    }
    return $this->successResponse($resp);
  }
}
