<?php

namespace App\Http\Controllers;

use App\Traits\ResponseTrait;

class BaseController extends Controller
{
  use ResponseTrait;
}
