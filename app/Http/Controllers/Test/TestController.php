<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\TestCategoryService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class TestController extends BaseController{

  use ResponseTrait;

	public $categoryService;

	public function __construct(TestCategoryService $categoryService)
	{
		$this->categoryService = $categoryService;
	}

  public function categories(Request $request)
  {
    $categories = $this->categoryService->getCategories($request);
		return $this->successResponse($categories);
  }
}