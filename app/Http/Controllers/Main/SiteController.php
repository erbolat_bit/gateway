<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\BaseController;
use App\Services\AddOn\CategoryService;
use App\Services\AddOn\ManufacturerService;
use App\Services\AdsService;
use App\Services\LotService;
use App\Services\ProductService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SiteController extends BaseController{

  use ResponseTrait;

  protected $productService;
  protected $adsService;
  protected $lotService;
  protected $categoryService;

  public function __construct(
    ProductService $productService, 
    AdsService $adsService, 
    LotService $lotService,
    CategoryService $categoryService
  )
  {
    $this->productService = $productService;
    $this->adsService = $adsService;
    $this->lotService = $lotService;
    $this->categoryService = $categoryService;
  }

  public function search(Request $request)
  {
    if ($request->parent_category_id) {
      $child_categories_id = $this->categoryService->getChildCategories($request);
      info($child_categories_id);
      $request->merge(['child_categories_id' => $child_categories_id]);
    }
    $request->merge(['main' => 1]);
    $data = [
      'products' => $this->productService->getProductList($request),
      'ads' => $this->adsService->getAllAds($request),
      'lot' => $this->lotService->getAllLot($request)
    ];

    return $this->successResponse($data);

  }

  public function getCategotyAndManufacture(Request $request)
  {
    $categories_cache_key = config('cache.storage_names.categories');
    $manufactures_cache_key = config('cache.storage_names.manufactures');

    if (Cache::has($categories_cache_key)) {
      $categories = Cache::get($categories_cache_key . $request->header('Lang'));
    }else{
      $categories = Cache::rememberForever($categories_cache_key . $request->header('Lang'), function() use($request){
        return  (new CategoryService)->getCategories($request);
      });
    }

    if (Cache::has($manufactures_cache_key)) {
      $manufactures = Cache::get($manufactures_cache_key);
    }else{
      $manufactures = Cache::rememberForever($manufactures_cache_key, function() use($request) {
        return (new ManufacturerService)->getManufacturers($request);
      });
    }

    return $this->successResponse([
      'categories' => $categories,
      'manufacturers' => $manufactures
    ]);
  }
}