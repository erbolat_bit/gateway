<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Requests\RatingRequest;
use App\Repositories\RatingRepository;
use App\Services\User\TicketService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class TicketController extends BaseController
{
  
  use ResponseTrait;

  private $ticketService;

  public function __construct(TicketService $ticketService)
  {
    $this->ticketService = $ticketService;
  }

  public function index(Request $request)
  {
    $tickets = $this->ticketService->getUserTickets($request);
    return $this->successResponse($tickets);
  }

  public function getTicketMessages(Request $request, $ticket_id)
  {
    $messages = $this->ticketService->getTicketMessages($request, $ticket_id);
    return $this->successResponse($messages);
  }

  public function createMessage(Request $request)
  {
    $result = $this->ticketService->createMessage($request);
    return $this->successResponse($result);
  }

  public function messageUpdate(Request $request)
  {
    $result = $this->ticketService->messageUpdate($request);
    return $this->successResponse($result);
  }

  public function searchMessages(Request $request, $ticket_id)
  {
    $messages = $this->ticketService->searchMessages($request, $ticket_id);
    return $this->successResponse($messages);
  }

  public function categoryList(Request $request)
  {
    $categories = $this->ticketService->categoryList($request);
    return $this->successResponse($categories);
  }

}
