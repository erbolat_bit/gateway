<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Services\User\BonusService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class BonusController extends BaseController
{

  use ResponseTrait;

	public $bonusService;


	public function __construct(BonusService $bonusService)
	{
		$this->bonusService = $bonusService;
	}

	/**
	 * @group Bonuses
	 * @response {
	 * 	
	 * }
	 */
	public function index(Request $request)
	{
    return $this->successResponse($this->bonusService->getBonuses($request));
	}
	
	/**
	 * @group Bonuses
	 * @response {
	 * 	
	 * }
	 */
	public function create(Request $request)
	{
    return $this->successResponse($this->bonusService->createBonus($request));
  }

	
}
