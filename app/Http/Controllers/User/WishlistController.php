<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Services\ProductService;
use App\Services\User\WishlistService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class WishlistController extends BaseController
{
  
  use ResponseTrait;

  public $wishlistService;
  public $productService;

  public function __construct(WishlistService $wishlistService, ProductService $productService)
  {
    $this->wishlistService = $wishlistService;
    $this->productService = $productService;
  }

  public function index(Request $request)
  {
    $products = Cache::get(config('cache.storage_names.user_wishlist') . $request->user()->id) ?? [];
    
    return $this->successResponse(array_values($products));
  }

  public function store(Request $request)
  {
    $products = Cache::get(config('cache.storage_names.user_wishlist') . $request->user()->id);

    if (isset($products[$request->product_id])) {
      return $this->errorResponse('Product is already in wishlist');
    }

    $product = $this->productService->getProductById($request, $request->product_id);
  
    if (isset($product['error']) && $product['error']) {
      return $this->errorResponse($product);
    }

    $products[$request->product_id] = $product;

    Cache::put(config('cache.storage_names.user_wishlist') . $request->user()->id, $products);

    return $this->successResponse($this->wishlistService->createWishlist($request));
  }

  public function destroy(Request $request)
  {
    $products = Cache::pull(config('cache.storage_names.user_wishlist') . $request->user()->id);
    unset($products[$request->product_id]);
    Cache::put(config('cache.storage_names.user_wishlist') . $request->user()->id, $products);
    return $this->successResponse($this->wishlistService->deleteWishlist($request, $request->product_id));
  }
}
