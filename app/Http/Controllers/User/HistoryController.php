<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Services\ProductService;
use App\Services\User\HistoryService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HistoryController extends BaseController
{
  
  use ResponseTrait;

  public $historyService;
  public $productService;

  public function __construct(HistoryService $historyService, ProductService $productService)
  {
    $this->historyService = $historyService;
    $this->productService = $productService;
  }

  public function index(Request $request)
  {
    $products = Cache::get(config('cache.storage_names.user_history') . $request->user()->id) ?? [];
    return $this->successResponse(array_values($products));
  }

  public function store(Request $request)
  {
    $products = Cache::get(config('cache.storage_names.user_history') . $request->user()->id);

    if (isset($products[$request->product_id])) {
      return $this->errorResponse('Product is already in history');
    }

    $product = $this->productService->getProductById($request, $request->product_id);
  
    if (isset($product['error']) && $product['error']) {
      return $this->errorResponse($product);
    }

    if (is_array($products) && count($products) > 9) {
      $key = array_key_first($products);
      unset($products[$key]);
    }

    $products[$request->product_id] = $product;

    Cache::put(config('cache.storage_names.user_history') . $request->user()->id, $products);

    return $this->successResponse($this->historyService->createHistory($request));
  }

  public function destroy(Request $request, $product_id)
  {
    $products = Cache::pull(config('cache.storage_names.user_history') . $request->user()->id);
    unset($products[$product_id]);
    Cache::put(config('cache.storage_names.user_history') . $request->user()->id, $products);

    return $this->successResponse($this->historyService->deleteHistory($request, $product_id));
  }
}
