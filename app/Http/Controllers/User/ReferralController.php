<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\User\ReferralService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ReferralController extends BaseController
{
  
  use ResponseTrait;

  public $referralService;

  public function __construct(ReferralService $referralService)
  {
    $this->referralService = $referralService;
  }

  public function index(Request $request)
  {

    $users = User::where('parent_id', $request->user()->id)->get();
    if ($users->isEmpty())
      return $this->successResponse([]);
    
    $referrals = collect($this->referralService->getUserReferrals($request, $users->pluck('id')->toArray()));

    $users = $users->transform(function($item) use($referrals){
      $item['referrals'] = isset($referrals[$item['id']]) ? $referrals[$item['id']] : null;
      return $item;
    });

    return $this->successResponse($users);
  }

  public function store(Request $request)
  {
    return $this->successResponse($this->referralService->createReferralLink($request));
  }

  public function getReferralByToken(Request $request, $token)
  {
    return $this->successResponse($this->referralService->getReferralByToken($request, $token));
  }

  public function createReferral(Request $request)
  {
    return $this->successResponse($this->referralService->createReferral($request, $request->input()));
  }
}
