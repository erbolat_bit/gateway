<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Requests\RatingRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\RatingRepository;
use App\Services\AdsService;
use App\Services\LotService;
use App\Services\ShopService;
use App\Services\User\AddressService;
use App\Services\User\DocumentService;
use App\Services\User\UserDataService;
use App\Traits\UploadFileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends BaseController
{
	use UploadFileTrait;

	public $documentService;
	public $shopService;
	public $adsService;
	public $lotService;
	public $userDataService;
	public $addressService;
	private $ratingRepository;

	public function __construct(
		DocumentService $documentService, 
		ShopService $shopService, 
		AdsService $adsService, 
		LotService $lotService, 
		UserDataService $userDataService, 
		AddressService $addressService,
		RatingRepository $ratingRepository)
	{
		$this->documentService = $documentService;
		$this->shopService = $shopService;
		$this->adsService = $adsService;
		$this->lotService = $lotService;
		$this->userDataService = $userDataService;
		$this->addressService = $addressService;
		$this->ratingRepository = $ratingRepository;
	}

	/**
	 * @group User
	 * @response {
	 * 	"id": 1
	 * 	"name": "name",
	 * 	"email": "email@email.com"
	 * }
	 */
	public function index(Request $request)
	{
		$data = Cache::rememberForever('user_data_' . $request->user()->id, function() use($request){
			return $this->userDataService->getDocsAndAddresses($request);
		});
		$user = new UserResource($request->user(), false, $data);
		return $this->successResponse($user->toArray($request));
	}

	public function updateProfile(Request $request)
	{
		$user = $request->user();

		$validator = Validator::make($request->all(), [
			'name' => 'required|string',
			'surname' => 'nullable|string',
			'email' => 'required|email',
			'birthdate' => 'required|date',
			'sex' => 'required|string',
			'phone' => 'required|integer',
			Rule::unique('nickname')->ignore($user->id),
			'avatar' => 'mimes:jpeg,bmp,png,gif'
		]);

		if ($validator->fails()) {
			return $this->errorResponse($validator->errors());
		}

		$input = $request->input();

		if ($request->hasFile('avatar')) {
			$input['path'] = $this->upload_file($request, 'avatar', 'user/avatars');
		}
		$user->update($input);

		return $this->successResponse(new UserResource($user));
	}

	public function addDocument(Request $request)
	{
		Cache::forget('user_data_' . $request->user()->id);
		return $this->successResponse($this->documentService->addDocument($request));
	}

	public function deleteDocument(Request $request, $id)
	{
		Cache::forget('user_data_' . $request->user()->id);
		return $this->successResponse($this->documentService->deleteDocument($request, $id));
	}

	public function updateUserBonuses(Request $request)
	{
		$validator = Validator::make($request->all(), [
      'bonuses' => 'required|integer'
		]);
		
		if ($validator->fails()) {
			return $this->errorResponse($validator->errors());
		}

		$user = $request->user();

    $user->bonuses += $request->bonuses;
		$user->save();
		
		return $this->successResponse(new UserResource($user));
	}

	public function getUserDataAndProducts(Request $request, $nickname)
	{
		$user = User::where('nickname', $nickname)->first();
		if (!$user) {
			return $this->errorResponse('Page not found', [], 404);
		}
		$user_id = $user->id;

		$shops = $this->shopService->getShops($request, false, $user_id);
		if (isset($shops['error']) && $shops['error']) {
			$shops = $this->errorResponse($shops['message'], [], $shops['code']);
		}

		$ads = $this->adsService->getUserAds($request, $user_id);
		if (isset($ads['error']) && $ads['error']) {
			$ads = $this->errorResponse($ads['message'], [], $ads['code']);
		}

		$lots = $this->lotService->getUserLots($request, $user_id);
		if (isset($lots['error']) && $lots['error']) {
			$lots = $this->errorResponse($lots['message'], [], $lots['code']);
		}

		return $this->successResponse([
			'user' => new UserResource($user),
			'shops' => $shops,
			'ads' => $ads,
			'lots' => $lots,
		]);
	}

	public function addAddress(Request $request)
	{
		Cache::forget('user_data_' . $request->user()->id);

		return $this->successResponse($this->addressService->addAddress($request));
	}

	public function deleteAddress(Request $request, $id)
	{
		Cache::forget('user_data_' . $request->user()->id);

		return $this->successResponse($this->addressService->deleteAddress($request, $id));
	}

	public function vote(RatingRequest $request)
  {
    $this->ratingRepository->create($request->validated());
    $rating = $this->ratingRepository->getUserRating($request->user_id);
    return $this->successResponse($rating);
  }

  public function getUserRating(Request $request, $user_id){
    $rating = $this->ratingRepository->getUserRating($user_id);
    return $this->successResponse($rating);
  }
}
