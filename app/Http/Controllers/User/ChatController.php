<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\User\ChatService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class ChatController extends BaseController
{
  
  use ResponseTrait;

  public $chatService;

  public function __construct(ChatService $chatService)
  {
    $this->chatService = $chatService;
  }

  public function index(Request $request)
  {
    $chats = $this->chatService->getUserChats($request);
    $chats_collect = collect($chats['data']);

    $chats_collect->transform(function($item) use($request){
      $users = UserResource::collection(User::whereIn('id', array_values($item['users']))->get());
      $item['users']['sender'] = $users->where('id', $item['users']['sender'])->first()->only(['id','name', 'path']);
      $item['users']['receiver'] = $users->where('id', $item['users']['receiver'])->first()->only(['id','name', 'path']);
      $sender = $users->where('id', $item['messages']['users']['sender'])->first();
      if (!$sender)
        $sender = new UserResource(User::find($item['messages']['users']['sender']));
      
      
      $item['messages']['users']['sender'] = $sender->only(['id','name', 'path']);
      
      return $item;
    });
    $chats['data'] = $chats_collect;
    return $this->successResponse($chats);
  }

  public function store(Request $request)
  {
    $message = $this->chatService->sendMessage($request);
    if (isset($message['error']) && $message['error']) {
      return $this->errorResponse($message['message'], [], 422);
    }

    $user = new UserResource(User::find($message['data']['users']['sender']));
    $message['data']['users']['sender'] = $user->only(['id','name', 'path']);
    return $this->successResponse($message['data']);
  }

  public function searchMessage(Request $request, $chat_id)
  {
    $messages = $this->chatService->searchMessFromChat($request, $chat_id);
    $user_ids = [];
    foreach ($messages as $key => $value) {
      $user_ids[] = $value['users']['sender'];
    }
    $users = UserResource::collection(User::whereIn('id', array_unique($user_ids))->get());
    $messages = collect($messages)->transform(function($item) use($users){
      $item['users']['sender'] = $users->where('id', $item['users']['sender'])->first()->only(['id','name', 'path']);
      return $item;
    });

    return $this->successResponse($messages);
  }

  public function show(Request $request, $chat_id)
  {
    $chat = $this->chatService->getChat($request, $chat_id);
    $users = UserResource::collection(User::whereIn('id', array_values($chat['users']))->get());
    $chat['users']['sender'] = $users->where('id', $chat['users']['sender'])->first()->only(['id','name', 'path']);
    $chat['users']['receiver'] = $users->where('id', $chat['users']['receiver'])->first()->only(['id','name', 'path']);

    return $this->successResponse($chat);
  }

  public function getChatMessages(Request $request, $chat_id)
  {
    $messages = $this->chatService->getChatMessages($request, $chat_id);
    $users = UserResource::collection(User::whereIn('id', [$messages['chat']['recevier_id'], $messages['chat']['sender_id']])->get());
    $messages['data'] = collect($messages['data'])->transform(function($item, $key) use($users){
      $user = $users->where('id', $item['users']['sender'])->first();
      if ($user) {
        $item['users']['sender'] = $users->where('id', $item['users']['sender'])->first()->only(['id','name', 'path']);
      }else{
        $item['users']['sender'] = $item['users']['sender'];
      }
      return $item;
    });
    return $this->successResponse(['data' => $messages['data'], 'links' => $messages['links'], 'meta' => $messages['meta']]);
  }

  public function messSetRead(Request $request)
  {
    return $this->successResponse($this->chatService->messSetRead($request));
  }
}
