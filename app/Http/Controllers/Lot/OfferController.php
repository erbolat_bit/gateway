<?php

namespace App\Http\Controllers\Lot;

use App\Http\Controllers\BaseController;
use App\Services\OfferService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class OfferController extends BaseController
{
  use ResponseTrait;

  public $offerService;

  public function __construct(OfferService $offerService)
  {
    $this->offerService = $offerService;
  }
  
  /**
   * @group Offer
   * 
   * @urlParam lot integer required Lot ID
   * 
   * @response [
   *     {
   *         "id": 3,
   *         "user_id": 1,
   *         "amount": "10000.00",
   *         "description": "Hello",
   *         "currency": "UZS",
   *         "created_at": "13.10.2020 12:27:00",
   *         "user": {
   *             "id": 1,
   *             "name": "Username",
   *             "email": "asd@asd.asd"
   *         }
   *     }
   * ]
   */
  public function getLotOffers(Request $request, $lot)
  {
    return $this->successResponse($this->offerService->getLotOffers($request, $lot));
  }
  
  /**
   * @group Offer
   * 
   * 
   * @response [
   *     {
   *         "id": 3,
   *         "user_id": 1,
   *         "who_is": 1,
   *         "category_id": 23,
   *         "title": "asd",
   *         "convert_starting_price": "100 000,00 UZS",
   *         "convert_without_bargaining_price": "200 000,00 UZS",
   *         "starting_price": "100000",
   *         "without_bargaining_price": "200000",
   *         "currency": {
   *             "id": 1,
   *             "title": "UZS"
   *         },
   *         "start_date": "01.06.2020",
   *         "end_date": "18.06.2020",
   *         "private_contact": 1,
   *         "description": "Краткое описание:",
   *         "country_id": 3,
   *         "manufacturer_id": 5,
   *         "detailed_description": "<p>sdfsdf</p>",
   *         "views": 0,
   *         "closed": 0,
   *         "finish": 0,
   *         "sold": 0,
   *         "param_options": [
   *             {
   *                 "id": 7,
   *                 "parameter_id": 7,
   *                 "value": "null",
   *                 "type": "select"
   *             }
   *         ],
   *         "photos": [
   *             {
   *                 "id": 9,
   *                 "photo": "/uploads/lot/photos/6UOxCgAwc3s3vlqygp96tudgw9ydoi1nr3IL1NPx.png"
   *             }
   *         ],
   *         "tags": [
   *             {
   *                 "id": 13,
   *                 "title": "sdfsdf"
   *             }
   *         ],
   *         "documents": []
   *     }
   * ]
   */
  public function getUserOffers(Request $request)
  {
    return $this->successResponse($this->offerService->getUserOffers($request));
  }
  
  /**
   * @group Offer
   * 
   * @bodyParam lot_id integer required Lot ID
   * @bodyParam amount integer required Sum
   * @bodyParam description string required
   * @bodyParam currency_id integer nullable 1 UZS, 2 USD, 3 RUB
   * 
   * @response {
   *     "id": 3,
   *     "amount": "10000.00",
   *     "description": "Hello",
   *     "currency": "UZS",
   *     "created_at": "13.10.2020 12:27:00"
   * }
   */
  public function store(Request $request)
  {
    return $this->successResponse($this->offerService->store($request));
  }
}
