<?php

namespace App\Http\Controllers\Lot;

use App\Http\Controllers\BaseController;
use App\Services\LotService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class LotController extends BaseController
{
  
  use ResponseTrait;

  public $lotService;

  public function __construct(LotService $lotService)
  {
    $this->lotService = $lotService;
  }
  
  /**
   * @group Lot
   * 
   * @response  {
   *  "product": {
   *    "data": [
   *      {
   *        "id": 1,
   *        "user_id": 1,
   *        "who_is": 1,
   *        "category_id": 24,
   *        "title": "asd",
   *        "convert_starting_price": "100 000,00 UZS",
   *        "convert_without_bargaining_price": "200 000,00 UZS",
   *        "starting_price": "100000",
   *        "without_bargaining_price": "200000",
   *        "currency": {
   *          "id": 1,
   *          "title": "UZS"
   *        },
   *        "start_date": "01.06.2020",
   *        "end_date": "18.06.2020",
   *        "private_contact": 1,
   *        "description": "Краткое описание:",
   *        "country_id": 3,
   *        "manufacturer_id": 5,
   *        "detailed_description": "<p>sdfsdf</p>",
   *        "views": 0,
   *        "closed": 0,
   *        "finish": 0,
   *        "sold": 0,
   *        "param_options": {
   *          "data": [
   *            {
   *              "id": 7,
   *              "parameter_id": 7,
   *              "value": "null",
   *              "type": "select"
   *            }
   *          ]
   *        },
   *        "photos": {
   *          "data": [
   *            {
   *              "id": 2,
   *              "photo": "/uploads/lot/photos/Bf8EqszCWmT6DxamUDuFkQ9Lxy1lnvrLwW6dKIpI.png"
   *            }
   *          ]
   *        },
   *        "tags": {
   *          "data": [
   *            {
   *              "id": 7,
   *              "title": "sdfsdf"
   *            }
   *          ]
   *        },
   *        "documents": {
   *          "data": []
   *        }
   *      }
   *    ],
   *    "meta": {
   *      "pagination": {
   *        "total": 1,
   *        "count": 1,
   *        "per_page": 15,
   *        "current_page": 1,
   *        "total_pages": 1,
   *        "links": []
   *      }
   *    }
   *  },
   *  "params": {
   *    "manufacturer_products_count": [
   *      {
   *        "id": 5,
   *        "products_count": 1
   *      }
   *    ],
   *    "category_products_count": [
   *      {
   *        "id": 24,
   *        "products_count": 1
   *      }
   *    ],
   *    "min_max_price": [
   *      "100000",
   *      "100000"
   *    ],
   *    "products_count": 1
   *  }
   * }
   */
  public function getAllLot(Request $request)
  {
    return $this->successResponse($this->lotService->getAllLot($request));
  }
    
  /**
   * @group Lot
   * 
   * @urlParam lot integer reauired Lot ID
   * 
   * @response {
   *  "id": 1,
   *  "user_id": 1,
   *  "who_is": 1,
   *  "category_id": 24,
   *  "title": "asd",
   *  "convert_starting_price": "100 000,00 UZS",
   *  "convert_without_bargaining_price": "200 000,00 UZS",
   *  "starting_price": "100000",
   *  "without_bargaining_price": "200000",
   *  "currency": {
   *    "id": 1,
   *    "title": "UZS"
   *  },
   *  "start_date": "01.06.2020",
   *  "end_date": "18.06.2020",
   *  "private_contact": 1,
   *  "description": "Краткое описание:",
   *  "country_id": 3,
   *  "manufacturer_id": 5,
   *  "detailed_description": "<p>sdfsdf</p>",
   *  "views": 0,
   *  "closed": 0,
   *  "finish": 0,
   *  "sold": 0,
   *  "param_options": [
   *    {
   *      "id": 7,
   *      "parameter_id": 7,
   *      "value": "null",
   *      "type": "select"
   *    }
   *  ],
   *  "photos": [
   *    {
   *      "id": 2,
   *      "photo": "/uploads/lot/photos/Bf8EqszCWmT6DxamUDuFkQ9Lxy1lnvrLwW6dKIpI.png"
   *    }
   *  ],
   *  "tags": [
   *    {
   *      "id": 7,
   *      "title": "sdfsdf"
   *    }
   *  ],
   *  "documents": []
   * }
   */
  public function getLotById(Request $request, $lot)
  {
    return $this->successResponse($this->lotService->getLotById($request, $lot));
  }
  
  /**
   * @group Lot
   * 
   * @response {
   *  "data": [
   *    {
   *      "id": 1,
   *      "user_id": 1,
   *      "who_is": 1,
   *      "category_id": 24,
   *      "title": "asd",
   *      "convert_starting_price": "100 000,00 UZS",
   *      "convert_without_bargaining_price": "200 000,00 UZS",
   *      "starting_price": "100000",
   *      "without_bargaining_price": "200000",
   *      "currency": {
   *        "id": 1,
   *        "title": "UZS"
   *      },
   *      "start_date": "01.06.2020",
   *      "end_date": "18.06.2020",
   *      "private_contact": 1,
   *      "description": "Краткое описание:",
   *      "country_id": 3,
   *      "manufacturer_id": 5,
   *      "detailed_description": "<p>sdfsdf</p>",
   *      "views": 0,
   *      "closed": 0,
   *      "finish": 0,
   *      "sold": 0,
   *      "param_options": {
   *        "data": [
   *          {
   *            "id": 7,
   *            "parameter_id": 7,
   *            "value": "null",
   *            "type": "select"
   *          }
   *        ]
   *      },
   *      "photos": {
   *        "data": [
   *          {
   *            "id": 2,
   *            "photo": "/uploads/lot/photos/Bf8EqszCWmT6DxamUDuFkQ9Lxy1lnvrLwW6dKIpI.png"
   *          }
   *        ]
   *      },
   *      "tags": {
   *        "data": [
   *          {
   *            "id": 7,
   *            "title": "sdfsdf"
   *          }
   *        ]
   *      },
   *      "documents": {
   *        "data": []
   *      }
   *    }
   *  ],
   *  "meta": {
   *    "pagination": {
   *      "total": 1,
   *      "count": 1,
   *      "per_page": 15,
   *      "current_page": 1,
   *      "total_pages": 1,
   *      "links": []
   *    }
   *  }
   * }
   */
  public function getUserLots(Request $request, $user_id=false)
  {
    return $this->successResponse($this->lotService->getUserLots($request, $user_id));
  }
  
  /**
   * @group Lot
   * 
   * @response {
   *     "id": 3,
   *     "user_id": 1,
   *     "who_is": 1,
   *     "category_id": 23,
   *     "title": "asd",
   *     "convert_starting_price": "100 000,00 UZS",
   *     "convert_without_bargaining_price": "200 000,00 UZS",
   *     "starting_price": "100000",
   *     "without_bargaining_price": "200000",
   *     "currency": {
   *         "id": "1",
   *         "title": "UZS"
   *     },
   *     "start_date": "01.06.2020",
   *     "end_date": "18.06.2020",
   *     "private_contact": "1",
   *     "description": "Краткое описание:",
   *     "country_id": 3,
   *     "manufacturer_id": 5,
   *     "detailed_description": "<p>sdfsdf</p>",
   *     "views": 0,
   *     "closed": 0,
   *     "finish": 0,
   *     "sold": 0,
   *     "param_options": [
   *         {
   *             "id": 7,
   *             "parameter_id": 7,
   *             "value": "null",
   *             "type": "select"
   *         },
   *         {
   *             "id": 3,
   *             "parameter_id": 1,
   *             "value": "null",
   *             "type": "checkbox"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 8,
   *             "photo": "/uploads/lot/photos/kKTKjo2vUzWJRkkZidh2oZ31ee0kKsBYgHzcUNk9.png"
   *         },
   *         {
   *             "id": 9,
   *             "photo": "/uploads/lot/photos/6UOxCgAwc3s3vlqygp96tudgw9ydoi1nr3IL1NPx.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 13,
   *             "title": "sdfsdf"
   *         },
   *         {
   *             "id": 14,
   *             "title": "sdfsd"
   *         },
   *         {
   *             "id": 15,
   *             "title": "sdfsd"
   *         }
   *     ],
   *     "documents": []
   * }
   */
  public function store(Request $request)
  {
    return $this->successResponse($this->lotService->createLot($request));
  }
      
  /**
   * @group Lot
   * 
   * @urlParam lot integer reauired Lot ID
   * 
   * @response {
   *     "id": 3,
   *     "user_id": 1,
   *     "who_is": 1,
   *     "category_id": 23,
   *     "title": "asd",
   *     "convert_starting_price": "100 000,00 UZS",
   *     "convert_without_bargaining_price": "200 000,00 UZS",
   *     "starting_price": "100000",
   *     "without_bargaining_price": "200000",
   *     "currency": {
   *         "id": "1",
   *         "title": "UZS"
   *     },
   *     "start_date": "01.06.2020",
   *     "end_date": "18.06.2020",
   *     "private_contact": "1",
   *     "description": "Краткое описание:",
   *     "country_id": 3,
   *     "manufacturer_id": 5,
   *     "detailed_description": "<p>sdfsdf</p>",
   *     "views": 0,
   *     "closed": 0,
   *     "finish": 0,
   *     "sold": 0,
   *     "param_options": [
   *         {
   *             "id": 7,
   *             "parameter_id": 7,
   *             "value": "null",
   *             "type": "select"
   *         },
   *         {
   *             "id": 3,
   *             "parameter_id": 1,
   *             "value": "null",
   *             "type": "checkbox"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 8,
   *             "photo": "/uploads/lot/photos/kKTKjo2vUzWJRkkZidh2oZ31ee0kKsBYgHzcUNk9.png"
   *         },
   *         {
   *             "id": 9,
   *             "photo": "/uploads/lot/photos/6UOxCgAwc3s3vlqygp96tudgw9ydoi1nr3IL1NPx.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 13,
   *             "title": "sdfsdf"
   *         },
   *         {
   *             "id": 14,
   *             "title": "sdfsd"
   *         },
   *         {
   *             "id": 15,
   *             "title": "sdfsd"
   *         }
   *     ],
   *     "documents": []
   * }
   */
  public function update(Request $request, $lot)
  {
    return $this->successResponse($this->lotService->updateLot($request, $lot));
  }
  
  /**
   * @group Lot
   * 
   * @urlParam lot integer reauired Lot ID
   * 
   * @response {
   *     "id": 3,
   *     "user_id": 1,
   *     "who_is": 1,
   *     "category_id": 23,
   *     "title": "asd",
   *     "convert_starting_price": "100 000,00 UZS",
   *     "convert_without_bargaining_price": "200 000,00 UZS",
   *     "starting_price": "100000",
   *     "without_bargaining_price": "200000",
   *     "currency": {
   *         "id": "1",
   *         "title": "UZS"
   *     },
   *     "start_date": "01.06.2020",
   *     "end_date": "18.06.2020",
   *     "private_contact": "1",
   *     "description": "Краткое описание:",
   *     "country_id": 3,
   *     "manufacturer_id": 5,
   *     "detailed_description": "<p>sdfsdf</p>",
   *     "views": 0,
   *     "closed": 0,
   *     "finish": 0,
   *     "sold": 0,
   *     "param_options": [
   *         {
   *             "id": 7,
   *             "parameter_id": 7,
   *             "value": "null",
   *             "type": "select"
   *         },
   *         {
   *             "id": 3,
   *             "parameter_id": 1,
   *             "value": "null",
   *             "type": "checkbox"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 8,
   *             "photo": "/uploads/lot/photos/kKTKjo2vUzWJRkkZidh2oZ31ee0kKsBYgHzcUNk9.png"
   *         },
   *         {
   *             "id": 9,
   *             "photo": "/uploads/lot/photos/6UOxCgAwc3s3vlqygp96tudgw9ydoi1nr3IL1NPx.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 13,
   *             "title": "sdfsdf"
   *         },
   *         {
   *             "id": 14,
   *             "title": "sdfsd"
   *         },
   *         {
   *             "id": 15,
   *             "title": "sdfsd"
   *         }
   *     ],
   *     "documents": []
   * }
   */
  public function destroy(Request $request, $lot)
  {
    return $this->successResponse($this->lotService->deleteLot($request, $lot));
  }
  
  /**
   * @group Lot
   * 
   * @urlParam photo integer reauired Lot photo ID
   * 
   * @response {
   *     "id": 8,
   *     "lot_id": 3,
   *     "photo": "/uploads/lot/photos/kKTKjo2vUzWJRkkZidh2oZ31ee0kKsBYgHzcUNk9.png"
   * }
   */
  public function deleteLotPhoto(Request $request, $photo)
  {
    return $this->successResponse($this->lotService->deleteLotPhoto($request, $photo));
  }
  
  /**
   * @group Lot
   * 
   * @urlParam lot integer reauired Lot ID
   * 
   * @response {
   *     "id": 2,
   *     "title": "adas",
   *     "document": "/uploads/lot/document/GcIF5XxlrQYoeQIvlcD2YSRmk3Ai61oDkDl0zXHd.pdf",
   *     "size": "1024"
   * }
   */
  public function uploadLotDoc(Request $request, $lot)
  {
    return $this->successResponse($this->lotService->uploadLotDoc($request, $lot));
  }
  
  /**
   * @group Lot
   * 
   * @urlParam document integer reauired Lot document ID
   * 
   * @response {
   *     "id": 2,
   *     "title": "adas",
   *     "document": "/uploads/lot/document/GcIF5XxlrQYoeQIvlcD2YSRmk3Ai61oDkDl0zXHd.pdf",
   *     "size": "1024"
   * }
   */
  public function deleteLotDoc(Request $request, $document)
  {
    return $this->successResponse($this->lotService->deleteLotDoc($request, $document));
  }

}
