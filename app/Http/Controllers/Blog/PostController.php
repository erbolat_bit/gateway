<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Services\Blog\PostService;
use App\Services\CommentService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PostController extends Controller
{
  use ResponseTrait;

  public $postService;

  public $commentService;

  public function __construct(PostService $postService, CommentService $commentService)
  {
    $this->postService = $postService;
    $this->commentService = $commentService;
  }

  public function postList(Request $request)
  {
    $posts = $this->postService->postList($request);
    return $this->gatherCommentCounts($request, $posts);
  }

  public function postSearch(Request $request)
  {
    $posts = $this->postService->postSearch($request);
    return $this->gatherCommentCounts($request, $posts);
  }

  public function postView(Request $request, $alias)
  {
    return $this->successResponse($this->postService->getPostByAlias($request, $alias));
  }

  private function gatherCommentCounts($request,  $posts)
  {
    $ids = array_column($posts['data'], 'id');
    $ids_counts = $this->commentService->getPostsCommentsCount($request, $ids);
    
    //we get all comment counts

    //We need to combine both
    foreach ($posts['data'] as $key_p => $post) {
      if (is_array($ids_counts['data'])) {
        foreach ($ids_counts['data'] as $counts) {
          if ($post['id'] == $counts['post_id']) {
            $posts['data'][$key_p]['comment_count'] = $counts['comment_count'];
          }
        }
      }else{
        $posts['data'][$key_p]['comment_count'] = 0;
      }
    }

    //And send the result
    return $this->successResponse($posts);
  }
}
