<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Services\Blog\CategoryService as BlogCategoryService;
use App\Services\CommentService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
  use ResponseTrait;

  public $categoryService;

  public function __construct(BlogCategoryService $categoryService)
  {
    $this->categoryService = $categoryService;
  }

  public function categoryList(Request $request)
  {
    $categories = $this->categoryService->categoryList($request);
    return $this->successResponse($categories);
  }
}
