<?php

namespace App\Http\Controllers\Ads;

use App\Http\Controllers\BaseController;
use App\Services\AdsService;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class AdsController extends BaseController
{
  
  use ResponseTrait;

  public $adsService;

  public function __construct(AdsService $adsService)
  {
    $this->adsService = $adsService;
  }
  
  /**
   * @group Ads
   * 
   * @response {
   *     "ads": {
   *         "data": [
   *             {
   *                 "id": 2,
   *                 "user_id": 1,
   *                 "category_id": 1,
   *                 "title": "asd",
   *                 "price": 20000,
   *                 "convert_price": "20 000,00 UZS",
   *                 "currency": {
   *                     "id": 1,
   *                     "title": "UZS"
   *                 },
   *                 "quantity": 2,
   *                 "quantity_size": 2,
   *                 "delivery_description": "asdsdas",
   *                 "description": "asdasdsd",
   *                 "country_id": 1,
   *                 "manufacturer_id": 2,
   *                 "detailed_description": "asdasdasd",
   *                 "sale": 0,
   *                 "param_options": {
   *                     "data": [
   *                         {
   *                             "id": 1,
   *                             "parameter_id": 2,
   *                             "value": "ads",
   *                             "type": "select"
   *                         }
   *                     ]
   *                 },
   *                 "photos": {
   *                     "data": [
   *                         {
   *                             "id": 4,
   *                             "photo": "/uploads/ads/photos/FIvG4prsoMWWKATIKrkR1WnQ6DpXK00vQURsA5TX.png"
   *                         }
   *                     ]
   *                 },
   *                 "tags": {
   *                     "data": [
   *                         {
   *                             "id": 3,
   *                             "title": "asdas"
   *                         },
   *                         {
   *                             "id": 4,
   *                             "title": "asdasd"
   *                         }
   *                     ]
   *                 },
   *                 "documents": {
   *                     "data": [
   *                         {
   *                             "id": 1,
   *                             "title": "asd",
   *                             "document": "/uploads/ads/document/BJ7T98Xa0bpwzXaGXktPEjoLK1RRuUZXa9Tiu1Eh.pdf",
   *                             "size": 1024
   *                         }
   *                     ]
   *                 }
   *             }
   *         ],
   *         "meta": {
   *             "pagination": {
   *                 "total": 1,
   *                 "count": 1,
   *                 "per_page": 12,
   *                 "current_page": 1,
   *                 "total_pages": 1,
   *                 "links": []
   *             }
   *         }
   *     },
   *     "count_ads": {
   *         "all_ads_count": 1,
   *         "sell_ads_count": 0,
   *         "buy_ads_count": 1
   *     }
   * } 
   */
  public function getUserAds(Request $request, $user_id=false)
  {
    return $this->successResponse($this->adsService->getUserAds($request, $user_id));
  }

  /**
   * @group Ads
   * 
   * @bodyParam category_id integer required Category ID
   * @bodyParam title string required
   * @bodyParam price integer required
   * @bodyParam currency_id integer required 1 USD, 2 UZS, 3 RUB
   * @bodyParam quantity integer nullable
   * @bodyParam quantity_size integer nullable
   * @bodyParam delivery_description string nullable
   * @bodyParam description string nullable
   * @bodyParam country_id integer required
   * @bodyParam manufacturer_id integer required
   * @bodyParam detailed_description string nullable
   * @bodyParam sale integer nullable 0 купить, 1 продажа
   * @bodyParam tags.*.title string nullable
   * @bodyParam param_options.*.option_id integer nullable 
   * @bodyParam param_options.*.value string nullable 
   * @bodyParam param_options.*.parameter_id integer nullable 
   * @bodyParam param_options.*.type string nullable 
   * 
   * @response {
   *     "id": 3,
   *     "user_id": 1,
   *     "category_id": 1,
   *     "title": "asd",
   *     "price": "20000",
   *     "convert_price": "20 000,00 UZS",
   *     "currency": {
   *         "id": "1",
   *         "title": "UZS"
   *     },
   *     "quantity": "2",
   *     "quantity_size": "2",
   *     "delivery_description": "asdsdas",
   *     "description": "asdasdsd",
   *     "country_id": 1,
   *     "manufacturer_id": 2,
   *     "detailed_description": "asdasdasd",
   *     "sale": 0,
   *     "param_options": [
   *         {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "ads",
   *             "type": "select"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 8,
   *             "photo": "/uploads/ads/photos/RcmgtLBPsp7PdptFZ8sMdojSqVarRSD7mNGinY0V.png"
   *         },
   *         {
   *             "id": 9,
   *             "photo": "/uploads/ads/photos/45KMWSN3b7O7Merir6u6ievfvWY0ulLhc0jo98sa.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 8,
   *             "title": "asdas"
   *         },
   *         {
   *             "id": 9,
   *             "title": "asdasd"
   *         }
   *     ],
   *     "documents": []
   * } 
   */
  public function store(Request $request)
  {
    return $this->successResponse($this->adsService->createAds($request));
  }

  /**
   * @group Ads
   * 
   * @urlParam id integer required Ads ID
   * @bodyParam category_id integer required Category ID
   * @bodyParam title string required
   * @bodyParam price integer required
   * @bodyParam currency_id integer required 1 USD, 2 UZS, 3 RUB
   * @bodyParam quantity integer nullable
   * @bodyParam quantity_size integer nullable
   * @bodyParam delivery_description string nullable
   * @bodyParam description string nullable
   * @bodyParam country_id integer required
   * @bodyParam manufacturer_id integer required
   * @bodyParam detailed_description string nullable
   * @bodyParam sale integer nullable 0 купить, 1 продажа
   * @bodyParam tags.*.title string nullable
   * @bodyParam param_options.*.option_id integer nullable 
   * @bodyParam param_options.*.value string nullable 
   * @bodyParam param_options.*.parameter_id integer nullable 
   * @bodyParam param_options.*.type string nullable 
   * 
   * @response {
   *     "id": 3,
   *     "user_id": 1,
   *     "category_id": 1,
   *     "title": "asd",
   *     "price": "20000",
   *     "convert_price": "20 000,00 UZS",
   *     "currency": {
   *         "id": "1",
   *         "title": "UZS"
   *     },
   *     "quantity": "2",
   *     "quantity_size": "2",
   *     "delivery_description": "asdsdas",
   *     "description": "asdasdsd",
   *     "country_id": 1,
   *     "manufacturer_id": 2,
   *     "detailed_description": "asdasdasd",
   *     "sale": 0,
   *     "param_options": [
   *         {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "ads",
   *             "type": "select"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 8,
   *             "photo": "/uploads/ads/photos/RcmgtLBPsp7PdptFZ8sMdojSqVarRSD7mNGinY0V.png"
   *         },
   *         {
   *             "id": 9,
   *             "photo": "/uploads/ads/photos/45KMWSN3b7O7Merir6u6ievfvWY0ulLhc0jo98sa.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 8,
   *             "title": "asdas"
   *         },
   *         {
   *             "id": 9,
   *             "title": "asdasd"
   *         }
   *     ],
   *     "documents": []
   * } 
   */
  public function update(Request $request, $ad)
  {
    return $this->successResponse($this->adsService->updateAds($request, $ad));
  }

  /**
   * @group Ads
   * 
   * @urlParam id integer required Ads ID
   * 
   * @response {
   *     "id": 3,
   *     "user_id": 1,
   *     "category_id": 1,
   *     "title": "asd",
   *     "price": "20000",
   *     "convert_price": "20 000,00 UZS",
   *     "currency": {
   *         "id": "1",
   *         "title": "UZS"
   *     },
   *     "quantity": "2",
   *     "quantity_size": "2",
   *     "delivery_description": "asdsdas",
   *     "description": "asdasdsd",
   *     "country_id": 1,
   *     "manufacturer_id": 2,
   *     "detailed_description": "asdasdasd",
   *     "sale": 0,
   *     "param_options": [
   *         {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "ads",
   *             "type": "select"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 8,
   *             "photo": "/uploads/ads/photos/RcmgtLBPsp7PdptFZ8sMdojSqVarRSD7mNGinY0V.png"
   *         },
   *         {
   *             "id": 9,
   *             "photo": "/uploads/ads/photos/45KMWSN3b7O7Merir6u6ievfvWY0ulLhc0jo98sa.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 8,
   *             "title": "asdas"
   *         },
   *         {
   *             "id": 9,
   *             "title": "asdasd"
   *         }
   *     ],
   *     "documents": []
   * } 
   */
  public function destroy(Request $request, $ad)
  {
    return $this->successResponse($this->adsService->deleteAds($request, $ad));
  }

  /**
   * @group Ads
   * @urlParam photo integer required
   * @response {
   *     "id": 8,
   *     "ad_id": 3,
   *     "photo": "/uploads/ads/photos/RcmgtLBPsp7PdptFZ8sMdojSqVarRSD7mNGinY0V.png"
   * } 
   */
  public function deleteAdsPhoto(Request $request, $photo)
  {
    return $this->successResponse($this->adsService->deleteAdsPhoto($request, $photo));
  }

  /**
   * @group Ads
   * 
   * @urlParam ad integer required Ads ID
   * @bodyParam title string required
   * @bodyParam size integer required File size (bayt)
   * @bodyParam document string required File
   * 
   * @response {
   *     "id": 3,
   *     "title": "asd",
   *     "document": "/uploads/ads/document/4wYUJMBmrmaWRotgPzXz7zZuIZvoRqtNlvErLIUL.pdf",
   *     "size": "1024"
   * } 
   */
  public function uploadAdsDoc(Request $request, $ad)
  {
    return $this->successResponse($this->adsService->uploadAdsDoc($request, $ad));
  }

  /**
   * @group Ads
   * 
   * @urlParam document integer required Ads document ID
   * 
   * @response {
   *     "id": 3,
   *     "title": "asd",
   *     "document": "/uploads/ads/document/4wYUJMBmrmaWRotgPzXz7zZuIZvoRqtNlvErLIUL.pdf",
   *     "size": "1024"
   * } 
   */
  public function deleteAdsDoc(Request $request, $document)
  {
    return $this->successResponse($this->adsService->deleteAdsDoc($request, $document));
  }

  /**
   * @group Ads
   * 
   * @response {
   *     "product": {
   *         "data": [
   *             {
   *                 "id": 2,
   *                 "user_id": 1,
   *                 "category_id": 1,
   *                 "title": "asd",
   *                 "price": 20000,
   *                 "convert_price": "20 000,00 UZS",
   *                 "currency": {
   *                     "id": 1,
   *                     "title": "UZS"
   *                 },
   *                 "quantity": 2,
   *                 "quantity_size": 2,
   *                 "delivery_description": "asdsdas",
   *                 "description": "asdasdsd",
   *                 "country_id": 1,
   *                 "manufacturer_id": 2,
   *                 "detailed_description": "asdasdasd",
   *                 "sale": 0,
   *                 "param_options": {
   *                     "data": [
   *                         {
   *                             "id": 1,
   *                             "parameter_id": 2,
   *                             "value": "ads",
   *                             "type": "select"
   *                         }
   *                     ]
   *                 },
   *                 "photos": {
   *                     "data": [
   *                         {
   *                             "id": 4,
   *                             "photo": "/uploads/ads/photos/FIvG4prsoMWWKATIKrkR1WnQ6DpXK00vQURsA5TX.png"
   *                         }
   *                     ]
   *                 },
   *                 "tags": {
   *                     "data": [
   *                         {
   *                             "id": 3,
   *                             "title": "asdas"
   *                         },
   *                         {
   *                             "id": 4,
   *                             "title": "asdasd"
   *                         }
   *                     ]
   *                 },
   *                 "documents": {
   *                     "data": [
   *                         {
   *                             "id": 1,
   *                             "title": "asd",
   *                             "document": "/uploads/ads/document/BJ7T98Xa0bpwzXaGXktPEjoLK1RRuUZXa9Tiu1Eh.pdf",
   *                             "size": 1024
   *                         },
   *                         {
   *                             "id": 3,
   *                             "title": "asd",
   *                             "document": "/uploads/ads/document/4wYUJMBmrmaWRotgPzXz7zZuIZvoRqtNlvErLIUL.pdf",
   *                             "size": 1024
   *                         }
   *                     ]
   *                 }
   *             }
   *         ],
   *         "meta": {
   *             "pagination": {
   *                 "total": 2,
   *                 "count": 2,
   *                 "per_page": 15,
   *                 "current_page": 1,
   *                 "total_pages": 1,
   *                 "links": []
   *             }
   *         }
   *     },
   *     "params": {
   *         "manufacturer_products_count": [
   *             {
   *                 "id": 2,
   *                 "products_count": 2
   *             }
   *         ],
   *         "category_products_count": [
   *             {
   *                 "id": 1,
   *                 "products_count": 2
   *             }
   *         ],
   *         "min_max_price": [
   *             20000,
   *             20000
   *         ],
   *         "products_count": 2
   *     }
   * } 
   */
  public function getAll(Request $request)
  {
    return $this->successResponse($this->adsService->getAllAds($request));
  }

  /**
   * @group Ads
   * 
   * @urlParam ad integer required Ads ID
   * 
   * @response {
   *     "id": 2,
   *     "user_id": 1,
   *     "category_id": 1,
   *     "title": "asd",
   *     "price": 20000,
   *     "convert_price": "20 000,00 UZS",
   *     "currency": {
   *         "id": 1,
   *         "title": "UZS"
   *     },
   *     "quantity": 2,
   *     "quantity_size": 2,
   *     "delivery_description": "asdsdas",
   *     "description": "asdasdsd",
   *     "country_id": 1,
   *     "manufacturer_id": 2,
   *     "detailed_description": "asdasdasd",
   *     "sale": 0,
   *     "param_options": [
   *         {
   *             "id": 1,
   *             "parameter_id": 2,
   *             "value": "ads",
   *             "type": "select"
   *         }
   *     ],
   *     "photos": [
   *         {
   *             "id": 4,
   *             "photo": "/uploads/ads/photos/FIvG4prsoMWWKATIKrkR1WnQ6DpXK00vQURsA5TX.png"
   *         }
   *     ],
   *     "tags": [
   *         {
   *             "id": 3,
   *             "title": "asdas"
   *         },
   *         {
   *             "id": 4,
   *             "title": "asdasd"
   *         }
   *     ],
   *     "documents": [
   *         {
   *             "id": 1,
   *             "title": "asd",
   *             "document": "/uploads/ads/document/BJ7T98Xa0bpwzXaGXktPEjoLK1RRuUZXa9Tiu1Eh.pdf",
   *             "size": 1024
   *         },
   *         {
   *             "id": 3,
   *             "title": "asd",
   *             "document": "/uploads/ads/document/4wYUJMBmrmaWRotgPzXz7zZuIZvoRqtNlvErLIUL.pdf",
   *             "size": 1024
   *         }
   *     ],
   *     "user": {
   *         "id": 1,
   *         "name": "Username",
   *         "email": "asd@asd.asd"
   *     }
   * } 
   */
  public function getAdsById(Request $request, $ad)
  {
    return $this->successResponse($this->adsService->getAdsById($request, $ad));
  }

}
