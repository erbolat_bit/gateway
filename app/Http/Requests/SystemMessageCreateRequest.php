<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SystemMessageCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required|string',
            'title' => 'required|string',
            'filter_params.letter' => 'required|string',
            'filter_params.age' => 'nullable|string',
            'filter_params.age_from' => 'nullable|string',
            'filter_params.age_to' => 'nullable|string',
            'filter_params.profile' => 'nullable|string',
        ];
    }
}
