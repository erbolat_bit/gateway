<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class ResetPassword extends Mailable
{
	use Queueable, SerializesModels;

	private $user;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->user = $user;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		$token = DB::table('password_resets')->where('email', $this->user->email)->first()->token;
		$url = config('setting.spa.domain') . '/' .config('setting.spa.password-reset') . '/' . $token . '?email=' . urlencode($this->user->email);
		return $this->view('emails.reset-password')
								->with([
									'user' => $this->user,
									'url' => $url
								]);
	}
}
