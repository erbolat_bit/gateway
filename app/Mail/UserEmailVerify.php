<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class UserEmailVerify extends Mailable
{
  use Queueable, SerializesModels;

  private $user;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct(User $user)
  {
    $this->user = $user;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $url = URL::temporarySignedRoute('auth.email.verify', now()->hour(24), [
      'id' => $this->user->id,
      'hash' => sha1($this->user->email)
    ]);
    // info($url);
    return $this->view('emails.user-verify')
      ->with([
        'user' => $this->user,
        'url' => config('setting.spa.domain') . '/' . config('setting.spa.verify-url') . '/' . $this->user->id . '/' . last(explode('/', $url))
      ]);
  }
}
