# Uzexpress gateway api v1

## Installation

git clone git@bitbucket.org:erbolat_bit/gateway.git

```bash
composer install
```

Copy .env.example to .env
```bash
cp .env.example .env
```
```bash
php artisan key:generate
```

Config DB
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```
### Migration

```php
php artisan migrate --seed
```

```php
php artisan passport:install
php artisan passport:client --personal
```
### Edit .env

```
PASSPORT_PERSONAL_ACCESS_CLIENT_ID=client_id
PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET=client_secret

SPA_DOMAIN=[http://uxezpress.ru] # frontend domain
SPA_VERIFY_URL=[email-verify] # frontend email verify url
SPA_PASS_RESET_URL=[password-reset] # frontend email password reset

QUEUE_CONNECTION=redis
REDIS_QUEUE=gateway

SHOP_SERVICE_BASE_URL=http://uzexpress-shop.loc
REVIEW_SERVICE_BASE_URL=http://uzexpress-review.loc
LOT_SERVICE_BASE_URL=http://uzexpress-lot.loc
ADS_SERVICE_BASE_URL=http://uzexpress-ads.loc
COUPON_SERVICE_BASE_URL=http://uzexpress-coupon.loc
ADD_ON_SERVICE_BASE_URL=http://uzexpress-add-on.loc
CART_SERVICE_BASE_URL=http://uzexpress-cart.loc
USER_SERVICE_BASE_URL=http://uzexpress-user.loc
BLOG_SERVICE_BASE_URL=http://uzexpress-blog.loc
POLL_SERVICE_BASE_URL=http://uzexpress-poll.loc

SHOP_SERVICE_TOKEN=
REVIEW_SERVICE_TOKEN=
LOT_SERVICE_TOKEN=
ADS_SERVICE_TOKEN=
COUPON_SERVICE_TOKEN=
ADD_ON_SERVICE_TOKEN=
CART_SERVICE_TOKEN=
USER_SERVICE_TOKEN=
BLOG_SERVICE_TOKEN=
POLL_SERVICE_TOKEN=

```

### Queue

```php
php artisan queue:work
```
