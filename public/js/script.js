$(document).ready(function() {
  if ($(".js-example-basic-multiple").length) {
    $(".js-example-basic-multiple").select2();
  }

  if ($(".editor").length) {
    var _token = $('meta[name="csrf-token"]').attr("content");

    var items = $(".editor");

    $.each(items, function(k, v) {
      ClassicEditor.create(v, {
        // plugins: [CKFinder],
        // toolbar: ["imageUpload"],
        ckfinder: {
          // Upload the images to the server using the CKFinder QuickUpload command.
          uploadUrl: "/image-upload"
          // headers: {
          // "X-CSRF-TOKEN": _token,
          // },
        }
      }).catch(error => {
        console.error(error);
      });
    });
  }

  $(".check-toggle").click(function() {
    if ($(this).is(":checked")) {
      $(this)
        .parents(".toggle-container")
        .find(".check-toggle-body")
        .show();
    } else {
      $(this)
        .parents(".toggle-container")
        .find(".check-toggle-body")
        .hide();
    }
  });

  if ($(".type-select").val()) {
    $(".params-sections")
      .find("." + $(".type-select").val())
      .show("slow");
    if ($(".type-select").val() == "select") {
      $("body")
        .find(".multiselect-confirm")
        .show("slow");
    }
  }

  $(".type-select").change(function() {
    var _this = $(this);
    _value = _this.val();
    $(".params-sections > div").hide("fast");
    $(".params-sections")
      .find("." + _value)
      .show("slow");
    if (_value == "select") {
      $(".multiselect-confirm").show("slow");
    } else {
      $(".multiselect-confirm").hide("fast");
    }
  });

  $("body").on("click", ".add-option", function() {
    var _this = $(this);
    _type = _this.parent().data("type");
    (_prev_id = _this.prev("div").attr("id")
      ? parseInt(_this.prev("div").attr("id")) + 1
      : 1),
      (_html =
        '<div style="margin-top:20px" id="' +
        _prev_id +
        '" class="row ">' +
        '<div class="col-md-3">' +
        '<label for="">Ru</label>' +
        '<input placeholder="" name="' +
        "new[" +
        _type +
        "][" +
        _prev_id +
        "]" +
        '[title_ru]" type="text" class="form-control">' +
        "</div>" +
        '<div class="col-md-3">' +
        '<label for="">Uz</label>' +
        '<input placeholder="" name="' +
        "new[" +
        _type +
        "][" +
        _prev_id +
        "]" +
        '[title_uz]" type="text" class="form-control">' +
        "</div>" +
        '<div class="col-md-3">' +
        '<label for="">En</label>' +
        '<input placeholder="" name="' +
        "new[" +
        _type +
        "][" +
        _prev_id +
        "]" +
        '[title_en]" type="text" class="form-control">' +
        "</div>" +
        '<div class="col-md-3">' +
        '<button type="button" class="btn btn-danger delete-option" style="margin-top: 28px;">Удалить значения</button>' +
        "</div>" +
        "</div>");

    _this.before(_html);
  });
  $("body").on("click", ".delete-option", function() {
    (_this = $(this)), (_data_url = _this.data("url"));
    if (_data_url) {
      $.ajax({
        url: _data_url,
        type: "DELETE",
        headers: {
          "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        },
        success: function(resp) {
          if (resp.status == "success") {
            _this
              .parent()
              .parent()
              .remove();
          }
        },
        error: function(xhr) {
          console.log(xhr);
        }
      });
    } else {
      _this
        .parent()
        .parent()
        .remove();
    }
  });
  $("#sandbox-container .input-daterange").datepicker({
    format: "yyyy-mm-dd"
  });
  $(".datetime").datepicker({
    format: "yyyy-mm-dd"
  });
  $('*[id$=".all*"]').on("click", function(e) {
    let str = e.currentTarget.id;
    let target = str.substring(0, str.length - 5);
    let status = $(this).prop("checked");
    $(`*[id^="${target}."]`).each(function() {
      if ($(this).prop("checked") !== status) {
        $(this).click();
      }
    });
  });

  function search(params, link) {
    let token = $('meta[name="csrf-token"]').attr("content");
    $.ajax({
      url: link,
      method: "GET",
      headers: {
        Accept: "Application/json"
      },
      data: params,
      success: function(data) {
        if (link === "/blog/categories") {
          let categories = data.data;
          let tableBody = $("#category_table tbody");
          tableBody.empty();
          drawPagination(data, link);
          for (let i = 0; i < categories.length; i++) {
            let markup = `
            <tr>
              <td>${categories[i].id}</td>
              <td>${categories[i].title_ru}</td>
              <td>
                <a class="btn btn-success btn-sm" href="http://gateway.nm/blog/categories/update/${categories[i].id}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                  <i class="mdi mdi-lead-pencil"></i>
                </a>
                <button type="button" class="btn btn-danger btn-sm d-inline" data-toggle="modal" data-target="#confirmationModal${categories[i].id}">
                  <i class="mdi mdi-delete"></i>
                </button>
                <div class="modal fade" id="confirmationModal${categories[i].id}" tabindex="-1" role="dialog" aria-labelledby="confirmationModalLabel${categories[i].id}" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="confirmationModalLabel${categories[i].id}">Вы уверены?</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                        <form class="d-inline" action="http://gateway.nm/blog/categories/delete?id=${categories[i].id}" method="post">
                          <input type="hidden" name="_token" value="${token}">          <input type="hidden" name="_method" value="delete">                    <button type="submit" class="btn btn-primary">Продолжить</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </td>
            </tr>`;
            tableBody.append(markup);
          }
        } else {
          let posts = data.data;
          let table_body = $("#posts_table tbody");
          table_body.empty();
          drawPagination(data, link);
          for (let i = 0; i < posts.length; i++) {
            let markup = `
            <tr>
              <td>${posts[i].id}</td>
              <td>${posts[i].title_ru}</td>
              <td>${posts[i].category}</td>
              <td>
                    <a class="btn btn-success btn-sm" href="http://gateway.nm/blog/update/${posts[i].id}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                                        <button type="button" class="btn btn-danger btn-sm d-inline" data-toggle="modal" data-target="#confirmationModal${posts[i].id}">
                      <i class="mdi mdi-delete"></i>
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="confirmationModal${posts[i].id}" tabindex="-1" role="dialog"                     aria-labelledby="confirmationModalLabel${posts[i].id}" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="confirmationModalLabel${posts[i].id}">Вы уверены?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                            <form class="d-inline" action="http://gateway.nm/blog/delete?id=${posts[i].id}" method="post">
                              <input type="hidden" name="_token"                    value="${token}">          <input type="hidden"                   name="_method" value="delete">                    <button type="submit" class="btn                    btn-primary">Продолжить</button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </td>
            </tr>
            `;
            table_body.append(markup);
          }
        }
      }
    });
  }
  $("#searchCategory").on("input", function() {
    search(
      { search: $(this).val(), per_page: $("#categoryQuantity").val() },
      "/blog/categories"
    );
  });
  $("#categoryQuantity").on("change", function() {
    let searchI = $("#searchCategory").val();
    let per_page = $("#categoryQuantity").val();
    search({ search: searchI, per_page: per_page }, "/blog/categories");
  });

  $("#searchPost").on("input", function() {
    search(
      {
        search: $(this).val(),
        per_page: $("#postQuantity").val(),
        status: $("#postStatus").val()
      },
      "/blog"
    );
  });
  $("#postQuantity").on("change", function() {
    let searchI = $("#searchPost").val();
    let per_page = $("#postQuantity").val();
    search(
      { search: searchI, per_page: per_page, status: $("#postStatus").val() },
      "/blog"
    );
  });
  $("#postStatus").on("change", function() {
    let searchI = $("#searchPost").val();
    let per_page = $("#postQuantity").val();
    search(
      { search: searchI, per_page: per_page, status: $("#postStatus").val() },
      "/blog"
    );
  });
  $("#shopStatus").on("change", function() {
    status = $(this).val();
    if (status === "2") {
      location.href = "/shop";
    } else {
      location.href = "/shop?status=" + status;
    }
  });
  function drawPagination(pagination_data, link) {
    let res = "";
    let last = pagination_data.last_page;
    let current = pagination_data.from;
    let per_page, search;
    if (link === "/blog/categories") {
      search = $("#searchCategory").val();
      per_page = $("#categoryQuantity").val();
    } else {
      search = $("#searchPost").val() + "&status=" + $("#postStatus").val();
      per_page = $("#postQuantity").val();
    }
    if (pagination_data.total > pagination_data.per_page) {
      res = `<ul class="pagination">`;
      if (!pagination_data.prev_page_url) {
        res += `<li class="page-item disabled"><a class="page-link">First</a></li>`;
        res += `<li class="page-item disabled"><a class="page-link"><<</a></li>`;
      } else {
        res += `<li class="page-item"><a class="page-link" href="${link}?search=${search}&per_page=${per_page}&page=1">First</a></li>`;
        res += `<li class="page-item"><a class="page-link" href="${link}?search=${search}&per_page=${per_page}&page=${current -
          1}"><<</a></li>`;
      }

      for (let i = current - 3; i < current; i++) {
        if (i > 0) {
          res += `<li class="page-item"><a class="page-link" href="${link}?search=${search}&per_page=${per_page}&page=${i}">${i}</a></li>`;
        }
      }
      for (let i = current; i <= current + 3; i++) {
        if (i <= last) {
          if (i == current) {
            res += `<li class="page-item active"><a class="page-link" href="#">${i}<span class="sr-only">Current</span></a></li>`;
          } else {
            res += `<li class="page-item"><a class="page-link" href="${link}?search=${search}&per_page=${per_page}&page=${i}">${i}</a></li>`;
          }
        }
      }

      if (!pagination_data.next_page_url) {
        res += `<li class="page-item disabled"><a class="page-link" href="#">>></a></li>`;
        res += `<li class="page-item disabled"><a class="page-link" href="#">Last</a></li>`;
      } else {
        res += `<li class="page-item"><a class="page-link" href="${link}?search=${search}&per_page=${per_page}&page=${current +
          1}">>></a></li>`;
        res += `<li class="page-item"><a class="page-link" href="${link}?search=${search}&per_page=${per_page}&page=${last}">Last</a></li>`;
      }
      res += `</ul>`;
      $("#pagination_component").html(res);
    } else {
      $("#pagination_component ul").remove();
      return;
    }
  }
  $("#logs-dates input").each(function() {
    $(this).datepicker({
      format: "yyyy-mm-dd"
    });
  });
  $("#params-from-submit-btn").on("click", function(e) {
    e.preventDefault();
    var _this = $(this),
      _form = _this.parents("form");
    _formData = new FormData(_form[0]);

    $.ajax({
      url: _form.attr("action"),
      type: "POST",
      enctype: "multipart/form-data",
      processData: false,
      contentType: false,
      data: _formData,
      success: function(resp) {
        if (resp.status == "success") {
          _form.submit();
        }
      },
      error: function(xhr) {
        for (let item in $.parseJSON(xhr.responseText).errors) {
          const element = $.parseJSON(xhr.responseText).errors[item];
          let messages = "";
          for (let index = 0; index < element.length; index++) {
            const msg = element[index];
            messages +=
              '<div class="invalid-feedback">This field is required.</div>';
          }

          let str = item.split(".");
          let input_name = "";
          for (let index = 0; index < str.length; index++) {
            const elem = str[index];
            if (index == 0) {
              input_name = elem;
            } else {
              input_name += "[" + elem + "]";
            }
          }
          $("body")
            .find('input[name="' + input_name + '"]')
            .addClass("is-invalid")
            .after(messages);
        }
      }
    });
  });
});
