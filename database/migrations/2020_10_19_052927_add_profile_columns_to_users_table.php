<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProfileColumnsToUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->string('path')->nullable();
			$table->string('surname')->nullable();
			$table->date('birthdate')->nullable();
			$table->bigInteger('phone')->nullable();
			$table->enum('sex', ['male', 'female'])->nullable();
			$table->string('nickname')->unique()->nullable();
			$table->integer('parent_id')->nullable();
			$table->integer('bonuses')->default(0);
			$table->decimal('balance', 19)->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->dropColumn('path');
			$table->dropColumn('surname');
			$table->dropColumn('birthdate');
			$table->dropColumn('phone');
			$table->dropColumn('sex', ['male', 'female']);
			$table->dropColumn('nickname');
			$table->dropColumn('parent_id');
			$table->dropColumn('bonuses');
			$table->dropColumn('balance');
		});
	}
}
