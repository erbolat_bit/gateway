<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $user = [];
    $faker = \Faker\Factory::create();
    for ($i = 0; $i < 120; $i++) {
      $user[] = [
        'name' => $faker->name(),
        'email' => $faker->email(),
        'password' => Hash::make('123456789'),
        'created_at' => $faker->dateTime()
      ];
    }
    DB::table('users')->insert($user);

    $user = Admin::create([
      'name' => 'Super Admin',
      'email' => 'superadmin@gmail.com',
      'password' => Hash::make('password123'),
      'is_admin' => 1
    ]);

    Role::create(['name' => 'Super Admin']);
    $user->assignRole('Super Admin');
  }
}
