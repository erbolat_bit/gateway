<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $categories = config('params.categories');
    $operations = config('params.operations');
    $permissions = [];
    foreach ($categories as $category) {
      foreach ($operations as $operation) {
        $permissions[] = [
          'name' => $category . '.' . $operation,
          'guard_name' => 'web',
          // 'created_at' => date("Y-m-d h:i:s")
        ];
      }
    }
    foreach ($permissions as $permission) {
      Permission::create($permission);
    }
  }
}
