@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Создать новый параметр</h3>
          <a class="btn btn-success float-right" href="{{ route('parameters.create.view') }}"> Создать новый
            параметр</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Имя</th>
                  <th>Тип</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['parametres']['data'] as $key => $param)
                <tr>
                  <td>{{ $param['id'] }}</td>
                  <td>{{ $param['title_ru'] }}</td>
                  <td>
                    {{ $param['type']}}
                  </td>
                  <td>
                    <a class="btn btn-success btn-sm"
                      href="{{ route('parameters.update.view',['id' => $param['id']]) }}" data-toggle="tooltip"
                      data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('parameters.destroy', $param['id']);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $param['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'parameters.index', 'paginator' => $data['parametres']])
</div>
@endsection
