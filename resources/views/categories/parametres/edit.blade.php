@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title float-left">Редактировать параметр</h3>
      <a class="btn btn-primary float-right" href="{{ route('parameters.index') }}"> Назад</a>
    </div>
    <form class="form-horizontal" action="{{route('parameters.update', $data['parameter']['id'])}}" method="post">
      @csrf
      <div class="row">
        <div class="col-md-4">
          <div class="card-body">
            <div class="form-group">
              <label for="cono1">Тип</label>
              <select name="type" class="select2 form-control custom-select type-select">
                <option selected>Choose type</option>
                @foreach ($data['types'] as $type)
                <option value="{{$type}}" {{ $data['parameter']['type'] == $type ? 'selected' : ''}}>{{$type}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group multiselect-confirm">
              <label>
                Multiselect
                <input type="checkbox" name="multiselect" {{$data['parameter']['multiselect'] == 1 ? 'checked' : ''}}>
              </label><br>
            </div>
            <div class="form-group">
              <label>Название</label><br>
              <label for="title_ru">Ru</label>
              <input type="text" id="title_ru" name="title_ru" class="form-control"
                value="{{ $data['parameter']['title_ru'] }}">
              <label for="title_uz">Uz</label>
              <input type="text" id="title_uz" name="title_uz" class="form-control"
                value="{{ $data['parameter']['title_uz'] }}">
              <label for="title_en">En</label>
              <input type="text" id="title_en" name="title_en" class="form-control"
                value="{{ $data['parameter']['title_en'] }}">
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="card-body params-sections">
            @if ($data['parameter']['type'] != 'text')
            <div class="{{ $data['parameter']['type'] }}" data-type="{{ $data['parameter']['type'] }}">
              <label>Возможные значения</label><br>
              @foreach ($data['parameter']['param_options'] as $item)
              <div class="row mb-4" id="{{ $item['id'] }}">
                <div class="col-md-3">
                  <label>Ru</label>
                  <input type="text"
                    name="{{ 'old['. $data['parameter']['type'] .']' . '['.$item['id'].'][title_ru]' }}"
                    value="{{ $item['title_ru'] }}" class="form-control">
                </div>
                <div class="col-md-3">
                  <label>Uz</label>
                  <input type="text"
                    name="{{ 'old['. $data['parameter']['type'] .']' . '['.$item['id'].'][title_uz]' }}"
                    value="{{ $item['title_uz'] }}" class="form-control">
                </div>
                <div class="col-md-3">
                  <label>En</label>
                  <input type="text"
                    name="{{ 'old['. $data['parameter']['type'] .']' . '['.$item['id'].'][title_en]' }}"
                    value="{{ $item['title_en'] }}" class="form-control">
                </div>
                <div class="col-md-3">
                  <button type="button" data-url="{{ route('parameter-option.delete', $item['id']) }}"
                    style="margin-top:28px;" class="btn btn-danger delete-option">Удалить значения</button>
                </div>
              </div>
              @endforeach
              <button type="button" style="margin-top:28px;" class="btn btn-info add-option">Добавить значения</button>
            </div>
            @else
            <div class="select" data-type="select">
              <label>Возможные значения</label><br>
              <div class="row " id="1">
              </div>
              <button type="button" style="margin-top:28px;" class="btn btn-info add-option">Добавить значения</button>
            </div>
            <div class="checkbox" data-type="checkbox">
              <label>Возможные значения</label><br>
              <div class="row " id="1">
              </div>
              <button type="button" style="margin-top:28px;" class="btn btn-info add-option">Добавить
                значения</button>
            </div>
            @endif
          </div>
        </div>
      </div>
      <div class="card-body">
        {!! $data['categories'] !!}
      </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
