@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Категории</h3>
          <a class="btn btn-success btn float-right" href="{{route('blog.categories.create.view')}}" title="">
            <i class="mdi mdi-plus"></i> Добавить
          </a>
        </div>
        <div>
          @php
          if(request()->has('search')){
          $value = request()->search;
          }else{
          $value = "";
          }
          @endphp
          <div class="d-flex w-full pl-4 pr-5 pt-2 justify-content-between">
            <div class="w-50">
              <label for="searchCategory">Поиск</label>
              <input type="text" value="{{$value}}" class="form-control" id="searchCategory">
            </div>
            <div class="ml-2" style="width: 80px">
              <label for="categoryQuantity">Количество</label>
              @php
              $options = [10, 25, 50, 75, 100];
              @endphp
              <select class="form-control" id="categoryQuantity">
                @foreach ($options as $option)
                <option @if (request()->per_page == $option)
                  selected
                  @endif value="{{$option}}">{{$option}}</option>
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table id="category_table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Заголовок</th>
                  <th>Действия</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data['categories']['data'] as $category)
                <tr>
                  <td>{{ $category['id'] }}</td>
                  <td>{{ $category['title_ru'] }}</td>
                  <td>
                    <a class="btn btn-success btn-sm"
                      href="{{route('blog.categories.update.view', ['id' => $category['id']])}}" data-toggle="tooltip"
                      data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('blog.categories.destroy', ['id' => $category['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $category['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      @include('components.pagination', ['paginator' => $data['categories'], 'route' => 'blog.categories'])
    </div>
  </div>
</div>

@endsection
