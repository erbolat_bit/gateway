@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Посты</h3>
          <a class="btn btn-success btn float-right" href="{{route('blog.posts.create.view')}}">
            <i class="mdi mdi-plus"></i> Добавить
          </a>
        </div>
        <div class="card-body">
          <div>
            @php
            if(request()->has('search')){
            $value = request()->search;
            }else{
            $value = "";
            }
            if(request()->has('status')){
            $status = request()->status;
            }else{
            $status = '1';
            }
            @endphp
            <div class="d-flex w-full pr-5 pt-2 pb-2 justify-content-between">
              <div class="w-50">
                <label for="searchPost">Поиск</label>
                <input type="text" value="{{$value}}" class="form-control" id="searchPost">
              </div>
              <div>
                <label for="postStatus">Статус</label>
                <select class="form-control" id="postStatus">
                  <option @if ($status=='1' ) selected @endif value="1">Active</option>
                  <option @if ($status=='0' ) selected @endif value="0">Inactive</option>
                </select>
              </div>
              <div class="ml-2" style="width: 80px">
                <label for="postQuantity">Количество</label>
                @php
                $options = [10, 25, 50, 75, 100];
                @endphp
                <select class="form-control" id="postQuantity">
                  @foreach ($options as $option)
                  <option @if (request()->per_page == $option)
                    selected
                    @endif value="{{$option}}">{{$option}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="posts_table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Заголовок</th>
                  <th>Категории</th>
                  <th>Действия</th>
                </tr>
              </thead>
              <tbody>
                @foreach($data['posts']['data'] as $post)
                <tr>
                  <td>{{ $post['id'] }}</td>
                  <td>
                    <a target="_blank" href="{{config('app.main_domain')}}/{{ app()->getLocale() }}/my-buynow/blog-posts/blog-post/{{ $post['alias'] }}">
                      {{ $post['title_ru'] }}
                    </a>
                  </td>
                  <td>{{$post['category']}}</td>
                  <td>
                    <a class="btn btn-success btn-sm" href="{{route('blog.posts.update.view', ['id' => $post['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('blog.posts.destroy', ['id' => $post['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $post['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    @include('components.pagination', ['paginator' => $data['posts'], 'route' => 'blog.posts'])
  </div>
</div>


@endsection
