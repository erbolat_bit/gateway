@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      @php
      $route = array_key_exists('post', $data) ? route('blog.posts.update', ['id' => $data['post']['id']]) :
      route('blog.posts.create');
      @endphp
      <form action="{{$route}}" method="post">

        <div class="card">

          <div class="card-body">
            <h4 class="card-title">{{(!empty($data['post'])) ? 'Изменить': 'Создать'}} Пост</h4>
            <a class="btn btn-primary float-right m-1" href="{{ route('blog.posts') }}"> Назад</a>
            @csrf

            <div class="form-group">
              <label for="lname" class="col-sm-3 control-label">Категория</label>
              <select name="category_id" id="" placeholder="Выберите категорию" , class="js-example-basic-multiple"
                style="height:36px; width:100%">
                @foreach ($data['categories'] as $k => $v)
                <option value="{{ $v['id'] }}">{{$v['title_ru']}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="lname" class="col-sm-3 control-label">Название (RU)</label>
              <div class="col-sm-9">
                <input type="text" name="title_ru" class="form-control" id="lname"
                  value="{{ (!empty($data['post'])) ? $data['post']['title_ru'] : null }}">
              </div>
            </div>


            <div class="form-group">
              <label for="lname" class="col-sm-3 control-label">Название (EN)</label>
              <div class="col-sm-9">
                <input type="text" name="title_en" class="form-control" id="lname"
                  value="{{ (!empty($data['post'])) ? $data['post']['title_en'] : null }}">
              </div>
            </div>

            <div class="form-group">
              <label for="lname" class="col-sm-3 control-label">Название (UZ)</label>
              <div class="col-sm-9">
                <input type="text" name="title_uz" class="form-control" id="lname"
                  value="{{ (!empty($data['post'])) ? $data['post']['title_uz'] : null }}">
              </div>
            </div>
            <div class="form-check">
              <input type="checkbox" name="status" @if ((!empty($data['post']))) @if ($data['post']['status']) checked
                @endif @endif class="form-check-input" id="status">
              <label class="form-check-label" for="status">Статус Активный</label>
            </div>
          </div>
        </div>
    </div>
    <div class="card">

      <div class="card-body">
        {{-- Short Text --}}
        <div class="form-group m-t-20">
          <label>Краткий текст (RU):</label>
          <textarea name="short_text_ru" id="editor"
            class="form-control editor">{{(!empty($data['post'])) ? $data['post']['short_text_ru'] : null}}</textarea>
        </div>

        <div class="form-group m-t-20">
          <label>Краткий текст (EN):</label>
          <textarea name="short_text_en" id="editor"
            class="form-control editor">{{(!empty($data['post'])) ? $data['post']['short_text_en'] : null}}</textarea>
        </div>

        <div class="form-group m-t-20">
          <label>Краткий текст (UZ):</label>
          <textarea name="short_text_uz" id="editor"
            class="form-control editor">{{(!empty($data['post'])) ? $data['post']['short_text_uz'] : null}}</textarea>
        </div>
        {{-- Fulll Text  --}}
        <div class="form-group m-t-20">
          <label>Текст (RU):</label>
          <textarea name="text_ru" id="editor"
            class="form-control editor">{{(!empty($data['post'])) ? $data['post']['text_ru'] : null}}</textarea>
        </div>

        <div class="form-group m-t-20">
          <label>Текст (EN):</label>
          <textarea name="text_en" id="editor"
            class="form-control editor">{{(!empty($data['post'])) ? $data['post']['text_en'] : null}}</textarea>
        </div>

        <div class="form-group m-t-20">
          <label>Текст (UZ):</label>
          <textarea name="text_uz" id="editor"
            class="form-control editor">{{(!empty($data['post'])) ? $data['post']['text_uz'] : null}}</textarea>
        </div>

        <div class="border-top">
          <div class="card-body">
            <button type="submit" class="btn btn-primary">Сохранить</button>
          </div>
        </div>

      </div>
      </form>
    </div>
  </div>
</div>
@endsection