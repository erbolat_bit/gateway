@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">

      <div class="card">

        <div class="card-body">
          <h4 class="card-title">Добавить категорию</h4>
          <a class="btn btn-primary float-right" href="{{ route('blog.categories') }}"> Назад</a>
          <form action="{{route('blog.categories.create')}}" method="post">
            @csrf
            <div class="form-group">
              <label for="title_ru" class="col-sm-3 control-label">Название (RU)</label>
              <div class="col-sm-9">
                <input type="text" name="title_ru" class="form-control" id="title_ru">
              </div>
            </div>


            <div class="form-group">
              <label for="title_en" class="col-sm-3 control-label">Название (EN)</label>
              <div class="col-sm-9">
                <input type="text" name="title_en" class="form-control" id="title_en">
              </div>
            </div>

            <div class="form-group">
              <label for="title_uz" class="col-sm-3 control-label">Название (UZ)</label>
              <div class="col-sm-9">
                <input type="text" name="title_uz" class="form-control" id="title_en">
              </div>
            </div>

            <div class="border-top">
              <div class="card-body">
                <button type="submit" class="btn btn-primary">Сохранить</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    @endsection
