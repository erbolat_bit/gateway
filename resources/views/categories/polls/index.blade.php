@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список опросов</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Магазин</th>
                  <th>Статус</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['polls']['data'] as $key => $poll)
                <tr>
                  <td>{{ $poll['id'] }}</td>
                  <td>{{ $poll['title'] }}</td>
                  <td>{{$poll['store_title']}}</td>
                  <td>
                    @if ($poll['validate'])
                    <i class="mdi mdi-check-all"></i>
                    @else
                    {{'Не подтверждено'}}
                    @endif
                  </td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('poll.show', ['id' => $poll['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    <a class="btn btn-success btn-sm" href="{{route('poll.update.view', ['id' => $poll['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('poll.delete', ['id' => $poll['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $poll['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'poll', 'paginator' => $data['polls']])
</div>
@endsection
