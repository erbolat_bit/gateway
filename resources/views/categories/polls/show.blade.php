@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Детали опросника</h3>
          <a class="btn btn-primary float-right" href="{{ route('poll') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Название:</strong>
                {{$data['poll']['title']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Статус:</strong>
                @if ($data['poll']['validate'])
                {{'Validated'}}
                @else
                {{'Not Validated'}}
                @endif
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Магазин:</strong>
                {{ $data['poll']['store_title']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Бонусы:</strong>
                {{ $data['poll']['bonuses']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Картинка:</strong>
                <img src="{{$data['poll']['photo']}}" class="mw-100" alt="" srcset="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
