@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Изменить
          </h3>
          <a class="btn btn-primary float-right" href="{{ route('poll') }}"> Назад</a>
        </div>
        <form action="{{route('poll.update', ['id' => $data['poll']['id']])}}" method="post"
          enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input type="text" name="title" class="form-control" value="{{$data['poll']['title']}}">
            </div>
            <div class="form-group">
              <div class="form-check">
                <input type="checkbox" name="validate" class="form-check-input" id="status"
                  @if($data['poll']['validate'])checked="true" @endif>
                <label class="form-check-label" for="status">Подтвердить</label>
              </div>
            </div>
            <div class="form-group">
              <label>Картинка</label><br>
              @if ($data['poll']['photo'])
              <img src="{{$data['poll']['photo']}}">
              @endif
              <input type="file" name="photo" class="form-control"">
            </div>
          </div>
          <div class=" border-top">
              <div class="card-body">
                <button type="submit" class="btn btn-primary">Сохранить</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
