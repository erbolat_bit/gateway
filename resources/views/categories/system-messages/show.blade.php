@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">{{$model->title}}</h3>
                    <a class="btn btn-primary float-right" href="{{ route('system-messages.index') }}"> Назад</a>
                </div>
                <div class="card-body">
                    @include('categories.system-messages.form', ['model' => $model])
                </div>
                <div class="card-body">
                    <h3 class="card-title float-left">Пользователи</h3>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Имя</th>
                                    <th>Никнейм</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $key => $user)
                                    <tr>
                                        <td>{{ $key }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->nickname }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
