@extends('layouts.app')

@section('inner_content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                {{-- @include('partials.alert') --}}
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title float-left">Список сообщений внутри платформы</h3>
                        <a class="btn btn-success float-right" href="{{ route('system-messages.create') }}"> Создать сообщение</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Название</th>
                                        <th>Сообщение</th>
                                        <th>Дата</th>
                                        <th width="150px">Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($system_messages as $key => $system_message)
                                        <tr>
                                            <td>{{ $key }}</td>
                                            <td>{{ $system_message->title }}</td>
                                            <td>{{ $system_message->message }}</td>
                                            <td>{{ $system_message->created_at->format('Y-m-d') }}</td>
                                            <td>
                                                <a class="btn btn-primary btn-sm" href="{{ route('system-messages.show', $system_message->id) }}" data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                                                    <i class="mdi mdi-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $system_messages->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


