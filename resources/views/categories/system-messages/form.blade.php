<form method="POST" action="{{ route('system-messages.store') }}" accept-charset="UTF-8">
  @csrf
  <div class="card-body">
    <div class="form-group m-t-20">
      <label for="">Название</label> 
      <input name="title" {{ $model ? 'disabled' : '' }} class="form-control" value="{{ old('title', $model && $model->title ? $model->title : '') }}">
    </div> 
    <div class="form-group m-t-20">
      <label for="">Пользователи</label> 
      <select {{ $model ? 'disabled' : '' }} name="filter_params[letter]" class="form-control js-example-basic" style="height: 36px; width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
        <option value="">Select</option>
        @foreach ($data['letters'] as $key => $item)
          <option value="{{ $key }}" {{ old('filter_params.letter') == $key || ($model && $model->filter_params['letter'] == $key) ? 'selected' : '' }}>{{ $item }}</option>
        @endforeach
      </select>
    </div> 
    <div class="row">
      <div class="col-md-3">
        <div class="form-group m-t-20 toggle-container">
          <div class="custom-control custom-checkbox mr-sm-2 ">
            <input {{ $model ? 'disabled' : '' }} type="checkbox" id="check1" name="filter_params[age]" {{ old('filter_params.age') || ($model && isset($model->filter_params['age']) && $model->filter_params['age'] == 'on') ? 'checked' : '' }} class="custom-control-input check-toggle"> 
            <label for="check1" class="custom-control-label">по возрасту</label>
          </div> 
          <div class="check-toggle-body" style="{{ old('filter_params.age') || ($model && isset($model->filter_params['age']) && $model->filter_params['age'] == 'on') ? 'display:block;' : '' }}">
            <div class="row ">
              <div class="col-md-4">
                <label for="">От</label> 
                <input {{ $model ? 'disabled' : '' }} name="filter_params[age_from]" type="text" class="form-control" value="{{ $model && $model->filter_params['age_from'] ? $model->filter_params['age_from'] : '' }}">
              </div> 
              <div class="col-md-4">
                <label for="">До</label> 
                <input {{ $model ? 'disabled' : '' }} name="filter_params[age_to]" type="text" class="form-control" value="{{ $model && $model->filter_params['age_to'] ? $model->filter_params['age_to'] : '' }}">
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="col-md-3">
        <div class="form-group m-t-20">
          <div class="custom-control custom-checkbox mr-sm-2">
            <input {{ $model ? 'disabled' : '' }} type="checkbox" name="filter_params[profile]" {{ old('filter_params.profile') || ($model && isset($model->filter_params['profile']) && $model->filter_params['profile'] == 'on') ? 'checked' : ''  }} id="check3" class="custom-control-input check-toggle"> 
            <label for="check3" class="custom-control-label">пользователям которые не до конца заполнили свою анкету</label>
          </div>
        </div>
      </div>
    </div>
    <div class="form-group m-t-20">
      <label>Cообщение:</label>
      <textarea {{ $model ? 'disabled' : '' }} placeholder="Cообщение" id="editor" name="message" cols="50" rows="10" class="form-control">{{ old('message', $model && $model->message ? $model->message : '') }}</textarea>
    </div> 
    <div class="form-group m-t-20">
      <label for="">Теги:</label>
        {user}
    </div>
  </div> 
  <div class="border-top">
    <div class="card-body">
      @if (!$model)
        <button type="submit" class="btn btn-primary">Отправить</button>
      @endif
    </div>
  </div>
</form>