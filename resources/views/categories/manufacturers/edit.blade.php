@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Редактировать производителея</h3>
          <a class="btn btn-primary float-right" href="{{ route('manufacturers.index') }}"> Назад</a>
        </div>
        <form action="{{route('manufacturers.update', ['id' => $data['manufacturer']['id']])}}" enctype="multipart/form-data" method="post">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input type="text" name="title" class="form-control" value="{{$data['manufacturer']['title']}}">
            </div>
            <div class="form-group">
              <label>Логотип</label><br>
              @if ($data['manufacturer']['logotype'])
                <img src="{{ $data['manufacturer']['logotype'] }}" width="100px" alt="" srcset="">
              <br>
              <br>
              @endif
              <input type="file" name="logotype" class="form-control">
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
