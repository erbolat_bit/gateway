@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Создать производителея</h3>
          <a class="btn btn-primary float-right" href="{{ route('manufacturers.index') }}"> Назад</a>
        </div>
        <form action="{{route('manufacturers.create')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input type="text" name="title" class="form-control">
            </div>
            <div class="form-group">
              <label>Логотип</label><br>
              <input type="file" name="logotype" class="form-control">
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
