@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список производителей</h3>
          <a class="btn btn-success float-right" href="{{ route('manufacturers.create') }}"> Создать производителея</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Имя</th>
                  <th>Логотип</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['manufacturers']['data'] as $key => $manufacturer)
                <tr>
                  <td>{{ $manufacturer['id'] }}</td>
                  <td>{{ $manufacturer['title'] }}</td>
                  <td>
                    <img src="{{ $manufacturer['logotype'] }}" alt="" sizes="" width="100px" srcset="">
                  </td>
                  <td>
                    <a class="btn btn-primary btn-sm"
                      href="{{ route('manufacturers.show', ['id' => $manufacturer['id']]) }}" data-toggle="tooltip"
                      data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    <a class="btn btn-success btn-sm"
                      href="{{ route('manufacturers.update.view',['id' => $manufacturer['id']]) }}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('manufacturers.destroy', ['id' => $manufacturer['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' =>
                    $manufacturer['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @php
  $paginator = $data['manufacturers'];
  $route = 'manufacturers.index';
  @endphp
  @include('components.pagination')
</div>
@endsection
