@extends('layouts.app')

@section('inner_content')
{{-- @php
          $thead = [
          'user_email' => 'Пользователь',
          'category_name' => 'Категория',
          'title' => 'Название',
          'country_name' => 'Страна',
          'manufacturer_name' => 'Производитель',
          'closed' => 'Статус'
          ];
          $tbody = $data['lots'];
          $operations = [
          'view' => [
          'link' => 'lot.show.1',
          'include_key' => 'id'
          ],
          'update' => [
          'link' => 'lot.update.view',
          'include_key' => 'id',
          ],
          'delete' => [
          'link' => 'lot.delete.1',
          'include_key' => 'id'
          ]
          ];
          @endphp
          @include('components.table', ['thead' => $thead, 'tbody' => $tbody, 'operations' => $operations]); --}}
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список аукционов</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Пользователь</th>
                  <th>Категория</th>
                  <th>Страна</th>
                  <th>Производитель</th>
                  <th>Статус</th>
                  <th>Картинка</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['lots'] as $key => $lot)
                <tr>
                  <td>{{ $lot['id'] }}</td>
                  <td>{{ $lot['title']}}</td>
                  <td>{{ $lot['user_email'] }} <br> {{ $lot['who_is'] }} </td>
                  <td>{{ $lot['category_name']}}</td>
                  <td>{{ $lot['country_name']}}</td>
                  <td>{{ $lot['manufacturer_name']}}</td>
                  <td>
                    @if ($lot['closed'])
                    {{'Закрыт'}}
                    @endif
                    @if ($lot['finish'])
                    {{'Закончен'}}
                    @endif
                    @if ($lot['sold'])
                    {{'Продан'}}
                    @endif
                  </td>
                  <td>
                    @if ($lot['photo'])
                      <img src="{{ $lot['photo']['photo'] }}" alt="" width="100px">
                    @endif
                  </td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('lot.show.1', ['id' => $lot['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    <a class="btn btn-success btn-sm" href="{{route('lot.update.view', ['id' => $lot['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('lot.delete.1', ['id' => $lot['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $lot['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'lot', 'paginator' => $data['pagination']])
</div>
@endsection
