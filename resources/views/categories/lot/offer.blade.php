@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список предложений</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Пользователь</th>
                  <th>Аукцион</th>
                  <th>Цена</th>
                  <th>Описание</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['offers'] as $key => $offer)
                <tr>
                  <td>{{ $offer['id'] }}</td>
                  <td>{{ $offer['user_email'] }}</td>
                  <td>{{ $offer['lot_title'] }}</td>
                  <td>{{ $offer['amount']}} {{$offer['currency']}}</td>
                  <td>{{ $offer['description']}}</td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('lot.show.1', ['id' => $offer['lot_id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    @php
                    $nextRoute = route('lot.offers.delete', ['id' => $offer['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $offer['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'lot.offers', 'paginator' => $data['pagination']])
</div>
@endsection
