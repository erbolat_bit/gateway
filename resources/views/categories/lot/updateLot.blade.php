@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Редактировать Аукцион</h3>
          <a class="btn btn-primary float-right" href="{{ route('lot') }}"> Назад</a>
        </div>
        <form method="post" action="{{route('lot.update.1', ['id' => $data['lot']['id']])}}">
          @csrf
          <div class=" card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input name="title" class="form-control" type="text" value="{{$data['lot']['title']}}">
            </div>
            <div class="form-group">
              <label>Описание:</label><br>
              <textarea rows="5" name="description" class="form-control"
                type="text">{{$data['lot']['description']}}</textarea>
            </div>
            <div class="form-group">
              <label>Длинное описание:</label><br>
              <textarea rows="7" name="detailed_description" class="form-control"
                type="text">{{$data['lot']['detailed_description']}}</textarea>
            </div>
            <div class="form-group" id="sandbox-container">
              <div class="input-daterange input-group" id="datepicker">
                <span>От: </span>
                <input type="text" class="input-sm form-control" name="start_date"
                  value="{{$data['lot']['start_date']}}" />
                <span class="input-group-addon">До: </span>
                <input type="text" class="input-sm form-control" name="end_date" value="{{$data['lot']['end_date']}}" />
              </div>
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
