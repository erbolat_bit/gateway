@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Показать детали Аукциона</h3>
          <a class="btn btn-primary float-right" href="{{ url()->previous() }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Пользователь:</strong>
                {{ $data['lot']['user_email'] }} <br>
                {{ $data['lot']['who_is']}}
              </div>
              <div class="form-group">
                <strong>Категория:</strong>
                {{ $data['lot']['category_name'] }}
              </div>
              <div class="form-group">
                <strong>Название:</strong>
                {{ $data['lot']['title'] }} <br>
                <strong>Описание:</strong>
                {{ $data['lot']['description']}} <br>
                <strong>Длинное описание:</strong>
                {{ $data['lot']['detailed_description']}}
              </div>
              <div class="form-group">
                <strong>Даты: От </strong>
                {{ $data['lot']['start_date'] }}
                <strong>До </strong>
                {{ $data['lot']['end_date']}}
              </div>
              <div class="form-group">
                <strong>Стартовая цена:</strong>
                {{ $data['lot']['convert_starting_price'] }} <br>
                <strong>Цена без торга:</strong>
                {{ $data['lot']['convert_without_bargaining_price']}}
              </div>
              <div class="form-group">
                <strong>Страна:</strong>
                {{ $data['lot']['country_name'] }}
              </div>
              <div class="form-group">
                <strong>Производитель:</strong>
                {{ $data['lot']['manufacturer_name'] }}
              </div>
              <div class="form-group">
                <strong>Просмотры:</strong>
                {{ $data['lot']['views'] }}
              </div>
              <div class="form-group">
                <strong>Колво. Предложений:</strong>
                {{ $data['lot']['offer_count'] }}
              </div>
              <div class="form-group">
                <strong>Статус:</strong>
                @if ($data['lot']['closed'])
                Закрыт
                @endif
                @if($data['lot']['finish'])
                Закончен
                @endif
                @if ($data['lot']['sold'])
                Продан
                @endif
              </div>
              <a href="{{route('lot.offers', ['lot_id' => $data['lot']['id']])}}">
                <button class="btn btn-success">Показать список предложений для текущего аукциона</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
