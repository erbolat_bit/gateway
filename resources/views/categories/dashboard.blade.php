@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h1 class="card-title mb-5">Статистика</h1>
                  <form action="{{route('dashboard')}}" method="GET" class="row align-bottom"
                      style="width: 66%">
                      <div class="d-flex flex-shrink-0 row col-6">
                          <label for="action" class="col-12">Дата</label>
                          <div class="input-group input-daterange" id="logs-dates">
                              <div class="input-group-addon mx-1">От</div>
                              <input type="text" class="form-control" name="from_date" autocomplete="off" value="{{ request('from_date') }}">
                              <div class="input-group-addon mx-1">До</div>
                              <input type="text" class="form-control" name="to_date" autocomplete="off" value="{{ request('to_date') }}">
                          </div>
                      </div>
                      <div class="col-4">
                        <label for="action" class="col-12">Магазины</label>
                        <select class="form-control" id="action" name="shop_id">
                          <option value="0">Select</option>
                          @foreach ($shops as $shop)
                            <option value="{{$shop['id']}}" {{ request('shop_id') == $shop['id'] ? 'selected' : null }}>
                              {{ $shop['title'] }}
                            </option>
                          @endforeach
                        </select>
                      </div>
                      <div class="col-2">
                        <label for="action" class="col-12"></label>
                        <button type="submit" class="btn btn-success mt-2">Поиск</button>
                      </div>
                  </form>
              </div>
              <div class="card-body">
                <h3>Посещение магазина</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="font-weight-bold">Всего пользователей</th>
                                <th class="font-weight-bold">Зарегестрированные пользователи</th>
                                
                                <th class="font-weight-bold">Пользователи с магазинами</th>
                                <th class="font-weight-bold">Пользователи с объявлениями</th>
                                <th class="font-weight-bold">Пользователи с аукционами</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>{{ $views['shop']['views'] }}</td>
                              <td>{{ $views['shop']['auth_user_views'] }}</td>
                              <td>{{ $views['shop']['auth_user_has_shop_views'] }}</td>
                              <td>{{ $views['shop']['auth_user_has_ad_views'] }}</td>
                              <td>{{ $views['shop']['auth_user_has_lot_views'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <h3>Посещение товаров</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="font-weight-bold">Всего пользователей</th>
                                <th class="font-weight-bold">Зарегестрированные пользователи</th>
                                
                                <th class="font-weight-bold">Пользователи с магазинами</th>
                                <th class="font-weight-bold">Пользователи с объявлениями</th>
                                <th class="font-weight-bold">Пользователи с аукционами</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>{{ $views['product']['views'] }}</td>
                              <td>{{ $views['product']['auth_user_views'] }}</td>
                              <td>{{ $views['product']['auth_user_has_shop_views'] }}</td>
                              <td>{{ $views['product']['auth_user_has_ad_views'] }}</td>
                              <td>{{ $views['product']['auth_user_has_lot_views'] }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <h3>Товары</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                                <th class="font-weight-bold">В избранном</th>
                                <th class="font-weight-bold">Покупки товаров</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>{{ isset($wishlist['count']) ? $wishlist['count'] : 0 }}</td>
                            <td>{{ $orders }}</td>
                          </tr>
                        </tbody>
                    </table>
                </div>
                <h3>Возраст</h3>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead class="bg-primary text-white">
                            <tr>
                              <tr>
                                @foreach ($users as $key => $item)
                                  <th class="font-weight-bold">{{ $key }}</th>                              
                                @endforeach
                              </tr>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            @foreach ($users as $key => $item)
                              <td>{{ $item }}</td>                              
                            @endforeach
                          </tr>
                        </tbody>
                    </table>
                </div>
              </div>
          </div>
      </div>
  </div>
  {{-- @php
  $paginator = $data['logs'];
  $route = 'logs.all';
  @endphp
  @include('components.pagination') --}}
</div>
@endsection
