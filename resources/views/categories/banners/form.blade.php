<form method="POST" 
      action="{{ $model ? route('banners.update', $model->id) : route('banners.store') }}" 
      accept-charset="UTF-8"
      enctype="multipart/form-data"
    >
  @csrf
  <div class="card-body">
    <div class="form-group m-t-20">
      <label for="">Тип</label> 
      <select name="type" class="form-control js-example-basic" style="height: 36px; width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
        <option value="">Select</option>
        <option value="1" {{ old('type') == 1 || $model && $model->type == 1 ? 'selected' : ''  }}>Большой баннер</option>
        <option value="2" {{ old('type') == 2 || $model && $model->type == 2 ? 'selected' : '' }}>Маленький баннер</option>
      </select>
    </div> 
    <div class="form-group m-t-20">
      <label>Сыылка:</label>
      <input type="text" class="form-control" name="link" value="{{ old('link', $model ? $model->link : '') }}">
    </div> 
    <div class="form-group m-t-20">
      @if ($model && $model->image)
          <img src="{{ $model->image }}" alt="" width="200px">
      @endif
      <input type="file" name="image" id="" value="{{ old('image') }}">
    </div>
  </div> 
  <div class="border-top">
    <div class="card-body">
      <button type="submit" class="btn btn-primary">Отправить</button>
    </div>
  </div>
</form>