@extends('layouts.app')

@section('inner_content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                {{-- @include('partials.alert') --}}
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title float-left">Баннеры</h3>
                        <a class="btn btn-success float-right" href="{{ route('banners.create') }}"> Создать баннер</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Ссылка</th>
                                        <th>Картинка</th>
                                        <th width="150px">Действие</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($banners as $key => $banner)
                                        <tr>
                                            <td>{{ $key }}</td>
                                            <td>{{ $banner->link }}</td>
                                            <td>
                                                <img src="{{ $banner->image }}" width="100px" alt="">
                                            </td>
                                            <td>
                                                <a 
                                                    class="btn btn-primary btn-sm" 
                                                    href="{{ route('banners.edit', $banner->id) }}" 
                                                    data-toggle="tooltip" 
                                                    data-placement="top" title="" 
                                                    data-original-title="Показать">
                                                    Обнавить
                                                </a>
                                                <a 
                                                    href="#" 
                                                    class="btn btn-sm btn-danger" 
                                                    onclick="
                                                        event.preventDefault(); 
                                                        if(confirm('Are you sure ?')){ 
                                                            document
                                                            .getElementById('banner_{{ $banner->id }}')
                                                            .submit()};">Удалить
                                                </a>
                                                <form id="banner_{{ $banner->id }}" action="{{ route('banners.destroy', $banner->id) }}" method="POST" class="d-none">
                                                    @csrf
                                                    <input type="hidden" name="_method" value="delete">
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $banners->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


