@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {{-- @include('partials.error-alert') --}}
                <div class="card-header">
                    <h3 class="card-title float-left">Обнавить баннер</h3>
                    <a class="btn btn-primary float-right" href="{{ route('banners.index') }}"> Назад</a>
                </div>
                @include('categories.banners.form', ['model' => $model])
            </div>
        </div>
    </div>
</div>
@endsection
