@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">Показать сообщений внутри платформы</h3>
                    <a class="btn btn-primary float-right" href="{{ route('system-messages.index') }}"> Назад</a>
                </div>
                <div class="card-body">
                    <label for="">Пользователи</label>
                    <ul>
                        @foreach ($systemMessage->position_id as $item)
                            <li>
                                {{ $letters[$item] }}
                            </li>
                        @endforeach
                    </ul>
                    <label for="">по возрасту</label>
                    <p>от {{ $systemMessage->categories['age_from'] }} до {{ $systemMessage->categories['age_to'] }}</p>
                    <label for="">по виду деятельности</label>
                    <ul>
                        @foreach ($systemMessage->categories['activities'] as $item)
                            <li>
                                {{ $categories[$item] }}
                            </li>
                        @endforeach
                    </ul>
                    <label for="">сообщения</label>
                    {!! $systemMessage->message !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
