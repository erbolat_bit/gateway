@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список стран</h3>
          <a class="btn btn-success float-right" href="{{ route('countries.create.view') }}"> Создать страну</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Флаг</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['countries']['data'] as $key => $country)
                <tr>
                  <td>{{ $country['id'] }}</td>
                  <td>{{ $country['title_ru'] }}</td>
                  <td>
                    {{ $country['flag']}}
                  </td>
                  <td>
                    <a class="btn btn-success btn-sm"
                      href="{{ route('countries.update.view',['id' => $country['id']]) }}" data-toggle="tooltip"
                      data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('countries.destroy');
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $country['id'],
                    'includeid' => true])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @php
  $paginator = $data['countries'];
  $route = 'countries.index';
  @endphp
  @include('components.pagination')
</div>
@endsection
