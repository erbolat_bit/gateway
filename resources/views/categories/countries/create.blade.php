@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Создать страну</h3>
          <a class="btn btn-primary float-right" href="{{ route('countries.index') }}"> Назад</a>
        </div>
        <form action="{{route('coutries.create')}}" method="post">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input class="form-control" type="text" name="title_ru" placeholder="Название">
            </div>
            <div class="form-group">
              <label>Флаг</label><br>
              <input class="form-control" type="text" name="flag" placeholder="Флаг">
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
