@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Показать параметр</h3>
          <a class="btn btn-primary float-right" href="{{ route('manufacturers.index') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Имя:</strong>
                {{ $manufacturer->title }}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Логотип:</strong>
                <img src="{{ $manufacturer->logotype }}" width="100px" alt="" srcset="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
