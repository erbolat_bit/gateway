@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <h1>All Users</h1>

  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['users'] as $user)
      <tr>
        <td>{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td>
          <a class="btn btn-primary" href="{{ route('users.user.edit', $user->id) }}">Update</a>
          <form 
            action="{{ route('users.user.destroy', $user->id) }}" 
            method="post"
            onsubmit="return confirm('Are you sure?')"
          >
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{$data['users']->links()}}
</div>
@endsection()
