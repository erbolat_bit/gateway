@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <h1>Permissions for {{$data['role']->name}}</h1>
  <form method="POST" action="{{route('permission.update', ['role' => $data['role']->id])}}">
    @csrf
    <div class="form-group">
      <label for="permissions">You can assign permissions to this role</label> <br>
      <div class="d-flex flex-wrap">
        @foreach ($data['allPermission'] as $key => $permission)
        <div class="p-2">
          <h5>{{__('permissions.' . $permission['name'])}}</h5>
          <div class="form-check">
            <input type="checkbox" @if (in_array($permission['name'] . '.update' , $data['permissionsOfRole']) &&
              in_array($permission['name'] . '.delete' , $data['permissionsOfRole']) && in_array($permission['name']
              . '.view' , $data['permissionsOfRole']) && in_array($permission['name'] . '.create' ,
              $data['permissionsOfRole'])) checked @endif class="form-check-input" id="{{$permission['name']}}.all*">
            <label class="form-check-label" for="{{$permission['name']}}.all*">Все</label>
          </div>
          @foreach ($permission['operations'] as $operation)
          <div class="form-check">
            <input type="checkbox" @if(in_array($permission['name'] . '.' . $operation, $data['permissionsOfRole']))
              checked @endif name="{{$permission['name'] . '.' . $operation}}" class="form-check-input"
              id="{{$permission['name'] . '.' . $operation}}">
            <label class="form-check-label {{$permission['name'] . '.' . $operation}}"
              for="{{$permission['name'] . '.' . $operation}}">{{__('permissions.' . $operation)}}</label>
          </div>
          @endforeach
        </div>
        @endforeach
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{route('users.admin.roles')}}"><button type="button" class="btn btn-danger">Отменить</button></a>
  </form>

</div>

@endsection()