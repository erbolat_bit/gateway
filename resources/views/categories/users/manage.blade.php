@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <h1>{{$data['admin']->name}}</h1>
  <form method="POST" action="{{route('users.admin.change', ['admin' => $data['admin']->id])}}">
    @csrf
    <div class="form-group">
      <label for="email">Email address</label>
      <input type="email" name="email" class="form-control" id="email" aria-describedby="emailHelp"
        value="{{$data['admin']->email}}">
      <small id="emailHelp" class="form-text text-muted">Email adress of the admin {{$data['admin']->name}}.</small>
    </div>
    <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" id="name" aria-describedby="nameHelp"
        value="{{$data['admin']->name}}">
      <small id="nameHelp" class="form-text text-muted">Username of the admin.</small>
    </div>
    <div class="form-group">
      <label for="password">Change Password</label>
      <input type="password" name="password" class="form-control" id="password" aria-describedby="passwordHelp">
      <small id="passwordHelp" class="form-text text-muted">You can change the password of the admin.</small>
    </div>
    <div class="d-flex justify-content-start">
      <div class="form-group col-md-3">
        <label for="departments">Отделение</label>
        <select id="departments" name="department_id" class="form-control js-example-basic">
          <option>Select</option>
          @foreach ($data['departments'] as $department)
          <option @if ($data['admin']['department_id']==$department['id']) selected @endif
            value="{{$department['id']}}">
            {{$department['title']}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="roles">Роли</label>
        <select id="roles" name="roles[]" class="form-control js-example-basic-multiple" multiple="multiple">
          @foreach ($data['roles'] as $role)
          <option @php if(in_array($role->name, $data['userRoles'])) echo 'selected' @endphp>{{$role->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
    <a href="{{route('users.admins')}}"><button type="button" class="btn btn-danger">Отменить</button></a>
  </form>
</div>

@endsection()