@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <h1>Admins</h1>

  <form autocomplete="off" method="post" action="{{route('users.admin.create')}}">
    @csrf
    <div class="form-row">
      <div class="form-group col-md-4">
        <label for="name">Name</label>
        <input type="text" name="name" class="form-control" id="name">
      </div>
      <div class="form-group col-md-4">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control" id="email">
      </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-3">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password">
      </div>
      <div class="form-group col-md-3">
        <label for="roles">Задать роль</label>
        <select id="roles" name="roles[]" class="form-control js-example-basic-multiple" multiple="multiple">
          @foreach ($data['roles'] as $role)
          <option>{{$role->name}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group col-md-3">
        <label for="departments">Отделение</label>
        <select id="departments" name="department_id" class="form-control js-example-basic">
          <option>Select</option>
          @foreach ($data['departments'] as $department)
          <option value="{{$department['id']}}">{{$department['title']}}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-4 p-3 d-flex align-items-end">
        <button type="submit" class="btn btn-primary">Создать Администратора</button>
      </div>
    </div>
  </form>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Электронная почта</th>
        <th scope="col">Отделение</th>
        <th scope="col">Имя</th>
        <th scope="col">Рейтинг</th>
        <th scope="col">Опции</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['admins']['data'] as $admin)
      <tr>
        <th scope="row">{{$admin['id']}}</th>
        <td>{{$admin['email']}}</td>
        <td>{{$admin['department'] ?? "Не задано"}}</td>
        <td>{{$admin['name']}}</td>
        <td>{{$admin['rating']}}</td>
        <td>
          <a class="btn btn-success btn-sm" href="{{route('users.admin.manage', ['admin' => $admin['id']])}}"
            data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
            <i class="mdi mdi-lead-pencil"></i>
          </a>
          @php
          $nextRoute = route('users.admin.delete', ['id' => $admin['id']]);
          @endphp
          @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' =>
          $admin['id']])
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @include('components.pagination', ['paginator' => $data['admins'], 'route' => 'users.admins'])
</div>
@endsection()