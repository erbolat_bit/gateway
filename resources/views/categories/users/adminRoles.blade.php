@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="d-flex justify-content-between">
    <h1>Admin Roles</h1>
    <form class="form-inline" autocomplete="off" method="post" action="{{route('role.create')}}">
      @csrf
      <div class="form-group mb-2">
        Создать новую роль
      </div>
      <div class="form-group mx-sm-3 mb-2">
        <label for="newRole" class="sr-only"></label>
        <input type="text" required name="role" class="form-control" id="newRole" placeholder="Названия роля">
      </div>
      <button type="submit" class="btn btn-primary mb-2">Создать</button>
    </form>
  </div>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Role Name</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data['roles'] as $key => $role)
      <tr>
        <th scope="row">{{$key + 1}}</th>
        <td>{{$role->name}}</td>
        <td>
          <a class="btn btn-success btn-sm" href="{{route('role.permissions', ['role' => $role->id])}}"
            data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
            <i class="mdi mdi-lead-pencil"></i>
          </a>
          @php
          $nextRoute = route('role.delete');
          @endphp
          @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' =>
          $role->id, 'includeid' => true])
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  {{$data['roles']->links()}}
</div>
@endsection()
