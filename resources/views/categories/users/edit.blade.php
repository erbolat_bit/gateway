@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title float-left">Редактировать пользователя ({{ $user->name }})</h3>
      <a class="btn btn-primary float-right" href="{{ route('users.all') }}"> Назад</a>
    </div>
    <form class="form-horizontal" action="{{ route('users.user.update', $user->id) }}" method="post">
      @method('PUT')
      @csrf
      <div class="row">
          <div class="card-body">
            <div class="row">
              <div class="form-group col-md-4">
                <label for="cono1">Имя</label>
                <input type="text" class="form-control" name="name" value="{{ old('name', $user->name) }}">
              </div>
              <div class="form-group col-md-4">
                <label for="cono1">Фамилия</label>
                <input type="text" class="form-control" name="surname" value="{{ old('surname', $user->surname) }}">
              </div>
              <div class="form-group col-md-4">
                <label for="cono1">Пароль</label>
                <input type="text" class="form-control" name="password" value="">
              </div>
              <div class="form-group col-md-4">
                <label for="cono1">Никнейм</label>
                <input type="text" class="form-control" name="nickname" value="{{ old('nickname', $user->nickname) }}">
              </div>
              <div class="form-group col-md-4">
                <label for="cono1">Емаил</label>
                <input type="text" class="form-control" name="email" value="{{ old('email', $user->email) }}">
              </div>
              <div class="form-group col-md-4">
                <label for="cono1">Телефон</label>
                <input type="text" class="form-control" name="phone" value="{{ old('phone', $user->phone) }}">
              </div>
              <div class="form-group col-md-4">
                <label>Дата рождения:</label> 
                <input type="text" class="form-control datetime" name="birthdate"  value="{{ old('birthdate', $user->birthdate) }}">
              </div>
              <div class="form-group col-md-4">
                <label for="cono1">Пол</label>
                <select name="sex" class="select2 form-control">
                  <option selected>Choose type</option>
                  @foreach ($user->sexList() as $key => $sex)
                    <option value="{{$key}}" {{ $key == $user->sex ? 'selected' : ''}}>{{$sex}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
      </div>
      <div class="border-top">
        <div class="card-body">
          <button type="submit" class="btn btn-primary">Сохранить</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
