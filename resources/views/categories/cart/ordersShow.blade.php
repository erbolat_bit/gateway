@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Заказ № {{$data['order']['id']}} </h3>
          <a class="btn btn-primary float-right" href="{{ route('cart.orders') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Пользователь:</strong>
                {{$data['order']['user_email']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Адреса:</strong>
                @foreach ($data['order']['address'] as $key => $address)
                <br>{{$key + 1}}:{{$address['address']}}
                @endforeach
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Тип оплаты:</strong>
                {{ $data['order']['payment']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Статус:</strong>
                {{$data['order']['status']}}
                @if (array_key_exists('error_message', $data['order']))
                <br>
                <strong>Ошибка</strong>
                {{$data['order']['error_message']}}
                @endif
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Дата:</strong>
                {{ $data['order']['created_at']}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if (array_key_exists('1', $data['order']['items']))

    @foreach ($data['order']['items']['1'] as $item)
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <strong>Продукт: </strong>{{$item['product_title']}}
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Магазин:</strong>
                {{$item['store_title']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Сумма:</strong>
                {{$item['amount']}}
                {{$item['currency']}}
                <strong>Цена доставки:</strong>
                {{$item['delivery_price']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Скидка:</strong>
                {{ $item['discount']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Количество:</strong>
                {{ $item['quantity']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Вид доставки:</strong>
                {{ $item['delivery_method']}}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Статус:</strong>
                {{ $item['status']}}
              </div>
            </div>
            @if($item['photo'])
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Картинка:</strong>
                <img src="{{$item['photo']}}" alt="">
              </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
    @endforeach
    @else
    <div class="card">
      <div class="card-header">Список товаров пустой!</div>
    </div>
    @endif

  </div>
</div>
@endsection
