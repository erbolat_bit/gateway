@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список Заказов</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Пользователь</th>
                  <th>Вид оплаты</th>
                  <th>Статус</th>
                  <th>Наличие ошибки</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['orders']['data'] as $key => $poll)
                <tr>
                  <td>{{ $poll['id'] }}</td>
                  <td>{{ $poll['user_email'] }}</td>
                  <td>{{ $poll['payment']}}</td>
                  <td>
                    {{ $poll['status_text']}}
                    @if ($poll['status'] == 4)
                      <form action="{{ route('cart.orders.update', $poll['id']) }}" method="post">
                        @csrf
                        <button type="submit" class="btn btn-primary btn-sm">
                          Подтвердить заказ
                        </button>
                      </form>
                    @endif
                  </td>
                  <td>
                    @if (array_key_exists('error_message', $poll))
                    {{$poll['error_message']}}
                    @else
                    {{"Без ошибок"}}
                    @endif
                  </td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('cart.orders.show', ['id' => $poll['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'cart.orders', 'paginator' => $data['pagination']])
</div>
@endsection
