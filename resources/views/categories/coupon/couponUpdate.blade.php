@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Изменить Купон</h3>
          <a class="btn btn-primary float-right" href="{{ route('coupon') }}"> Назад</a>
        </div>
        <form method="post" action="{{route('coupon.update', ['id' => $data['coupon']['id']])}}"
          enctype="multipart/form-data">
          @csrf
          <div class=" card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input name="title" class="form-control" type="text" value="{{$data['coupon']['title']}}">
            </div>
            <div class="form-group">
              <label>Описание:</label><br>
              <textarea name="description" class="form-control">{{$data['coupon']['description']}}</textarea>
            </div>
            @if ($data['coupon']['image'])
            <div class="form-group">
              <img src="{{$data['coupon']['image']}}" class="mw-100" alt="">
              <label>Картинка:</label><br>
              <input name="image" class="form-control" type="file">
            </div>
            @endif
            <div class="form-group" id="sandbox-container">
              <div class="input-daterange input-group" id="datepicker">
                <input type="text" class="input-sm form-control" name="start_date"
                  value="{{$data['coupon']['start_date']}}" />
                <span class="input-group-addon">to</span>
                <input type="text" class="input-sm form-control" name="end_date"
                  value="{{$data['coupon']['end_date']}}" />
              </div>
            </div>
            <div class="form-group">
              <label>Процентов:</label><br>
              <input name="percent" class="form-control" type="text" value="{{$data['coupon']['percent']}}">
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
