<form method="POST" action="{{ route('admin.coupon.store') }}" accept-charset="UTF-8">
  @csrf
  <div class="card-body">
    <div class="form-group m-t-20">
      <label for="">Пользователи</label> 
      <select name="filter_params[letter]" class="form-control js-example-basic" style="height: 36px; width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
        <option value="">Select</option>
        @foreach ($data['letters'] as $key => $item)
          <option value="{{ $key }}" {{ old('filter_params.letter') == $key ? 'selected' : '' }}>{{ $item }}</option>
        @endforeach
      </select>
    </div> 
    <div class="row">
      <div class="col-md-3">
        <div class="form-group m-t-20 toggle-container">
          <div class="custom-control custom-checkbox mr-sm-2 ">
            <input type="checkbox" id="check1" name="filter_params[age]" {{ old('filter_params.age') ? 'checked' : '' }} class="custom-control-input check-toggle"> 
            <label for="check1" class="custom-control-label">по возрасту</label>
          </div> 
          <div class="check-toggle-body" style="{{ old('filter_params.age') ? "display: block;" : '' }}">
            <div class="row ">
              <div class="col-md-4">
                <label for="">От</label> 
                <input name="filter_params[age_from]" type="text" value="{{ old('filter_params.age_from') }}" class="form-control">
              </div> 
              <div class="col-md-4">
                <label for="">До</label> 
                <input name="filter_params[age_to]" type="text" value="{{ old('filter_params.age_to') }}" class="form-control">
              </div>
            </div>
          </div>
        </div>
      </div> 
      <div class="col-md-3">
        <div class="form-group m-t-20">
          <div class="custom-control custom-checkbox mr-sm-2">
            <input type="checkbox" name="filter_params[profile]" {{ old('filter_params.profile') ? 'checked' : '' }} id="check3" class="custom-control-input check-toggle"> 
            <label for="check3" class="custom-control-label">пользователям которые не до конца заполнили свою анкету</label>
          </div>
        </div>
      </div>
    </div> 
    <div class="form-group m-t-20">
      <label>Название:</label> 
      <input class="form-control" type="text" value="{{ old('title', '') }}" name="title">
    </div> 
    <div class="form-group m-t-20">
      <label>Описание:</label> 
      <textarea placeholder="Cообщение" id="editor" name="description" cols="50" rows="10" class="form-control">
        {{ old('description', '') }}
      </textarea>
    </div>
    <div class="form-group m-t-20">
      <label>Процент:</label> 
      <input class="form-control" type="text" value="{{ old('percent', '') }}" name="percent">
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group m-t-20">
          <label>Дата начало:</label> 
          <input type="text" class="form-control datetime" name="start_date" autocomplete="off" value="{{ old('from_date', date('Y-m-d', time())) }}">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group m-t-20">
          <label>Дата окончания:</label> 
          <input type="text" class="form-control datetime" name="end_date" autocomplete="off" value="{{ old('end_date', date('Y-m-d', time() + (86000 * 7))) }}">
        </div>
      </div>
    </div>
  </div> 
  <div class="border-top">
    <div class="card-body">
      <button type="submit" class="btn btn-primary">Отправить</button>
    </div>
  </div>
</form>