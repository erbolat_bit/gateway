@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Показать детали купона</h3>
          <a class="btn btn-primary float-right" href="{{ route('coupon') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Название:</strong>
                {{ $data['coupon']['title'] }}
              </div>
              <div class="form-group">
                <strong>Описание:</strong>
                {{ $data['coupon']['description'] }}
              </div>
              <div class="form-group">
                <strong>Процент:</strong>
                {{ $data['coupon']['percent'] }}
              </div>
              <div class="form-group">
                <strong>Использованных:</strong>
                {{ $data['coupon']['used'] }}
                <strong>Не использованных:</strong>
                {{ $data['coupon']['not_used'] }}
              </div>
              <div class="form-group">
                <strong>От:</strong>
                {{ $data['coupon']['start_date'] }} <strong>До:</strong> {{$data['coupon']['end_date']}}
              </div>
              <div class="form-group">
                <strong>Создан:</strong>
                {{ $data['coupon']['created_at'] }}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Картинка :</strong>
                @if ($data['coupon']['image'])
                <img src="{{ $data['coupon']['image'] }}" class="mw-100" alt="">
                @else
                {{'Изображение отсутствует'}}
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
