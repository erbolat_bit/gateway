@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список купонов</h3>
          <a class="btn btn-success float-right" href="{{route('coupon.create')}}"> Создать купон</a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Магазин</th>
                  <th>Название</th>
                  <th>Описание</th>
                  <th class="d-flex justify-content-center align-items-center"><i class="mdi mdi-percent"></i></th>
                  <th>Картинка</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['coupons']['data'] as $key => $coupon)
                <tr>
                  <td>{{ $coupon['id'] }}</td>
                  <td>
                    @if (isset($coupon['store_title']) && $coupon['store_title'])
                      {{$coupon['store_title']}}
                    @else
                      Администратор
                    @endif
                  </td>
                  <td>{{ $coupon['title'] }}</td>
                  <td>{{ $coupon['description']}}</td>
                  <td>{{ $coupon['percent']}}%</td>
                  <td>
                    <img src="{{$coupon['image']}}" width="100px" alt="">
                  </td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('coupon.show', ['id' => $coupon['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    <a class="btn btn-success btn-sm" href="{{route('coupon.update.view', ['id' => $coupon['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'coupon', 'paginator' => $data['coupons']])
</div>
@endsection
