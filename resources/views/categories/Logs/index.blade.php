@extends('layouts.app')
@php
$action_types = [
'*',
'create',
'delete',
'update'
];
@endphp

@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <h3 class="card-title float-left">Логи</h3>
                    <form action="{{route('logs.all.search')}}" method="POST" class="row align-bottom"
                        style="width: 66%">
                        @csrf
                        <div class="col-4" style="width: 40%;">
                            <label for="action">Action</label>
                            <select class="form-control" id="action" name="action">
                                @foreach($action_types as $at)
                                <option @if ($data['action']==$at) selected @endif>{{$at}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="d-flex flex-shrink-0 row col-8" style="width: 60%;">
                            <label for="action" class="col-12">Даты</label>
                            <div class="input-group input-daterange" id="logs-dates">
                                <input type="text" class="form-control" name="from_date" autocomplete="off"
                                    @unless(is_null($data['from_date'])) value="{{$data['from_date']}}" @endunless>
                                <div class="input-group-addon mx-1">До</div>
                                <input type="text" class="form-control" name="to_date" autocomplete="off"
                                    @unless(is_null($data['to_date'])) value="{{$data['to_date']}}" @endunless>
                                <button type="submit" class="btn btn-success ml-1">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="bg-primary text-white">
                                <tr>
                                    <th class="font-weight-bold">№</th>
                                    <th class="font-weight-bold">Имя пользователя</th>
                                    <th class="font-weight-bold">Тип пользователя</th>
                                    <th class="font-weight-bold">Действие</th>
                                    <th class="font-weight-bold">API</th>
                                    <th class="font-weight-bold">Параметры</th>
                                    <th class="font-weight-bold">Дата</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['logs']['data'] as $log)
                                <tr>
                                    <td>{{ $log['id'] }}</td>
                                    <td>{{ $log['user_name'] }}</td>
                                    <td>{{ $log['user_role'] }}</td>
                                    <td style="font-size: 20px"><b>
                                            @if($log['action'] == 'delete')
                                            <span class="badge badge-danger">{{$log['action']}}</span>
                                            @elseif($log['action'] == 'update')
                                            <span class="badge badge-info">{{$log['action']}}</span>
                                            @elseif($log['action'] == 'create')
                                            <span class="badge badge-success">{{$log['action']}}</span>
                                            @else
                                            <span class="badge badge-dark">{{$log['action']}}</span>
                                            @endif
                                        </b> <br></td>
                                    <td>
                                        {{ $log['route'] }}
                                    </td>
                                    <td style="width: 450px">
                                        @foreach($log['properties'] as $k => $v)
                                        <b>{{$k}}</b> : {{is_string($v) ? $v : 'Content'}} <br>
                                        @endforeach
                                    </td>
                                    <td style="width: 200px">{{ $log['created_at'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
    $paginator = $data['logs'];
    $route = 'logs.all';
    @endphp
    @include('components.pagination')
</div>
@endsection