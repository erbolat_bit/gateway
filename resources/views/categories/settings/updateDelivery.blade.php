@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Изменить
          </h3>
          <a class="btn btn-primary float-right" href="{{ route('setting.delivery') }}"> Назад</a>
        </div>
        <form action="{{route('setting.delivery.update', ['id' => $data['setting']['id']])}}" method="post">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input type="text" name="title_ru" class="form-control" value="{{$data['setting']['title_ru']}}">
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label>Сумма</label><br>
              <input type="text" name="amount" class="form-control" value="{{$data['setting']['amount']}}">
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
              <label for="currency">Валюта</label>
              <select id="currency" name="currency" class="form-control">
                @foreach ($data['setting']['currencies'] as $currency)
                <option value="{{$currency}}" @if($currency==$data['setting']['currency']) {{'selected'}} @endif>
                  {{$currency}}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
