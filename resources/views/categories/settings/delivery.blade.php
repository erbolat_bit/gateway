@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список доставки</h3>
          <a class="btn btn-success float-right" href="{{route('setting.delivery.create.view')}}"> Создать </a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Сумма</th>
                  <th>Валюта</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['delivery']['data'] as $key => $delivery)
                <tr>
                  <td>{{ $delivery['id'] }}</td>
                  <td>{{ $delivery['title_ru'] }}</td>
                  <td>
                    {{ $delivery['amount']}}
                  </td>
                  <td>{{$delivery['currency']}}</td>
                  <td>
                    <a class="btn btn-success btn-sm"
                      href="{{route('setting.delivery.update.view', ['id' => $delivery['id']])}}" data-toggle="tooltip"
                      data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('setting.delivery.delete', ['id' => $delivery['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $delivery['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'delivery', 'paginator' => $data['delivery']])
</div>

@endsection
