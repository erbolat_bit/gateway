@extends('layouts.app')

@section('inner_content')

<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список социальных сети</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Описание</th>
                  <th>Client ID</th>
                  <th>Secret Key</th>
                  <th>Callback url</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['settings']['data'] as $key => $setting)
                <tr>
                  <td>{{ $setting['id'] }}</td>
                  <td>{{ $setting['description'] }}</td>
                  <td>{{$setting['client_id']}}</td>
                  <td>{{$setting['client_secret']}}</td>
                  <td>{{$setting['redirect']}}</td>
                  <td>
                    <a class="btn btn-success btn-sm" href="{{ route('setting.update.view',['id' => $setting['id']]) }}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'setting.socials', 'paginator' => $data['settings']])
</div>

@endsection
