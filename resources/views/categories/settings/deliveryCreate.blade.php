@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Создать
          </h3>
          <a class="btn btn-primary float-right" href="{{ route('setting.delivery') }}"> Назад</a>
        </div>
        <form action="{{route('setting.delivery.create')}}" method="post">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input type="text" name="title_ru" class="form-control" placeholder="Название">
            </div>
            <div class="form-group">
              <label>Сумма</label><br>
              <input type="text" name="amount" placeholder="Сумма" class="form-control"">
            </div>
              <div class=" form-group">
              <label for="currency">Валюта</label>
              <select id="currency" name="currency" class="form-control">
                @foreach ($data['currencies'] as $currency)
                <option value="{{$currency}}">
                  {{$currency}}
                </option>
                @endforeach
              </select>
            </div>
            <div class="border-top">
              <div class="card-body">
                <button type="submit" class="btn btn-primary">Сохранить</button>
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
