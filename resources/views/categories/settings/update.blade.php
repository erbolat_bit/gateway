@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Изменить
          </h3>
          <a class="btn btn-primary float-right" href="{{ route('setting.socials') }}"> Назад</a>
        </div>
        <form action="{{route('setting.update', ['id' => $data['setting']['id']])}}" method="post">
          @csrf
          @if ($data['setting']['type'] == 'social')
          <div class="card-body">
            <h2>{{$data['setting']['description']}}</h2>
            <div class="form-group m-t-20">
              <label>Client Id</label><br>
              <input type="text" name="client_id" class="form-control" value="{{$data['setting']['client_id']}}">
            </div>
            <div class="form-group">
              <label>Secret Key</label><br>
              <input type="text" name="client_secret" class="form-control"
                value="{{$data['setting']['client_secret']}}">
            </div>
            <div class="form-group">
              <label>Callback url</label><br>
              <input type="text" name="redirect" class="form-control" value="{{$data['setting']['redirect']}}">
            </div>
          </div>
          @else
          <div class="card-body">
            <div class="form-group">
              <label>{{$data['setting']['description']}}</label><br>
              <input type="text" name="value" class="form-control" value="{{$data['setting']['value']}}">
            </div>
          </div>
          @endif
          <input type="hidden" name="type" value="{{$data['setting']['type']}}">
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
