@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список тикетов</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Пользователь</th>
                  <th>Категоря</th>
                  <th>Статус</th>
                  <th>Создано</th>
                  <th>Последнее сообщение</th>
                  <th>Непрочитанные</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($ticktes['data'] as $key => $ticket)
                  <tr>
                    <td>{{ $ticket['id'] }}</td>
                    <td>{{ $ticket['user']['name'] }}</td>
                    <td>{{ $ticket['category']['title']}}</td>
                    <td>
                      @if ($ticket['status'] == 1)
                        <span class="p-2 bg-success">
                      @elseif($ticket['status'] == 2)
                        <span class="p-2 bg-warning">
                      @else
                        <span class="p-2 bg-secondary">
                      @endif
                        {{ $ticket['status_text'] }}
                      </span>
                    </td>
                    <td>{{ date('Y-m-d H:i:s', strtotime($ticket['created_at'])) }}</td>
                    <td>
                      User: {{ $ticket['message']['user']['name'] }} <br>
                      Message: {{ $ticket['message']['message'] }}
                    </td>
                    <td><span class="btn btn-info btn-circle">{{ $ticket['measseges_not_read_count'] }}</span></td>
                    <td>
                      <a class="btn btn-primary btn-sm" href="{{route('ticket.show', ['ticket_id' => $ticket['id']])}}"
                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                        <i class="mdi mdi-eye"></i>
                      </a>
                      {{-- <a class="btn btn-success btn-sm" href="{{route('shop.update.view', ['id' => $ticket['id']])}}"
                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                        <i class="mdi mdi-lead-pencil"></i>
                      </a> --}}
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'ticket.index', 'paginator' => $ticktes['pagination']])
</div>
@endsection
