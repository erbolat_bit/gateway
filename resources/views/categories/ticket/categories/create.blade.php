@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">Создать категорию для тикетов</h3>
                    <a class="btn btn-primary float-right" href="{{ route('ticket.categories') }}"> Назад</a>
                </div>
                <form action="{{route('ticket.categories.create')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="department_id">Отделение </label>
                            <select class="form-control" name="department_id" id="department_id" required>
                                <option value="none">Выбрать</option>
                                @foreach ($data['departments'] as $department)
                                <option value="{{$department['id']}}">{{$department['title']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Название(RU)</label><br>
                            <input type="text" name="title_ru" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Название(EN)</label><br>
                            <input type="text" name="title_en" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Название(UZ)</label><br>
                            <input type="text" name="title_uz" class="form-control">
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection