@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">Список категорий тикетов</h3>
                    <a class="btn btn-success float-right" href="{{ route('ticket.categories.create.view') }}"> Создать
                        категорию</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Название</th>
                                    <th>Отделение</th>
                                    <th>Колво. активных тикетов</th>
                                    <th width="150px">Действие</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['ticketCategories']['data'] as $ticketCategory)
                                <tr>
                                    <td>{{ $ticketCategory['id'] }}</td>
                                    <td>{{ $ticketCategory['title'] }}</td>
                                    <td>{{ $ticketCategory['department'] }}</td>
                                    <td>{{ $ticketCategory['active_ticket_count']}}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm"
                                            href="{{ route('ticket.categories.update.view',['id' => $ticketCategory['id']]) }}"
                                            data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="Редактировать">
                                            <i class="mdi mdi-lead-pencil"></i>
                                        </a>
                                        @php
                                        $nextRoute = route('ticket.categories.delete', ['id' => $ticketCategory['id']]);
                                        @endphp
                                        @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' =>
                                        $ticketCategory['id']])
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
    $paginator = $data['ticketCategories'];
    $route = 'ticket.categories';
    @endphp
    @include('components.pagination')
</div>
@endsection