@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Тикет № {{ $ticket_id }}</h3>
          <a class="btn btn-success float-right" href="{{route('ticket.index')}}"> Назад </a>
          <form action="{{ route('ticket.update', ['ticket_id' => $ticket_id]) }}" method="post">
            @csrf
            <input type="hidden" name="status" value="3">
            <button type="submit" class="btn btn-primary mr-3 float-right">Закрыть</button>
          </form>
        </div>
        <div class="card-body">
            <div class="chat-box scrollable" style="height:475px;">
                <!--chat Row -->
                <ul class="chat-list">
                  @foreach ($messages['data'] as $message)
                    <!--chat Row -->
                    <li class="chat-item {{ $message['user_id'] == $user->id ? 'odd' : '' }}">
                      <div class="chat-content">
                        <div class="chat-img"><img src="{{ $message['user']['path'] ?? '/theme/assets/images/users/1.jpg' }}" alt="user"></div>
                        <div class="box bg-light-success">
                          <h5 class="font-medium">{{ $message['user']['name'] }}</h5>
                          <p class="font-light mb-0">{{ $message['message'] }}</p>
                          <div class="chat-time">{{ date('Y-m-d H:i:s', strtotime($message['created_at'])) }}</div>
                        </div>
                      </div>
                    </li>
                  @endforeach
                </ul>
            </div>
        </div>
        <div class="card-footer border-top">
          <form action="{{ route('ticket.message.store', ['ticket_id' => $ticket_id]) }}" method="post">
            @csrf
            <div class="row">
                <div class="col-10">
                    <input type="hidden" name="ticket_id" value="{{ $ticket_id }}">
                    <div class="input-field mt-0 mb-0">
                        <textarea name="message" id="textarea1" placeholder="Type and enter" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-2">
                  <button type="submit" class="btn btn-circle btn-lg btn-cyan float-right text-white"><i class="fas fa-paper-plane"></i></button>
                </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-3">
      <div class="card">
        <div class="card-header">
          <h4 class="card-title float-left">Добавить пользователя в чат</h4>
        </div>
        <div class="card-body">
          <form action="{{ route('ticket.add.audits', ['ticket_id' => $ticket_id]) }}" method="post">
            @csrf
            <select name="user_ids[]" class="form-control js-example-basic-multiple" multiple="multiple" placeholder="Select">
              @foreach ($users as $user)
                <option value="{{ $user->id }}">{{$user->name}}</option>
              @endforeach
            </select>
            <hr>
            <button type="submit" class="btn btn-primary">Добавить</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
