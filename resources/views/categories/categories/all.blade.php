@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="card">
    <div class="card-header">
      <h3 class="card-title float-left">Список категории</h3>
      <a class="btn btn-success float-right" href="{{route('categories.create.view')}}"> Создать категорию</a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>№</th>
              <th>Имя</th>
              <th>Дочерние категории</th>
              <th>Иконка</th>
              <th width="150px">Действие</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($data['categories']['data'] as $key => $category)
            <tr>
              <td>{{ $category['id'] }}</td>
              <td>{{ $category['title_ru'] }}</td>
              <td>
                @if ($category['children'])
                <a href="{{route('categories', ['parent_id' => $category['id']])}}"
                  class="btn btn-outline-secondary">Дочерние категории</a>
                @else
                Нет дочерных елементов
                @endif
              </td>
              <td>
                <img src="{{$category['image']}}" alt="" sizes="" width="100px" srcset="">
              </td>
              <td>
                <a class="btn btn-primary btn-sm" href="{{route('categories.show', ['id' => $category['id']])}}"
                  data-toggle=" tooltip" data-placement="top" title="" data-original-title="Показать">
                  <i class="mdi mdi-eye"></i>
                </a>
                <a class="btn btn-success btn-sm" href="{{route('categories.update.view', ['id' => $category['id']])}}"
                  data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                  <i class="mdi mdi-lead-pencil"></i>
                </a>
                @php
                $nextRoute = route('categories.delete', ['id' => $category['id']]);
                @endphp
                @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' =>
                $category['id']])
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {{-- Example of using pagination component --}}
  @php
  $paginator = $data['categories'];
  $route = 'categories';
  @endphp
  @include('components.pagination')
</div>
@endsection