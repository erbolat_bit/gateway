@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Update Category</h3>
          <a class="btn btn-primary float-right" href="{{route('categories')}}"> Назад</a>
        </div>
        <div class="card-body">
          <form enctype="multipart/form-data" action="{{route('categories.update', ['id' => $data['category']['id']])}}"
            method="post">
            @csrf
            <div class="form-group m-t-20">
              <label>Радитель</label><br>
              <select name="parent_id" id="" class="js-example-basic-multiple" style="height:36px; width:100%">
                <option value="0" @php if($data['category']['parent_id']==0) echo 'selected' ; @endphp>Основная
                  категория
                </option>
                @foreach ($data['categories'] as $key => $item)
                <option @php if($item['id']==$data['category']['parent_id']) echo 'selected' ; @endphp
                  value="{{ $item['id'] }}">{{ $item['title'] }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <p>
                <strong>Название</strong>
              </p>
              <label for="title_ru">Ru</label>
              <input type="text" name="title_ru" class="form-control" value="{{$data['category']['title_ru']}}"
                id="title_ru" aria-describedby="nameHelp">
              <label for="title_uz">Uz</label>
              <input type="text" name="title_uz" class="form-control" value="{{$data['category']['title_uz']}}"
                id="title_uz" aria-describedby="nameHelp">
              <label for="title_en">En</label>
              <input type="text" name="title_en" class="form-control" value="{{$data['category']['title_en']}}"
                id="title_en" aria-describedby="nameHelp">
            </div>
            <div class="form-group">
              <label>Иконка</label><br>
              <img class="mw-100" src="{{$data['category']['image']}}" alt="">
              <input type="file" name="image">
            </div>
        </div>
        <div class="border-top">
          <div class="card-body">
            <button type="submit" class="btn btn-primary">Сохранить</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection