@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Показать категорию</h3>
          <a class="btn btn-primary float-right" href="{{ route('categories') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Родитель:</strong>
                @if (isset($data['parent']['title_ru']))
                {{$data['parent']['title_ru']}}
                @else
                Основная категория
                @endif
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Имя:</strong>
                {{ $data['category']['title_ru'] }}
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Иконка:</strong> <br>
                @if ($data['category']['image'])
                <img src="{{ $data['category']['image'] }}" class="mw-100" alt="" srcset="">
                @else
                Category Doesn't have an Image
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
