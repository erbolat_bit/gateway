@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список объявлении</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Пользователь</th>
                  <th>Категория</th>
                  <th>Количество</th>
                  <th>Страна</th>
                  <th>Производитель</th>
                  <th>Продажа</th>
                  <th>Картинка</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['ads']['data'] as $key => $ad)
                <tr>
                  <td>{{ $ad['id'] }}</td>
                  <td>
                    {{-- <a href="categorys/elektronika/televizory-hi-fi-i-video/subcategory/televizory/ad/14"> --}}
                      {{ $ad['title']}}
                    {{-- </a> --}}
                  </td>
                  <td>{{ $ad['user_email'] }}</td>
                  <td>{{ $ad['category_name'] }}</td>
                  <td>{{ $ad['quantity'] }}</td>
                  <td>{{ $ad['country_name'] }}</td>
                  <td>{{ $ad['manufacturer_name'] }}</td>
                  <td>{{ $ad['sale'] }}</td>
                  <td>
                    @if ($ad['photo'])
                      <img src="{{ $ad['photo']['photo'] }}" alt="" width="100px">
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'ads', 'paginator' => $data['ads']])
</div>
@endsection
