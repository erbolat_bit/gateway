@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Детали продукта №{{$data['product']['id']}}</h3>
          <a class="btn btn-primary float-right" href="{{ route('shop.products') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Магазин:</strong>
                {{ $data['product']['store']['title'] }}
              </div>
              <div class="form-group">
                <strong>Категория:</strong>
                {{ $data['product']['category_name'] }}
              </div>
              <div class="form-group">
                <strong>Название:</strong>
                {{ $data['product']['title'] }}
              </div>
              <div class="form-group">
                <strong>Цена:</strong>
                {{ $data['product']['price'] }} {{$data['product']['currency']}}
              </div>
              <div class="form-group">
                <strong>Скидка:</strong>
                {{ $data['product']['discount_price'] }} {{$data['product']['currency']}}
              </div>
              <div class="form-group">
                <strong>Количество:</strong>
                {{ $data['product']['quantity'] }} {{$data['product']['quantity_size']}}
              </div>
              <div class="form-group">
                <strong>Описание:</strong>
                <div class="jumbotron p-2">
                  {!! $data['product']['description'] !!}
                </div>
              </div>
              <div class="form-group">
                <strong>Страна:</strong>
                {{ $data['product']['country_name'] }}
              </div>
              <div class="form-group">
                <strong>Производитель:</strong>
                {{ $data['product']['manufacturer_name'] }}
              </div>
              <div class="form-group">
                <strong>Детальное описание:</strong>
                <div class="jumbotron p-2">
                  {!! $data['product']['detailed_description'] !!}
                </div>
              </div>
              <div class="form-group">
                <strong>Описание достаки:</strong>
                <div class="jumbotron p-2">
                  {!! $data['product']['delivery_description'] !!}
                </div>
              </div>
              <div class="form-group">
                <strong>Просмотров:</strong>
                {{ $data['product']['views'] }}
              </div>
              <div class="form-group">
                <strong>Картинки:</strong> <br>
                @foreach ($data['product']['photos'] as $photo)
                <img src="{{$photo['photo']}}" alt="" style="max-width: 100%">
                @endforeach
              </div>
              {{-- <div class="form-group">
                <strong>Документы:</strong>
                {{ $data['product']['documents'] }}
            </div> --}}
            <a href="{{route('shop.show', ['id' => $data['product']['store_id']])}}">
              <button class="btn btn-success">Показать Магазин продукта</button>
            </a>
            <a
              href="https://uzexpress.ru/ru/store-inside/{{$data['product']['store']['id']}}/product/{{$data['product']['id']}}">
              <button class="btn btn-primary">Показать На Сайте</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection