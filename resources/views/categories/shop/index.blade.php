@extends('layouts.app')
@php
$status = request()->has('status') ? request()->status :'2';
$statusOptions = [
'2' => 'All',
'1' => 'Active',
'0' => 'Inactive'
];
@endphp

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header d-flex justify-content-between">
          <div>
            <h3 class="card-title float-left">Список @if ($status == '1')
              активних
              @elseif($status == '0')
              не активных
              @else
              всех
              @endif магазинов</h3>
          </div>
          <div>
            <select class="custom-select" id="shopStatus">
              @foreach ($statusOptions as $key => $option)
              <option @if ($status==$key) selected @endif value="{{$key}}">{{$option}}</option>
              @endforeach
            </select>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Название</th>
                  <th>Пользователь</th>
                  <th>Субдомен</th>
                  <th>Тип</th>
                  <th>Просмотры</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['shops'] as $key => $shop)
                <tr>
                  <td>{{ $shop['id'] }}</td>
                  <td><a href="{{ config('app.main_domain') . '/' . $shop['subdomain'] }}" target="_blank">{{ $shop['title'] }}</a></td>
                  <td>{{ $shop['user_email'] }}</td>
                  <td>{{ $shop['subdomain']}}</td>
                  <td>{{ $shop['type']}}</td>
                  <td>{{ $shop['views']}}</td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('shop.show', ['id' => $shop['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    <a class="btn btn-success btn-sm" href="{{route('shop.update.view', ['id' => $shop['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('shop.delete.1', ['id' => $shop['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $shop['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'shop', 'paginator' => $data['pagination']])
</div>
@endsection
