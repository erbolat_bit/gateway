@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Детали магазина №{{$data['shop']['id']}}</h3>
          <a class="btn btn-primary float-right" href="{{ route('shop') }}"> Назад</a>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
              <div class="form-group">
                <strong>Полюзователь:</strong>
                {{ $data['shop']['user_email'] }}
              </div>
              <div class="form-group">
                <strong>Назание:</strong>
                {{ $data['shop']['title'] }}
              </div>
              <div class="form-group">
                <strong>Тип:</strong>
                {{ $data['shop']['type'] }}
              </div>
              <div class="form-group">
                <strong>Описание:</strong>
                {{ $data['shop']['description'] }}
              </div>
              <div class="form-group">
                <strong>Логотип:</strong>
                @if ($data['shop']['logotype'])
                <img class="mw-100" src="{{ $data['shop']['logotype'] }}" alt="">
                @else
                Магазин без логотипа
                @endif
              </div>
              <div class="form-group">
                <strong>Субдомен:</strong>
                {{ $data['shop']['subdomain'] }}
              </div>
              <div class="form-group">
                <strong>Количество просмотров:</strong>
                {{ $data['shop']['views'] }}
              </div>
              <div class="form-group">
                <strong>Количество продуктов:</strong>
                @if (array_key_exists('product_count',$data['shop']))
                {{ $data['shop']['product_count'] }}
                @else
                0
                @endif
              </div>
              <div class="form-group">
                <strong>Изображение на заднем плане:</strong>
                @if ($data['shop']['bg_image'])
                <img src="{{$data['shop']['bg_image']}}" class="mw-100" alt="">
                @else
                У магазина нет изображение на заднем плане
                @endif
              </div>
              <a href="{{route('shop.products', ['store_id' => $data['shop']['id']])}}">
                <button class="btn btn-success">Показать товары текущего магазина</button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
