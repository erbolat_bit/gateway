@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Редактировать Магазин №{{$data['shop']['id']}}</h3>
          <a class="btn btn-primary float-right" href="{{ route('shop') }}"> Назад</a>
        </div>
        <form method="post" action="{{route('shop.update.1', ['id' => $data['shop']['id']])}}"
          enctype="multipart/form-data">
          @csrf
          <div class=" card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input name="title" class="form-control" type="text" value="{{$data['shop']['title']}}">
            </div>
            <div class="form-group">
              <label for="type">Тип:</label>
              <select class="form-control" name="type" id="type">
                @foreach ($data['shop']['type_list'] as $key => $type)
                <option value="{{$key}}" @if ($type==$data['shop']['type']) selected @endif>{{$type}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Описание:</label><br>
              <input name="description" class="form-control" type="text" value="{{$data['shop']['description']}}">
            </div>
            <div class="form-group">
              <label>Логотип:</label><br>
              @if ($data['shop']['logotype'])
              <img class="mw-100" src="{{$data['shop']['logotype']}}" alt="">
              @endif
              <input name="logotype" class="form-control" type="file">
            </div>
            <div class="form-group">
              <label>Субдомен:</label><br>
              <input name="subdomain" class="form-control" type="text" value="{{$data['shop']['subdomain']}}">
            </div>
            <div class="form-group">
              <label>Изображение на заднем плане:</label><br>
              @if ($data['shop']['bg_image'])
              <img class="mw-100" src="{{$data['shop']['bg_image']}}" alt="">
              @endif
              <input type="file" name="bg_image" class="form-control">
            </div>
            <div class="form-check">
              <input type="checkbox" @if ($data['shop']['status']) checked @endif name="status" class="form-check-input"
                id="status1">
              <label class="form-check-label" for="status1">Активировать</label>
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection