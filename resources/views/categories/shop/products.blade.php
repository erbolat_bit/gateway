@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">

          @if (request()->has('store_id'))
          <h3 class="card-title float-left">
            Товары магазина № {{request()->store_id}}
          </h3>
          <a class="btn btn-primary float-right" href="{{ route('shop.show', ['id' => request()->store_id]) }}">
            Назад</a>
          @else
          <h3 class="card-title float-left">
            Список товаров
          </h3>
          @endif
        </div>
        <div class="card-body">
          @if (!empty($data['products']))

          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Картинка</th>
                  <th>Название</th>
                  <th>Категория</th>
                  <th>Магазин</th>
                  <th>Цена</th>
                  <th>Количество</th>
                  <th>Просмотры</th>
                  <th width="150px">Действие</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['products'] as $key => $product)
                <tr>
                  <td>{{ $product['id'] }}</td>
                  @if(count($product['photos']) > 0)
                  <td><img src="{{$product['photos'][0]['photo']}}" alt="" style="width: 100px"></td>
                  @else
                  <td>Нет Картинок</td>
                  @endif
                  <td>
                    <a
                      href="{{config('app.main_domain')}}/{{ app()->getLocale() }}/store-inside/{{$product['store']['id']}}/product/{{$product['id']}}" target="_blank">
                      {{ $product['title']}}
                    </a>
                  </td>
                  <td>{{ $product['category_name']}}</td>
                  <td>
                    <a href="{{config('app.main_domain')}}/store-inside/{{$product['store']['id']}}" target="_blank">
                      {{ $product['store']['title'] }}
                    </a>
                  </td>
                  <td>{{ $product['price']}} {{$product['currency']}}</td>
                  <td>{{ $product['quantity']}}</td>
                  <td>{{ $product['views']}}</td>
                  <td>
                    <a class="btn btn-primary btn-sm" href="{{route('shop.products.show', ['id' => $product['id']])}}"
                      data-toggle="tooltip" data-placement="top" title="" data-original-title="Показать">
                      <i class="mdi mdi-eye"></i>
                    </a>
                    <a class="btn btn-success btn-sm"
                      href="{{route('shop.products.update.view', ['id' => $product['id']])}}" data-toggle="tooltip"
                      data-placement="top" title="" data-original-title="Редактировать">
                      <i class="mdi mdi-lead-pencil"></i>
                    </a>
                    @php
                    $nextRoute = route('shop.products.delete', ['id' => $product['id']]);
                    @endphp
                    @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' => $product['id']])
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          У магазина пока нет товаров
          @endif
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'shop.products', 'paginator' => $data['pagination']])
</div>
@endsection