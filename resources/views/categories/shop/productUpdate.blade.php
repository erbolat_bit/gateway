@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Редактировать Продукт №{{$data['product']['id']}}</h3>
          <a class="btn btn-primary float-right" href="{{ route('shop.products') }}"> Назад</a>
        </div>
        <form method="post" action="{{route('shop.products.update', ['id' => $data['product']['id']])}}">
          @csrf
          <div class=" card-body">
            <div class="form-group">
              <label>Название</label><br>
              <input name="title" class="form-control" type="text" value="{{$data['product']['title']}}">
            </div>
            <div class="form-group m-t-20">
              <label>Описание:</label>
              <textarea name="description" id="editor"
                class="form-control editor">{{$data['product']['description']}}</textarea>
            </div>
            <div class="form-group m-t-20">
              <label>Детальное описание:</label>
              <textarea name="detailed_description" id="editor"
                class="form-control editor">{{$data['product']['detailed_description']}}</textarea>
            </div>
            <div class="form-group m-t-20">
              <label>Описание доставки:</label>
              <textarea name="delivery_description" id="editor"
                class="form-control editor">{{$data['product']['delivery_description']}}</textarea>
            </div>
          </div>
          <div class="border-top">
            <div class="card-body">
              <button type="submit" class="btn btn-primary">Сохранить</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
