@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">Создать отделениe</h3>
                    <a class="btn btn-primary float-right" href="{{ route('department.all') }}"> Назад</a>
                </div>
                <form action="{{route('department.create')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>Название(RU)</label><br>
                            <input class="form-control" type="text" name="title_ru" placeholder="Название">
                        </div>
                        <div class="form-group">
                            <label>Название(EN)</label><br>
                            <input class="form-control" type="text" name="title_en" placeholder="Название">
                        </div>
                        <div class="form-group">
                            <label>Название(UZ)</label><br>
                            <input class="form-control" type="text" name="title_uz" placeholder="Название">
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection