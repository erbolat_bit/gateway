@extends('layouts.app')

@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">Редактировать отделение</h3>
                    <a class="btn btn-primary float-right" href="{{ route('department.all') }}"> Назад</a>
                </div>
                <form method="post" action="{{route('department.update', ['id' => $data['department']['id']])}}">
                    @csrf
                    <div class=" card-body">
                        <div class="form-group">
                            <label>Название(RU)</label><br>
                            <input name="title_ru" class="form-control" type="text"
                                value="{{$data['department']['title_ru']}}">
                        </div>
                        <div class="form-group">
                            <label>Название(EN)</label><br>
                            <input name="title_en" class="form-control" type="text"
                                value="{{$data['department']['title_en']}}">
                        </div>
                        <div class="form-group">
                            <label>Название(UZ)</label><br>
                            <input name="title_uz" class="form-control" type="text"
                                value="{{$data['department']['title_uz']}}">
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection