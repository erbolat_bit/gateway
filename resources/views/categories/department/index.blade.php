@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">Список отделений</h3>
                    <a class="btn btn-success float-right" href="{{ route('department.create.view') }}">Создать
                        отделениe</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Название</th>
                                    <th width="150px">Действие</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['departments']['data'] as $department)
                                <tr>
                                    <td>{{ $department['id'] }}</td>
                                    <td>{{ $department['title'] }}</td>
                                    <td>
                                        <a class="btn btn-success btn-sm"
                                            href="{{route('department.update.view', ['id' => $department['id']])}}"
                                            data-toggle="tooltip" data-placement="top" title=""
                                            data-original-title="Редактировать">
                                            <i class="mdi mdi-lead-pencil"></i>
                                        </a>
                                        @php
                                        $nextRoute = route('department.delete');
                                        $message = "
                                        Это действие может привести к удалению всех принадлежащих ему тикетов";
                                        @endphp
                                        @include('components.modals.confirmation', ['targetRoute' => $nextRoute, 'id' =>
                                        $department['id'],
                                        'includeid' => true,
                                        'message' => $message
                                        ])
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @php
    $paginator = $data['departments'];
    $route = 'department.all';
    @endphp
    @include('components.pagination')
</div>
@endsection