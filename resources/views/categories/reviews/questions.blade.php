@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список вопросов</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Товар</th>
                  <th>Пользователь</th>
                  <th>Вопрос</th>
                  <th>Статус</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['questions']['data'] as $key => $question)
                <tr>
                  <td>{{ $question['id'] }}</td>
                  <td>{{ $question['product_title'] }}</td>
                  <td>{{ $question['user_email'] }}</td>
                  <td>{{ $question['text'] }}</td>
                  <td>
                    {{-- {{ $question['status'] }} --}}
                    <form 
                      action="{{ route('review.questions.update', $question['id']) }}" 
                      method="post"
                    >
                      @csrf
                      @method('PUT')
                      <input type="hidden" name="status" value="{{ $question['status'] ? 0 : 1 }}">
                      <button class="btn btn-primary">{{ $question['status'] == 1 ? 'Active' : 'Inactive'}}</button>
                    </form>
                  </td>
                  <td>
                    <form 
                      action="{{ route('review.questions.delete', $question['id']) }}" 
                      method="post"
                      onsubmit="return confirm('Are you sure?')"
                    >
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger">Delete</button>
                    </form>
                  </td>
                  @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'review.questions', 'paginator' => $data['questions']])
</div>
@endsection
