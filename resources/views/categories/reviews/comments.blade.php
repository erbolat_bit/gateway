@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список комментарий</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Пост</th>
                  <th>Пользователь</th>
                  <th>Комментарий</th>
                  <th>Статус</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @empty(!$data)
                  @foreach ($data['comments']['data'] as $key => $comment)
                  <tr>
                    <td>{{ $comment['id'] }}</td>
                    <td>
                      <a target="_blank" href="{{config('app.main_domain')}}/{{ app()->getLocale() }}/my-buynow/blog-posts/blog-post/{{ $comment['post_alias'] }}">
                        {{ $comment['post_title'] }}
                      </a>
                    </td>
                    <td>{{ $comment['user_email'] }}</td>
                    <td>{{ $comment['comment'] }}</td>
                    <td>
                      <form 
                        action="{{ route('review.comments.update', $comment['id']) }}" 
                        method="post"
                      >
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="status" value="{{ $comment['status'] ? 0 : 1 }}">
                        <button class="btn btn-primary">{{ $comment['status'] == 1 ? 'Active' : 'Inactive'}}</button>
                      </form>
                    </td>
                    <td>
                      <form 
                        action="{{ route('review.comments.delete', $comment['id']) }}" 
                        method="post"
                        onsubmit="return confirm('Are you sure?')"
                      >
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">Delete</button>
                      </form>
                      
                    </td>
                  </tr>
                  @endforeach
                @endempty
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @empty(!$data)
    @include('components.pagination', ['route' => 'review.comments', 'paginator' => $data['comments']])
  @endempty
</div>
@endsection
