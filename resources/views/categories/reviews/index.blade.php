@extends('layouts.app')


@section('inner_content')
<div class="container-fluid">
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title float-left">Список Отзывов</h3>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>№</th>
                  <th>Товар</th>
                  <th>Пользователь</th>
                  <th>Звезды</th>
                  <th>Преимущества</th>
                  <th>Недостатки</th>
                  <th>Обзор</th>
                  <th>Статус</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data['reviews']['data'] as $key => $review)
                <tr>
                  <td>{{ $review['id'] }}</td>
                  <td>{{ $review['product_title'] }}</td>
                  <td>{{ $review['user_email'] }}</td>
                  <td>@for ($i = 0; $i < $review['stars']; $i++) <i class="mdi mdi-star"></i>
                      @endfor</td>
                  <td>{{ $review['advantages'] }}</td>
                  <td>{{ $review['disadvantages'] }}</td>
                  <td>{{ $review['review'] }}</td>
                  <td>
                    {{-- {{ $review['status'] }} --}}
                    <form 
                      action="{{ route('review.update', $review['id']) }}" 
                      method="post"
                    >
                      @csrf
                      @method('PUT')
                      <input type="hidden" name="status" value="{{ $review['status'] ? 0 : 1 }}">
                      <button class="btn btn-primary">{{ $review['status'] == 1 ? 'Active' : 'Inactive'}}</button>
                    </form>
                  </td>
                  <td>
                    <form 
                      action="{{ route('review.delete', $review['id']) }}" 
                      method="post"
                      onsubmit="return confirm('Are you sure?')"
                    >
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger">Delete</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('components.pagination', ['route' => 'review', 'paginator' => $data['reviews']])
</div>
@endsection
