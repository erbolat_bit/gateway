{{-- Note: Before Using this component please instantiate $paginator property with pagination data! and route property --}}
{{-- You can see the one example at the end of categories/categproes/all.blade.php file --}}
<div id="pagination_component">
  @unless ($paginator['current_page'] == $paginator['last_page'] && !$paginator['prev_page_url'])
  @php
  $current = $paginator['current_page'];
  $last = $paginator['last_page'];
  $elements = [];
  $query_params = request()->input();
  for ($i=$current - 3; $i < $current; $i++) { if($i> 0 and $i <= $last){ $elements[]=$i; } } for($k=$current; $k
      <=$current + 3; $k++){ if($k> 0 and $k <= $last){ $elements[]=$k; } } @endphp <nav aria-label="...">
        <ul class="pagination">
          @if ($paginator['prev_page_url'])
          <li class="page-item">
            <a class="page-link" href="{{route($route, array_merge($query_params, ['page' => 1]))}}">First</a>
          </li>
          <li class="page-item">
            <a class="page-link" href="{{route($route, array_merge($query_params,['page' => $current - 1]))}}">
              <<</a> </li> @else <li class="page-item disabled">
                <span class="page-link">First</span>
          </li>
          <li class="page-item disabled">
            <span class="page-link">
              <<</span> </li> @endif @foreach ($elements as $element) @if ($element==$current) <li
                class="page-item active">
                <span class="page-link">
                  {{$element}}
                  <span class="sr-only">(current)</span>
                </span>
          </li>
          @else
          <li class="page-item"><a class="page-link"
              href="{{route($route, array_merge($query_params,['page' => $element]))}}">{{$element}}</a>
          </li>
          @endif
          @endforeach

          @if ($paginator['next_page_url'])
          <li class="page-item">
            <a class="page-link" href="{{route($route, array_merge($query_params,['page' => $current + 1]))}}">>></a>
          </li>
          <li class="page-item">
            <a class="page-link" href="{{route($route, array_merge($query_params,['page' => $last]))}}">Last</a>
          </li>
          @else
          <li class="page-item disabled">
            <span class="page-link">>></span>
          </li>
          <li class="page-item disabled">
            <span class="page-link">Last</span>
          </li>
          @endif
        </ul>
        </nav>
        @endunless
</div>
