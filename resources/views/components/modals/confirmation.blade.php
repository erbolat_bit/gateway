{{-- ATTENTION INITIATE NEXT ROUTE AS $targetRoute AND $id BEFORE CALLING THIS COMPONENT --}}
<button type="button" class="btn btn-danger btn-sm d-inline" data-toggle="modal"
  data-target="#confirmationModal{{$id}}">
  <i class="mdi mdi-delete"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="confirmationModal{{$id}}" tabindex="-1" role="dialog"
  aria-labelledby="confirmationModalLabel{{$id}}" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmationModalLabel{{$id}}">Вы уверены?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @isset($message)
      <div class="modal-body">
        <h4>
          {{$message}}
        </h4>
      </div>
      @endisset
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
        <form class="d-inline" action="{{$targetRoute}}" method="post">
          @csrf
          @method('delete')
          @isset($includeid)
          <input type="hidden" name="id" value="{{$id}}">
          @endisset
          <button type="submit" class="btn btn-primary">Продолжить</button>
        </form>
      </div>
    </div>
  </div>
</div>