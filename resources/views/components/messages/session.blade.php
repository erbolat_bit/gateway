@if(session('message'))
<div class="p-3">
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Success: </strong> {{session('message')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
</div>
@endif
@if(session('error') and is_string(session('error')))
<div class="p-3">
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Error: </strong> {{session('error')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
</div>
@endif
@if(session('error') and is_array(session('error')))
<div class="p-3">
  <div class="alert alert-danger alert-dismissible fade show" role="alert">
    <strong>Error: </strong>
    <br>
    @foreach(session('error') as $errors)
    @foreach ($errors as $error)
    {{$error}} <br>
    @endforeach
    @endforeach
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
</div>
@endif

@if(session('warning'))
<div class="p-3">
  <div class="alert alert-warning alert-dismissible fade show" role="alert">
    <strong>Success: </strong> {{session('warning')}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
</div>
@endif
