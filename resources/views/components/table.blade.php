{{-- ATTENTION PLEASE INITIALIZE NEEDED VARIABLES IN ORDER FOR THIS TABLE COMPONENT TO WORK PROPERLY --}}

{{-- The array must be in the following format ['index_key' => 'russian translation', 'index_key2' => 'russian translation'] This is fro the thead only for the tbody you need to give the whole array --}}

{{-- Initialize variable $thead and $tbody with their following data --}}

{{-- You should also pass $operations variable in the form
  [
    'view' => [
      'link' => 'view.link',
      'include_key' => 'id'
    ],
    'update' => [
      'link' => 'update.link'
    ],
    'delete' => [
      'link' => 'delete.link'
      'include_key' => [
        'tbody' => 'id',
        'name' => 'shop'
      ]
    ]
    ]
  --}}
{{-- You can exclude the operations you don't want --}}

<div class="table-responsive">
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>№</th>
        @foreach ($thead as $headItem)
        <th>{{$headItem}}</th>
        @endforeach
        <th>Действие</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tbody as $bodyItem)
      <tr>
        <td>{{$bodyItem['id']}}</td>
        @foreach ($thead as $key => $headEl)
        <td>{{$bodyItem[$key]}}</td>
        @endforeach
        <td>
          @php
          $links = array();
          $operationsMightHave = ['view', 'update', 'delete'];
          foreach($operationsMightHave as $opn){
          if(array_key_exists($opn, $operations)){
          if(is_array($operations[$opn]['include_key'])){
          $params[$opn] = [$operations[$opn]['include_key']['name'] =>
          $bodyItem[$operations[$opn]['include_key']['tbody']]];
          }else{
          $params[$opn] = [$operations[$opn]['include_key'] => $bodyItem[$operations[$opn]['include_key']]];
          }
          $links[$opn] = route($operations[$opn]['link'], $params[$opn]);
          }
          }
          @endphp
          @if (array_key_exists('view', $links))
          <a class="btn btn-primary btn-sm" href="{{$links['view']}}" data-toggle="tooltip" data-placement="top"
            title="" data-original-title="Показать">
            <i class="mdi mdi-eye"></i>
          </a>
          @endif
          @if (array_key_exists('update', $links))
          <a class="btn btn-success btn-sm" href="{{$links['update']}}" data-toggle="tooltip" data-placement="top"
            title="" data-original-title="Редактировать">
            <i class="mdi mdi-lead-pencil"></i>
          </a>
          @endif
          @if (array_key_exists('delete', $links))
          @include('components.modals.confirmation', ['targetRoute' => $links['delete'], 'id' => $bodyItem['id']])
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
