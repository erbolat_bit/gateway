@extends('layouts.main')

@section('content')
<div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
  <div>
    <div class="text-white text-center">
      <h3>Admin Panel</h3>
    </div>
    <div class="auth-box bg-dark border-top border-secondary m-0">
      <div id="loginform">
        <form method="POST" action="{{ route('login') }}" class="form-horizontal m-t-20" id="loginform">
          @csrf
          <div class="row p-b-30">
            <div class="col-12">
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                </div>
                <input type="email" name="email"
                  class="form-control form-control-lg @error('email') is-invalid @enderror" value="{{ old('email') }}"
                  placeholder="Email" aria-label="Email" aria-describedby="basic-addon1" required="">
                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text bg-warning text-white" id="basic-addon2"><i
                      class="ti-pencil"></i></span>
                </div>
                <input id="password" type="password"
                  class="form-control form-control-lg @error('password') is-invalid @enderror" name="password"
                  placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
                @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
              <div class="custom-control custom-checkbox mr-sm-2">
                <input type="checkbox" class="custom-control-input" name="remember" id="remember"
                  {{ old('remember') ? 'checked' : '' }}>
                <label class="custom-control-label text-white" for="remember">{{ __('Remember Me') }}</label>
              </div>
              <br>
            </div>
          </div>
          <div class="row border-top border-secondary">
            <div class="col-12">
              <div class="form-group">
                <div class="p-t-20">
                  @if (Route::has('password.request'))
                  <a class="btn btn-info" id="to-recover" href="{{ route('password.request') }}">
                    <i class="fa fa-lock m-r-5"></i> {{ __('Forgot Your Password?') }}
                  </a>
                  @endif
                  <button class="btn btn-success float-right mt-4" type="submit">Login</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
