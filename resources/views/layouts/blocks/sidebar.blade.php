<aside class="left-sidebar" data-sidebarbg="skin5">
  <!-- Sidebar scroll-->
  <div class="scroll-sidebar">
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
      <ul id="sidebarnav" class="p-t-30">
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('dashboard')}}"
            aria-expanded="false">
            <i class="mdi mdi-view-dashboard"></i>
            <span class="hide-menu">Dashboard</span>
          </a>
        </li>
        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
            aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span
              class="hide-menu">Пользователи</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('users.admins')}}" class="sidebar-link"><i
                  class="mdi mdi-account-network"></i><span class="hide-menu">Aдминистраторы</span></a></li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users.admin.roles')}}"
                aria-expanded="false">
                <i class="mdi mdi-account-check"></i>
                <span class="hide-menu">Роли администраторов</span>
              </a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users.all')}}"
                aria-expanded="false">
                <i class="mdi mdi-account-check"></i>
                <span class="hide-menu">Пользователи</span>
              </a>
            </li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('department.all')}}"
            aria-expanded="false">
            <i class="mdi mdi-collage"></i>
            <span class="hide-menu">Отделения</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('categories')}}"
            aria-expanded="false">
            <i class="mdi mdi-format-list-bulleted-type"></i>
            <span class="hide-menu">Категории</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('manufacturers.index')}}"
            aria-expanded="false">
            <i class="mdi mdi-factory"></i>
            <span class="hide-menu">Производители</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-comment-text"></i><span class="hide-menu">Центр поддержки</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('ticket.index')}}" class="sidebar-link"><span
                  class="hide-menu">Тикеты</span></a>
            </li>
            <li class="sidebar-item"><a href="{{route('ticket.categories')}}" class="sidebar-link"><span
                  class="hide-menu">Категории тикетов</span></a></li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('countries.index')}}"
            aria-expanded="false">
            <i class="mdi mdi-google-maps"></i>
            <span class="hide-menu">Страна</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href={{route('parameters.index')}}
            aria-expanded="false">
            <i class="mdi mdi-view-list"></i>
            <span class="hide-menu">Параметры товара</span>
          </a>
        </li>
        {{-- <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false">
            <i class="mdi mdi-view-list"></i>
            <span class="hide-menu">Журнал действий</span>
          </a>
        </li> --}}

        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)"
            aria-expanded="false"><i class="mdi mdi-information"></i><span class="hide-menu">База знаний</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('blog.categories')}}" class="sidebar-link"><i
                  class="mdi mdi-account-network"></i><span class="hide-menu">Категории</span></a></li>
            <li class="sidebar-item"><a href="{{route('blog.posts')}}" class="sidebar-link"><i
                  class="mdi mdi-account-network"></i><span class="hide-menu">Посты</span></a></li>
          </ul>
        </li>

        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-settings"></i></i><span class="hide-menu">Настройки</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            {{-- <li class="sidebar-item"><a href="{{route('setting')}}" class="sidebar-link"><span class="hide-menu">Общие
                  Настройки</span></a>
            </li> --}}
            <li class="sidebar-item"><a href="{{route('setting.socials')}}" class="sidebar-link"><span
                  class="hide-menu">API социальных
                  сети</span></a></li>
            <li class="sidebar-item"><a href="{{route('setting.delivery')}}" class="sidebar-link"><span
                  class="hide-menu">Вид доставки</span></a>
            </li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('poll')}}" aria-expanded="false">
            <i class="mdi mdi-comment-text"></i>
            <span class="hide-menu">Опросники</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-cart"></i></i><span class="hide-menu">Корзина</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('cart.orders')}}" class="sidebar-link"><span
                  class="hide-menu">Заказы</span></a>
            </li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-ticket-account"></i></i><span class="hide-menu">Купоны</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('coupon')}}" class="sidebar-link"><span
                  class="hide-menu">Купоны</span></a>
            </li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-bank"></i></i><span class="hide-menu">Аукционы</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('lot')}}" class="sidebar-link"><span
                  class="hide-menu">Аукционы</span></a>
            </li>
            <li class="sidebar-item"><a href="{{route('lot.offers')}}" class="sidebar-link"><span
                  class="hide-menu">Предложение</span></a></li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-shopping"></i></i><span class="hide-menu">Магазин</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('shop')}}" class="sidebar-link"><span
                  class="hide-menu">Магазины</span></a>
            </li>
            <li class="sidebar-item"><a href="{{route('shop.products')}}" class="sidebar-link"><span
                  class="hide-menu">Товары</span></a></li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
              class="mdi mdi-star-circle"></i></i><span class="hide-menu">Отзывы</span></a>
          <ul aria-expanded="false" class="collapse  first-level">
            <li class="sidebar-item"><a href="{{route('review')}}" class="sidebar-link"><span
                  class="hide-menu">Отзывы</span></a>
            </li>
            <li class="sidebar-item"><a href="{{route('review.comments')}}" class="sidebar-link"><span
                  class="hide-menu">Комментарии</span></a></li>
            <li class="sidebar-item"><a href="{{route('review.questions')}}" class="sidebar-link"><span
                  class="hide-menu">Вопросник</span></a></li>
          </ul>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('ads')}}" aria-expanded="false">
            <i class="mdi mdi-comment-text"></i>
            <span class="hide-menu">Объявления</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('banners.index')}}" aria-expanded="false">
            <i class="mdi mdi-comment-text"></i>
            <span class="hide-menu">Баннеры</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('logs.all')}}"
            aria-expanded="false">
            <i class="mdi mdi-pencil"></i>
            <span class="hide-menu">Логи</span>
          </a>
        </li>
        <li class="sidebar-item">
          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('system-messages.index')}}"
            aria-expanded="false">
            <i class="mdi mdi-email"></i>
            <span class="hide-menu">Рассылка </span>
          </a>
        </li>
        {{-- <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('posts.index') }}"
        aria-expanded="false">
        <i class="mdi mdi-view-dashboard"></i>
        <span class="hide-menu">Posts</span>
        </a>
        </li> --}}
        {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-move-resize-variant"></i><span class="hide-menu">Addons </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="index2.html" class="sidebar-link"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"> Dashboard-2 </span></a></li>
                        <li class="sidebar-item"><a href="pages-gallery.html" class="sidebar-link"><i class="mdi mdi-multiplication-box"></i><span class="hide-menu"> Gallery </span></a></li>
                        <li class="sidebar-item"><a href="pages-calendar.html" class="sidebar-link"><i class="mdi mdi-calendar-check"></i><span class="hide-menu"> Calendar </span></a></li>
                        <li class="sidebar-item"><a href="pages-invoice.html" class="sidebar-link"><i class="mdi mdi-bulletin-board"></i><span class="hide-menu"> Invoice </span></a></li>
                        <li class="sidebar-item"><a href="pages-chat.html" class="sidebar-link"><i class="mdi mdi-message-outline"></i><span class="hide-menu"> Chat Option </span></a></li>
                    </ul>
                </li> --}}
      </ul>
    </nav>
    <!-- End Sidebar navigation -->
  </div>
  <!-- End Sidebar scroll-->
</aside>