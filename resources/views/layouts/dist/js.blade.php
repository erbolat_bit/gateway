<script src="{{ asset('theme/assets/libs/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('theme/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') }}"></script>

<script src="{{ asset('theme/assets/extra-libs/sparkline/sparkline.js') }}"></script>
<script src="{{ asset('theme/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script src="{{ asset('theme/dist/js/waves.js') }}"></script>
<script src="{{ asset('theme/dist/js/sidebarmenu.js') }}"></script>
<script src="{{ asset('theme/dist/js/custom.min.js') }}"></script>
<script src="{{ asset('theme/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('theme/assets/ckeditor5/ckeditor.js') }}"></script>
<script src="{{ asset('theme/assets/ckfinder/ckfinder.js') }}"></script>
<script src="{{ asset('theme/assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{asset('theme/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script src="{{ asset('js/script.js') }}"></script>
