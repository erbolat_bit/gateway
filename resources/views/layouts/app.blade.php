@extends('layouts.main')

@section('content')
@include('layouts.blocks.header')
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
@include('layouts.blocks.sidebar')
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <div class="page-breadcrumb">
    <div class="row">
      <div class="col-12 no-block align-items-center">
        <div class="ml-auto">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
              @php
              $path = request()->path();
              $pieces = explode("/", $path);
              @endphp
              @foreach ($pieces as $piece)
              @if ($loop->last)
              <li class="breadcrumb-item" aria-current="page">{{$piece}}</li>
              @else
              <li class="breadcrumb-item">{{$piece}}</li>
              @endif
              @endforeach
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
  @include('components.messages.session')
  @include('components.messages.errors')

  <!-- ============================================================== -->
  <!-- End Bread crumb and right sidebar toggle -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  @yield('inner_content')
  <!-- ============================================================== -->
  <!-- End Container fluid  -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- footer -->
  @include('layouts.blocks.footer')
  <!-- ============================================================== -->
  <!-- End footer -->
  <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
@endsection
