<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <script src="{{ asset('js/app.js') }}"></script>
  <!-- ============================================================== -->
  <!-- Css Files -->
  @include('layouts.dist.css')
  <!-- ============================================================== -->
</head>

<body>
  <div id="app">
    @include('layouts.blocks.preloader')
    <div id="main-wrapper">
      @yield('content')
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Javascript Files -->
  @include('layouts.dist.js')
  <!-- ============================================================== -->
</body>

</html>
